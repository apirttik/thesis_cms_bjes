

## Analysis code for the master's thesis

The events are generated in the folder `event_generation` and the calculation of bJES is done in `bJES_analysis` folder.


The subfolders `scale_factors`, `toyPF` and `bJES_plotting` are found inside `bJES_analysis`. The scale factors should be first calculated inside `scale_factors` after which one can run the toyPF simulation in `toyPF` folder. Finally the effect of bJES can be visualized using the scripts in `bJES_plotting` folder.
