string n_events = "20000000";

string OpenName = "../toyPF/output_ROOT_files/CMSJES_P8_Zjet_" + n_events + ".root";

string OpenName_BtoSLBr = "../toyPF/output_ROOT_files/CMSJES_P8_Zjet_" + n_events + "_BtoSlBrScaling.root";
string OpenName_BtoSLBr_p = "../toyPF/output_ROOT_files/CMSJES_P8_Zjet_" + n_events + "_BtoSlBrScaling_plus.root";
string OpenName_BtoSLBr_m = "../toyPF/output_ROOT_files/CMSJES_P8_Zjet_" + n_events + "_BtoSlBrScaling_minus.root";

string OpenName_indvBtoSLBr = "../toyPF/output_ROOT_files/CMSJES_P8_Zjet_" + n_events + "_indvBtoSlBrScaling.root";
string OpenName_indvBtoSLBr_p = "../toyPF/output_ROOT_files/CMSJES_P8_Zjet_" + n_events + "_indvBtoSlBrScaling_plus.root";
string OpenName_indvBtoSLBr_m = "../toyPF/output_ROOT_files/CMSJES_P8_Zjet_" + n_events + "_indvBtoSlBrScaling_minus.root";

string OpenName_BtoCBr = "../toyPF/output_ROOT_files/CMSJES_P8_Zjet_" + n_events + "_BtoCBrScaling.root";
string OpenName_BtoCBr_p = "../toyPF/output_ROOT_files/CMSJES_P8_Zjet_" + n_events + "_BtoCBrScaling_plus.root";
string OpenName_BtoCBr_m = "../toyPF/output_ROOT_files/CMSJES_P8_Zjet_" + n_events + "_BtoCBrScaling_minus.root";

string OpenName_BprodFrac = "../toyPF/output_ROOT_files/CMSJES_P8_Zjet_" + n_events + "_BprodFracScaling.root";
string OpenName_BprodFrac_p = "../toyPF/output_ROOT_files/CMSJES_P8_Zjet_" + n_events + "_BprodFracScaling_plus.root";
string OpenName_BprodFrac_m = "../toyPF/output_ROOT_files/CMSJES_P8_Zjet_" + n_events + "_BprodFracScaling_minus.root";

string OpenName_lundBowler = "../toyPF/output_ROOT_files/CMSJES_P8_Zjet_" + n_events + "_lundBowlerScaling.root";
string OpenName_lundBowler_p = "../toyPF/output_ROOT_files/CMSJES_P8_Zjet_" + n_events + "_lundBowlerScaling_plus.root";
string OpenName_lundBowler_m = "../toyPF/output_ROOT_files/CMSJES_P8_Zjet_" + n_events + "_lundBowlerScaling_minus.root";

void ReadFromFile(std::vector<double> &x, const std::string &file_name)
{
  std::ifstream read_file(file_name);
  assert(read_file.is_open());

  std::copy(std::istream_iterator<double>(read_file), std::istream_iterator<double>(),
      std::back_inserter(x));

  read_file.close();
}

map<string, TH1D*> mpf_BtoSLBr () {
	map <string, TH1D*> MPFb_map;

  TFile* f_normal = TFile::Open(OpenName.c_str());
  TFile* f_scaled = TFile::Open(OpenName_BtoSLBr.c_str());
  TFile* f_scaled_p = TFile::Open(OpenName_BtoSLBr_p.c_str());
  TFile* f_scaled_m = TFile::Open(OpenName_BtoSLBr_m.c_str());

	TProfile *prMPFb;
	TProfile *prMPFb_s;
	TProfile *prMPFb_s_p;
	TProfile *prMPFb_s_m;

  //Read the normal sample
  f_normal->GetObject("prMPFb",   prMPFb);

  //Read the weighted sample
  f_scaled->GetObject("prMPFb",   prMPFb_s);
  f_scaled_p->GetObject("prMPFb",   prMPFb_s_p);
  f_scaled_m->GetObject("prMPFb",   prMPFb_s_m);

	//2D Tprofiles for the errorb calculations
	TProfile2D* pr2DMPFb;

	TProfile2D* pr2DMPFb_BSl; 
	TProfile2D* pr2DMPFb_BTauSl; 
	TProfile2D* pr2DMPFb_BCSl; 
	TProfile2D* pr2DMPFb_BCbarSl; 
	TProfile2D* pr2DMPFb_BnonSl; 

	TProfile2D* pr2DMPFb_BCSl_BCbarSl; 
	TProfile2D* pr2DMPFb_BCSl_BSl; 
	TProfile2D* pr2DMPFb_BCSl_BTauSl; 
	TProfile2D* pr2DMPFb_BCbarSl_BSl; 
	TProfile2D* pr2DMPFb_BCbarSl_BTauSl; 

  //The sample with the additional weighting (*_s)
	TProfile2D* pr2DMPFb_s;

	TProfile2D* pr2DMPFb_BSl_s; 
	TProfile2D* pr2DMPFb_BTauSl_s; 
	TProfile2D* pr2DMPFb_BCSl_s; 
	TProfile2D* pr2DMPFb_BCbarSl_s; 
	TProfile2D* pr2DMPFb_BnonSl_s; 

	TProfile2D* pr2DMPFb_BCSl_BCbarSl_s; 
	TProfile2D* pr2DMPFb_BCSl_BSl_s; 
	TProfile2D* pr2DMPFb_BCSl_BTauSl_s; 
	TProfile2D* pr2DMPFb_BCbarSl_BSl_s; 
	TProfile2D* pr2DMPFb_BCbarSl_BTauSl_s; 

	TProfile2D* pr2DMPFb_s_p;

	TProfile2D* pr2DMPFb_BSl_s_p; 
	TProfile2D* pr2DMPFb_BTauSl_s_p; 
	TProfile2D* pr2DMPFb_BCSl_s_p; 
	TProfile2D* pr2DMPFb_BCbarSl_s_p; 
	TProfile2D* pr2DMPFb_BnonSl_s_p; 

	TProfile2D* pr2DMPFb_BCSl_BCbarSl_s_p; 
	TProfile2D* pr2DMPFb_BCSl_BSl_s_p; 
	TProfile2D* pr2DMPFb_BCSl_BTauSl_s_p; 
	TProfile2D* pr2DMPFb_BCbarSl_BSl_s_p; 
	TProfile2D* pr2DMPFb_BCbarSl_BTauSl_s_p; 

	TProfile2D* pr2DMPFb_s_m;

	TProfile2D* pr2DMPFb_BSl_s_m; 
	TProfile2D* pr2DMPFb_BTauSl_s_m; 
	TProfile2D* pr2DMPFb_BCSl_s_m; 
	TProfile2D* pr2DMPFb_BCbarSl_s_m; 
	TProfile2D* pr2DMPFb_BnonSl_s_m; 

	TProfile2D* pr2DMPFb_BCSl_BCbarSl_s_m; 
	TProfile2D* pr2DMPFb_BCSl_BSl_s_m; 
	TProfile2D* pr2DMPFb_BCSl_BTauSl_s_m; 
	TProfile2D* pr2DMPFb_BCbarSl_BSl_s_m; 
	TProfile2D* pr2DMPFb_BCbarSl_BTauSl_s_m; 

	//Tprofiles for the errorb calculations
	TProfile* prMPFb_BSl; 
	TProfile* prMPFb_BTauSl; 
	TProfile* prMPFb_BCSl; 
	TProfile* prMPFb_BCbarSl; 
	TProfile* prMPFb_BnonSl; 

	TProfile* prMPFb_BCSl_BCbarSl; 
	TProfile* prMPFb_BCSl_BSl; 
	TProfile* prMPFb_BCSl_BTauSl; 
	TProfile* prMPFb_BCbarSl_BSl; 
	TProfile* prMPFb_BCbarSl_BTauSl; 

  //The sample with the additional weighting (*_s)
	TProfile* prMPFb_BSl_s; 
	TProfile* prMPFb_BTauSl_s; 
	TProfile* prMPFb_BCSl_s; 
	TProfile* prMPFb_BCbarSl_s; 
	TProfile* prMPFb_BnonSl_s; 

	TProfile* prMPFb_BCSl_BCbarSl_s; 
	TProfile* prMPFb_BCSl_BSl_s; 
	TProfile* prMPFb_BCSl_BTauSl_s; 
	TProfile* prMPFb_BCbarSl_BSl_s; 
	TProfile* prMPFb_BCbarSl_BTauSl_s; 

	TProfile* prMPFb_BSl_s_p; 
	TProfile* prMPFb_BTauSl_s_p; 
	TProfile* prMPFb_BCSl_s_p; 
	TProfile* prMPFb_BCbarSl_s_p; 
	TProfile* prMPFb_BnonSl_s_p; 

	TProfile* prMPFb_BCSl_BCbarSl_s_p; 
	TProfile* prMPFb_BCSl_BSl_s_p; 
	TProfile* prMPFb_BCSl_BTauSl_s_p; 
	TProfile* prMPFb_BCbarSl_BSl_s_p; 
	TProfile* prMPFb_BCbarSl_BTauSl_s_p; 

	TProfile* prMPFb_BSl_s_m; 
	TProfile* prMPFb_BTauSl_s_m; 
	TProfile* prMPFb_BCSl_s_m; 
	TProfile* prMPFb_BCbarSl_s_m; 
	TProfile* prMPFb_BnonSl_s_m; 

	TProfile* prMPFb_BCSl_BCbarSl_s_m; 
	TProfile* prMPFb_BCSl_BSl_s_m; 
	TProfile* prMPFb_BCSl_BTauSl_s_m; 
	TProfile* prMPFb_BCbarSl_BSl_s_m; 
	TProfile* prMPFb_BCbarSl_BTauSl_s_m; 

  //Read the normal sample
	f_normal->GetObject("pr2DMPFb",pr2DMPFb);

	f_normal->GetObject("pr2DMPFb_BSl",pr2DMPFb_BSl); 
	f_normal->GetObject("pr2DMPFb_BTauSl",pr2DMPFb_BTauSl); 
	f_normal->GetObject("pr2DMPFb_BCSl",pr2DMPFb_BCSl); 
	f_normal->GetObject("pr2DMPFb_BCbarSl",pr2DMPFb_BCbarSl); 
	f_normal->GetObject("pr2DMPFb_BnonSl",pr2DMPFb_BnonSl); 

	f_normal->GetObject("pr2DMPFb_BCSl_BCbarSl",pr2DMPFb_BCSl_BCbarSl); 
	f_normal->GetObject("pr2DMPFb_BCSl_BSl",pr2DMPFb_BCSl_BSl); 
	f_normal->GetObject("pr2DMPFb_BCSl_BTauSl",pr2DMPFb_BCSl_BTauSl); 
	f_normal->GetObject("pr2DMPFb_BCbarSl_BSl",pr2DMPFb_BCbarSl_BSl); 
	f_normal->GetObject("pr2DMPFb_BCbarSl_BTauSl",pr2DMPFb_BCbarSl_BTauSl);

//Read the weighted sample
	f_scaled->GetObject("pr2DMPFb",pr2DMPFb_s);

	f_scaled->GetObject("pr2DMPFb_BSl",pr2DMPFb_BSl_s); 
	f_scaled->GetObject("pr2DMPFb_BTauSl",pr2DMPFb_BTauSl_s); 
	f_scaled->GetObject("pr2DMPFb_BCSl",pr2DMPFb_BCSl_s); 
	f_scaled->GetObject("pr2DMPFb_BCbarSl",pr2DMPFb_BCbarSl_s); 
	f_scaled->GetObject("pr2DMPFb_BnonSl",pr2DMPFb_BnonSl_s); 

	f_scaled->GetObject("pr2DMPFb_BCSl_BCbarSl",pr2DMPFb_BCSl_BCbarSl_s); 
	f_scaled->GetObject("pr2DMPFb_BCSl_BSl",pr2DMPFb_BCSl_BSl_s); 
	f_scaled->GetObject("pr2DMPFb_BCSl_BTauSl",pr2DMPFb_BCSl_BTauSl_s); 
	f_scaled->GetObject("pr2DMPFb_BCbarSl_BSl",pr2DMPFb_BCbarSl_BSl_s); 
	f_scaled->GetObject("pr2DMPFb_BCbarSl_BTauSl",pr2DMPFb_BCbarSl_BTauSl_s);

	f_scaled_p->GetObject("pr2DMPFb",pr2DMPFb_s_p);

	f_scaled_p->GetObject("pr2DMPFb_BSl",pr2DMPFb_BSl_s_p); 
	f_scaled_p->GetObject("pr2DMPFb_BTauSl",pr2DMPFb_BTauSl_s_p); 
	f_scaled_p->GetObject("pr2DMPFb_BCSl",pr2DMPFb_BCSl_s_p); 
	f_scaled_p->GetObject("pr2DMPFb_BCbarSl",pr2DMPFb_BCbarSl_s_p); 
	f_scaled_p->GetObject("pr2DMPFb_BnonSl",pr2DMPFb_BnonSl_s_p); 

	f_scaled_p->GetObject("pr2DMPFb_BCSl_BCbarSl",pr2DMPFb_BCSl_BCbarSl_s_p); 
	f_scaled_p->GetObject("pr2DMPFb_BCSl_BSl",pr2DMPFb_BCSl_BSl_s_p); 
	f_scaled_p->GetObject("pr2DMPFb_BCSl_BTauSl",pr2DMPFb_BCSl_BTauSl_s_p); 
	f_scaled_p->GetObject("pr2DMPFb_BCbarSl_BSl",pr2DMPFb_BCbarSl_BSl_s_p); 
	f_scaled_p->GetObject("pr2DMPFb_BCbarSl_BTauSl",pr2DMPFb_BCbarSl_BTauSl_s_p);

	f_scaled_m->GetObject("pr2DMPFb",pr2DMPFb_s_m);

	f_scaled_m->GetObject("pr2DMPFb_BSl",pr2DMPFb_BSl_s_m); 
	f_scaled_m->GetObject("pr2DMPFb_BTauSl",pr2DMPFb_BTauSl_s_m); 
	f_scaled_m->GetObject("pr2DMPFb_BCSl",pr2DMPFb_BCSl_s_m); 
	f_scaled_m->GetObject("pr2DMPFb_BCbarSl",pr2DMPFb_BCbarSl_s_m); 
	f_scaled_m->GetObject("pr2DMPFb_BnonSl",pr2DMPFb_BnonSl_s_m); 

	f_scaled_m->GetObject("pr2DMPFb_BCSl_BCbarSl",pr2DMPFb_BCSl_BCbarSl_s_m); 
	f_scaled_m->GetObject("pr2DMPFb_BCSl_BSl",pr2DMPFb_BCSl_BSl_s_m); 
	f_scaled_m->GetObject("pr2DMPFb_BCSl_BTauSl",pr2DMPFb_BCSl_BTauSl_s_m); 
	f_scaled_m->GetObject("pr2DMPFb_BCbarSl_BSl",pr2DMPFb_BCbarSl_BSl_s_m); 
	f_scaled_m->GetObject("pr2DMPFb_BCbarSl_BTauSl",pr2DMPFb_BCbarSl_BTauSl_s_m);

	//calculate the difference in response between normal and the rescaled sample 
	//the second ProjectionX has new name as argument, otherwise would have same name as the previous
	//histo and causes memory leak 
	TH1D* hMPFb     = prMPFb   ->ProjectionX();
	TH1D* hMPFb_s   = prMPFb_s ->ProjectionX("pr2DMPFb_s");
	TH1D* hMPFb_s_p = prMPFb_s_p ->ProjectionX("pr2DMPFb_s_p");
	TH1D* hMPFb_s_m = prMPFb_s_m ->ProjectionX("pr2DMPFb_s_m");

	TH1D* diffb 		= (TH1D*) hMPFb_s->Clone();
	TH1D* diffb_p 	= (TH1D*) hMPFb_s_p->Clone();
	TH1D* diffb_m 	= (TH1D*) hMPFb_s_m->Clone();

	diffb->Add(hMPFb,-1);
	diffb_p->Add(hMPFb,-1);
	diffb_m->Add(hMPFb,-1);

	//evtW_* = event weight, sem_* = standard errorb of the mean,   
	double errorb, evtWtot, evtWtots;

	double evtWtot_BSl;
	double evtWtot_BTauSl;
	double evtWtot_BCSl;
	double evtWtot_BCbarSl;
	double evtWtot_BnonSl;
	double evtWtot_BCSl_BCbarSl;
	double evtWtot_BCSl_BSl;
	double evtWtot_BCSl_BTauSl;
	double evtWtot_BCbarSl_BSl;
	double evtWtot_BCbarSl_BTauSl;

	double evtWtots_BSl;
	double evtWtots_BTauSl;
	double evtWtots_BCSl;
	double evtWtots_BCbarSl;
	double evtWtots_BnonSl;
	double evtWtots_BCSl_BCbarSl;
	double evtWtots_BCSl_BSl;
	double evtWtots_BCSl_BTauSl;
	double evtWtots_BCbarSl_BSl;
	double evtWtots_BCbarSl_BTauSl;

	vector<double> evtW_BSl;
	vector<double> evtW_BTauSl;
	vector<double> evtW_BCSl;
	vector<double> evtW_BCbarSl;
	vector<double> evtW_BnonSl;
	vector<double> evtW_BCSl_BCbarSl;
	vector<double> evtW_BCSl_BSl;
	vector<double> evtW_BCSl_BTauSl;
	vector<double> evtW_BCbarSl_BSl;
	vector<double> evtW_BCbarSl_BTauSl;

	vector<double> evtWs_BSl;
	vector<double> evtWs_BTauSl;
	vector<double> evtWs_BCSl;
	vector<double> evtWs_BCbarSl;
	vector<double> evtWs_BnonSl;
	vector<double> evtWs_BCSl_BCbarSl;
	vector<double> evtWs_BCSl_BSl;
	vector<double> evtWs_BCSl_BTauSl;
	vector<double> evtWs_BCbarSl_BSl;
	vector<double> evtWs_BCbarSl_BTauSl;

	vector<double> sem_BSl;
	vector<double> sem_BTauSl;
	vector<double> sem_BCSl;
	vector<double> sem_BCbarSl;
	vector<double> sem_BnonSl;
	vector<double> sem_BCSl_BCbarSl;
	vector<double> sem_BCSl_BSl;
	vector<double> sem_BCSl_BTauSl;
	vector<double> sem_BCbarSl_BSl;
	vector<double> sem_BCbarSl_BTauSl;

	vector<double> vals_BSl;
	vector<double> vals_BTauSl;
	vector<double> vals_BCSl;
	vector<double> vals_BCbarSl;
	vector<double> vals_BnonSl;
	vector<double> vals_BCSl_BCbarSl;
	vector<double> vals_BCSl_BSl;
	vector<double> vals_BCSl_BTauSl;
	vector<double> vals_BCbarSl_BSl;
	vector<double> vals_BCbarSl_BTauSl;

	vector<double> vec_BSl_stat, vec_BTauSl_stat, vec_BCSl_stat, vec_BCbarSl_stat, vec_BNonSl_stat,
								 vec_BCSl_BCbarSl_stat, vec_BCSl_BSl_stat, vec_BCSl_BTauSl_stat, vec_BCbarSl_BSl_stat, vec_BCbarSl_BTauSl_stat;

	ReadFromFile(vec_BSl_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoSLBr/calc_scale_factors/scale_factors/BtoSlBr/stat_err/BSl_sf_stat.txt");
	ReadFromFile(vec_BTauSl_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoSLBr/calc_scale_factors/scale_factors/BtoSlBr/stat_err/BTauSl_sf_stat.txt");
	ReadFromFile(vec_BCSl_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoSLBr/calc_scale_factors/scale_factors/BtoSlBr/stat_err/BCSl_sf_stat.txt");
	ReadFromFile(vec_BCbarSl_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoSLBr/calc_scale_factors/scale_factors/BtoSlBr/stat_err/BCbarSl_sf_stat.txt");
	ReadFromFile(vec_BNonSl_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoSLBr/calc_scale_factors/scale_factors/BtoSlBr/stat_err/BnonSl_sf_stat.txt");

	//calculating the stat error of the categories with two scaling factors 
	vector<double> vec_BSl_sf, vec_BTauSl_sf, vec_BCSl_sf, vec_BCbarSl_sf, vec_BNonSl_sf;

	ReadFromFile(vec_BSl_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoSLBr/calc_scale_factors/scale_factors/BtoSlBr/normal/BSl_sf.txt");
	ReadFromFile(vec_BTauSl_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoSLBr/calc_scale_factors/scale_factors/BtoSlBr/normal/BTauSl_sf.txt");
	ReadFromFile(vec_BCSl_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoSLBr/calc_scale_factors/scale_factors/BtoSlBr/normal/BCSl_sf.txt");
	ReadFromFile(vec_BCbarSl_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoSLBr/calc_scale_factors/scale_factors/BtoSlBr/normal/BCbarSl_sf.txt");
	ReadFromFile(vec_BNonSl_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoSLBr/calc_scale_factors/scale_factors/BtoSlBr/normal/BnonSl_sf.txt");	

	for (int i = 0; i < vec_BSl_sf.size(); i++) {
		vec_BCSl_BCbarSl_stat.push_back(sqrt(pow(vec_BCbarSl_sf[i]*vec_BCSl_stat[i],2) + pow(vec_BCSl_sf[i]*vec_BCbarSl_stat[i],2)));
		vec_BCSl_BSl_stat.push_back(sqrt(pow(vec_BSl_sf[i]*vec_BCSl_stat[i],2) + pow(vec_BCSl_sf[i]*vec_BSl_stat[i],2)));
		vec_BCSl_BTauSl_stat.push_back(sqrt(pow(vec_BTauSl_sf[i]*vec_BCSl_stat[i],2) + pow(vec_BCSl_sf[i]*vec_BTauSl_stat[i],2)));
		vec_BCbarSl_BSl_stat.push_back(sqrt(pow(vec_BSl_sf[i]*vec_BCbarSl_stat[i],2) + pow(vec_BCbarSl_sf[i]*vec_BSl_stat[i],2)));
		vec_BCbarSl_BTauSl_stat.push_back(sqrt(pow(vec_BTauSl_sf[i]*vec_BCbarSl_stat[i],2) + pow(vec_BCbarSl_sf[i]*vec_BTauSl_stat[i],2)));
	}

	vector<vector<double>> vec_sf_sem{vec_BSl_stat, vec_BTauSl_stat, vec_BCSl_stat, vec_BCbarSl_stat, vec_BNonSl_stat,
							 														   vec_BCSl_BCbarSl_stat, vec_BCSl_BSl_stat, vec_BCSl_BTauSl_stat, vec_BCbarSl_BSl_stat, vec_BCbarSl_BTauSl_stat};

	//removing the under- and overflow bin values
	for (int i = 0; i < vec_sf_sem.size(); i++) {
		vec_sf_sem[i].erase(vec_sf_sem[i].begin() + 0);
		vec_sf_sem[i].erase(vec_sf_sem[i].begin() + vec_sf_sem[i].size() - 1);
	}

  //Loop over the TProfile bins
  for (int i = 1; i < hMPFb->GetNbinsX()+1; i++){
		evtW_BSl.clear();
		evtW_BTauSl.clear();
		evtW_BCSl.clear();
		evtW_BCbarSl.clear();
		evtW_BnonSl.clear();
		evtW_BCSl_BCbarSl.clear();
		evtW_BCSl_BSl.clear();
		evtW_BCSl_BTauSl.clear();
		evtW_BCbarSl_BSl.clear();
		evtW_BCbarSl_BTauSl.clear();

		evtWs_BSl.clear();
		evtWs_BTauSl.clear();
		evtWs_BCSl.clear();
		evtWs_BCbarSl.clear();
		evtWs_BnonSl.clear();
		evtWs_BCSl_BCbarSl.clear();
		evtWs_BCSl_BSl.clear();
		evtWs_BCSl_BTauSl.clear();
		evtWs_BCbarSl_BSl.clear();
		evtWs_BCbarSl_BTauSl.clear();

		sem_BSl.clear();
		sem_BTauSl.clear();
		sem_BCSl.clear();
		sem_BCbarSl.clear();
		sem_BnonSl.clear();
		sem_BCSl_BCbarSl.clear();
		sem_BCSl_BSl.clear();
		sem_BCSl_BTauSl.clear();
		sem_BCbarSl_BSl.clear();
		sem_BCbarSl_BTauSl.clear();

		vals_BSl.clear();
		vals_BTauSl.clear();
		vals_BCSl.clear();
		vals_BCbarSl.clear();
		vals_BnonSl.clear();
		vals_BCSl_BCbarSl.clear();
		vals_BCSl_BSl.clear();
		vals_BCSl_BTauSl.clear();
		vals_BCbarSl_BSl.clear();
		vals_BCbarSl_BTauSl.clear();

		for (int j = 1; j < hMPFb->GetNbinsX()+1; j++) {
			string pr_ind = "_" + to_string(i) + "_" + to_string(j);

			prMPFb_BSl = pr2DMPFb_BSl->ProfileX(("BSl"+pr_ind).c_str(),j,j);  
			prMPFb_BTauSl = pr2DMPFb_BTauSl->ProfileX(("BTauSl"+pr_ind).c_str(),j,j);  
			prMPFb_BCSl = pr2DMPFb_BCSl->ProfileX(("BCSl"+pr_ind).c_str(),j,j);  
			prMPFb_BCbarSl = pr2DMPFb_BCbarSl->ProfileX(("BCbarSl"+pr_ind).c_str(),j,j);  
			prMPFb_BnonSl = pr2DMPFb_BnonSl->ProfileX(("BnonSl"+pr_ind).c_str(),j,j);  
			prMPFb_BCSl_BCbarSl = pr2DMPFb_BCSl_BCbarSl->ProfileX(("BCSl_BCbarSl"+pr_ind).c_str(),j,j);  
			prMPFb_BCSl_BSl = pr2DMPFb_BCSl_BSl->ProfileX(("BCSl_BSl"+pr_ind).c_str(),j,j);  
			prMPFb_BCSl_BTauSl = pr2DMPFb_BCSl_BTauSl->ProfileX(("BCSl_BTauSl"+pr_ind).c_str(),j,j);  
			prMPFb_BCbarSl_BSl = pr2DMPFb_BCbarSl_BSl->ProfileX(("BCbarSl_BSl"+pr_ind).c_str(),j,j);  
			prMPFb_BCbarSl_BTauSl = pr2DMPFb_BCbarSl_BTauSl->ProfileX(("BCbarSl_BTauSl"+pr_ind).c_str(),j,j);  

			prMPFb_BSl_s = pr2DMPFb_BSl_s->ProfileX(("BSl_s"+pr_ind).c_str(),j,j);  
			prMPFb_BTauSl_s = pr2DMPFb_BTauSl_s->ProfileX(("BTauSl_s"+pr_ind).c_str(),j,j);  
			prMPFb_BCSl_s = pr2DMPFb_BCSl_s->ProfileX(("BCSl_s"+pr_ind).c_str(),j,j);  
			prMPFb_BCbarSl_s = pr2DMPFb_BCbarSl_s->ProfileX(("BCbarSl_s"+pr_ind).c_str(),j,j);  
			prMPFb_BnonSl_s = pr2DMPFb_BnonSl_s->ProfileX(("BnonSl_s"+pr_ind).c_str(),j,j);  
			prMPFb_BCSl_BCbarSl_s = pr2DMPFb_BCSl_BCbarSl_s->ProfileX(("BCSl_BCbarSl_s"+pr_ind).c_str(),j,j);  
			prMPFb_BCSl_BSl_s = pr2DMPFb_BCSl_BSl_s->ProfileX(("BCSl_BSl_s"+pr_ind).c_str(),j,j);  
			prMPFb_BCSl_BTauSl_s = pr2DMPFb_BCSl_BTauSl_s->ProfileX(("BCSl_BTauSl_s"+pr_ind).c_str(),j,j);  
			prMPFb_BCbarSl_BSl_s = pr2DMPFb_BCbarSl_BSl_s->ProfileX(("BCbarSl_BSl_s"+pr_ind).c_str(),j,j);  
			prMPFb_BCbarSl_BTauSl_s = pr2DMPFb_BCbarSl_BTauSl_s->ProfileX(("BCbarSl_BTauSl_s"+pr_ind).c_str(),j,j);  

			evtW_BSl.push_back(prMPFb_BSl->GetBinEntries(i));
			evtW_BTauSl.push_back(prMPFb_BTauSl->GetBinEntries(i));
			evtW_BCSl.push_back(prMPFb_BCSl->GetBinEntries(i));
			evtW_BCbarSl.push_back(prMPFb_BCbarSl->GetBinEntries(i));
			evtW_BnonSl.push_back(prMPFb_BnonSl->GetBinEntries(i));
			evtW_BCSl_BCbarSl.push_back(prMPFb_BCSl_BCbarSl->GetBinEntries(i));
			evtW_BCSl_BSl.push_back(prMPFb_BCSl_BSl->GetBinEntries(i));
			evtW_BCSl_BTauSl.push_back(prMPFb_BCSl_BTauSl->GetBinEntries(i));
			evtW_BCbarSl_BSl.push_back(prMPFb_BCbarSl_BSl->GetBinEntries(i));
			evtW_BCbarSl_BTauSl.push_back(prMPFb_BCbarSl_BTauSl->GetBinEntries(i));

			evtWs_BSl.push_back(prMPFb_BSl_s->GetBinEntries(i));
			evtWs_BTauSl.push_back(prMPFb_BTauSl_s->GetBinEntries(i));
			evtWs_BCSl.push_back(prMPFb_BCSl_s->GetBinEntries(i));
			evtWs_BCbarSl.push_back(prMPFb_BCbarSl_s->GetBinEntries(i));
			evtWs_BnonSl.push_back(prMPFb_BnonSl_s->GetBinEntries(i));
			evtWs_BCSl_BCbarSl.push_back(prMPFb_BCSl_BCbarSl_s->GetBinEntries(i));
			evtWs_BCSl_BSl.push_back(prMPFb_BCSl_BSl_s->GetBinEntries(i));
			evtWs_BCSl_BTauSl.push_back(prMPFb_BCSl_BTauSl_s->GetBinEntries(i));
			evtWs_BCbarSl_BSl.push_back(prMPFb_BCbarSl_BSl_s->GetBinEntries(i));
			evtWs_BCbarSl_BTauSl.push_back(prMPFb_BCbarSl_BTauSl_s->GetBinEntries(i));

			sem_BSl.push_back(prMPFb_BSl->GetBinError(i));
			sem_BTauSl.push_back(prMPFb_BTauSl->GetBinError(i));
			sem_BCSl.push_back(prMPFb_BCSl->GetBinError(i));
			sem_BCbarSl.push_back(prMPFb_BCbarSl->GetBinError(i));
			sem_BnonSl.push_back(prMPFb_BnonSl->GetBinError(i));
			sem_BCSl_BCbarSl.push_back(prMPFb_BCSl_BCbarSl->GetBinError(i));
			sem_BCSl_BSl.push_back(prMPFb_BCSl_BSl->GetBinError(i));
			sem_BCSl_BTauSl.push_back(prMPFb_BCSl_BTauSl->GetBinError(i));
			sem_BCbarSl_BSl.push_back(prMPFb_BCbarSl_BSl->GetBinError(i));
			sem_BCbarSl_BTauSl.push_back(prMPFb_BCbarSl_BTauSl->GetBinError(i));

			vals_BSl.push_back(prMPFb_BSl->GetBinContent(i));
			vals_BTauSl.push_back(prMPFb_BTauSl->GetBinContent(i));
			vals_BCSl.push_back(prMPFb_BCSl->GetBinContent(i));
			vals_BCbarSl.push_back(prMPFb_BCbarSl->GetBinContent(i));
			vals_BnonSl.push_back(prMPFb_BnonSl->GetBinContent(i));
			vals_BCSl_BCbarSl.push_back(prMPFb_BCSl_BCbarSl->GetBinContent(i));
			vals_BCSl_BSl.push_back(prMPFb_BCSl_BSl->GetBinContent(i));
			vals_BCSl_BTauSl.push_back(prMPFb_BCSl_BTauSl->GetBinContent(i));
			vals_BCbarSl_BSl.push_back(prMPFb_BCbarSl_BSl->GetBinContent(i));
			vals_BCbarSl_BTauSl.push_back(prMPFb_BCbarSl_BTauSl->GetBinContent(i));

			delete prMPFb_BSl;
			delete prMPFb_BTauSl;
			delete prMPFb_BCSl;
			delete prMPFb_BCbarSl;
			delete prMPFb_BnonSl;
			delete prMPFb_BCSl_BCbarSl;
			delete prMPFb_BCSl_BSl;
			delete prMPFb_BCSl_BTauSl;
			delete prMPFb_BCbarSl_BSl;
			delete prMPFb_BCbarSl_BTauSl;

			delete prMPFb_BSl_s;
			delete prMPFb_BTauSl_s;
			delete prMPFb_BCSl_s;
			delete prMPFb_BCbarSl_s;
			delete prMPFb_BnonSl_s;
			delete prMPFb_BCSl_BCbarSl_s;
			delete prMPFb_BCSl_BSl_s;
			delete prMPFb_BCSl_BTauSl_s;
			delete prMPFb_BCbarSl_BSl_s;
			delete prMPFb_BCbarSl_BTauSl_s;
		}

		 
		vector<vector<double>> vec_evtW{evtW_BSl,evtW_BTauSl,evtW_BCSl,evtW_BCbarSl,evtW_BnonSl,evtW_BCSl_BCbarSl,
																		evtW_BCSl_BSl,evtW_BCSl_BTauSl,evtW_BCbarSl_BSl,evtW_BCbarSl_BTauSl};

		vector<vector<double>> vec_evtWs{evtWs_BSl,evtWs_BTauSl,evtWs_BCSl,evtWs_BCbarSl,evtWs_BnonSl,evtWs_BCSl_BCbarSl,
																		evtWs_BCSl_BSl,evtWs_BCSl_BTauSl,evtWs_BCbarSl_BSl,evtWs_BCbarSl_BTauSl};

		vector<vector<double>> vec_sem{sem_BSl,sem_BTauSl,sem_BCSl,sem_BCbarSl,sem_BnonSl,sem_BCSl_BCbarSl,
																		sem_BCSl_BSl,sem_BCSl_BTauSl,sem_BCbarSl_BSl,sem_BCbarSl_BTauSl};

		vector<vector<double>> vec_vals{vals_BSl,vals_BTauSl,vals_BCSl,vals_BCbarSl,vals_BnonSl,vals_BCSl_BCbarSl,
																		vals_BCSl_BSl,vals_BCSl_BTauSl,vals_BCbarSl_BSl,vals_BCbarSl_BTauSl};

		//set small number so that we dont divide by zero 
		evtWtot_BSl = 1e-10;
		evtWtot_BTauSl = 1e-10;
		evtWtot_BCSl = 1e-10;
		evtWtot_BCbarSl = 1e-10;
		evtWtot_BnonSl = 1e-10;
		evtWtot_BCSl_BCbarSl = 1e-10;
		evtWtot_BCSl_BSl = 1e-10;
		evtWtot_BCSl_BTauSl = 1e-10;
		evtWtot_BCbarSl_BSl = 1e-10;
		evtWtot_BCbarSl_BTauSl = 1e-10;

		evtWtots_BSl = 1e-10;
		evtWtots_BTauSl = 1e-10;
		evtWtots_BCSl = 1e-10;
		evtWtots_BCbarSl = 1e-10;
		evtWtots_BnonSl = 1e-10;
		evtWtots_BCSl_BCbarSl = 1e-10;
		evtWtots_BCSl_BSl = 1e-10;
		evtWtots_BCSl_BTauSl = 1e-10;
		evtWtots_BCbarSl_BSl = 1e-10;
		evtWtots_BCbarSl_BTauSl = 1e-10;

		for (double x : evtW_BSl) evtWtot_BSl += x;
		for (double x : evtW_BTauSl) evtWtot_BTauSl += x;
		for (double x : evtW_BCSl) evtWtot_BCSl += x;
		for (double x : evtW_BCbarSl) evtWtot_BCbarSl += x;
		for (double x : evtW_BnonSl) evtWtot_BnonSl += x;
		for (double x : evtW_BCSl_BCbarSl) evtWtot_BCSl_BCbarSl += x;
		for (double x : evtW_BCSl_BSl) evtWtot_BCSl_BSl += x;
		for (double x : evtW_BCSl_BTauSl) evtWtot_BCSl_BTauSl += x;
		for (double x : evtW_BCbarSl_BSl) evtWtot_BCbarSl_BSl += x;
		for (double x : evtW_BCbarSl_BTauSl) evtWtot_BCbarSl_BTauSl += x;

		for (double x : evtWs_BSl) evtWtots_BSl += x;
		for (double x : evtWs_BTauSl) evtWtots_BTauSl += x;
		for (double x : evtWs_BCSl) evtWtots_BCSl += x;
		for (double x : evtWs_BCbarSl) evtWtots_BCbarSl += x;
		for (double x : evtWs_BnonSl) evtWtots_BnonSl += x;
		for (double x : evtWs_BCSl_BCbarSl) evtWtots_BCSl_BCbarSl += x;
		for (double x : evtWs_BCSl_BSl) evtWtots_BCSl_BSl += x;
		for (double x : evtWs_BCSl_BTauSl) evtWtots_BCSl_BTauSl += x;
		for (double x : evtWs_BCbarSl_BSl) evtWtots_BCbarSl_BSl += x;
		for (double x : evtWs_BCbarSl_BTauSl) evtWtots_BCbarSl_BTauSl += x;
		
		evtWtot = evtWtot_BSl + evtWtot_BTauSl + evtWtot_BCSl + evtWtot_BCbarSl + 
							evtWtot_BnonSl + evtWtot_BCSl_BCbarSl + evtWtot_BCSl_BSl +
							evtWtot_BCSl_BTauSl + evtWtot_BCbarSl_BSl + evtWtot_BCbarSl_BTauSl;

		evtWtots = evtWtots_BSl + evtWtots_BTauSl + evtWtots_BCSl + evtWtots_BCbarSl + 
							 evtWtots_BnonSl + evtWtots_BCSl_BCbarSl + evtWtots_BCSl_BSl +
							 evtWtots_BCSl_BTauSl + evtWtots_BCbarSl_BSl + evtWtots_BCbarSl_BTauSl;
		
		//loop over diff scale factors 
		errorb = 0;
		for (int j = 0; j < hMPFb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hMPFb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}
		
		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb->GetBinContent(i) == 0) diffb->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb->SetBinError(i,errorb);
  }

	//############### plus variation ##################
  //Loop over the TProfile bins
  for (int i = 1; i < hMPFb->GetNbinsX()+1; i++){
		evtW_BSl.clear();
		evtW_BTauSl.clear();
		evtW_BCSl.clear();
		evtW_BCbarSl.clear();
		evtW_BnonSl.clear();
		evtW_BCSl_BCbarSl.clear();
		evtW_BCSl_BSl.clear();
		evtW_BCSl_BTauSl.clear();
		evtW_BCbarSl_BSl.clear();
		evtW_BCbarSl_BTauSl.clear();

		evtWs_BSl.clear();
		evtWs_BTauSl.clear();
		evtWs_BCSl.clear();
		evtWs_BCbarSl.clear();
		evtWs_BnonSl.clear();
		evtWs_BCSl_BCbarSl.clear();
		evtWs_BCSl_BSl.clear();
		evtWs_BCSl_BTauSl.clear();
		evtWs_BCbarSl_BSl.clear();
		evtWs_BCbarSl_BTauSl.clear();

		sem_BSl.clear();
		sem_BTauSl.clear();
		sem_BCSl.clear();
		sem_BCbarSl.clear();
		sem_BnonSl.clear();
		sem_BCSl_BCbarSl.clear();
		sem_BCSl_BSl.clear();
		sem_BCSl_BTauSl.clear();
		sem_BCbarSl_BSl.clear();
		sem_BCbarSl_BTauSl.clear();

		vals_BSl.clear();
		vals_BTauSl.clear();
		vals_BCSl.clear();
		vals_BCbarSl.clear();
		vals_BnonSl.clear();
		vals_BCSl_BCbarSl.clear();
		vals_BCSl_BSl.clear();
		vals_BCSl_BTauSl.clear();
		vals_BCbarSl_BSl.clear();
		vals_BCbarSl_BTauSl.clear();

		for (int j = 1; j < hMPFb->GetNbinsX()+1; j++) {
			string pr_ind = "_" + to_string(i) + "_" + to_string(j);

			prMPFb_BSl = pr2DMPFb_BSl->ProfileX(("BSl"+pr_ind).c_str(),j,j);  
			prMPFb_BTauSl = pr2DMPFb_BTauSl->ProfileX(("BTauSl"+pr_ind).c_str(),j,j);  
			prMPFb_BCSl = pr2DMPFb_BCSl->ProfileX(("BCSl"+pr_ind).c_str(),j,j);  
			prMPFb_BCbarSl = pr2DMPFb_BCbarSl->ProfileX(("BCbarSl"+pr_ind).c_str(),j,j);  
			prMPFb_BnonSl = pr2DMPFb_BnonSl->ProfileX(("BnonSl"+pr_ind).c_str(),j,j);  
			prMPFb_BCSl_BCbarSl = pr2DMPFb_BCSl_BCbarSl->ProfileX(("BCSl_BCbarSl"+pr_ind).c_str(),j,j);  
			prMPFb_BCSl_BSl = pr2DMPFb_BCSl_BSl->ProfileX(("BCSl_BSl"+pr_ind).c_str(),j,j);  
			prMPFb_BCSl_BTauSl = pr2DMPFb_BCSl_BTauSl->ProfileX(("BCSl_BTauSl"+pr_ind).c_str(),j,j);  
			prMPFb_BCbarSl_BSl = pr2DMPFb_BCbarSl_BSl->ProfileX(("BCbarSl_BSl"+pr_ind).c_str(),j,j);  
			prMPFb_BCbarSl_BTauSl = pr2DMPFb_BCbarSl_BTauSl->ProfileX(("BCbarSl_BTauSl"+pr_ind).c_str(),j,j);  

			prMPFb_BSl_s_p = pr2DMPFb_BSl_s_p->ProfileX(("BSl_s"+pr_ind).c_str(),j,j);  
			prMPFb_BTauSl_s_p = pr2DMPFb_BTauSl_s_p->ProfileX(("BTauSl_s"+pr_ind).c_str(),j,j);  
			prMPFb_BCSl_s_p = pr2DMPFb_BCSl_s_p->ProfileX(("BCSl_s"+pr_ind).c_str(),j,j);  
			prMPFb_BCbarSl_s_p = pr2DMPFb_BCbarSl_s_p->ProfileX(("BCbarSl_s"+pr_ind).c_str(),j,j);  
			prMPFb_BnonSl_s_p = pr2DMPFb_BnonSl_s_p->ProfileX(("BnonSl_s"+pr_ind).c_str(),j,j);  
			prMPFb_BCSl_BCbarSl_s_p = pr2DMPFb_BCSl_BCbarSl_s_p->ProfileX(("BCSl_BCbarSl_s"+pr_ind).c_str(),j,j);  
			prMPFb_BCSl_BSl_s_p = pr2DMPFb_BCSl_BSl_s_p->ProfileX(("BCSl_BSl_s"+pr_ind).c_str(),j,j);  
			prMPFb_BCSl_BTauSl_s_p = pr2DMPFb_BCSl_BTauSl_s_p->ProfileX(("BCSl_BTauSl_s"+pr_ind).c_str(),j,j);  
			prMPFb_BCbarSl_BSl_s_p = pr2DMPFb_BCbarSl_BSl_s_p->ProfileX(("BCbarSl_BSl_s"+pr_ind).c_str(),j,j);  
			prMPFb_BCbarSl_BTauSl_s_p = pr2DMPFb_BCbarSl_BTauSl_s_p->ProfileX(("BCbarSl_BTauSl_s"+pr_ind).c_str(),j,j);  

			evtW_BSl.push_back(prMPFb_BSl->GetBinEntries(i));
			evtW_BTauSl.push_back(prMPFb_BTauSl->GetBinEntries(i));
			evtW_BCSl.push_back(prMPFb_BCSl->GetBinEntries(i));
			evtW_BCbarSl.push_back(prMPFb_BCbarSl->GetBinEntries(i));
			evtW_BnonSl.push_back(prMPFb_BnonSl->GetBinEntries(i));
			evtW_BCSl_BCbarSl.push_back(prMPFb_BCSl_BCbarSl->GetBinEntries(i));
			evtW_BCSl_BSl.push_back(prMPFb_BCSl_BSl->GetBinEntries(i));
			evtW_BCSl_BTauSl.push_back(prMPFb_BCSl_BTauSl->GetBinEntries(i));
			evtW_BCbarSl_BSl.push_back(prMPFb_BCbarSl_BSl->GetBinEntries(i));
			evtW_BCbarSl_BTauSl.push_back(prMPFb_BCbarSl_BTauSl->GetBinEntries(i));

			evtWs_BSl.push_back(prMPFb_BSl_s_p->GetBinEntries(i));
			evtWs_BTauSl.push_back(prMPFb_BTauSl_s_p->GetBinEntries(i));
			evtWs_BCSl.push_back(prMPFb_BCSl_s_p->GetBinEntries(i));
			evtWs_BCbarSl.push_back(prMPFb_BCbarSl_s_p->GetBinEntries(i));
			evtWs_BnonSl.push_back(prMPFb_BnonSl_s_p->GetBinEntries(i));
			evtWs_BCSl_BCbarSl.push_back(prMPFb_BCSl_BCbarSl_s_p->GetBinEntries(i));
			evtWs_BCSl_BSl.push_back(prMPFb_BCSl_BSl_s_p->GetBinEntries(i));
			evtWs_BCSl_BTauSl.push_back(prMPFb_BCSl_BTauSl_s_p->GetBinEntries(i));
			evtWs_BCbarSl_BSl.push_back(prMPFb_BCbarSl_BSl_s_p->GetBinEntries(i));
			evtWs_BCbarSl_BTauSl.push_back(prMPFb_BCbarSl_BTauSl_s_p->GetBinEntries(i));

			sem_BSl.push_back(prMPFb_BSl->GetBinError(i));
			sem_BTauSl.push_back(prMPFb_BTauSl->GetBinError(i));
			sem_BCSl.push_back(prMPFb_BCSl->GetBinError(i));
			sem_BCbarSl.push_back(prMPFb_BCbarSl->GetBinError(i));
			sem_BnonSl.push_back(prMPFb_BnonSl->GetBinError(i));
			sem_BCSl_BCbarSl.push_back(prMPFb_BCSl_BCbarSl->GetBinError(i));
			sem_BCSl_BSl.push_back(prMPFb_BCSl_BSl->GetBinError(i));
			sem_BCSl_BTauSl.push_back(prMPFb_BCSl_BTauSl->GetBinError(i));
			sem_BCbarSl_BSl.push_back(prMPFb_BCbarSl_BSl->GetBinError(i));
			sem_BCbarSl_BTauSl.push_back(prMPFb_BCbarSl_BTauSl->GetBinError(i));

			vals_BSl.push_back(prMPFb_BSl->GetBinContent(i));
			vals_BTauSl.push_back(prMPFb_BTauSl->GetBinContent(i));
			vals_BCSl.push_back(prMPFb_BCSl->GetBinContent(i));
			vals_BCbarSl.push_back(prMPFb_BCbarSl->GetBinContent(i));
			vals_BnonSl.push_back(prMPFb_BnonSl->GetBinContent(i));
			vals_BCSl_BCbarSl.push_back(prMPFb_BCSl_BCbarSl->GetBinContent(i));
			vals_BCSl_BSl.push_back(prMPFb_BCSl_BSl->GetBinContent(i));
			vals_BCSl_BTauSl.push_back(prMPFb_BCSl_BTauSl->GetBinContent(i));
			vals_BCbarSl_BSl.push_back(prMPFb_BCbarSl_BSl->GetBinContent(i));
			vals_BCbarSl_BTauSl.push_back(prMPFb_BCbarSl_BTauSl->GetBinContent(i));

			delete prMPFb_BSl;
			delete prMPFb_BTauSl;
			delete prMPFb_BCSl;
			delete prMPFb_BCbarSl;
			delete prMPFb_BnonSl;
			delete prMPFb_BCSl_BCbarSl;
			delete prMPFb_BCSl_BSl;
			delete prMPFb_BCSl_BTauSl;
			delete prMPFb_BCbarSl_BSl;
			delete prMPFb_BCbarSl_BTauSl;

			delete prMPFb_BSl_s_p;
			delete prMPFb_BTauSl_s_p;
			delete prMPFb_BCSl_s_p;
			delete prMPFb_BCbarSl_s_p;
			delete prMPFb_BnonSl_s_p;
			delete prMPFb_BCSl_BCbarSl_s_p;
			delete prMPFb_BCSl_BSl_s_p;
			delete prMPFb_BCSl_BTauSl_s_p;
			delete prMPFb_BCbarSl_BSl_s_p;
			delete prMPFb_BCbarSl_BTauSl_s_p;
		}

		 
		vector<vector<double>> vec_evtW{evtW_BSl,evtW_BTauSl,evtW_BCSl,evtW_BCbarSl,evtW_BnonSl,evtW_BCSl_BCbarSl,
																		evtW_BCSl_BSl,evtW_BCSl_BTauSl,evtW_BCbarSl_BSl,evtW_BCbarSl_BTauSl};

		vector<vector<double>> vec_evtWs{evtWs_BSl,evtWs_BTauSl,evtWs_BCSl,evtWs_BCbarSl,evtWs_BnonSl,evtWs_BCSl_BCbarSl,
																		evtWs_BCSl_BSl,evtWs_BCSl_BTauSl,evtWs_BCbarSl_BSl,evtWs_BCbarSl_BTauSl};

		vector<vector<double>> vec_sem{sem_BSl,sem_BTauSl,sem_BCSl,sem_BCbarSl,sem_BnonSl,sem_BCSl_BCbarSl,
																		sem_BCSl_BSl,sem_BCSl_BTauSl,sem_BCbarSl_BSl,sem_BCbarSl_BTauSl};

		vector<vector<double>> vec_vals{vals_BSl,vals_BTauSl,vals_BCSl,vals_BCbarSl,vals_BnonSl,vals_BCSl_BCbarSl,
																		vals_BCSl_BSl,vals_BCSl_BTauSl,vals_BCbarSl_BSl,vals_BCbarSl_BTauSl};

		//set small number so that we dont divide by zero 
		evtWtot_BSl = 1e-10;
		evtWtot_BTauSl = 1e-10;
		evtWtot_BCSl = 1e-10;
		evtWtot_BCbarSl = 1e-10;
		evtWtot_BnonSl = 1e-10;
		evtWtot_BCSl_BCbarSl = 1e-10;
		evtWtot_BCSl_BSl = 1e-10;
		evtWtot_BCSl_BTauSl = 1e-10;
		evtWtot_BCbarSl_BSl = 1e-10;
		evtWtot_BCbarSl_BTauSl = 1e-10;

		evtWtots_BSl = 1e-10;
		evtWtots_BTauSl = 1e-10;
		evtWtots_BCSl = 1e-10;
		evtWtots_BCbarSl = 1e-10;
		evtWtots_BnonSl = 1e-10;
		evtWtots_BCSl_BCbarSl = 1e-10;
		evtWtots_BCSl_BSl = 1e-10;
		evtWtots_BCSl_BTauSl = 1e-10;
		evtWtots_BCbarSl_BSl = 1e-10;
		evtWtots_BCbarSl_BTauSl = 1e-10;

		for (double x : evtW_BSl) evtWtot_BSl += x;
		for (double x : evtW_BTauSl) evtWtot_BTauSl += x;
		for (double x : evtW_BCSl) evtWtot_BCSl += x;
		for (double x : evtW_BCbarSl) evtWtot_BCbarSl += x;
		for (double x : evtW_BnonSl) evtWtot_BnonSl += x;
		for (double x : evtW_BCSl_BCbarSl) evtWtot_BCSl_BCbarSl += x;
		for (double x : evtW_BCSl_BSl) evtWtot_BCSl_BSl += x;
		for (double x : evtW_BCSl_BTauSl) evtWtot_BCSl_BTauSl += x;
		for (double x : evtW_BCbarSl_BSl) evtWtot_BCbarSl_BSl += x;
		for (double x : evtW_BCbarSl_BTauSl) evtWtot_BCbarSl_BTauSl += x;

		for (double x : evtWs_BSl) evtWtots_BSl += x;
		for (double x : evtWs_BTauSl) evtWtots_BTauSl += x;
		for (double x : evtWs_BCSl) evtWtots_BCSl += x;
		for (double x : evtWs_BCbarSl) evtWtots_BCbarSl += x;
		for (double x : evtWs_BnonSl) evtWtots_BnonSl += x;
		for (double x : evtWs_BCSl_BCbarSl) evtWtots_BCSl_BCbarSl += x;
		for (double x : evtWs_BCSl_BSl) evtWtots_BCSl_BSl += x;
		for (double x : evtWs_BCSl_BTauSl) evtWtots_BCSl_BTauSl += x;
		for (double x : evtWs_BCbarSl_BSl) evtWtots_BCbarSl_BSl += x;
		for (double x : evtWs_BCbarSl_BTauSl) evtWtots_BCbarSl_BTauSl += x;
		
		evtWtot = evtWtot_BSl + evtWtot_BTauSl + evtWtot_BCSl + evtWtot_BCbarSl + 
							evtWtot_BnonSl + evtWtot_BCSl_BCbarSl + evtWtot_BCSl_BSl +
							evtWtot_BCSl_BTauSl + evtWtot_BCbarSl_BSl + evtWtot_BCbarSl_BTauSl;

		evtWtots = evtWtots_BSl + evtWtots_BTauSl + evtWtots_BCSl + evtWtots_BCbarSl + 
							 evtWtots_BnonSl + evtWtots_BCSl_BCbarSl + evtWtots_BCSl_BSl +
							 evtWtots_BCSl_BTauSl + evtWtots_BCbarSl_BSl + evtWtots_BCbarSl_BTauSl;
		
		//loop over diff scale factors 
		errorb = 0;
		for (int j = 0; j < hMPFb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hMPFb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}
		
		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb_p->GetBinContent(i) == 0) diffb_p->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb_p->SetBinError(i,errorb);
  }

	//############### minus variation ####################
  //Loop over the TProfile bins
  for (int i = 1; i < hMPFb->GetNbinsX()+1; i++){
		evtW_BSl.clear();
		evtW_BTauSl.clear();
		evtW_BCSl.clear();
		evtW_BCbarSl.clear();
		evtW_BnonSl.clear();
		evtW_BCSl_BCbarSl.clear();
		evtW_BCSl_BSl.clear();
		evtW_BCSl_BTauSl.clear();
		evtW_BCbarSl_BSl.clear();
		evtW_BCbarSl_BTauSl.clear();

		evtWs_BSl.clear();
		evtWs_BTauSl.clear();
		evtWs_BCSl.clear();
		evtWs_BCbarSl.clear();
		evtWs_BnonSl.clear();
		evtWs_BCSl_BCbarSl.clear();
		evtWs_BCSl_BSl.clear();
		evtWs_BCSl_BTauSl.clear();
		evtWs_BCbarSl_BSl.clear();
		evtWs_BCbarSl_BTauSl.clear();

		sem_BSl.clear();
		sem_BTauSl.clear();
		sem_BCSl.clear();
		sem_BCbarSl.clear();
		sem_BnonSl.clear();
		sem_BCSl_BCbarSl.clear();
		sem_BCSl_BSl.clear();
		sem_BCSl_BTauSl.clear();
		sem_BCbarSl_BSl.clear();
		sem_BCbarSl_BTauSl.clear();

		vals_BSl.clear();
		vals_BTauSl.clear();
		vals_BCSl.clear();
		vals_BCbarSl.clear();
		vals_BnonSl.clear();
		vals_BCSl_BCbarSl.clear();
		vals_BCSl_BSl.clear();
		vals_BCSl_BTauSl.clear();
		vals_BCbarSl_BSl.clear();
		vals_BCbarSl_BTauSl.clear();

		for (int j = 1; j < hMPFb->GetNbinsX()+1; j++) {
			string pr_ind = "_" + to_string(i) + "_" + to_string(j);

			prMPFb_BSl = pr2DMPFb_BSl->ProfileX(("BSl"+pr_ind).c_str(),j,j);  
			prMPFb_BTauSl = pr2DMPFb_BTauSl->ProfileX(("BTauSl"+pr_ind).c_str(),j,j);  
			prMPFb_BCSl = pr2DMPFb_BCSl->ProfileX(("BCSl"+pr_ind).c_str(),j,j);  
			prMPFb_BCbarSl = pr2DMPFb_BCbarSl->ProfileX(("BCbarSl"+pr_ind).c_str(),j,j);  
			prMPFb_BnonSl = pr2DMPFb_BnonSl->ProfileX(("BnonSl"+pr_ind).c_str(),j,j);  
			prMPFb_BCSl_BCbarSl = pr2DMPFb_BCSl_BCbarSl->ProfileX(("BCSl_BCbarSl"+pr_ind).c_str(),j,j);  
			prMPFb_BCSl_BSl = pr2DMPFb_BCSl_BSl->ProfileX(("BCSl_BSl"+pr_ind).c_str(),j,j);  
			prMPFb_BCSl_BTauSl = pr2DMPFb_BCSl_BTauSl->ProfileX(("BCSl_BTauSl"+pr_ind).c_str(),j,j);  
			prMPFb_BCbarSl_BSl = pr2DMPFb_BCbarSl_BSl->ProfileX(("BCbarSl_BSl"+pr_ind).c_str(),j,j);  
			prMPFb_BCbarSl_BTauSl = pr2DMPFb_BCbarSl_BTauSl->ProfileX(("BCbarSl_BTauSl"+pr_ind).c_str(),j,j);  

			prMPFb_BSl_s_m = pr2DMPFb_BSl_s_m->ProfileX(("BSl_s"+pr_ind).c_str(),j,j);  
			prMPFb_BTauSl_s_m = pr2DMPFb_BTauSl_s_m->ProfileX(("BTauSl_s"+pr_ind).c_str(),j,j);  
			prMPFb_BCSl_s_m = pr2DMPFb_BCSl_s_m->ProfileX(("BCSl_s"+pr_ind).c_str(),j,j);  
			prMPFb_BCbarSl_s_m = pr2DMPFb_BCbarSl_s_m->ProfileX(("BCbarSl_s"+pr_ind).c_str(),j,j);  
			prMPFb_BnonSl_s_m = pr2DMPFb_BnonSl_s_m->ProfileX(("BnonSl_s"+pr_ind).c_str(),j,j);  
			prMPFb_BCSl_BCbarSl_s_m = pr2DMPFb_BCSl_BCbarSl_s_m->ProfileX(("BCSl_BCbarSl_s"+pr_ind).c_str(),j,j);  
			prMPFb_BCSl_BSl_s_m = pr2DMPFb_BCSl_BSl_s_m->ProfileX(("BCSl_BSl_s"+pr_ind).c_str(),j,j);  
			prMPFb_BCSl_BTauSl_s_m = pr2DMPFb_BCSl_BTauSl_s_m->ProfileX(("BCSl_BTauSl_s"+pr_ind).c_str(),j,j);  
			prMPFb_BCbarSl_BSl_s_m = pr2DMPFb_BCbarSl_BSl_s_m->ProfileX(("BCbarSl_BSl_s"+pr_ind).c_str(),j,j);  
			prMPFb_BCbarSl_BTauSl_s_m = pr2DMPFb_BCbarSl_BTauSl_s_m->ProfileX(("BCbarSl_BTauSl_s"+pr_ind).c_str(),j,j); 

			evtW_BSl.push_back(prMPFb_BSl->GetBinEntries(i));
			evtW_BTauSl.push_back(prMPFb_BTauSl->GetBinEntries(i));
			evtW_BCSl.push_back(prMPFb_BCSl->GetBinEntries(i));
			evtW_BCbarSl.push_back(prMPFb_BCbarSl->GetBinEntries(i));
			evtW_BnonSl.push_back(prMPFb_BnonSl->GetBinEntries(i));
			evtW_BCSl_BCbarSl.push_back(prMPFb_BCSl_BCbarSl->GetBinEntries(i));
			evtW_BCSl_BSl.push_back(prMPFb_BCSl_BSl->GetBinEntries(i));
			evtW_BCSl_BTauSl.push_back(prMPFb_BCSl_BTauSl->GetBinEntries(i));
			evtW_BCbarSl_BSl.push_back(prMPFb_BCbarSl_BSl->GetBinEntries(i));
			evtW_BCbarSl_BTauSl.push_back(prMPFb_BCbarSl_BTauSl->GetBinEntries(i));

			evtWs_BSl.push_back(prMPFb_BSl_s_m->GetBinEntries(i));
			evtWs_BTauSl.push_back(prMPFb_BTauSl_s_m->GetBinEntries(i));
			evtWs_BCSl.push_back(prMPFb_BCSl_s_m->GetBinEntries(i));
			evtWs_BCbarSl.push_back(prMPFb_BCbarSl_s_m->GetBinEntries(i));
			evtWs_BnonSl.push_back(prMPFb_BnonSl_s_m->GetBinEntries(i));
			evtWs_BCSl_BCbarSl.push_back(prMPFb_BCSl_BCbarSl_s_m->GetBinEntries(i));
			evtWs_BCSl_BSl.push_back(prMPFb_BCSl_BSl_s_m->GetBinEntries(i));
			evtWs_BCSl_BTauSl.push_back(prMPFb_BCSl_BTauSl_s_m->GetBinEntries(i));
			evtWs_BCbarSl_BSl.push_back(prMPFb_BCbarSl_BSl_s_m->GetBinEntries(i));
			evtWs_BCbarSl_BTauSl.push_back(prMPFb_BCbarSl_BTauSl_s_m->GetBinEntries(i));

			sem_BSl.push_back(prMPFb_BSl->GetBinError(i));
			sem_BTauSl.push_back(prMPFb_BTauSl->GetBinError(i));
			sem_BCSl.push_back(prMPFb_BCSl->GetBinError(i));
			sem_BCbarSl.push_back(prMPFb_BCbarSl->GetBinError(i));
			sem_BnonSl.push_back(prMPFb_BnonSl->GetBinError(i));
			sem_BCSl_BCbarSl.push_back(prMPFb_BCSl_BCbarSl->GetBinError(i));
			sem_BCSl_BSl.push_back(prMPFb_BCSl_BSl->GetBinError(i));
			sem_BCSl_BTauSl.push_back(prMPFb_BCSl_BTauSl->GetBinError(i));
			sem_BCbarSl_BSl.push_back(prMPFb_BCbarSl_BSl->GetBinError(i));
			sem_BCbarSl_BTauSl.push_back(prMPFb_BCbarSl_BTauSl->GetBinError(i));

			vals_BSl.push_back(prMPFb_BSl->GetBinContent(i));
			vals_BTauSl.push_back(prMPFb_BTauSl->GetBinContent(i));
			vals_BCSl.push_back(prMPFb_BCSl->GetBinContent(i));
			vals_BCbarSl.push_back(prMPFb_BCbarSl->GetBinContent(i));
			vals_BnonSl.push_back(prMPFb_BnonSl->GetBinContent(i));
			vals_BCSl_BCbarSl.push_back(prMPFb_BCSl_BCbarSl->GetBinContent(i));
			vals_BCSl_BSl.push_back(prMPFb_BCSl_BSl->GetBinContent(i));
			vals_BCSl_BTauSl.push_back(prMPFb_BCSl_BTauSl->GetBinContent(i));
			vals_BCbarSl_BSl.push_back(prMPFb_BCbarSl_BSl->GetBinContent(i));
			vals_BCbarSl_BTauSl.push_back(prMPFb_BCbarSl_BTauSl->GetBinContent(i));

			delete prMPFb_BSl;
			delete prMPFb_BTauSl;
			delete prMPFb_BCSl;
			delete prMPFb_BCbarSl;
			delete prMPFb_BnonSl;
			delete prMPFb_BCSl_BCbarSl;
			delete prMPFb_BCSl_BSl;
			delete prMPFb_BCSl_BTauSl;
			delete prMPFb_BCbarSl_BSl;
			delete prMPFb_BCbarSl_BTauSl;

			delete prMPFb_BSl_s_m;
			delete prMPFb_BTauSl_s_m;
			delete prMPFb_BCSl_s_m;
			delete prMPFb_BCbarSl_s_m;
			delete prMPFb_BnonSl_s_m;
			delete prMPFb_BCSl_BCbarSl_s_m;
			delete prMPFb_BCSl_BSl_s_m;
			delete prMPFb_BCSl_BTauSl_s_m;
			delete prMPFb_BCbarSl_BSl_s_m;
			delete prMPFb_BCbarSl_BTauSl_s_m;
		}

		 
		vector<vector<double>> vec_evtW{evtW_BSl,evtW_BTauSl,evtW_BCSl,evtW_BCbarSl,evtW_BnonSl,evtW_BCSl_BCbarSl,
																		evtW_BCSl_BSl,evtW_BCSl_BTauSl,evtW_BCbarSl_BSl,evtW_BCbarSl_BTauSl};

		vector<vector<double>> vec_evtWs{evtWs_BSl,evtWs_BTauSl,evtWs_BCSl,evtWs_BCbarSl,evtWs_BnonSl,evtWs_BCSl_BCbarSl,
																		evtWs_BCSl_BSl,evtWs_BCSl_BTauSl,evtWs_BCbarSl_BSl,evtWs_BCbarSl_BTauSl};

		vector<vector<double>> vec_sem{sem_BSl,sem_BTauSl,sem_BCSl,sem_BCbarSl,sem_BnonSl,sem_BCSl_BCbarSl,
																		sem_BCSl_BSl,sem_BCSl_BTauSl,sem_BCbarSl_BSl,sem_BCbarSl_BTauSl};

		vector<vector<double>> vec_vals{vals_BSl,vals_BTauSl,vals_BCSl,vals_BCbarSl,vals_BnonSl,vals_BCSl_BCbarSl,
																		vals_BCSl_BSl,vals_BCSl_BTauSl,vals_BCbarSl_BSl,vals_BCbarSl_BTauSl};

		//set small number so that we dont divide by zero 
		evtWtot_BSl = 1e-10;
		evtWtot_BTauSl = 1e-10;
		evtWtot_BCSl = 1e-10;
		evtWtot_BCbarSl = 1e-10;
		evtWtot_BnonSl = 1e-10;
		evtWtot_BCSl_BCbarSl = 1e-10;
		evtWtot_BCSl_BSl = 1e-10;
		evtWtot_BCSl_BTauSl = 1e-10;
		evtWtot_BCbarSl_BSl = 1e-10;
		evtWtot_BCbarSl_BTauSl = 1e-10;

		evtWtots_BSl = 1e-10;
		evtWtots_BTauSl = 1e-10;
		evtWtots_BCSl = 1e-10;
		evtWtots_BCbarSl = 1e-10;
		evtWtots_BnonSl = 1e-10;
		evtWtots_BCSl_BCbarSl = 1e-10;
		evtWtots_BCSl_BSl = 1e-10;
		evtWtots_BCSl_BTauSl = 1e-10;
		evtWtots_BCbarSl_BSl = 1e-10;
		evtWtots_BCbarSl_BTauSl = 1e-10;

		for (double x : evtW_BSl) evtWtot_BSl += x;
		for (double x : evtW_BTauSl) evtWtot_BTauSl += x;
		for (double x : evtW_BCSl) evtWtot_BCSl += x;
		for (double x : evtW_BCbarSl) evtWtot_BCbarSl += x;
		for (double x : evtW_BnonSl) evtWtot_BnonSl += x;
		for (double x : evtW_BCSl_BCbarSl) evtWtot_BCSl_BCbarSl += x;
		for (double x : evtW_BCSl_BSl) evtWtot_BCSl_BSl += x;
		for (double x : evtW_BCSl_BTauSl) evtWtot_BCSl_BTauSl += x;
		for (double x : evtW_BCbarSl_BSl) evtWtot_BCbarSl_BSl += x;
		for (double x : evtW_BCbarSl_BTauSl) evtWtot_BCbarSl_BTauSl += x;

		for (double x : evtWs_BSl) evtWtots_BSl += x;
		for (double x : evtWs_BTauSl) evtWtots_BTauSl += x;
		for (double x : evtWs_BCSl) evtWtots_BCSl += x;
		for (double x : evtWs_BCbarSl) evtWtots_BCbarSl += x;
		for (double x : evtWs_BnonSl) evtWtots_BnonSl += x;
		for (double x : evtWs_BCSl_BCbarSl) evtWtots_BCSl_BCbarSl += x;
		for (double x : evtWs_BCSl_BSl) evtWtots_BCSl_BSl += x;
		for (double x : evtWs_BCSl_BTauSl) evtWtots_BCSl_BTauSl += x;
		for (double x : evtWs_BCbarSl_BSl) evtWtots_BCbarSl_BSl += x;
		for (double x : evtWs_BCbarSl_BTauSl) evtWtots_BCbarSl_BTauSl += x;
		
		evtWtot = evtWtot_BSl + evtWtot_BTauSl + evtWtot_BCSl + evtWtot_BCbarSl + 
							evtWtot_BnonSl + evtWtot_BCSl_BCbarSl + evtWtot_BCSl_BSl +
							evtWtot_BCSl_BTauSl + evtWtot_BCbarSl_BSl + evtWtot_BCbarSl_BTauSl;

		evtWtots = evtWtots_BSl + evtWtots_BTauSl + evtWtots_BCSl + evtWtots_BCbarSl + 
							 evtWtots_BnonSl + evtWtots_BCSl_BCbarSl + evtWtots_BCSl_BSl +
							 evtWtots_BCSl_BTauSl + evtWtots_BCbarSl_BSl + evtWtots_BCbarSl_BTauSl;
		
		//loop over diff scale factors 
		errorb = 0;
		for (int j = 0; j < hMPFb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hMPFb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}
		
		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb_m->GetBinContent(i) == 0) diffb_m->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb_m->SetBinError(i,errorb);
  }

	//Scale to the percentages
  diffb->Scale(100.0);
  diffb_p->Scale(100.0);
  diffb_m->Scale(100.0);

	MPFb_map["diffb"] = diffb;
	MPFb_map["diffb_p"] = diffb_p;
	MPFb_map["diffb_m"] = diffb_m;

	return MPFb_map;
}
map<string, TH1D*> pTbal_BtoSLBr () {
	map <string, TH1D*> pTbalb_map;

  TFile* f_normal = TFile::Open(OpenName.c_str());
  TFile* f_scaled = TFile::Open(OpenName_BtoSLBr.c_str());
  TFile* f_scaled_p = TFile::Open(OpenName_BtoSLBr_p.c_str());
  TFile* f_scaled_m = TFile::Open(OpenName_BtoSLBr_m.c_str());

	TProfile *prpTbalb;
	TProfile *prpTbalb_s;
	TProfile *prpTbalb_s_p;
	TProfile *prpTbalb_s_m;

  //Read the normal sample
  f_normal->GetObject("prpTbalb",   prpTbalb);

  //Read the weighted sample
  f_scaled->GetObject("prpTbalb",   prpTbalb_s);
  f_scaled_p->GetObject("prpTbalb",   prpTbalb_s_p);
  f_scaled_m->GetObject("prpTbalb",   prpTbalb_s_m);

	//2D Tprofiles for the errorb calculations
	TProfile2D* pr2DpTbalb;

	TProfile2D* pr2DpTbalb_BSl; 
	TProfile2D* pr2DpTbalb_BTauSl; 
	TProfile2D* pr2DpTbalb_BCSl; 
	TProfile2D* pr2DpTbalb_BCbarSl; 
	TProfile2D* pr2DpTbalb_BnonSl; 

	TProfile2D* pr2DpTbalb_BCSl_BCbarSl; 
	TProfile2D* pr2DpTbalb_BCSl_BSl; 
	TProfile2D* pr2DpTbalb_BCSl_BTauSl; 
	TProfile2D* pr2DpTbalb_BCbarSl_BSl; 
	TProfile2D* pr2DpTbalb_BCbarSl_BTauSl; 

  //The sample with the additional weighting (*_s)
	TProfile2D* pr2DpTbalb_s;

	TProfile2D* pr2DpTbalb_BSl_s; 
	TProfile2D* pr2DpTbalb_BTauSl_s; 
	TProfile2D* pr2DpTbalb_BCSl_s; 
	TProfile2D* pr2DpTbalb_BCbarSl_s; 
	TProfile2D* pr2DpTbalb_BnonSl_s; 

	TProfile2D* pr2DpTbalb_BCSl_BCbarSl_s; 
	TProfile2D* pr2DpTbalb_BCSl_BSl_s; 
	TProfile2D* pr2DpTbalb_BCSl_BTauSl_s; 
	TProfile2D* pr2DpTbalb_BCbarSl_BSl_s; 
	TProfile2D* pr2DpTbalb_BCbarSl_BTauSl_s; 

	TProfile2D* pr2DpTbalb_s_p;

	TProfile2D* pr2DpTbalb_BSl_s_p; 
	TProfile2D* pr2DpTbalb_BTauSl_s_p; 
	TProfile2D* pr2DpTbalb_BCSl_s_p; 
	TProfile2D* pr2DpTbalb_BCbarSl_s_p; 
	TProfile2D* pr2DpTbalb_BnonSl_s_p; 

	TProfile2D* pr2DpTbalb_BCSl_BCbarSl_s_p; 
	TProfile2D* pr2DpTbalb_BCSl_BSl_s_p; 
	TProfile2D* pr2DpTbalb_BCSl_BTauSl_s_p; 
	TProfile2D* pr2DpTbalb_BCbarSl_BSl_s_p; 
	TProfile2D* pr2DpTbalb_BCbarSl_BTauSl_s_p; 

	TProfile2D* pr2DpTbalb_s_m;

	TProfile2D* pr2DpTbalb_BSl_s_m; 
	TProfile2D* pr2DpTbalb_BTauSl_s_m; 
	TProfile2D* pr2DpTbalb_BCSl_s_m; 
	TProfile2D* pr2DpTbalb_BCbarSl_s_m; 
	TProfile2D* pr2DpTbalb_BnonSl_s_m; 

	TProfile2D* pr2DpTbalb_BCSl_BCbarSl_s_m; 
	TProfile2D* pr2DpTbalb_BCSl_BSl_s_m; 
	TProfile2D* pr2DpTbalb_BCSl_BTauSl_s_m; 
	TProfile2D* pr2DpTbalb_BCbarSl_BSl_s_m; 
	TProfile2D* pr2DpTbalb_BCbarSl_BTauSl_s_m; 

	//Tprofiles for the errorb calculations
	TProfile* prpTbalb_BSl; 
	TProfile* prpTbalb_BTauSl; 
	TProfile* prpTbalb_BCSl; 
	TProfile* prpTbalb_BCbarSl; 
	TProfile* prpTbalb_BnonSl; 

	TProfile* prpTbalb_BCSl_BCbarSl; 
	TProfile* prpTbalb_BCSl_BSl; 
	TProfile* prpTbalb_BCSl_BTauSl; 
	TProfile* prpTbalb_BCbarSl_BSl; 
	TProfile* prpTbalb_BCbarSl_BTauSl; 

  //The sample with the additional weighting (*_s)
	TProfile* prpTbalb_BSl_s; 
	TProfile* prpTbalb_BTauSl_s; 
	TProfile* prpTbalb_BCSl_s; 
	TProfile* prpTbalb_BCbarSl_s; 
	TProfile* prpTbalb_BnonSl_s; 

	TProfile* prpTbalb_BCSl_BCbarSl_s; 
	TProfile* prpTbalb_BCSl_BSl_s; 
	TProfile* prpTbalb_BCSl_BTauSl_s; 
	TProfile* prpTbalb_BCbarSl_BSl_s; 
	TProfile* prpTbalb_BCbarSl_BTauSl_s; 

	TProfile* prpTbalb_BSl_s_p; 
	TProfile* prpTbalb_BTauSl_s_p; 
	TProfile* prpTbalb_BCSl_s_p; 
	TProfile* prpTbalb_BCbarSl_s_p; 
	TProfile* prpTbalb_BnonSl_s_p; 

	TProfile* prpTbalb_BCSl_BCbarSl_s_p; 
	TProfile* prpTbalb_BCSl_BSl_s_p; 
	TProfile* prpTbalb_BCSl_BTauSl_s_p; 
	TProfile* prpTbalb_BCbarSl_BSl_s_p; 
	TProfile* prpTbalb_BCbarSl_BTauSl_s_p; 

	TProfile* prpTbalb_BSl_s_m; 
	TProfile* prpTbalb_BTauSl_s_m; 
	TProfile* prpTbalb_BCSl_s_m; 
	TProfile* prpTbalb_BCbarSl_s_m; 
	TProfile* prpTbalb_BnonSl_s_m; 

	TProfile* prpTbalb_BCSl_BCbarSl_s_m; 
	TProfile* prpTbalb_BCSl_BSl_s_m; 
	TProfile* prpTbalb_BCSl_BTauSl_s_m; 
	TProfile* prpTbalb_BCbarSl_BSl_s_m; 
	TProfile* prpTbalb_BCbarSl_BTauSl_s_m; 

  //Read the normal sample
	f_normal->GetObject("pr2DpTbalb",pr2DpTbalb);

	f_normal->GetObject("pr2DpTbalb_BSl",pr2DpTbalb_BSl); 
	f_normal->GetObject("pr2DpTbalb_BTauSl",pr2DpTbalb_BTauSl); 
	f_normal->GetObject("pr2DpTbalb_BCSl",pr2DpTbalb_BCSl); 
	f_normal->GetObject("pr2DpTbalb_BCbarSl",pr2DpTbalb_BCbarSl); 
	f_normal->GetObject("pr2DpTbalb_BnonSl",pr2DpTbalb_BnonSl); 

	f_normal->GetObject("pr2DpTbalb_BCSl_BCbarSl",pr2DpTbalb_BCSl_BCbarSl); 
	f_normal->GetObject("pr2DpTbalb_BCSl_BSl",pr2DpTbalb_BCSl_BSl); 
	f_normal->GetObject("pr2DpTbalb_BCSl_BTauSl",pr2DpTbalb_BCSl_BTauSl); 
	f_normal->GetObject("pr2DpTbalb_BCbarSl_BSl",pr2DpTbalb_BCbarSl_BSl); 
	f_normal->GetObject("pr2DpTbalb_BCbarSl_BTauSl",pr2DpTbalb_BCbarSl_BTauSl);

  //Read the weighted sample
	f_scaled->GetObject("pr2DpTbalb",pr2DpTbalb_s);

	f_scaled->GetObject("pr2DpTbalb_BSl",pr2DpTbalb_BSl_s); 
	f_scaled->GetObject("pr2DpTbalb_BTauSl",pr2DpTbalb_BTauSl_s); 
	f_scaled->GetObject("pr2DpTbalb_BCSl",pr2DpTbalb_BCSl_s); 
	f_scaled->GetObject("pr2DpTbalb_BCbarSl",pr2DpTbalb_BCbarSl_s); 
	f_scaled->GetObject("pr2DpTbalb_BnonSl",pr2DpTbalb_BnonSl_s); 

	f_scaled->GetObject("pr2DpTbalb_BCSl_BCbarSl",pr2DpTbalb_BCSl_BCbarSl_s); 
	f_scaled->GetObject("pr2DpTbalb_BCSl_BSl",pr2DpTbalb_BCSl_BSl_s); 
	f_scaled->GetObject("pr2DpTbalb_BCSl_BTauSl",pr2DpTbalb_BCSl_BTauSl_s); 
	f_scaled->GetObject("pr2DpTbalb_BCbarSl_BSl",pr2DpTbalb_BCbarSl_BSl_s); 
	f_scaled->GetObject("pr2DpTbalb_BCbarSl_BTauSl",pr2DpTbalb_BCbarSl_BTauSl_s);

	f_scaled_p->GetObject("pr2DpTbalb",pr2DpTbalb_s_p);

	f_scaled_p->GetObject("pr2DpTbalb_BSl",pr2DpTbalb_BSl_s_p); 
	f_scaled_p->GetObject("pr2DpTbalb_BTauSl",pr2DpTbalb_BTauSl_s_p); 
	f_scaled_p->GetObject("pr2DpTbalb_BCSl",pr2DpTbalb_BCSl_s_p); 
	f_scaled_p->GetObject("pr2DpTbalb_BCbarSl",pr2DpTbalb_BCbarSl_s_p); 
	f_scaled_p->GetObject("pr2DpTbalb_BnonSl",pr2DpTbalb_BnonSl_s_p); 

	f_scaled_p->GetObject("pr2DpTbalb_BCSl_BCbarSl",pr2DpTbalb_BCSl_BCbarSl_s_p); 
	f_scaled_p->GetObject("pr2DpTbalb_BCSl_BSl",pr2DpTbalb_BCSl_BSl_s_p); 
	f_scaled_p->GetObject("pr2DpTbalb_BCSl_BTauSl",pr2DpTbalb_BCSl_BTauSl_s_p); 
	f_scaled_p->GetObject("pr2DpTbalb_BCbarSl_BSl",pr2DpTbalb_BCbarSl_BSl_s_p); 
	f_scaled_p->GetObject("pr2DpTbalb_BCbarSl_BTauSl",pr2DpTbalb_BCbarSl_BTauSl_s_p);

	f_scaled_m->GetObject("pr2DpTbalb",pr2DpTbalb_s_m);

	f_scaled_m->GetObject("pr2DpTbalb_BSl",pr2DpTbalb_BSl_s_m); 
	f_scaled_m->GetObject("pr2DpTbalb_BTauSl",pr2DpTbalb_BTauSl_s_m); 
	f_scaled_m->GetObject("pr2DpTbalb_BCSl",pr2DpTbalb_BCSl_s_m); 
	f_scaled_m->GetObject("pr2DpTbalb_BCbarSl",pr2DpTbalb_BCbarSl_s_m); 
	f_scaled_m->GetObject("pr2DpTbalb_BnonSl",pr2DpTbalb_BnonSl_s_m); 

	f_scaled_m->GetObject("pr2DpTbalb_BCSl_BCbarSl",pr2DpTbalb_BCSl_BCbarSl_s_m); 
	f_scaled_m->GetObject("pr2DpTbalb_BCSl_BSl",pr2DpTbalb_BCSl_BSl_s_m); 
	f_scaled_m->GetObject("pr2DpTbalb_BCSl_BTauSl",pr2DpTbalb_BCSl_BTauSl_s_m); 
	f_scaled_m->GetObject("pr2DpTbalb_BCbarSl_BSl",pr2DpTbalb_BCbarSl_BSl_s_m); 
	f_scaled_m->GetObject("pr2DpTbalb_BCbarSl_BTauSl",pr2DpTbalb_BCbarSl_BTauSl_s_m);

	//calculate the difference in response between normal and the rescaled sample 
	//the second ProjectionX has new name as argument, otherwise would have same name as the previous
	//histo and causes memory leak 
	TH1D* hpTbalb     = prpTbalb   ->ProjectionX();
	TH1D* hpTbalb_s   = prpTbalb_s ->ProjectionX("pr2DpTbalb_s");
	TH1D* hpTbalb_s_p = prpTbalb_s_p ->ProjectionX("pr2DpTbalb_s_p");
	TH1D* hpTbalb_s_m = prpTbalb_s_m ->ProjectionX("pr2DpTbalb_s_m");

	TH1D* diffb 		= (TH1D*) hpTbalb_s->Clone();
	TH1D* diffb_p 	= (TH1D*) hpTbalb_s_p->Clone();
	TH1D* diffb_m 	= (TH1D*) hpTbalb_s_m->Clone();

	diffb->Add(hpTbalb,-1);
	diffb_p->Add(hpTbalb,-1);
	diffb_m->Add(hpTbalb,-1);

	//evtW_* = event weight, sem_* = standard errorb of the mean,   
	double errorb, evtWtot, evtWtots;

	double evtWtot_BSl;
	double evtWtot_BTauSl;
	double evtWtot_BCSl;
	double evtWtot_BCbarSl;
	double evtWtot_BnonSl;
	double evtWtot_BCSl_BCbarSl;
	double evtWtot_BCSl_BSl;
	double evtWtot_BCSl_BTauSl;
	double evtWtot_BCbarSl_BSl;
	double evtWtot_BCbarSl_BTauSl;

	double evtWtots_BSl;
	double evtWtots_BTauSl;
	double evtWtots_BCSl;
	double evtWtots_BCbarSl;
	double evtWtots_BnonSl;
	double evtWtots_BCSl_BCbarSl;
	double evtWtots_BCSl_BSl;
	double evtWtots_BCSl_BTauSl;
	double evtWtots_BCbarSl_BSl;
	double evtWtots_BCbarSl_BTauSl;

	vector<double> evtW_BSl;
	vector<double> evtW_BTauSl;
	vector<double> evtW_BCSl;
	vector<double> evtW_BCbarSl;
	vector<double> evtW_BnonSl;
	vector<double> evtW_BCSl_BCbarSl;
	vector<double> evtW_BCSl_BSl;
	vector<double> evtW_BCSl_BTauSl;
	vector<double> evtW_BCbarSl_BSl;
	vector<double> evtW_BCbarSl_BTauSl;

	vector<double> evtWs_BSl;
	vector<double> evtWs_BTauSl;
	vector<double> evtWs_BCSl;
	vector<double> evtWs_BCbarSl;
	vector<double> evtWs_BnonSl;
	vector<double> evtWs_BCSl_BCbarSl;
	vector<double> evtWs_BCSl_BSl;
	vector<double> evtWs_BCSl_BTauSl;
	vector<double> evtWs_BCbarSl_BSl;
	vector<double> evtWs_BCbarSl_BTauSl;

	vector<double> sem_BSl;
	vector<double> sem_BTauSl;
	vector<double> sem_BCSl;
	vector<double> sem_BCbarSl;
	vector<double> sem_BnonSl;
	vector<double> sem_BCSl_BCbarSl;
	vector<double> sem_BCSl_BSl;
	vector<double> sem_BCSl_BTauSl;
	vector<double> sem_BCbarSl_BSl;
	vector<double> sem_BCbarSl_BTauSl;

	vector<double> vals_BSl;
	vector<double> vals_BTauSl;
	vector<double> vals_BCSl;
	vector<double> vals_BCbarSl;
	vector<double> vals_BnonSl;
	vector<double> vals_BCSl_BCbarSl;
	vector<double> vals_BCSl_BSl;
	vector<double> vals_BCSl_BTauSl;
	vector<double> vals_BCbarSl_BSl;
	vector<double> vals_BCbarSl_BTauSl;

	vector<double> vec_BSl_stat, vec_BTauSl_stat, vec_BCSl_stat, vec_BCbarSl_stat, vec_BNonSl_stat,
								 vec_BCSl_BCbarSl_stat, vec_BCSl_BSl_stat, vec_BCSl_BTauSl_stat, vec_BCbarSl_BSl_stat, vec_BCbarSl_BTauSl_stat;

	ReadFromFile(vec_BSl_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoSLBr/calc_scale_factors/scale_factors/BtoSlBr/stat_err/BSl_sf_stat.txt");
	ReadFromFile(vec_BTauSl_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoSLBr/calc_scale_factors/scale_factors/BtoSlBr/stat_err/BTauSl_sf_stat.txt");
	ReadFromFile(vec_BCSl_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoSLBr/calc_scale_factors/scale_factors/BtoSlBr/stat_err/BCSl_sf_stat.txt");
	ReadFromFile(vec_BCbarSl_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoSLBr/calc_scale_factors/scale_factors/BtoSlBr/stat_err/BCbarSl_sf_stat.txt");
	ReadFromFile(vec_BNonSl_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoSLBr/calc_scale_factors/scale_factors/BtoSlBr/stat_err/BnonSl_sf_stat.txt");

	//calculating the stat error of the categories with two scaling factors 
	vector<double> vec_BSl_sf, vec_BTauSl_sf, vec_BCSl_sf, vec_BCbarSl_sf, vec_BNonSl_sf;

	ReadFromFile(vec_BSl_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoSLBr/calc_scale_factors/scale_factors/BtoSlBr/normal/BSl_sf.txt");
	ReadFromFile(vec_BTauSl_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoSLBr/calc_scale_factors/scale_factors/BtoSlBr/normal/BTauSl_sf.txt");
	ReadFromFile(vec_BCSl_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoSLBr/calc_scale_factors/scale_factors/BtoSlBr/normal/BCSl_sf.txt");
	ReadFromFile(vec_BCbarSl_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoSLBr/calc_scale_factors/scale_factors/BtoSlBr/normal/BCbarSl_sf.txt");
	ReadFromFile(vec_BNonSl_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoSLBr/calc_scale_factors/scale_factors/BtoSlBr/normal/BnonSl_sf.txt");	

	for (int i = 0; i < vec_BSl_sf.size(); i++) {
		vec_BCSl_BCbarSl_stat.push_back(sqrt(pow(vec_BCbarSl_sf[i]*vec_BCSl_stat[i],2) + pow(vec_BCSl_sf[i]*vec_BCbarSl_stat[i],2)));
		vec_BCSl_BSl_stat.push_back(sqrt(pow(vec_BSl_sf[i]*vec_BCSl_stat[i],2) + pow(vec_BCSl_sf[i]*vec_BSl_stat[i],2)));
		vec_BCSl_BTauSl_stat.push_back(sqrt(pow(vec_BTauSl_sf[i]*vec_BCSl_stat[i],2) + pow(vec_BCSl_sf[i]*vec_BTauSl_stat[i],2)));
		vec_BCbarSl_BSl_stat.push_back(sqrt(pow(vec_BSl_sf[i]*vec_BCbarSl_stat[i],2) + pow(vec_BCbarSl_sf[i]*vec_BSl_stat[i],2)));
		vec_BCbarSl_BTauSl_stat.push_back(sqrt(pow(vec_BTauSl_sf[i]*vec_BCbarSl_stat[i],2) + pow(vec_BCbarSl_sf[i]*vec_BTauSl_stat[i],2)));
	}

	vector<vector<double>> vec_sf_sem{vec_BSl_stat, vec_BTauSl_stat, vec_BCSl_stat, vec_BCbarSl_stat, vec_BNonSl_stat,
							 														   vec_BCSl_BCbarSl_stat, vec_BCSl_BSl_stat, vec_BCSl_BTauSl_stat, vec_BCbarSl_BSl_stat, vec_BCbarSl_BTauSl_stat};

	//removing the under- and overflow bin values
	for (int i = 0; i < vec_sf_sem.size(); i++) {
		vec_sf_sem[i].erase(vec_sf_sem[i].begin() + 0);
		vec_sf_sem[i].erase(vec_sf_sem[i].begin() + vec_sf_sem[i].size() - 1);
	}

  //Loop over the TProfile bins
  for (int i = 1; i < hpTbalb->GetNbinsX()+1; i++){
		evtW_BSl.clear();
		evtW_BTauSl.clear();
		evtW_BCSl.clear();
		evtW_BCbarSl.clear();
		evtW_BnonSl.clear();
		evtW_BCSl_BCbarSl.clear();
		evtW_BCSl_BSl.clear();
		evtW_BCSl_BTauSl.clear();
		evtW_BCbarSl_BSl.clear();
		evtW_BCbarSl_BTauSl.clear();

		evtWs_BSl.clear();
		evtWs_BTauSl.clear();
		evtWs_BCSl.clear();
		evtWs_BCbarSl.clear();
		evtWs_BnonSl.clear();
		evtWs_BCSl_BCbarSl.clear();
		evtWs_BCSl_BSl.clear();
		evtWs_BCSl_BTauSl.clear();
		evtWs_BCbarSl_BSl.clear();
		evtWs_BCbarSl_BTauSl.clear();

		sem_BSl.clear();
		sem_BTauSl.clear();
		sem_BCSl.clear();
		sem_BCbarSl.clear();
		sem_BnonSl.clear();
		sem_BCSl_BCbarSl.clear();
		sem_BCSl_BSl.clear();
		sem_BCSl_BTauSl.clear();
		sem_BCbarSl_BSl.clear();
		sem_BCbarSl_BTauSl.clear();

		vals_BSl.clear();
		vals_BTauSl.clear();
		vals_BCSl.clear();
		vals_BCbarSl.clear();
		vals_BnonSl.clear();
		vals_BCSl_BCbarSl.clear();
		vals_BCSl_BSl.clear();
		vals_BCSl_BTauSl.clear();
		vals_BCbarSl_BSl.clear();
		vals_BCbarSl_BTauSl.clear();

		for (int j = 1; j < hpTbalb->GetNbinsX()+1; j++) {
			string pr_ind = "_" + to_string(i) + "_" + to_string(j);

			prpTbalb_BSl = pr2DpTbalb_BSl->ProfileX(("BSl"+pr_ind).c_str(),j,j);  
			prpTbalb_BTauSl = pr2DpTbalb_BTauSl->ProfileX(("BTauSl"+pr_ind).c_str(),j,j);  
			prpTbalb_BCSl = pr2DpTbalb_BCSl->ProfileX(("BCSl"+pr_ind).c_str(),j,j);  
			prpTbalb_BCbarSl = pr2DpTbalb_BCbarSl->ProfileX(("BCbarSl"+pr_ind).c_str(),j,j);  
			prpTbalb_BnonSl = pr2DpTbalb_BnonSl->ProfileX(("BnonSl"+pr_ind).c_str(),j,j);  
			prpTbalb_BCSl_BCbarSl = pr2DpTbalb_BCSl_BCbarSl->ProfileX(("BCSl_BCbarSl"+pr_ind).c_str(),j,j);  
			prpTbalb_BCSl_BSl = pr2DpTbalb_BCSl_BSl->ProfileX(("BCSl_BSl"+pr_ind).c_str(),j,j);  
			prpTbalb_BCSl_BTauSl = pr2DpTbalb_BCSl_BTauSl->ProfileX(("BCSl_BTauSl"+pr_ind).c_str(),j,j);  
			prpTbalb_BCbarSl_BSl = pr2DpTbalb_BCbarSl_BSl->ProfileX(("BCbarSl_BSl"+pr_ind).c_str(),j,j);  
			prpTbalb_BCbarSl_BTauSl = pr2DpTbalb_BCbarSl_BTauSl->ProfileX(("BCbarSl_BTauSl"+pr_ind).c_str(),j,j);  

			prpTbalb_BSl_s = pr2DpTbalb_BSl_s->ProfileX(("BSl_s"+pr_ind).c_str(),j,j);  
			prpTbalb_BTauSl_s = pr2DpTbalb_BTauSl_s->ProfileX(("BTauSl_s"+pr_ind).c_str(),j,j);  
			prpTbalb_BCSl_s = pr2DpTbalb_BCSl_s->ProfileX(("BCSl_s"+pr_ind).c_str(),j,j);  
			prpTbalb_BCbarSl_s = pr2DpTbalb_BCbarSl_s->ProfileX(("BCbarSl_s"+pr_ind).c_str(),j,j);  
			prpTbalb_BnonSl_s = pr2DpTbalb_BnonSl_s->ProfileX(("BnonSl_s"+pr_ind).c_str(),j,j);  
			prpTbalb_BCSl_BCbarSl_s = pr2DpTbalb_BCSl_BCbarSl_s->ProfileX(("BCSl_BCbarSl_s"+pr_ind).c_str(),j,j);  
			prpTbalb_BCSl_BSl_s = pr2DpTbalb_BCSl_BSl_s->ProfileX(("BCSl_BSl_s"+pr_ind).c_str(),j,j);  
			prpTbalb_BCSl_BTauSl_s = pr2DpTbalb_BCSl_BTauSl_s->ProfileX(("BCSl_BTauSl_s"+pr_ind).c_str(),j,j);  
			prpTbalb_BCbarSl_BSl_s = pr2DpTbalb_BCbarSl_BSl_s->ProfileX(("BCbarSl_BSl_s"+pr_ind).c_str(),j,j);  
			prpTbalb_BCbarSl_BTauSl_s = pr2DpTbalb_BCbarSl_BTauSl_s->ProfileX(("BCbarSl_BTauSl_s"+pr_ind).c_str(),j,j);  

			evtW_BSl.push_back(prpTbalb_BSl->GetBinEntries(i));
			evtW_BTauSl.push_back(prpTbalb_BTauSl->GetBinEntries(i));
			evtW_BCSl.push_back(prpTbalb_BCSl->GetBinEntries(i));
			evtW_BCbarSl.push_back(prpTbalb_BCbarSl->GetBinEntries(i));
			evtW_BnonSl.push_back(prpTbalb_BnonSl->GetBinEntries(i));
			evtW_BCSl_BCbarSl.push_back(prpTbalb_BCSl_BCbarSl->GetBinEntries(i));
			evtW_BCSl_BSl.push_back(prpTbalb_BCSl_BSl->GetBinEntries(i));
			evtW_BCSl_BTauSl.push_back(prpTbalb_BCSl_BTauSl->GetBinEntries(i));
			evtW_BCbarSl_BSl.push_back(prpTbalb_BCbarSl_BSl->GetBinEntries(i));
			evtW_BCbarSl_BTauSl.push_back(prpTbalb_BCbarSl_BTauSl->GetBinEntries(i));

			evtWs_BSl.push_back(prpTbalb_BSl_s->GetBinEntries(i));
			evtWs_BTauSl.push_back(prpTbalb_BTauSl_s->GetBinEntries(i));
			evtWs_BCSl.push_back(prpTbalb_BCSl_s->GetBinEntries(i));
			evtWs_BCbarSl.push_back(prpTbalb_BCbarSl_s->GetBinEntries(i));
			evtWs_BnonSl.push_back(prpTbalb_BnonSl_s->GetBinEntries(i));
			evtWs_BCSl_BCbarSl.push_back(prpTbalb_BCSl_BCbarSl_s->GetBinEntries(i));
			evtWs_BCSl_BSl.push_back(prpTbalb_BCSl_BSl_s->GetBinEntries(i));
			evtWs_BCSl_BTauSl.push_back(prpTbalb_BCSl_BTauSl_s->GetBinEntries(i));
			evtWs_BCbarSl_BSl.push_back(prpTbalb_BCbarSl_BSl_s->GetBinEntries(i));
			evtWs_BCbarSl_BTauSl.push_back(prpTbalb_BCbarSl_BTauSl_s->GetBinEntries(i));

			sem_BSl.push_back(prpTbalb_BSl->GetBinError(i));
			sem_BTauSl.push_back(prpTbalb_BTauSl->GetBinError(i));
			sem_BCSl.push_back(prpTbalb_BCSl->GetBinError(i));
			sem_BCbarSl.push_back(prpTbalb_BCbarSl->GetBinError(i));
			sem_BnonSl.push_back(prpTbalb_BnonSl->GetBinError(i));
			sem_BCSl_BCbarSl.push_back(prpTbalb_BCSl_BCbarSl->GetBinError(i));
			sem_BCSl_BSl.push_back(prpTbalb_BCSl_BSl->GetBinError(i));
			sem_BCSl_BTauSl.push_back(prpTbalb_BCSl_BTauSl->GetBinError(i));
			sem_BCbarSl_BSl.push_back(prpTbalb_BCbarSl_BSl->GetBinError(i));
			sem_BCbarSl_BTauSl.push_back(prpTbalb_BCbarSl_BTauSl->GetBinError(i));

			vals_BSl.push_back(prpTbalb_BSl->GetBinContent(i));
			vals_BTauSl.push_back(prpTbalb_BTauSl->GetBinContent(i));
			vals_BCSl.push_back(prpTbalb_BCSl->GetBinContent(i));
			vals_BCbarSl.push_back(prpTbalb_BCbarSl->GetBinContent(i));
			vals_BnonSl.push_back(prpTbalb_BnonSl->GetBinContent(i));
			vals_BCSl_BCbarSl.push_back(prpTbalb_BCSl_BCbarSl->GetBinContent(i));
			vals_BCSl_BSl.push_back(prpTbalb_BCSl_BSl->GetBinContent(i));
			vals_BCSl_BTauSl.push_back(prpTbalb_BCSl_BTauSl->GetBinContent(i));
			vals_BCbarSl_BSl.push_back(prpTbalb_BCbarSl_BSl->GetBinContent(i));
			vals_BCbarSl_BTauSl.push_back(prpTbalb_BCbarSl_BTauSl->GetBinContent(i));

			delete prpTbalb_BSl;
			delete prpTbalb_BTauSl;
			delete prpTbalb_BCSl;
			delete prpTbalb_BCbarSl;
			delete prpTbalb_BnonSl;
			delete prpTbalb_BCSl_BCbarSl;
			delete prpTbalb_BCSl_BSl;
			delete prpTbalb_BCSl_BTauSl;
			delete prpTbalb_BCbarSl_BSl;
			delete prpTbalb_BCbarSl_BTauSl;

			delete prpTbalb_BSl_s;
			delete prpTbalb_BTauSl_s;
			delete prpTbalb_BCSl_s;
			delete prpTbalb_BCbarSl_s;
			delete prpTbalb_BnonSl_s;
			delete prpTbalb_BCSl_BCbarSl_s;
			delete prpTbalb_BCSl_BSl_s;
			delete prpTbalb_BCSl_BTauSl_s;
			delete prpTbalb_BCbarSl_BSl_s;
			delete prpTbalb_BCbarSl_BTauSl_s;
		}

		 
		vector<vector<double>> vec_evtW{evtW_BSl,evtW_BTauSl,evtW_BCSl,evtW_BCbarSl,evtW_BnonSl,evtW_BCSl_BCbarSl,
																		evtW_BCSl_BSl,evtW_BCSl_BTauSl,evtW_BCbarSl_BSl,evtW_BCbarSl_BTauSl};

		vector<vector<double>> vec_evtWs{evtWs_BSl,evtWs_BTauSl,evtWs_BCSl,evtWs_BCbarSl,evtWs_BnonSl,evtWs_BCSl_BCbarSl,
																		evtWs_BCSl_BSl,evtWs_BCSl_BTauSl,evtWs_BCbarSl_BSl,evtWs_BCbarSl_BTauSl};

		vector<vector<double>> vec_sem{sem_BSl,sem_BTauSl,sem_BCSl,sem_BCbarSl,sem_BnonSl,sem_BCSl_BCbarSl,
																		sem_BCSl_BSl,sem_BCSl_BTauSl,sem_BCbarSl_BSl,sem_BCbarSl_BTauSl};

		vector<vector<double>> vec_vals{vals_BSl,vals_BTauSl,vals_BCSl,vals_BCbarSl,vals_BnonSl,vals_BCSl_BCbarSl,
																		vals_BCSl_BSl,vals_BCSl_BTauSl,vals_BCbarSl_BSl,vals_BCbarSl_BTauSl};

		//set small number so that we dont divide by zero 
		evtWtot_BSl = 1e-10;
		evtWtot_BTauSl = 1e-10;
		evtWtot_BCSl = 1e-10;
		evtWtot_BCbarSl = 1e-10;
		evtWtot_BnonSl = 1e-10;
		evtWtot_BCSl_BCbarSl = 1e-10;
		evtWtot_BCSl_BSl = 1e-10;
		evtWtot_BCSl_BTauSl = 1e-10;
		evtWtot_BCbarSl_BSl = 1e-10;
		evtWtot_BCbarSl_BTauSl = 1e-10;

		evtWtots_BSl = 1e-10;
		evtWtots_BTauSl = 1e-10;
		evtWtots_BCSl = 1e-10;
		evtWtots_BCbarSl = 1e-10;
		evtWtots_BnonSl = 1e-10;
		evtWtots_BCSl_BCbarSl = 1e-10;
		evtWtots_BCSl_BSl = 1e-10;
		evtWtots_BCSl_BTauSl = 1e-10;
		evtWtots_BCbarSl_BSl = 1e-10;
		evtWtots_BCbarSl_BTauSl = 1e-10;

		for (double x : evtW_BSl) evtWtot_BSl += x;
		for (double x : evtW_BTauSl) evtWtot_BTauSl += x;
		for (double x : evtW_BCSl) evtWtot_BCSl += x;
		for (double x : evtW_BCbarSl) evtWtot_BCbarSl += x;
		for (double x : evtW_BnonSl) evtWtot_BnonSl += x;
		for (double x : evtW_BCSl_BCbarSl) evtWtot_BCSl_BCbarSl += x;
		for (double x : evtW_BCSl_BSl) evtWtot_BCSl_BSl += x;
		for (double x : evtW_BCSl_BTauSl) evtWtot_BCSl_BTauSl += x;
		for (double x : evtW_BCbarSl_BSl) evtWtot_BCbarSl_BSl += x;
		for (double x : evtW_BCbarSl_BTauSl) evtWtot_BCbarSl_BTauSl += x;

		for (double x : evtWs_BSl) evtWtots_BSl += x;
		for (double x : evtWs_BTauSl) evtWtots_BTauSl += x;
		for (double x : evtWs_BCSl) evtWtots_BCSl += x;
		for (double x : evtWs_BCbarSl) evtWtots_BCbarSl += x;
		for (double x : evtWs_BnonSl) evtWtots_BnonSl += x;
		for (double x : evtWs_BCSl_BCbarSl) evtWtots_BCSl_BCbarSl += x;
		for (double x : evtWs_BCSl_BSl) evtWtots_BCSl_BSl += x;
		for (double x : evtWs_BCSl_BTauSl) evtWtots_BCSl_BTauSl += x;
		for (double x : evtWs_BCbarSl_BSl) evtWtots_BCbarSl_BSl += x;
		for (double x : evtWs_BCbarSl_BTauSl) evtWtots_BCbarSl_BTauSl += x;
		
		evtWtot = evtWtot_BSl + evtWtot_BTauSl + evtWtot_BCSl + evtWtot_BCbarSl + 
							evtWtot_BnonSl + evtWtot_BCSl_BCbarSl + evtWtot_BCSl_BSl +
							evtWtot_BCSl_BTauSl + evtWtot_BCbarSl_BSl + evtWtot_BCbarSl_BTauSl;

		evtWtots = evtWtots_BSl + evtWtots_BTauSl + evtWtots_BCSl + evtWtots_BCbarSl + 
							 evtWtots_BnonSl + evtWtots_BCSl_BCbarSl + evtWtots_BCSl_BSl +
							 evtWtots_BCSl_BTauSl + evtWtots_BCbarSl_BSl + evtWtots_BCbarSl_BTauSl;
		
		//loop over diff scale factors 
		errorb = 0;
		for (int j = 0; j < hpTbalb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hpTbalb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}
		
		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb->GetBinContent(i) == 0) diffb->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb->SetBinError(i,errorb);
  }

	//############### plus variation ##################
  //Loop over the TProfile bins
  for (int i = 1; i < hpTbalb->GetNbinsX()+1; i++){
		evtW_BSl.clear();
		evtW_BTauSl.clear();
		evtW_BCSl.clear();
		evtW_BCbarSl.clear();
		evtW_BnonSl.clear();
		evtW_BCSl_BCbarSl.clear();
		evtW_BCSl_BSl.clear();
		evtW_BCSl_BTauSl.clear();
		evtW_BCbarSl_BSl.clear();
		evtW_BCbarSl_BTauSl.clear();

		evtWs_BSl.clear();
		evtWs_BTauSl.clear();
		evtWs_BCSl.clear();
		evtWs_BCbarSl.clear();
		evtWs_BnonSl.clear();
		evtWs_BCSl_BCbarSl.clear();
		evtWs_BCSl_BSl.clear();
		evtWs_BCSl_BTauSl.clear();
		evtWs_BCbarSl_BSl.clear();
		evtWs_BCbarSl_BTauSl.clear();

		sem_BSl.clear();
		sem_BTauSl.clear();
		sem_BCSl.clear();
		sem_BCbarSl.clear();
		sem_BnonSl.clear();
		sem_BCSl_BCbarSl.clear();
		sem_BCSl_BSl.clear();
		sem_BCSl_BTauSl.clear();
		sem_BCbarSl_BSl.clear();
		sem_BCbarSl_BTauSl.clear();

		vals_BSl.clear();
		vals_BTauSl.clear();
		vals_BCSl.clear();
		vals_BCbarSl.clear();
		vals_BnonSl.clear();
		vals_BCSl_BCbarSl.clear();
		vals_BCSl_BSl.clear();
		vals_BCSl_BTauSl.clear();
		vals_BCbarSl_BSl.clear();
		vals_BCbarSl_BTauSl.clear();

		for (int j = 1; j < hpTbalb->GetNbinsX()+1; j++) {
			string pr_ind = "_" + to_string(i) + "_" + to_string(j);

			prpTbalb_BSl = pr2DpTbalb_BSl->ProfileX(("BSl"+pr_ind).c_str(),j,j);  
			prpTbalb_BTauSl = pr2DpTbalb_BTauSl->ProfileX(("BTauSl"+pr_ind).c_str(),j,j);  
			prpTbalb_BCSl = pr2DpTbalb_BCSl->ProfileX(("BCSl"+pr_ind).c_str(),j,j);  
			prpTbalb_BCbarSl = pr2DpTbalb_BCbarSl->ProfileX(("BCbarSl"+pr_ind).c_str(),j,j);  
			prpTbalb_BnonSl = pr2DpTbalb_BnonSl->ProfileX(("BnonSl"+pr_ind).c_str(),j,j);  
			prpTbalb_BCSl_BCbarSl = pr2DpTbalb_BCSl_BCbarSl->ProfileX(("BCSl_BCbarSl"+pr_ind).c_str(),j,j);  
			prpTbalb_BCSl_BSl = pr2DpTbalb_BCSl_BSl->ProfileX(("BCSl_BSl"+pr_ind).c_str(),j,j);  
			prpTbalb_BCSl_BTauSl = pr2DpTbalb_BCSl_BTauSl->ProfileX(("BCSl_BTauSl"+pr_ind).c_str(),j,j);  
			prpTbalb_BCbarSl_BSl = pr2DpTbalb_BCbarSl_BSl->ProfileX(("BCbarSl_BSl"+pr_ind).c_str(),j,j);  
			prpTbalb_BCbarSl_BTauSl = pr2DpTbalb_BCbarSl_BTauSl->ProfileX(("BCbarSl_BTauSl"+pr_ind).c_str(),j,j);  

			prpTbalb_BSl_s_p = pr2DpTbalb_BSl_s_p->ProfileX(("BSl_s"+pr_ind).c_str(),j,j);  
			prpTbalb_BTauSl_s_p = pr2DpTbalb_BTauSl_s_p->ProfileX(("BTauSl_s"+pr_ind).c_str(),j,j);  
			prpTbalb_BCSl_s_p = pr2DpTbalb_BCSl_s_p->ProfileX(("BCSl_s"+pr_ind).c_str(),j,j);  
			prpTbalb_BCbarSl_s_p = pr2DpTbalb_BCbarSl_s_p->ProfileX(("BCbarSl_s"+pr_ind).c_str(),j,j);  
			prpTbalb_BnonSl_s_p = pr2DpTbalb_BnonSl_s_p->ProfileX(("BnonSl_s"+pr_ind).c_str(),j,j);  
			prpTbalb_BCSl_BCbarSl_s_p = pr2DpTbalb_BCSl_BCbarSl_s_p->ProfileX(("BCSl_BCbarSl_s"+pr_ind).c_str(),j,j);  
			prpTbalb_BCSl_BSl_s_p = pr2DpTbalb_BCSl_BSl_s_p->ProfileX(("BCSl_BSl_s"+pr_ind).c_str(),j,j);  
			prpTbalb_BCSl_BTauSl_s_p = pr2DpTbalb_BCSl_BTauSl_s_p->ProfileX(("BCSl_BTauSl_s"+pr_ind).c_str(),j,j);  
			prpTbalb_BCbarSl_BSl_s_p = pr2DpTbalb_BCbarSl_BSl_s_p->ProfileX(("BCbarSl_BSl_s"+pr_ind).c_str(),j,j);  
			prpTbalb_BCbarSl_BTauSl_s_p = pr2DpTbalb_BCbarSl_BTauSl_s_p->ProfileX(("BCbarSl_BTauSl_s"+pr_ind).c_str(),j,j);  

			evtW_BSl.push_back(prpTbalb_BSl->GetBinEntries(i));
			evtW_BTauSl.push_back(prpTbalb_BTauSl->GetBinEntries(i));
			evtW_BCSl.push_back(prpTbalb_BCSl->GetBinEntries(i));
			evtW_BCbarSl.push_back(prpTbalb_BCbarSl->GetBinEntries(i));
			evtW_BnonSl.push_back(prpTbalb_BnonSl->GetBinEntries(i));
			evtW_BCSl_BCbarSl.push_back(prpTbalb_BCSl_BCbarSl->GetBinEntries(i));
			evtW_BCSl_BSl.push_back(prpTbalb_BCSl_BSl->GetBinEntries(i));
			evtW_BCSl_BTauSl.push_back(prpTbalb_BCSl_BTauSl->GetBinEntries(i));
			evtW_BCbarSl_BSl.push_back(prpTbalb_BCbarSl_BSl->GetBinEntries(i));
			evtW_BCbarSl_BTauSl.push_back(prpTbalb_BCbarSl_BTauSl->GetBinEntries(i));

			evtWs_BSl.push_back(prpTbalb_BSl_s_p->GetBinEntries(i));
			evtWs_BTauSl.push_back(prpTbalb_BTauSl_s_p->GetBinEntries(i));
			evtWs_BCSl.push_back(prpTbalb_BCSl_s_p->GetBinEntries(i));
			evtWs_BCbarSl.push_back(prpTbalb_BCbarSl_s_p->GetBinEntries(i));
			evtWs_BnonSl.push_back(prpTbalb_BnonSl_s_p->GetBinEntries(i));
			evtWs_BCSl_BCbarSl.push_back(prpTbalb_BCSl_BCbarSl_s_p->GetBinEntries(i));
			evtWs_BCSl_BSl.push_back(prpTbalb_BCSl_BSl_s_p->GetBinEntries(i));
			evtWs_BCSl_BTauSl.push_back(prpTbalb_BCSl_BTauSl_s_p->GetBinEntries(i));
			evtWs_BCbarSl_BSl.push_back(prpTbalb_BCbarSl_BSl_s_p->GetBinEntries(i));
			evtWs_BCbarSl_BTauSl.push_back(prpTbalb_BCbarSl_BTauSl_s_p->GetBinEntries(i));

			sem_BSl.push_back(prpTbalb_BSl->GetBinError(i));
			sem_BTauSl.push_back(prpTbalb_BTauSl->GetBinError(i));
			sem_BCSl.push_back(prpTbalb_BCSl->GetBinError(i));
			sem_BCbarSl.push_back(prpTbalb_BCbarSl->GetBinError(i));
			sem_BnonSl.push_back(prpTbalb_BnonSl->GetBinError(i));
			sem_BCSl_BCbarSl.push_back(prpTbalb_BCSl_BCbarSl->GetBinError(i));
			sem_BCSl_BSl.push_back(prpTbalb_BCSl_BSl->GetBinError(i));
			sem_BCSl_BTauSl.push_back(prpTbalb_BCSl_BTauSl->GetBinError(i));
			sem_BCbarSl_BSl.push_back(prpTbalb_BCbarSl_BSl->GetBinError(i));
			sem_BCbarSl_BTauSl.push_back(prpTbalb_BCbarSl_BTauSl->GetBinError(i));

			vals_BSl.push_back(prpTbalb_BSl->GetBinContent(i));
			vals_BTauSl.push_back(prpTbalb_BTauSl->GetBinContent(i));
			vals_BCSl.push_back(prpTbalb_BCSl->GetBinContent(i));
			vals_BCbarSl.push_back(prpTbalb_BCbarSl->GetBinContent(i));
			vals_BnonSl.push_back(prpTbalb_BnonSl->GetBinContent(i));
			vals_BCSl_BCbarSl.push_back(prpTbalb_BCSl_BCbarSl->GetBinContent(i));
			vals_BCSl_BSl.push_back(prpTbalb_BCSl_BSl->GetBinContent(i));
			vals_BCSl_BTauSl.push_back(prpTbalb_BCSl_BTauSl->GetBinContent(i));
			vals_BCbarSl_BSl.push_back(prpTbalb_BCbarSl_BSl->GetBinContent(i));
			vals_BCbarSl_BTauSl.push_back(prpTbalb_BCbarSl_BTauSl->GetBinContent(i));

			delete prpTbalb_BSl;
			delete prpTbalb_BTauSl;
			delete prpTbalb_BCSl;
			delete prpTbalb_BCbarSl;
			delete prpTbalb_BnonSl;
			delete prpTbalb_BCSl_BCbarSl;
			delete prpTbalb_BCSl_BSl;
			delete prpTbalb_BCSl_BTauSl;
			delete prpTbalb_BCbarSl_BSl;
			delete prpTbalb_BCbarSl_BTauSl;

			delete prpTbalb_BSl_s_p;
			delete prpTbalb_BTauSl_s_p;
			delete prpTbalb_BCSl_s_p;
			delete prpTbalb_BCbarSl_s_p;
			delete prpTbalb_BnonSl_s_p;
			delete prpTbalb_BCSl_BCbarSl_s_p;
			delete prpTbalb_BCSl_BSl_s_p;
			delete prpTbalb_BCSl_BTauSl_s_p;
			delete prpTbalb_BCbarSl_BSl_s_p;
			delete prpTbalb_BCbarSl_BTauSl_s_p;
		}

		 
		vector<vector<double>> vec_evtW{evtW_BSl,evtW_BTauSl,evtW_BCSl,evtW_BCbarSl,evtW_BnonSl,evtW_BCSl_BCbarSl,
																		evtW_BCSl_BSl,evtW_BCSl_BTauSl,evtW_BCbarSl_BSl,evtW_BCbarSl_BTauSl};

		vector<vector<double>> vec_evtWs{evtWs_BSl,evtWs_BTauSl,evtWs_BCSl,evtWs_BCbarSl,evtWs_BnonSl,evtWs_BCSl_BCbarSl,
																		evtWs_BCSl_BSl,evtWs_BCSl_BTauSl,evtWs_BCbarSl_BSl,evtWs_BCbarSl_BTauSl};

		vector<vector<double>> vec_sem{sem_BSl,sem_BTauSl,sem_BCSl,sem_BCbarSl,sem_BnonSl,sem_BCSl_BCbarSl,
																		sem_BCSl_BSl,sem_BCSl_BTauSl,sem_BCbarSl_BSl,sem_BCbarSl_BTauSl};

		vector<vector<double>> vec_vals{vals_BSl,vals_BTauSl,vals_BCSl,vals_BCbarSl,vals_BnonSl,vals_BCSl_BCbarSl,
																		vals_BCSl_BSl,vals_BCSl_BTauSl,vals_BCbarSl_BSl,vals_BCbarSl_BTauSl};

		//set small number so that we dont divide by zero 
		evtWtot_BSl = 1e-10;
		evtWtot_BTauSl = 1e-10;
		evtWtot_BCSl = 1e-10;
		evtWtot_BCbarSl = 1e-10;
		evtWtot_BnonSl = 1e-10;
		evtWtot_BCSl_BCbarSl = 1e-10;
		evtWtot_BCSl_BSl = 1e-10;
		evtWtot_BCSl_BTauSl = 1e-10;
		evtWtot_BCbarSl_BSl = 1e-10;
		evtWtot_BCbarSl_BTauSl = 1e-10;

		evtWtots_BSl = 1e-10;
		evtWtots_BTauSl = 1e-10;
		evtWtots_BCSl = 1e-10;
		evtWtots_BCbarSl = 1e-10;
		evtWtots_BnonSl = 1e-10;
		evtWtots_BCSl_BCbarSl = 1e-10;
		evtWtots_BCSl_BSl = 1e-10;
		evtWtots_BCSl_BTauSl = 1e-10;
		evtWtots_BCbarSl_BSl = 1e-10;
		evtWtots_BCbarSl_BTauSl = 1e-10;

		for (double x : evtW_BSl) evtWtot_BSl += x;
		for (double x : evtW_BTauSl) evtWtot_BTauSl += x;
		for (double x : evtW_BCSl) evtWtot_BCSl += x;
		for (double x : evtW_BCbarSl) evtWtot_BCbarSl += x;
		for (double x : evtW_BnonSl) evtWtot_BnonSl += x;
		for (double x : evtW_BCSl_BCbarSl) evtWtot_BCSl_BCbarSl += x;
		for (double x : evtW_BCSl_BSl) evtWtot_BCSl_BSl += x;
		for (double x : evtW_BCSl_BTauSl) evtWtot_BCSl_BTauSl += x;
		for (double x : evtW_BCbarSl_BSl) evtWtot_BCbarSl_BSl += x;
		for (double x : evtW_BCbarSl_BTauSl) evtWtot_BCbarSl_BTauSl += x;

		for (double x : evtWs_BSl) evtWtots_BSl += x;
		for (double x : evtWs_BTauSl) evtWtots_BTauSl += x;
		for (double x : evtWs_BCSl) evtWtots_BCSl += x;
		for (double x : evtWs_BCbarSl) evtWtots_BCbarSl += x;
		for (double x : evtWs_BnonSl) evtWtots_BnonSl += x;
		for (double x : evtWs_BCSl_BCbarSl) evtWtots_BCSl_BCbarSl += x;
		for (double x : evtWs_BCSl_BSl) evtWtots_BCSl_BSl += x;
		for (double x : evtWs_BCSl_BTauSl) evtWtots_BCSl_BTauSl += x;
		for (double x : evtWs_BCbarSl_BSl) evtWtots_BCbarSl_BSl += x;
		for (double x : evtWs_BCbarSl_BTauSl) evtWtots_BCbarSl_BTauSl += x;
		
		evtWtot = evtWtot_BSl + evtWtot_BTauSl + evtWtot_BCSl + evtWtot_BCbarSl + 
							evtWtot_BnonSl + evtWtot_BCSl_BCbarSl + evtWtot_BCSl_BSl +
							evtWtot_BCSl_BTauSl + evtWtot_BCbarSl_BSl + evtWtot_BCbarSl_BTauSl;

		evtWtots = evtWtots_BSl + evtWtots_BTauSl + evtWtots_BCSl + evtWtots_BCbarSl + 
							 evtWtots_BnonSl + evtWtots_BCSl_BCbarSl + evtWtots_BCSl_BSl +
							 evtWtots_BCSl_BTauSl + evtWtots_BCbarSl_BSl + evtWtots_BCbarSl_BTauSl;
		
		//loop over diff scale factors 
		errorb = 0;
		for (int j = 0; j < hpTbalb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hpTbalb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}
		
		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb_p->GetBinContent(i) == 0) diffb_p->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb_p->SetBinError(i,errorb);
  }

	//############### minus variation ####################
  //Loop over the TProfile bins
  for (int i = 1; i < hpTbalb->GetNbinsX()+1; i++){
		evtW_BSl.clear();
		evtW_BTauSl.clear();
		evtW_BCSl.clear();
		evtW_BCbarSl.clear();
		evtW_BnonSl.clear();
		evtW_BCSl_BCbarSl.clear();
		evtW_BCSl_BSl.clear();
		evtW_BCSl_BTauSl.clear();
		evtW_BCbarSl_BSl.clear();
		evtW_BCbarSl_BTauSl.clear();

		evtWs_BSl.clear();
		evtWs_BTauSl.clear();
		evtWs_BCSl.clear();
		evtWs_BCbarSl.clear();
		evtWs_BnonSl.clear();
		evtWs_BCSl_BCbarSl.clear();
		evtWs_BCSl_BSl.clear();
		evtWs_BCSl_BTauSl.clear();
		evtWs_BCbarSl_BSl.clear();
		evtWs_BCbarSl_BTauSl.clear();

		sem_BSl.clear();
		sem_BTauSl.clear();
		sem_BCSl.clear();
		sem_BCbarSl.clear();
		sem_BnonSl.clear();
		sem_BCSl_BCbarSl.clear();
		sem_BCSl_BSl.clear();
		sem_BCSl_BTauSl.clear();
		sem_BCbarSl_BSl.clear();
		sem_BCbarSl_BTauSl.clear();

		vals_BSl.clear();
		vals_BTauSl.clear();
		vals_BCSl.clear();
		vals_BCbarSl.clear();
		vals_BnonSl.clear();
		vals_BCSl_BCbarSl.clear();
		vals_BCSl_BSl.clear();
		vals_BCSl_BTauSl.clear();
		vals_BCbarSl_BSl.clear();
		vals_BCbarSl_BTauSl.clear();

		for (int j = 1; j < hpTbalb->GetNbinsX()+1; j++) {
			string pr_ind = "_" + to_string(i) + "_" + to_string(j);

			prpTbalb_BSl = pr2DpTbalb_BSl->ProfileX(("BSl"+pr_ind).c_str(),j,j);  
			prpTbalb_BTauSl = pr2DpTbalb_BTauSl->ProfileX(("BTauSl"+pr_ind).c_str(),j,j);  
			prpTbalb_BCSl = pr2DpTbalb_BCSl->ProfileX(("BCSl"+pr_ind).c_str(),j,j);  
			prpTbalb_BCbarSl = pr2DpTbalb_BCbarSl->ProfileX(("BCbarSl"+pr_ind).c_str(),j,j);  
			prpTbalb_BnonSl = pr2DpTbalb_BnonSl->ProfileX(("BnonSl"+pr_ind).c_str(),j,j);  
			prpTbalb_BCSl_BCbarSl = pr2DpTbalb_BCSl_BCbarSl->ProfileX(("BCSl_BCbarSl"+pr_ind).c_str(),j,j);  
			prpTbalb_BCSl_BSl = pr2DpTbalb_BCSl_BSl->ProfileX(("BCSl_BSl"+pr_ind).c_str(),j,j);  
			prpTbalb_BCSl_BTauSl = pr2DpTbalb_BCSl_BTauSl->ProfileX(("BCSl_BTauSl"+pr_ind).c_str(),j,j);  
			prpTbalb_BCbarSl_BSl = pr2DpTbalb_BCbarSl_BSl->ProfileX(("BCbarSl_BSl"+pr_ind).c_str(),j,j);  
			prpTbalb_BCbarSl_BTauSl = pr2DpTbalb_BCbarSl_BTauSl->ProfileX(("BCbarSl_BTauSl"+pr_ind).c_str(),j,j);  

			prpTbalb_BSl_s_m = pr2DpTbalb_BSl_s_m->ProfileX(("BSl_s"+pr_ind).c_str(),j,j);  
			prpTbalb_BTauSl_s_m = pr2DpTbalb_BTauSl_s_m->ProfileX(("BTauSl_s"+pr_ind).c_str(),j,j);  
			prpTbalb_BCSl_s_m = pr2DpTbalb_BCSl_s_m->ProfileX(("BCSl_s"+pr_ind).c_str(),j,j);  
			prpTbalb_BCbarSl_s_m = pr2DpTbalb_BCbarSl_s_m->ProfileX(("BCbarSl_s"+pr_ind).c_str(),j,j);  
			prpTbalb_BnonSl_s_m = pr2DpTbalb_BnonSl_s_m->ProfileX(("BnonSl_s"+pr_ind).c_str(),j,j);  
			prpTbalb_BCSl_BCbarSl_s_m = pr2DpTbalb_BCSl_BCbarSl_s_m->ProfileX(("BCSl_BCbarSl_s"+pr_ind).c_str(),j,j);  
			prpTbalb_BCSl_BSl_s_m = pr2DpTbalb_BCSl_BSl_s_m->ProfileX(("BCSl_BSl_s"+pr_ind).c_str(),j,j);  
			prpTbalb_BCSl_BTauSl_s_m = pr2DpTbalb_BCSl_BTauSl_s_m->ProfileX(("BCSl_BTauSl_s"+pr_ind).c_str(),j,j);  
			prpTbalb_BCbarSl_BSl_s_m = pr2DpTbalb_BCbarSl_BSl_s_m->ProfileX(("BCbarSl_BSl_s"+pr_ind).c_str(),j,j);  
			prpTbalb_BCbarSl_BTauSl_s_m = pr2DpTbalb_BCbarSl_BTauSl_s_m->ProfileX(("BCbarSl_BTauSl_s"+pr_ind).c_str(),j,j); 

			evtW_BSl.push_back(prpTbalb_BSl->GetBinEntries(i));
			evtW_BTauSl.push_back(prpTbalb_BTauSl->GetBinEntries(i));
			evtW_BCSl.push_back(prpTbalb_BCSl->GetBinEntries(i));
			evtW_BCbarSl.push_back(prpTbalb_BCbarSl->GetBinEntries(i));
			evtW_BnonSl.push_back(prpTbalb_BnonSl->GetBinEntries(i));
			evtW_BCSl_BCbarSl.push_back(prpTbalb_BCSl_BCbarSl->GetBinEntries(i));
			evtW_BCSl_BSl.push_back(prpTbalb_BCSl_BSl->GetBinEntries(i));
			evtW_BCSl_BTauSl.push_back(prpTbalb_BCSl_BTauSl->GetBinEntries(i));
			evtW_BCbarSl_BSl.push_back(prpTbalb_BCbarSl_BSl->GetBinEntries(i));
			evtW_BCbarSl_BTauSl.push_back(prpTbalb_BCbarSl_BTauSl->GetBinEntries(i));

			evtWs_BSl.push_back(prpTbalb_BSl_s_m->GetBinEntries(i));
			evtWs_BTauSl.push_back(prpTbalb_BTauSl_s_m->GetBinEntries(i));
			evtWs_BCSl.push_back(prpTbalb_BCSl_s_m->GetBinEntries(i));
			evtWs_BCbarSl.push_back(prpTbalb_BCbarSl_s_m->GetBinEntries(i));
			evtWs_BnonSl.push_back(prpTbalb_BnonSl_s_m->GetBinEntries(i));
			evtWs_BCSl_BCbarSl.push_back(prpTbalb_BCSl_BCbarSl_s_m->GetBinEntries(i));
			evtWs_BCSl_BSl.push_back(prpTbalb_BCSl_BSl_s_m->GetBinEntries(i));
			evtWs_BCSl_BTauSl.push_back(prpTbalb_BCSl_BTauSl_s_m->GetBinEntries(i));
			evtWs_BCbarSl_BSl.push_back(prpTbalb_BCbarSl_BSl_s_m->GetBinEntries(i));
			evtWs_BCbarSl_BTauSl.push_back(prpTbalb_BCbarSl_BTauSl_s_m->GetBinEntries(i));

			sem_BSl.push_back(prpTbalb_BSl->GetBinError(i));
			sem_BTauSl.push_back(prpTbalb_BTauSl->GetBinError(i));
			sem_BCSl.push_back(prpTbalb_BCSl->GetBinError(i));
			sem_BCbarSl.push_back(prpTbalb_BCbarSl->GetBinError(i));
			sem_BnonSl.push_back(prpTbalb_BnonSl->GetBinError(i));
			sem_BCSl_BCbarSl.push_back(prpTbalb_BCSl_BCbarSl->GetBinError(i));
			sem_BCSl_BSl.push_back(prpTbalb_BCSl_BSl->GetBinError(i));
			sem_BCSl_BTauSl.push_back(prpTbalb_BCSl_BTauSl->GetBinError(i));
			sem_BCbarSl_BSl.push_back(prpTbalb_BCbarSl_BSl->GetBinError(i));
			sem_BCbarSl_BTauSl.push_back(prpTbalb_BCbarSl_BTauSl->GetBinError(i));

			vals_BSl.push_back(prpTbalb_BSl->GetBinContent(i));
			vals_BTauSl.push_back(prpTbalb_BTauSl->GetBinContent(i));
			vals_BCSl.push_back(prpTbalb_BCSl->GetBinContent(i));
			vals_BCbarSl.push_back(prpTbalb_BCbarSl->GetBinContent(i));
			vals_BnonSl.push_back(prpTbalb_BnonSl->GetBinContent(i));
			vals_BCSl_BCbarSl.push_back(prpTbalb_BCSl_BCbarSl->GetBinContent(i));
			vals_BCSl_BSl.push_back(prpTbalb_BCSl_BSl->GetBinContent(i));
			vals_BCSl_BTauSl.push_back(prpTbalb_BCSl_BTauSl->GetBinContent(i));
			vals_BCbarSl_BSl.push_back(prpTbalb_BCbarSl_BSl->GetBinContent(i));
			vals_BCbarSl_BTauSl.push_back(prpTbalb_BCbarSl_BTauSl->GetBinContent(i));

			delete prpTbalb_BSl;
			delete prpTbalb_BTauSl;
			delete prpTbalb_BCSl;
			delete prpTbalb_BCbarSl;
			delete prpTbalb_BnonSl;
			delete prpTbalb_BCSl_BCbarSl;
			delete prpTbalb_BCSl_BSl;
			delete prpTbalb_BCSl_BTauSl;
			delete prpTbalb_BCbarSl_BSl;
			delete prpTbalb_BCbarSl_BTauSl;

			delete prpTbalb_BSl_s_m;
			delete prpTbalb_BTauSl_s_m;
			delete prpTbalb_BCSl_s_m;
			delete prpTbalb_BCbarSl_s_m;
			delete prpTbalb_BnonSl_s_m;
			delete prpTbalb_BCSl_BCbarSl_s_m;
			delete prpTbalb_BCSl_BSl_s_m;
			delete prpTbalb_BCSl_BTauSl_s_m;
			delete prpTbalb_BCbarSl_BSl_s_m;
			delete prpTbalb_BCbarSl_BTauSl_s_m;
		}

		 
		vector<vector<double>> vec_evtW{evtW_BSl,evtW_BTauSl,evtW_BCSl,evtW_BCbarSl,evtW_BnonSl,evtW_BCSl_BCbarSl,
																		evtW_BCSl_BSl,evtW_BCSl_BTauSl,evtW_BCbarSl_BSl,evtW_BCbarSl_BTauSl};

		vector<vector<double>> vec_evtWs{evtWs_BSl,evtWs_BTauSl,evtWs_BCSl,evtWs_BCbarSl,evtWs_BnonSl,evtWs_BCSl_BCbarSl,
																		evtWs_BCSl_BSl,evtWs_BCSl_BTauSl,evtWs_BCbarSl_BSl,evtWs_BCbarSl_BTauSl};

		vector<vector<double>> vec_sem{sem_BSl,sem_BTauSl,sem_BCSl,sem_BCbarSl,sem_BnonSl,sem_BCSl_BCbarSl,
																		sem_BCSl_BSl,sem_BCSl_BTauSl,sem_BCbarSl_BSl,sem_BCbarSl_BTauSl};

		vector<vector<double>> vec_vals{vals_BSl,vals_BTauSl,vals_BCSl,vals_BCbarSl,vals_BnonSl,vals_BCSl_BCbarSl,
																		vals_BCSl_BSl,vals_BCSl_BTauSl,vals_BCbarSl_BSl,vals_BCbarSl_BTauSl};

		//set small number so that we dont divide by zero 
		evtWtot_BSl = 1e-10;
		evtWtot_BTauSl = 1e-10;
		evtWtot_BCSl = 1e-10;
		evtWtot_BCbarSl = 1e-10;
		evtWtot_BnonSl = 1e-10;
		evtWtot_BCSl_BCbarSl = 1e-10;
		evtWtot_BCSl_BSl = 1e-10;
		evtWtot_BCSl_BTauSl = 1e-10;
		evtWtot_BCbarSl_BSl = 1e-10;
		evtWtot_BCbarSl_BTauSl = 1e-10;

		evtWtots_BSl = 1e-10;
		evtWtots_BTauSl = 1e-10;
		evtWtots_BCSl = 1e-10;
		evtWtots_BCbarSl = 1e-10;
		evtWtots_BnonSl = 1e-10;
		evtWtots_BCSl_BCbarSl = 1e-10;
		evtWtots_BCSl_BSl = 1e-10;
		evtWtots_BCSl_BTauSl = 1e-10;
		evtWtots_BCbarSl_BSl = 1e-10;
		evtWtots_BCbarSl_BTauSl = 1e-10;

		for (double x : evtW_BSl) evtWtot_BSl += x;
		for (double x : evtW_BTauSl) evtWtot_BTauSl += x;
		for (double x : evtW_BCSl) evtWtot_BCSl += x;
		for (double x : evtW_BCbarSl) evtWtot_BCbarSl += x;
		for (double x : evtW_BnonSl) evtWtot_BnonSl += x;
		for (double x : evtW_BCSl_BCbarSl) evtWtot_BCSl_BCbarSl += x;
		for (double x : evtW_BCSl_BSl) evtWtot_BCSl_BSl += x;
		for (double x : evtW_BCSl_BTauSl) evtWtot_BCSl_BTauSl += x;
		for (double x : evtW_BCbarSl_BSl) evtWtot_BCbarSl_BSl += x;
		for (double x : evtW_BCbarSl_BTauSl) evtWtot_BCbarSl_BTauSl += x;

		for (double x : evtWs_BSl) evtWtots_BSl += x;
		for (double x : evtWs_BTauSl) evtWtots_BTauSl += x;
		for (double x : evtWs_BCSl) evtWtots_BCSl += x;
		for (double x : evtWs_BCbarSl) evtWtots_BCbarSl += x;
		for (double x : evtWs_BnonSl) evtWtots_BnonSl += x;
		for (double x : evtWs_BCSl_BCbarSl) evtWtots_BCSl_BCbarSl += x;
		for (double x : evtWs_BCSl_BSl) evtWtots_BCSl_BSl += x;
		for (double x : evtWs_BCSl_BTauSl) evtWtots_BCSl_BTauSl += x;
		for (double x : evtWs_BCbarSl_BSl) evtWtots_BCbarSl_BSl += x;
		for (double x : evtWs_BCbarSl_BTauSl) evtWtots_BCbarSl_BTauSl += x;
		
		evtWtot = evtWtot_BSl + evtWtot_BTauSl + evtWtot_BCSl + evtWtot_BCbarSl + 
							evtWtot_BnonSl + evtWtot_BCSl_BCbarSl + evtWtot_BCSl_BSl +
							evtWtot_BCSl_BTauSl + evtWtot_BCbarSl_BSl + evtWtot_BCbarSl_BTauSl;

		evtWtots = evtWtots_BSl + evtWtots_BTauSl + evtWtots_BCSl + evtWtots_BCbarSl + 
							 evtWtots_BnonSl + evtWtots_BCSl_BCbarSl + evtWtots_BCSl_BSl +
							 evtWtots_BCSl_BTauSl + evtWtots_BCbarSl_BSl + evtWtots_BCbarSl_BTauSl;
		
		//loop over diff scale factors 
		errorb = 0;
		for (int j = 0; j < hpTbalb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hpTbalb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}
		
		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb_m->GetBinContent(i) == 0) diffb_m->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb_m->SetBinError(i,errorb);
  }

	//Scale to the percentages
  diffb->Scale(100.0);
  diffb_p->Scale(100.0);
  diffb_m->Scale(100.0);

	pTbalb_map["diffb"] = diffb;
	pTbalb_map["diffb_p"] = diffb_p;
	pTbalb_map["diffb_m"] = diffb_m;

	return pTbalb_map;
}

map<string, TH1D*> Rjet_BtoSLBr () {
	map <string, TH1D*> Rjetb_map;

  TFile* f_normal = TFile::Open(OpenName.c_str());
  TFile* f_scaled = TFile::Open(OpenName_BtoSLBr.c_str());
  TFile* f_scaled_p = TFile::Open(OpenName_BtoSLBr_p.c_str());
  TFile* f_scaled_m = TFile::Open(OpenName_BtoSLBr_m.c_str());

	TProfile *prRjetb;
	TProfile *prRjetb_s;
	TProfile *prRjetb_s_p;
	TProfile *prRjetb_s_m;

  //Read the normal sample
  f_normal->GetObject("prRjetb",   prRjetb);

  //Read the weighted sample
  f_scaled->GetObject("prRjetb",   prRjetb_s);
  f_scaled_p->GetObject("prRjetb",   prRjetb_s_p);
  f_scaled_m->GetObject("prRjetb",   prRjetb_s_m);

	//2D Tprofiles for the errorb calculations
	TProfile2D* pr2DRjetb;

	TProfile2D* pr2DRjetb_BSl; 
	TProfile2D* pr2DRjetb_BTauSl; 
	TProfile2D* pr2DRjetb_BCSl; 
	TProfile2D* pr2DRjetb_BCbarSl; 
	TProfile2D* pr2DRjetb_BnonSl; 

	TProfile2D* pr2DRjetb_BCSl_BCbarSl; 
	TProfile2D* pr2DRjetb_BCSl_BSl; 
	TProfile2D* pr2DRjetb_BCSl_BTauSl; 
	TProfile2D* pr2DRjetb_BCbarSl_BSl; 
	TProfile2D* pr2DRjetb_BCbarSl_BTauSl; 

  //The sample with the additional weighting (*_s)
	TProfile2D* pr2DRjetb_s;

	TProfile2D* pr2DRjetb_BSl_s; 
	TProfile2D* pr2DRjetb_BTauSl_s; 
	TProfile2D* pr2DRjetb_BCSl_s; 
	TProfile2D* pr2DRjetb_BCbarSl_s; 
	TProfile2D* pr2DRjetb_BnonSl_s; 

	TProfile2D* pr2DRjetb_BCSl_BCbarSl_s; 
	TProfile2D* pr2DRjetb_BCSl_BSl_s; 
	TProfile2D* pr2DRjetb_BCSl_BTauSl_s; 
	TProfile2D* pr2DRjetb_BCbarSl_BSl_s; 
	TProfile2D* pr2DRjetb_BCbarSl_BTauSl_s; 

	TProfile2D* pr2DRjetb_s_p;

	TProfile2D* pr2DRjetb_BSl_s_p; 
	TProfile2D* pr2DRjetb_BTauSl_s_p; 
	TProfile2D* pr2DRjetb_BCSl_s_p; 
	TProfile2D* pr2DRjetb_BCbarSl_s_p; 
	TProfile2D* pr2DRjetb_BnonSl_s_p; 

	TProfile2D* pr2DRjetb_BCSl_BCbarSl_s_p; 
	TProfile2D* pr2DRjetb_BCSl_BSl_s_p; 
	TProfile2D* pr2DRjetb_BCSl_BTauSl_s_p; 
	TProfile2D* pr2DRjetb_BCbarSl_BSl_s_p; 
	TProfile2D* pr2DRjetb_BCbarSl_BTauSl_s_p; 

	TProfile2D* pr2DRjetb_s_m;

	TProfile2D* pr2DRjetb_BSl_s_m; 
	TProfile2D* pr2DRjetb_BTauSl_s_m; 
	TProfile2D* pr2DRjetb_BCSl_s_m; 
	TProfile2D* pr2DRjetb_BCbarSl_s_m; 
	TProfile2D* pr2DRjetb_BnonSl_s_m; 

	TProfile2D* pr2DRjetb_BCSl_BCbarSl_s_m; 
	TProfile2D* pr2DRjetb_BCSl_BSl_s_m; 
	TProfile2D* pr2DRjetb_BCSl_BTauSl_s_m; 
	TProfile2D* pr2DRjetb_BCbarSl_BSl_s_m; 
	TProfile2D* pr2DRjetb_BCbarSl_BTauSl_s_m; 

	//Tprofiles for the errorb calculations
	TProfile* prRjetb_BSl; 
	TProfile* prRjetb_BTauSl; 
	TProfile* prRjetb_BCSl; 
	TProfile* prRjetb_BCbarSl; 
	TProfile* prRjetb_BnonSl; 

	TProfile* prRjetb_BCSl_BCbarSl; 
	TProfile* prRjetb_BCSl_BSl; 
	TProfile* prRjetb_BCSl_BTauSl; 
	TProfile* prRjetb_BCbarSl_BSl; 
	TProfile* prRjetb_BCbarSl_BTauSl; 

  //The sample with the additional weighting (*_s)
	TProfile* prRjetb_BSl_s; 
	TProfile* prRjetb_BTauSl_s; 
	TProfile* prRjetb_BCSl_s; 
	TProfile* prRjetb_BCbarSl_s; 
	TProfile* prRjetb_BnonSl_s; 

	TProfile* prRjetb_BCSl_BCbarSl_s; 
	TProfile* prRjetb_BCSl_BSl_s; 
	TProfile* prRjetb_BCSl_BTauSl_s; 
	TProfile* prRjetb_BCbarSl_BSl_s; 
	TProfile* prRjetb_BCbarSl_BTauSl_s; 

	TProfile* prRjetb_BSl_s_p; 
	TProfile* prRjetb_BTauSl_s_p; 
	TProfile* prRjetb_BCSl_s_p; 
	TProfile* prRjetb_BCbarSl_s_p; 
	TProfile* prRjetb_BnonSl_s_p; 

	TProfile* prRjetb_BCSl_BCbarSl_s_p; 
	TProfile* prRjetb_BCSl_BSl_s_p; 
	TProfile* prRjetb_BCSl_BTauSl_s_p; 
	TProfile* prRjetb_BCbarSl_BSl_s_p; 
	TProfile* prRjetb_BCbarSl_BTauSl_s_p; 

	TProfile* prRjetb_BSl_s_m; 
	TProfile* prRjetb_BTauSl_s_m; 
	TProfile* prRjetb_BCSl_s_m; 
	TProfile* prRjetb_BCbarSl_s_m; 
	TProfile* prRjetb_BnonSl_s_m; 

	TProfile* prRjetb_BCSl_BCbarSl_s_m; 
	TProfile* prRjetb_BCSl_BSl_s_m; 
	TProfile* prRjetb_BCSl_BTauSl_s_m; 
	TProfile* prRjetb_BCbarSl_BSl_s_m; 
	TProfile* prRjetb_BCbarSl_BTauSl_s_m; 

  //Read the normal sample
	f_normal->GetObject("pr2DRjetb",pr2DRjetb);

	f_normal->GetObject("pr2DRjetb_BSl",pr2DRjetb_BSl); 
	f_normal->GetObject("pr2DRjetb_BTauSl",pr2DRjetb_BTauSl); 
	f_normal->GetObject("pr2DRjetb_BCSl",pr2DRjetb_BCSl); 
	f_normal->GetObject("pr2DRjetb_BCbarSl",pr2DRjetb_BCbarSl); 
	f_normal->GetObject("pr2DRjetb_BnonSl",pr2DRjetb_BnonSl); 

	f_normal->GetObject("pr2DRjetb_BCSl_BCbarSl",pr2DRjetb_BCSl_BCbarSl); 
	f_normal->GetObject("pr2DRjetb_BCSl_BSl",pr2DRjetb_BCSl_BSl); 
	f_normal->GetObject("pr2DRjetb_BCSl_BTauSl",pr2DRjetb_BCSl_BTauSl); 
	f_normal->GetObject("pr2DRjetb_BCbarSl_BSl",pr2DRjetb_BCbarSl_BSl); 
	f_normal->GetObject("pr2DRjetb_BCbarSl_BTauSl",pr2DRjetb_BCbarSl_BTauSl);

  //Read the weighted sample
	f_scaled->GetObject("pr2DRjetb",pr2DRjetb_s);

	f_scaled->GetObject("pr2DRjetb_BSl",pr2DRjetb_BSl_s); 
	f_scaled->GetObject("pr2DRjetb_BTauSl",pr2DRjetb_BTauSl_s); 
	f_scaled->GetObject("pr2DRjetb_BCSl",pr2DRjetb_BCSl_s); 
	f_scaled->GetObject("pr2DRjetb_BCbarSl",pr2DRjetb_BCbarSl_s); 
	f_scaled->GetObject("pr2DRjetb_BnonSl",pr2DRjetb_BnonSl_s); 

	f_scaled->GetObject("pr2DRjetb_BCSl_BCbarSl",pr2DRjetb_BCSl_BCbarSl_s); 
	f_scaled->GetObject("pr2DRjetb_BCSl_BSl",pr2DRjetb_BCSl_BSl_s); 
	f_scaled->GetObject("pr2DRjetb_BCSl_BTauSl",pr2DRjetb_BCSl_BTauSl_s); 
	f_scaled->GetObject("pr2DRjetb_BCbarSl_BSl",pr2DRjetb_BCbarSl_BSl_s); 
	f_scaled->GetObject("pr2DRjetb_BCbarSl_BTauSl",pr2DRjetb_BCbarSl_BTauSl_s);

	f_scaled_p->GetObject("pr2DRjetb",pr2DRjetb_s_p);

	f_scaled_p->GetObject("pr2DRjetb_BSl",pr2DRjetb_BSl_s_p); 
	f_scaled_p->GetObject("pr2DRjetb_BTauSl",pr2DRjetb_BTauSl_s_p); 
	f_scaled_p->GetObject("pr2DRjetb_BCSl",pr2DRjetb_BCSl_s_p); 
	f_scaled_p->GetObject("pr2DRjetb_BCbarSl",pr2DRjetb_BCbarSl_s_p); 
	f_scaled_p->GetObject("pr2DRjetb_BnonSl",pr2DRjetb_BnonSl_s_p); 

	f_scaled_p->GetObject("pr2DRjetb_BCSl_BCbarSl",pr2DRjetb_BCSl_BCbarSl_s_p); 
	f_scaled_p->GetObject("pr2DRjetb_BCSl_BSl",pr2DRjetb_BCSl_BSl_s_p); 
	f_scaled_p->GetObject("pr2DRjetb_BCSl_BTauSl",pr2DRjetb_BCSl_BTauSl_s_p); 
	f_scaled_p->GetObject("pr2DRjetb_BCbarSl_BSl",pr2DRjetb_BCbarSl_BSl_s_p); 
	f_scaled_p->GetObject("pr2DRjetb_BCbarSl_BTauSl",pr2DRjetb_BCbarSl_BTauSl_s_p);

	f_scaled_m->GetObject("pr2DRjetb",pr2DRjetb_s_m);

	f_scaled_m->GetObject("pr2DRjetb_BSl",pr2DRjetb_BSl_s_m); 
	f_scaled_m->GetObject("pr2DRjetb_BTauSl",pr2DRjetb_BTauSl_s_m); 
	f_scaled_m->GetObject("pr2DRjetb_BCSl",pr2DRjetb_BCSl_s_m); 
	f_scaled_m->GetObject("pr2DRjetb_BCbarSl",pr2DRjetb_BCbarSl_s_m); 
	f_scaled_m->GetObject("pr2DRjetb_BnonSl",pr2DRjetb_BnonSl_s_m); 

	f_scaled_m->GetObject("pr2DRjetb_BCSl_BCbarSl",pr2DRjetb_BCSl_BCbarSl_s_m); 
	f_scaled_m->GetObject("pr2DRjetb_BCSl_BSl",pr2DRjetb_BCSl_BSl_s_m); 
	f_scaled_m->GetObject("pr2DRjetb_BCSl_BTauSl",pr2DRjetb_BCSl_BTauSl_s_m); 
	f_scaled_m->GetObject("pr2DRjetb_BCbarSl_BSl",pr2DRjetb_BCbarSl_BSl_s_m); 
	f_scaled_m->GetObject("pr2DRjetb_BCbarSl_BTauSl",pr2DRjetb_BCbarSl_BTauSl_s_m);

	//calculate the difference in response between normal and the rescaled sample 
	//the second ProjectionX has new name as argument, otherwise would have same name as the previous
	//histo and causes memory leak 
	TH1D* hRjetb     = prRjetb   ->ProjectionX();
	TH1D* hRjetb_s   = prRjetb_s ->ProjectionX("pr2DRjetb_s");
	TH1D* hRjetb_s_p = prRjetb_s_p ->ProjectionX("pr2DRjetb_s_p");
	TH1D* hRjetb_s_m = prRjetb_s_m ->ProjectionX("pr2DRjetb_s_m");

	TH1D* diffb 		= (TH1D*) hRjetb_s->Clone();
	TH1D* diffb_p 	= (TH1D*) hRjetb_s_p->Clone();
	TH1D* diffb_m 	= (TH1D*) hRjetb_s_m->Clone();

	diffb->Add(hRjetb,-1);
	diffb_p->Add(hRjetb,-1);
	diffb_m->Add(hRjetb,-1);

	//evtW_* = event weight, sem_* = standard errorb of the mean,   
	double errorb, evtWtot, evtWtots;

	double evtWtot_BSl;
	double evtWtot_BTauSl;
	double evtWtot_BCSl;
	double evtWtot_BCbarSl;
	double evtWtot_BnonSl;
	double evtWtot_BCSl_BCbarSl;
	double evtWtot_BCSl_BSl;
	double evtWtot_BCSl_BTauSl;
	double evtWtot_BCbarSl_BSl;
	double evtWtot_BCbarSl_BTauSl;

	double evtWtots_BSl;
	double evtWtots_BTauSl;
	double evtWtots_BCSl;
	double evtWtots_BCbarSl;
	double evtWtots_BnonSl;
	double evtWtots_BCSl_BCbarSl;
	double evtWtots_BCSl_BSl;
	double evtWtots_BCSl_BTauSl;
	double evtWtots_BCbarSl_BSl;
	double evtWtots_BCbarSl_BTauSl;

	vector<double> evtW_BSl;
	vector<double> evtW_BTauSl;
	vector<double> evtW_BCSl;
	vector<double> evtW_BCbarSl;
	vector<double> evtW_BnonSl;
	vector<double> evtW_BCSl_BCbarSl;
	vector<double> evtW_BCSl_BSl;
	vector<double> evtW_BCSl_BTauSl;
	vector<double> evtW_BCbarSl_BSl;
	vector<double> evtW_BCbarSl_BTauSl;

	vector<double> evtWs_BSl;
	vector<double> evtWs_BTauSl;
	vector<double> evtWs_BCSl;
	vector<double> evtWs_BCbarSl;
	vector<double> evtWs_BnonSl;
	vector<double> evtWs_BCSl_BCbarSl;
	vector<double> evtWs_BCSl_BSl;
	vector<double> evtWs_BCSl_BTauSl;
	vector<double> evtWs_BCbarSl_BSl;
	vector<double> evtWs_BCbarSl_BTauSl;

	vector<double> sem_BSl;
	vector<double> sem_BTauSl;
	vector<double> sem_BCSl;
	vector<double> sem_BCbarSl;
	vector<double> sem_BnonSl;
	vector<double> sem_BCSl_BCbarSl;
	vector<double> sem_BCSl_BSl;
	vector<double> sem_BCSl_BTauSl;
	vector<double> sem_BCbarSl_BSl;
	vector<double> sem_BCbarSl_BTauSl;

	vector<double> vals_BSl;
	vector<double> vals_BTauSl;
	vector<double> vals_BCSl;
	vector<double> vals_BCbarSl;
	vector<double> vals_BnonSl;
	vector<double> vals_BCSl_BCbarSl;
	vector<double> vals_BCSl_BSl;
	vector<double> vals_BCSl_BTauSl;
	vector<double> vals_BCbarSl_BSl;
	vector<double> vals_BCbarSl_BTauSl;

	vector<double> vec_BSl_stat, vec_BTauSl_stat, vec_BCSl_stat, vec_BCbarSl_stat, vec_BNonSl_stat,
								 vec_BCSl_BCbarSl_stat, vec_BCSl_BSl_stat, vec_BCSl_BTauSl_stat, vec_BCbarSl_BSl_stat, vec_BCbarSl_BTauSl_stat;

	ReadFromFile(vec_BSl_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoSLBr/calc_scale_factors/scale_factors/BtoSlBr/stat_err/BSl_sf_stat.txt");
	ReadFromFile(vec_BTauSl_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoSLBr/calc_scale_factors/scale_factors/BtoSlBr/stat_err/BTauSl_sf_stat.txt");
	ReadFromFile(vec_BCSl_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoSLBr/calc_scale_factors/scale_factors/BtoSlBr/stat_err/BCSl_sf_stat.txt");
	ReadFromFile(vec_BCbarSl_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoSLBr/calc_scale_factors/scale_factors/BtoSlBr/stat_err/BCbarSl_sf_stat.txt");
	ReadFromFile(vec_BNonSl_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoSLBr/calc_scale_factors/scale_factors/BtoSlBr/stat_err/BnonSl_sf_stat.txt");

	//calculating the stat error of the categories with two scaling factors 
	vector<double> vec_BSl_sf, vec_BTauSl_sf, vec_BCSl_sf, vec_BCbarSl_sf, vec_BNonSl_sf;

	ReadFromFile(vec_BSl_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoSLBr/calc_scale_factors/scale_factors/BtoSlBr/normal/BSl_sf.txt");
	ReadFromFile(vec_BTauSl_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoSLBr/calc_scale_factors/scale_factors/BtoSlBr/normal/BTauSl_sf.txt");
	ReadFromFile(vec_BCSl_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoSLBr/calc_scale_factors/scale_factors/BtoSlBr/normal/BCSl_sf.txt");
	ReadFromFile(vec_BCbarSl_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoSLBr/calc_scale_factors/scale_factors/BtoSlBr/normal/BCbarSl_sf.txt");
	ReadFromFile(vec_BNonSl_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoSLBr/calc_scale_factors/scale_factors/BtoSlBr/normal/BnonSl_sf.txt");	

	for (int i = 0; i < vec_BSl_sf.size(); i++) {
		vec_BCSl_BCbarSl_stat.push_back(sqrt(pow(vec_BCbarSl_sf[i]*vec_BCSl_stat[i],2) + pow(vec_BCSl_sf[i]*vec_BCbarSl_stat[i],2)));
		vec_BCSl_BSl_stat.push_back(sqrt(pow(vec_BSl_sf[i]*vec_BCSl_stat[i],2) + pow(vec_BCSl_sf[i]*vec_BSl_stat[i],2)));
		vec_BCSl_BTauSl_stat.push_back(sqrt(pow(vec_BTauSl_sf[i]*vec_BCSl_stat[i],2) + pow(vec_BCSl_sf[i]*vec_BTauSl_stat[i],2)));
		vec_BCbarSl_BSl_stat.push_back(sqrt(pow(vec_BSl_sf[i]*vec_BCbarSl_stat[i],2) + pow(vec_BCbarSl_sf[i]*vec_BSl_stat[i],2)));
		vec_BCbarSl_BTauSl_stat.push_back(sqrt(pow(vec_BTauSl_sf[i]*vec_BCbarSl_stat[i],2) + pow(vec_BCbarSl_sf[i]*vec_BTauSl_stat[i],2)));
	}

	vector<vector<double>> vec_sf_sem{vec_BSl_stat, vec_BTauSl_stat, vec_BCSl_stat, vec_BCbarSl_stat, vec_BNonSl_stat,
							 														   vec_BCSl_BCbarSl_stat, vec_BCSl_BSl_stat, vec_BCSl_BTauSl_stat, vec_BCbarSl_BSl_stat, vec_BCbarSl_BTauSl_stat};

	//removing the under- and overflow bin values
	for (int i = 0; i < vec_sf_sem.size(); i++) {
		vec_sf_sem[i].erase(vec_sf_sem[i].begin() + 0);
		vec_sf_sem[i].erase(vec_sf_sem[i].begin() + vec_sf_sem[i].size() - 1);
	}


  //Loop over the TProfile bins
  for (int i = 1; i < hRjetb->GetNbinsX()+1; i++){
		evtW_BSl.clear();
		evtW_BTauSl.clear();
		evtW_BCSl.clear();
		evtW_BCbarSl.clear();
		evtW_BnonSl.clear();
		evtW_BCSl_BCbarSl.clear();
		evtW_BCSl_BSl.clear();
		evtW_BCSl_BTauSl.clear();
		evtW_BCbarSl_BSl.clear();
		evtW_BCbarSl_BTauSl.clear();

		evtWs_BSl.clear();
		evtWs_BTauSl.clear();
		evtWs_BCSl.clear();
		evtWs_BCbarSl.clear();
		evtWs_BnonSl.clear();
		evtWs_BCSl_BCbarSl.clear();
		evtWs_BCSl_BSl.clear();
		evtWs_BCSl_BTauSl.clear();
		evtWs_BCbarSl_BSl.clear();
		evtWs_BCbarSl_BTauSl.clear();

		sem_BSl.clear();
		sem_BTauSl.clear();
		sem_BCSl.clear();
		sem_BCbarSl.clear();
		sem_BnonSl.clear();
		sem_BCSl_BCbarSl.clear();
		sem_BCSl_BSl.clear();
		sem_BCSl_BTauSl.clear();
		sem_BCbarSl_BSl.clear();
		sem_BCbarSl_BTauSl.clear();

		vals_BSl.clear();
		vals_BTauSl.clear();
		vals_BCSl.clear();
		vals_BCbarSl.clear();
		vals_BnonSl.clear();
		vals_BCSl_BCbarSl.clear();
		vals_BCSl_BSl.clear();
		vals_BCSl_BTauSl.clear();
		vals_BCbarSl_BSl.clear();
		vals_BCbarSl_BTauSl.clear();

		for (int j = 1; j < hRjetb->GetNbinsX()+1; j++) {
			string pr_ind = "_" + to_string(i) + "_" + to_string(j);

			prRjetb_BSl = pr2DRjetb_BSl->ProfileX(("BSl"+pr_ind).c_str(),j,j);  
			prRjetb_BTauSl = pr2DRjetb_BTauSl->ProfileX(("BTauSl"+pr_ind).c_str(),j,j);  
			prRjetb_BCSl = pr2DRjetb_BCSl->ProfileX(("BCSl"+pr_ind).c_str(),j,j);  
			prRjetb_BCbarSl = pr2DRjetb_BCbarSl->ProfileX(("BCbarSl"+pr_ind).c_str(),j,j);  
			prRjetb_BnonSl = pr2DRjetb_BnonSl->ProfileX(("BnonSl"+pr_ind).c_str(),j,j);  
			prRjetb_BCSl_BCbarSl = pr2DRjetb_BCSl_BCbarSl->ProfileX(("BCSl_BCbarSl"+pr_ind).c_str(),j,j);  
			prRjetb_BCSl_BSl = pr2DRjetb_BCSl_BSl->ProfileX(("BCSl_BSl"+pr_ind).c_str(),j,j);  
			prRjetb_BCSl_BTauSl = pr2DRjetb_BCSl_BTauSl->ProfileX(("BCSl_BTauSl"+pr_ind).c_str(),j,j);  
			prRjetb_BCbarSl_BSl = pr2DRjetb_BCbarSl_BSl->ProfileX(("BCbarSl_BSl"+pr_ind).c_str(),j,j);  
			prRjetb_BCbarSl_BTauSl = pr2DRjetb_BCbarSl_BTauSl->ProfileX(("BCbarSl_BTauSl"+pr_ind).c_str(),j,j);  

			prRjetb_BSl_s = pr2DRjetb_BSl_s->ProfileX(("BSl_s"+pr_ind).c_str(),j,j);  
			prRjetb_BTauSl_s = pr2DRjetb_BTauSl_s->ProfileX(("BTauSl_s"+pr_ind).c_str(),j,j);  
			prRjetb_BCSl_s = pr2DRjetb_BCSl_s->ProfileX(("BCSl_s"+pr_ind).c_str(),j,j);  
			prRjetb_BCbarSl_s = pr2DRjetb_BCbarSl_s->ProfileX(("BCbarSl_s"+pr_ind).c_str(),j,j);  
			prRjetb_BnonSl_s = pr2DRjetb_BnonSl_s->ProfileX(("BnonSl_s"+pr_ind).c_str(),j,j);  
			prRjetb_BCSl_BCbarSl_s = pr2DRjetb_BCSl_BCbarSl_s->ProfileX(("BCSl_BCbarSl_s"+pr_ind).c_str(),j,j);  
			prRjetb_BCSl_BSl_s = pr2DRjetb_BCSl_BSl_s->ProfileX(("BCSl_BSl_s"+pr_ind).c_str(),j,j);  
			prRjetb_BCSl_BTauSl_s = pr2DRjetb_BCSl_BTauSl_s->ProfileX(("BCSl_BTauSl_s"+pr_ind).c_str(),j,j);  
			prRjetb_BCbarSl_BSl_s = pr2DRjetb_BCbarSl_BSl_s->ProfileX(("BCbarSl_BSl_s"+pr_ind).c_str(),j,j);  
			prRjetb_BCbarSl_BTauSl_s = pr2DRjetb_BCbarSl_BTauSl_s->ProfileX(("BCbarSl_BTauSl_s"+pr_ind).c_str(),j,j);  

			evtW_BSl.push_back(prRjetb_BSl->GetBinEntries(i));
			evtW_BTauSl.push_back(prRjetb_BTauSl->GetBinEntries(i));
			evtW_BCSl.push_back(prRjetb_BCSl->GetBinEntries(i));
			evtW_BCbarSl.push_back(prRjetb_BCbarSl->GetBinEntries(i));
			evtW_BnonSl.push_back(prRjetb_BnonSl->GetBinEntries(i));
			evtW_BCSl_BCbarSl.push_back(prRjetb_BCSl_BCbarSl->GetBinEntries(i));
			evtW_BCSl_BSl.push_back(prRjetb_BCSl_BSl->GetBinEntries(i));
			evtW_BCSl_BTauSl.push_back(prRjetb_BCSl_BTauSl->GetBinEntries(i));
			evtW_BCbarSl_BSl.push_back(prRjetb_BCbarSl_BSl->GetBinEntries(i));
			evtW_BCbarSl_BTauSl.push_back(prRjetb_BCbarSl_BTauSl->GetBinEntries(i));

			evtWs_BSl.push_back(prRjetb_BSl_s->GetBinEntries(i));
			evtWs_BTauSl.push_back(prRjetb_BTauSl_s->GetBinEntries(i));
			evtWs_BCSl.push_back(prRjetb_BCSl_s->GetBinEntries(i));
			evtWs_BCbarSl.push_back(prRjetb_BCbarSl_s->GetBinEntries(i));
			evtWs_BnonSl.push_back(prRjetb_BnonSl_s->GetBinEntries(i));
			evtWs_BCSl_BCbarSl.push_back(prRjetb_BCSl_BCbarSl_s->GetBinEntries(i));
			evtWs_BCSl_BSl.push_back(prRjetb_BCSl_BSl_s->GetBinEntries(i));
			evtWs_BCSl_BTauSl.push_back(prRjetb_BCSl_BTauSl_s->GetBinEntries(i));
			evtWs_BCbarSl_BSl.push_back(prRjetb_BCbarSl_BSl_s->GetBinEntries(i));
			evtWs_BCbarSl_BTauSl.push_back(prRjetb_BCbarSl_BTauSl_s->GetBinEntries(i));

			sem_BSl.push_back(prRjetb_BSl->GetBinError(i));
			sem_BTauSl.push_back(prRjetb_BTauSl->GetBinError(i));
			sem_BCSl.push_back(prRjetb_BCSl->GetBinError(i));
			sem_BCbarSl.push_back(prRjetb_BCbarSl->GetBinError(i));
			sem_BnonSl.push_back(prRjetb_BnonSl->GetBinError(i));
			sem_BCSl_BCbarSl.push_back(prRjetb_BCSl_BCbarSl->GetBinError(i));
			sem_BCSl_BSl.push_back(prRjetb_BCSl_BSl->GetBinError(i));
			sem_BCSl_BTauSl.push_back(prRjetb_BCSl_BTauSl->GetBinError(i));
			sem_BCbarSl_BSl.push_back(prRjetb_BCbarSl_BSl->GetBinError(i));
			sem_BCbarSl_BTauSl.push_back(prRjetb_BCbarSl_BTauSl->GetBinError(i));

			vals_BSl.push_back(prRjetb_BSl->GetBinContent(i));
			vals_BTauSl.push_back(prRjetb_BTauSl->GetBinContent(i));
			vals_BCSl.push_back(prRjetb_BCSl->GetBinContent(i));
			vals_BCbarSl.push_back(prRjetb_BCbarSl->GetBinContent(i));
			vals_BnonSl.push_back(prRjetb_BnonSl->GetBinContent(i));
			vals_BCSl_BCbarSl.push_back(prRjetb_BCSl_BCbarSl->GetBinContent(i));
			vals_BCSl_BSl.push_back(prRjetb_BCSl_BSl->GetBinContent(i));
			vals_BCSl_BTauSl.push_back(prRjetb_BCSl_BTauSl->GetBinContent(i));
			vals_BCbarSl_BSl.push_back(prRjetb_BCbarSl_BSl->GetBinContent(i));
			vals_BCbarSl_BTauSl.push_back(prRjetb_BCbarSl_BTauSl->GetBinContent(i));

			delete prRjetb_BSl;
			delete prRjetb_BTauSl;
			delete prRjetb_BCSl;
			delete prRjetb_BCbarSl;
			delete prRjetb_BnonSl;
			delete prRjetb_BCSl_BCbarSl;
			delete prRjetb_BCSl_BSl;
			delete prRjetb_BCSl_BTauSl;
			delete prRjetb_BCbarSl_BSl;
			delete prRjetb_BCbarSl_BTauSl;

			delete prRjetb_BSl_s;
			delete prRjetb_BTauSl_s;
			delete prRjetb_BCSl_s;
			delete prRjetb_BCbarSl_s;
			delete prRjetb_BnonSl_s;
			delete prRjetb_BCSl_BCbarSl_s;
			delete prRjetb_BCSl_BSl_s;
			delete prRjetb_BCSl_BTauSl_s;
			delete prRjetb_BCbarSl_BSl_s;
			delete prRjetb_BCbarSl_BTauSl_s;
		}

		 
		vector<vector<double>> vec_evtW{evtW_BSl,evtW_BTauSl,evtW_BCSl,evtW_BCbarSl,evtW_BnonSl,evtW_BCSl_BCbarSl,
																		evtW_BCSl_BSl,evtW_BCSl_BTauSl,evtW_BCbarSl_BSl,evtW_BCbarSl_BTauSl};

		vector<vector<double>> vec_evtWs{evtWs_BSl,evtWs_BTauSl,evtWs_BCSl,evtWs_BCbarSl,evtWs_BnonSl,evtWs_BCSl_BCbarSl,
																		evtWs_BCSl_BSl,evtWs_BCSl_BTauSl,evtWs_BCbarSl_BSl,evtWs_BCbarSl_BTauSl};

		vector<vector<double>> vec_sem{sem_BSl,sem_BTauSl,sem_BCSl,sem_BCbarSl,sem_BnonSl,sem_BCSl_BCbarSl,
																		sem_BCSl_BSl,sem_BCSl_BTauSl,sem_BCbarSl_BSl,sem_BCbarSl_BTauSl};

		vector<vector<double>> vec_vals{vals_BSl,vals_BTauSl,vals_BCSl,vals_BCbarSl,vals_BnonSl,vals_BCSl_BCbarSl,
																		vals_BCSl_BSl,vals_BCSl_BTauSl,vals_BCbarSl_BSl,vals_BCbarSl_BTauSl};

		//set small number so that we dont divide by zero 
		evtWtot_BSl = 1e-10;
		evtWtot_BTauSl = 1e-10;
		evtWtot_BCSl = 1e-10;
		evtWtot_BCbarSl = 1e-10;
		evtWtot_BnonSl = 1e-10;
		evtWtot_BCSl_BCbarSl = 1e-10;
		evtWtot_BCSl_BSl = 1e-10;
		evtWtot_BCSl_BTauSl = 1e-10;
		evtWtot_BCbarSl_BSl = 1e-10;
		evtWtot_BCbarSl_BTauSl = 1e-10;

		evtWtots_BSl = 1e-10;
		evtWtots_BTauSl = 1e-10;
		evtWtots_BCSl = 1e-10;
		evtWtots_BCbarSl = 1e-10;
		evtWtots_BnonSl = 1e-10;
		evtWtots_BCSl_BCbarSl = 1e-10;
		evtWtots_BCSl_BSl = 1e-10;
		evtWtots_BCSl_BTauSl = 1e-10;
		evtWtots_BCbarSl_BSl = 1e-10;
		evtWtots_BCbarSl_BTauSl = 1e-10;

		for (double x : evtW_BSl) evtWtot_BSl += x;
		for (double x : evtW_BTauSl) evtWtot_BTauSl += x;
		for (double x : evtW_BCSl) evtWtot_BCSl += x;
		for (double x : evtW_BCbarSl) evtWtot_BCbarSl += x;
		for (double x : evtW_BnonSl) evtWtot_BnonSl += x;
		for (double x : evtW_BCSl_BCbarSl) evtWtot_BCSl_BCbarSl += x;
		for (double x : evtW_BCSl_BSl) evtWtot_BCSl_BSl += x;
		for (double x : evtW_BCSl_BTauSl) evtWtot_BCSl_BTauSl += x;
		for (double x : evtW_BCbarSl_BSl) evtWtot_BCbarSl_BSl += x;
		for (double x : evtW_BCbarSl_BTauSl) evtWtot_BCbarSl_BTauSl += x;

		for (double x : evtWs_BSl) evtWtots_BSl += x;
		for (double x : evtWs_BTauSl) evtWtots_BTauSl += x;
		for (double x : evtWs_BCSl) evtWtots_BCSl += x;
		for (double x : evtWs_BCbarSl) evtWtots_BCbarSl += x;
		for (double x : evtWs_BnonSl) evtWtots_BnonSl += x;
		for (double x : evtWs_BCSl_BCbarSl) evtWtots_BCSl_BCbarSl += x;
		for (double x : evtWs_BCSl_BSl) evtWtots_BCSl_BSl += x;
		for (double x : evtWs_BCSl_BTauSl) evtWtots_BCSl_BTauSl += x;
		for (double x : evtWs_BCbarSl_BSl) evtWtots_BCbarSl_BSl += x;
		for (double x : evtWs_BCbarSl_BTauSl) evtWtots_BCbarSl_BTauSl += x;
		
		evtWtot = evtWtot_BSl + evtWtot_BTauSl + evtWtot_BCSl + evtWtot_BCbarSl + 
							evtWtot_BnonSl + evtWtot_BCSl_BCbarSl + evtWtot_BCSl_BSl +
							evtWtot_BCSl_BTauSl + evtWtot_BCbarSl_BSl + evtWtot_BCbarSl_BTauSl;

		evtWtots = evtWtots_BSl + evtWtots_BTauSl + evtWtots_BCSl + evtWtots_BCbarSl + 
							 evtWtots_BnonSl + evtWtots_BCSl_BCbarSl + evtWtots_BCSl_BSl +
							 evtWtots_BCSl_BTauSl + evtWtots_BCbarSl_BSl + evtWtots_BCbarSl_BTauSl;
		
		//loop over diff scale factors 
		errorb = 0;
		for (int j = 0; j < hRjetb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hRjetb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}
		
		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb->GetBinContent(i) == 0) diffb->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb->SetBinError(i,errorb);
  }

	//############### plus variation ##################
  //Loop over the TProfile bins
  for (int i = 1; i < hRjetb->GetNbinsX()+1; i++){
		evtW_BSl.clear();
		evtW_BTauSl.clear();
		evtW_BCSl.clear();
		evtW_BCbarSl.clear();
		evtW_BnonSl.clear();
		evtW_BCSl_BCbarSl.clear();
		evtW_BCSl_BSl.clear();
		evtW_BCSl_BTauSl.clear();
		evtW_BCbarSl_BSl.clear();
		evtW_BCbarSl_BTauSl.clear();

		evtWs_BSl.clear();
		evtWs_BTauSl.clear();
		evtWs_BCSl.clear();
		evtWs_BCbarSl.clear();
		evtWs_BnonSl.clear();
		evtWs_BCSl_BCbarSl.clear();
		evtWs_BCSl_BSl.clear();
		evtWs_BCSl_BTauSl.clear();
		evtWs_BCbarSl_BSl.clear();
		evtWs_BCbarSl_BTauSl.clear();

		sem_BSl.clear();
		sem_BTauSl.clear();
		sem_BCSl.clear();
		sem_BCbarSl.clear();
		sem_BnonSl.clear();
		sem_BCSl_BCbarSl.clear();
		sem_BCSl_BSl.clear();
		sem_BCSl_BTauSl.clear();
		sem_BCbarSl_BSl.clear();
		sem_BCbarSl_BTauSl.clear();

		vals_BSl.clear();
		vals_BTauSl.clear();
		vals_BCSl.clear();
		vals_BCbarSl.clear();
		vals_BnonSl.clear();
		vals_BCSl_BCbarSl.clear();
		vals_BCSl_BSl.clear();
		vals_BCSl_BTauSl.clear();
		vals_BCbarSl_BSl.clear();
		vals_BCbarSl_BTauSl.clear();

		for (int j = 1; j < hRjetb->GetNbinsX()+1; j++) {
			string pr_ind = "_" + to_string(i) + "_" + to_string(j);

			prRjetb_BSl = pr2DRjetb_BSl->ProfileX(("BSl"+pr_ind).c_str(),j,j);  
			prRjetb_BTauSl = pr2DRjetb_BTauSl->ProfileX(("BTauSl"+pr_ind).c_str(),j,j);  
			prRjetb_BCSl = pr2DRjetb_BCSl->ProfileX(("BCSl"+pr_ind).c_str(),j,j);  
			prRjetb_BCbarSl = pr2DRjetb_BCbarSl->ProfileX(("BCbarSl"+pr_ind).c_str(),j,j);  
			prRjetb_BnonSl = pr2DRjetb_BnonSl->ProfileX(("BnonSl"+pr_ind).c_str(),j,j);  
			prRjetb_BCSl_BCbarSl = pr2DRjetb_BCSl_BCbarSl->ProfileX(("BCSl_BCbarSl"+pr_ind).c_str(),j,j);  
			prRjetb_BCSl_BSl = pr2DRjetb_BCSl_BSl->ProfileX(("BCSl_BSl"+pr_ind).c_str(),j,j);  
			prRjetb_BCSl_BTauSl = pr2DRjetb_BCSl_BTauSl->ProfileX(("BCSl_BTauSl"+pr_ind).c_str(),j,j);  
			prRjetb_BCbarSl_BSl = pr2DRjetb_BCbarSl_BSl->ProfileX(("BCbarSl_BSl"+pr_ind).c_str(),j,j);  
			prRjetb_BCbarSl_BTauSl = pr2DRjetb_BCbarSl_BTauSl->ProfileX(("BCbarSl_BTauSl"+pr_ind).c_str(),j,j);  

			prRjetb_BSl_s_p = pr2DRjetb_BSl_s_p->ProfileX(("BSl_s"+pr_ind).c_str(),j,j);  
			prRjetb_BTauSl_s_p = pr2DRjetb_BTauSl_s_p->ProfileX(("BTauSl_s"+pr_ind).c_str(),j,j);  
			prRjetb_BCSl_s_p = pr2DRjetb_BCSl_s_p->ProfileX(("BCSl_s"+pr_ind).c_str(),j,j);  
			prRjetb_BCbarSl_s_p = pr2DRjetb_BCbarSl_s_p->ProfileX(("BCbarSl_s"+pr_ind).c_str(),j,j);  
			prRjetb_BnonSl_s_p = pr2DRjetb_BnonSl_s_p->ProfileX(("BnonSl_s"+pr_ind).c_str(),j,j);  
			prRjetb_BCSl_BCbarSl_s_p = pr2DRjetb_BCSl_BCbarSl_s_p->ProfileX(("BCSl_BCbarSl_s"+pr_ind).c_str(),j,j);  
			prRjetb_BCSl_BSl_s_p = pr2DRjetb_BCSl_BSl_s_p->ProfileX(("BCSl_BSl_s"+pr_ind).c_str(),j,j);  
			prRjetb_BCSl_BTauSl_s_p = pr2DRjetb_BCSl_BTauSl_s_p->ProfileX(("BCSl_BTauSl_s"+pr_ind).c_str(),j,j);  
			prRjetb_BCbarSl_BSl_s_p = pr2DRjetb_BCbarSl_BSl_s_p->ProfileX(("BCbarSl_BSl_s"+pr_ind).c_str(),j,j);  
			prRjetb_BCbarSl_BTauSl_s_p = pr2DRjetb_BCbarSl_BTauSl_s_p->ProfileX(("BCbarSl_BTauSl_s"+pr_ind).c_str(),j,j);    

			evtW_BSl.push_back(prRjetb_BSl->GetBinEntries(i));
			evtW_BTauSl.push_back(prRjetb_BTauSl->GetBinEntries(i));
			evtW_BCSl.push_back(prRjetb_BCSl->GetBinEntries(i));
			evtW_BCbarSl.push_back(prRjetb_BCbarSl->GetBinEntries(i));
			evtW_BnonSl.push_back(prRjetb_BnonSl->GetBinEntries(i));
			evtW_BCSl_BCbarSl.push_back(prRjetb_BCSl_BCbarSl->GetBinEntries(i));
			evtW_BCSl_BSl.push_back(prRjetb_BCSl_BSl->GetBinEntries(i));
			evtW_BCSl_BTauSl.push_back(prRjetb_BCSl_BTauSl->GetBinEntries(i));
			evtW_BCbarSl_BSl.push_back(prRjetb_BCbarSl_BSl->GetBinEntries(i));
			evtW_BCbarSl_BTauSl.push_back(prRjetb_BCbarSl_BTauSl->GetBinEntries(i));

			evtWs_BSl.push_back(prRjetb_BSl_s_p->GetBinEntries(i));
			evtWs_BTauSl.push_back(prRjetb_BTauSl_s_p->GetBinEntries(i));
			evtWs_BCSl.push_back(prRjetb_BCSl_s_p->GetBinEntries(i));
			evtWs_BCbarSl.push_back(prRjetb_BCbarSl_s_p->GetBinEntries(i));
			evtWs_BnonSl.push_back(prRjetb_BnonSl_s_p->GetBinEntries(i));
			evtWs_BCSl_BCbarSl.push_back(prRjetb_BCSl_BCbarSl_s_p->GetBinEntries(i));
			evtWs_BCSl_BSl.push_back(prRjetb_BCSl_BSl_s_p->GetBinEntries(i));
			evtWs_BCSl_BTauSl.push_back(prRjetb_BCSl_BTauSl_s_p->GetBinEntries(i));
			evtWs_BCbarSl_BSl.push_back(prRjetb_BCbarSl_BSl_s_p->GetBinEntries(i));
			evtWs_BCbarSl_BTauSl.push_back(prRjetb_BCbarSl_BTauSl_s_p->GetBinEntries(i));

			sem_BSl.push_back(prRjetb_BSl->GetBinError(i));
			sem_BTauSl.push_back(prRjetb_BTauSl->GetBinError(i));
			sem_BCSl.push_back(prRjetb_BCSl->GetBinError(i));
			sem_BCbarSl.push_back(prRjetb_BCbarSl->GetBinError(i));
			sem_BnonSl.push_back(prRjetb_BnonSl->GetBinError(i));
			sem_BCSl_BCbarSl.push_back(prRjetb_BCSl_BCbarSl->GetBinError(i));
			sem_BCSl_BSl.push_back(prRjetb_BCSl_BSl->GetBinError(i));
			sem_BCSl_BTauSl.push_back(prRjetb_BCSl_BTauSl->GetBinError(i));
			sem_BCbarSl_BSl.push_back(prRjetb_BCbarSl_BSl->GetBinError(i));
			sem_BCbarSl_BTauSl.push_back(prRjetb_BCbarSl_BTauSl->GetBinError(i));

			vals_BSl.push_back(prRjetb_BSl->GetBinContent(i));
			vals_BTauSl.push_back(prRjetb_BTauSl->GetBinContent(i));
			vals_BCSl.push_back(prRjetb_BCSl->GetBinContent(i));
			vals_BCbarSl.push_back(prRjetb_BCbarSl->GetBinContent(i));
			vals_BnonSl.push_back(prRjetb_BnonSl->GetBinContent(i));
			vals_BCSl_BCbarSl.push_back(prRjetb_BCSl_BCbarSl->GetBinContent(i));
			vals_BCSl_BSl.push_back(prRjetb_BCSl_BSl->GetBinContent(i));
			vals_BCSl_BTauSl.push_back(prRjetb_BCSl_BTauSl->GetBinContent(i));
			vals_BCbarSl_BSl.push_back(prRjetb_BCbarSl_BSl->GetBinContent(i));
			vals_BCbarSl_BTauSl.push_back(prRjetb_BCbarSl_BTauSl->GetBinContent(i));

			delete prRjetb_BSl;
			delete prRjetb_BTauSl;
			delete prRjetb_BCSl;
			delete prRjetb_BCbarSl;
			delete prRjetb_BnonSl;
			delete prRjetb_BCSl_BCbarSl;
			delete prRjetb_BCSl_BSl;
			delete prRjetb_BCSl_BTauSl;
			delete prRjetb_BCbarSl_BSl;
			delete prRjetb_BCbarSl_BTauSl;

			delete prRjetb_BSl_s_p;
			delete prRjetb_BTauSl_s_p;
			delete prRjetb_BCSl_s_p;
			delete prRjetb_BCbarSl_s_p;
			delete prRjetb_BnonSl_s_p;
			delete prRjetb_BCSl_BCbarSl_s_p;
			delete prRjetb_BCSl_BSl_s_p;
			delete prRjetb_BCSl_BTauSl_s_p;
			delete prRjetb_BCbarSl_BSl_s_p;
			delete prRjetb_BCbarSl_BTauSl_s_p;
		}

		 
		vector<vector<double>> vec_evtW{evtW_BSl,evtW_BTauSl,evtW_BCSl,evtW_BCbarSl,evtW_BnonSl,evtW_BCSl_BCbarSl,
																		evtW_BCSl_BSl,evtW_BCSl_BTauSl,evtW_BCbarSl_BSl,evtW_BCbarSl_BTauSl};

		vector<vector<double>> vec_evtWs{evtWs_BSl,evtWs_BTauSl,evtWs_BCSl,evtWs_BCbarSl,evtWs_BnonSl,evtWs_BCSl_BCbarSl,
																		evtWs_BCSl_BSl,evtWs_BCSl_BTauSl,evtWs_BCbarSl_BSl,evtWs_BCbarSl_BTauSl};

		vector<vector<double>> vec_sem{sem_BSl,sem_BTauSl,sem_BCSl,sem_BCbarSl,sem_BnonSl,sem_BCSl_BCbarSl,
																		sem_BCSl_BSl,sem_BCSl_BTauSl,sem_BCbarSl_BSl,sem_BCbarSl_BTauSl};

		vector<vector<double>> vec_vals{vals_BSl,vals_BTauSl,vals_BCSl,vals_BCbarSl,vals_BnonSl,vals_BCSl_BCbarSl,
																		vals_BCSl_BSl,vals_BCSl_BTauSl,vals_BCbarSl_BSl,vals_BCbarSl_BTauSl};

		//set small number so that we dont divide by zero 
		evtWtot_BSl = 1e-10;
		evtWtot_BTauSl = 1e-10;
		evtWtot_BCSl = 1e-10;
		evtWtot_BCbarSl = 1e-10;
		evtWtot_BnonSl = 1e-10;
		evtWtot_BCSl_BCbarSl = 1e-10;
		evtWtot_BCSl_BSl = 1e-10;
		evtWtot_BCSl_BTauSl = 1e-10;
		evtWtot_BCbarSl_BSl = 1e-10;
		evtWtot_BCbarSl_BTauSl = 1e-10;

		evtWtots_BSl = 1e-10;
		evtWtots_BTauSl = 1e-10;
		evtWtots_BCSl = 1e-10;
		evtWtots_BCbarSl = 1e-10;
		evtWtots_BnonSl = 1e-10;
		evtWtots_BCSl_BCbarSl = 1e-10;
		evtWtots_BCSl_BSl = 1e-10;
		evtWtots_BCSl_BTauSl = 1e-10;
		evtWtots_BCbarSl_BSl = 1e-10;
		evtWtots_BCbarSl_BTauSl = 1e-10;

		for (double x : evtW_BSl) evtWtot_BSl += x;
		for (double x : evtW_BTauSl) evtWtot_BTauSl += x;
		for (double x : evtW_BCSl) evtWtot_BCSl += x;
		for (double x : evtW_BCbarSl) evtWtot_BCbarSl += x;
		for (double x : evtW_BnonSl) evtWtot_BnonSl += x;
		for (double x : evtW_BCSl_BCbarSl) evtWtot_BCSl_BCbarSl += x;
		for (double x : evtW_BCSl_BSl) evtWtot_BCSl_BSl += x;
		for (double x : evtW_BCSl_BTauSl) evtWtot_BCSl_BTauSl += x;
		for (double x : evtW_BCbarSl_BSl) evtWtot_BCbarSl_BSl += x;
		for (double x : evtW_BCbarSl_BTauSl) evtWtot_BCbarSl_BTauSl += x;

		for (double x : evtWs_BSl) evtWtots_BSl += x;
		for (double x : evtWs_BTauSl) evtWtots_BTauSl += x;
		for (double x : evtWs_BCSl) evtWtots_BCSl += x;
		for (double x : evtWs_BCbarSl) evtWtots_BCbarSl += x;
		for (double x : evtWs_BnonSl) evtWtots_BnonSl += x;
		for (double x : evtWs_BCSl_BCbarSl) evtWtots_BCSl_BCbarSl += x;
		for (double x : evtWs_BCSl_BSl) evtWtots_BCSl_BSl += x;
		for (double x : evtWs_BCSl_BTauSl) evtWtots_BCSl_BTauSl += x;
		for (double x : evtWs_BCbarSl_BSl) evtWtots_BCbarSl_BSl += x;
		for (double x : evtWs_BCbarSl_BTauSl) evtWtots_BCbarSl_BTauSl += x;
		
		evtWtot = evtWtot_BSl + evtWtot_BTauSl + evtWtot_BCSl + evtWtot_BCbarSl + 
							evtWtot_BnonSl + evtWtot_BCSl_BCbarSl + evtWtot_BCSl_BSl +
							evtWtot_BCSl_BTauSl + evtWtot_BCbarSl_BSl + evtWtot_BCbarSl_BTauSl;

		evtWtots = evtWtots_BSl + evtWtots_BTauSl + evtWtots_BCSl + evtWtots_BCbarSl + 
							 evtWtots_BnonSl + evtWtots_BCSl_BCbarSl + evtWtots_BCSl_BSl +
							 evtWtots_BCSl_BTauSl + evtWtots_BCbarSl_BSl + evtWtots_BCbarSl_BTauSl;
		
		//loop over diff scale factors 
		errorb = 0;
		for (int j = 0; j < hRjetb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hRjetb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}
		
		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb_p->GetBinContent(i) == 0) diffb_p->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb_p->SetBinError(i,errorb);
  }

	//############### minus variation ####################
  //Loop over the TProfile bins
  for (int i = 1; i < hRjetb->GetNbinsX()+1; i++){
		evtW_BSl.clear();
		evtW_BTauSl.clear();
		evtW_BCSl.clear();
		evtW_BCbarSl.clear();
		evtW_BnonSl.clear();
		evtW_BCSl_BCbarSl.clear();
		evtW_BCSl_BSl.clear();
		evtW_BCSl_BTauSl.clear();
		evtW_BCbarSl_BSl.clear();
		evtW_BCbarSl_BTauSl.clear();

		evtWs_BSl.clear();
		evtWs_BTauSl.clear();
		evtWs_BCSl.clear();
		evtWs_BCbarSl.clear();
		evtWs_BnonSl.clear();
		evtWs_BCSl_BCbarSl.clear();
		evtWs_BCSl_BSl.clear();
		evtWs_BCSl_BTauSl.clear();
		evtWs_BCbarSl_BSl.clear();
		evtWs_BCbarSl_BTauSl.clear();

		sem_BSl.clear();
		sem_BTauSl.clear();
		sem_BCSl.clear();
		sem_BCbarSl.clear();
		sem_BnonSl.clear();
		sem_BCSl_BCbarSl.clear();
		sem_BCSl_BSl.clear();
		sem_BCSl_BTauSl.clear();
		sem_BCbarSl_BSl.clear();
		sem_BCbarSl_BTauSl.clear();

		vals_BSl.clear();
		vals_BTauSl.clear();
		vals_BCSl.clear();
		vals_BCbarSl.clear();
		vals_BnonSl.clear();
		vals_BCSl_BCbarSl.clear();
		vals_BCSl_BSl.clear();
		vals_BCSl_BTauSl.clear();
		vals_BCbarSl_BSl.clear();
		vals_BCbarSl_BTauSl.clear();

		for (int j = 1; j < hRjetb->GetNbinsX()+1; j++) {
			string pr_ind = "_" + to_string(i) + "_" + to_string(j);

			prRjetb_BSl = pr2DRjetb_BSl->ProfileX(("BSl"+pr_ind).c_str(),j,j);  
			prRjetb_BTauSl = pr2DRjetb_BTauSl->ProfileX(("BTauSl"+pr_ind).c_str(),j,j);  
			prRjetb_BCSl = pr2DRjetb_BCSl->ProfileX(("BCSl"+pr_ind).c_str(),j,j);  
			prRjetb_BCbarSl = pr2DRjetb_BCbarSl->ProfileX(("BCbarSl"+pr_ind).c_str(),j,j);  
			prRjetb_BnonSl = pr2DRjetb_BnonSl->ProfileX(("BnonSl"+pr_ind).c_str(),j,j);  
			prRjetb_BCSl_BCbarSl = pr2DRjetb_BCSl_BCbarSl->ProfileX(("BCSl_BCbarSl"+pr_ind).c_str(),j,j);  
			prRjetb_BCSl_BSl = pr2DRjetb_BCSl_BSl->ProfileX(("BCSl_BSl"+pr_ind).c_str(),j,j);  
			prRjetb_BCSl_BTauSl = pr2DRjetb_BCSl_BTauSl->ProfileX(("BCSl_BTauSl"+pr_ind).c_str(),j,j);  
			prRjetb_BCbarSl_BSl = pr2DRjetb_BCbarSl_BSl->ProfileX(("BCbarSl_BSl"+pr_ind).c_str(),j,j);  
			prRjetb_BCbarSl_BTauSl = pr2DRjetb_BCbarSl_BTauSl->ProfileX(("BCbarSl_BTauSl"+pr_ind).c_str(),j,j);  

			prRjetb_BSl_s_m = pr2DRjetb_BSl_s_m->ProfileX(("BSl_s"+pr_ind).c_str(),j,j);  
			prRjetb_BTauSl_s_m = pr2DRjetb_BTauSl_s_m->ProfileX(("BTauSl_s"+pr_ind).c_str(),j,j);  
			prRjetb_BCSl_s_m = pr2DRjetb_BCSl_s_m->ProfileX(("BCSl_s"+pr_ind).c_str(),j,j);  
			prRjetb_BCbarSl_s_m = pr2DRjetb_BCbarSl_s_m->ProfileX(("BCbarSl_s"+pr_ind).c_str(),j,j);  
			prRjetb_BnonSl_s_m = pr2DRjetb_BnonSl_s_m->ProfileX(("BnonSl_s"+pr_ind).c_str(),j,j);  
			prRjetb_BCSl_BCbarSl_s_m = pr2DRjetb_BCSl_BCbarSl_s_m->ProfileX(("BCSl_BCbarSl_s"+pr_ind).c_str(),j,j);  
			prRjetb_BCSl_BSl_s_m = pr2DRjetb_BCSl_BSl_s_m->ProfileX(("BCSl_BSl_s"+pr_ind).c_str(),j,j);  
			prRjetb_BCSl_BTauSl_s_m = pr2DRjetb_BCSl_BTauSl_s_m->ProfileX(("BCSl_BTauSl_s"+pr_ind).c_str(),j,j);  
			prRjetb_BCbarSl_BSl_s_m = pr2DRjetb_BCbarSl_BSl_s_m->ProfileX(("BCbarSl_BSl_s"+pr_ind).c_str(),j,j);  
			prRjetb_BCbarSl_BTauSl_s_m = pr2DRjetb_BCbarSl_BTauSl_s_m->ProfileX(("BCbarSl_BTauSl_s"+pr_ind).c_str(),j,j);   

			evtW_BSl.push_back(prRjetb_BSl->GetBinEntries(i));
			evtW_BTauSl.push_back(prRjetb_BTauSl->GetBinEntries(i));
			evtW_BCSl.push_back(prRjetb_BCSl->GetBinEntries(i));
			evtW_BCbarSl.push_back(prRjetb_BCbarSl->GetBinEntries(i));
			evtW_BnonSl.push_back(prRjetb_BnonSl->GetBinEntries(i));
			evtW_BCSl_BCbarSl.push_back(prRjetb_BCSl_BCbarSl->GetBinEntries(i));
			evtW_BCSl_BSl.push_back(prRjetb_BCSl_BSl->GetBinEntries(i));
			evtW_BCSl_BTauSl.push_back(prRjetb_BCSl_BTauSl->GetBinEntries(i));
			evtW_BCbarSl_BSl.push_back(prRjetb_BCbarSl_BSl->GetBinEntries(i));
			evtW_BCbarSl_BTauSl.push_back(prRjetb_BCbarSl_BTauSl->GetBinEntries(i));

			evtWs_BSl.push_back(prRjetb_BSl_s_m->GetBinEntries(i));
			evtWs_BTauSl.push_back(prRjetb_BTauSl_s_m->GetBinEntries(i));
			evtWs_BCSl.push_back(prRjetb_BCSl_s_m->GetBinEntries(i));
			evtWs_BCbarSl.push_back(prRjetb_BCbarSl_s_m->GetBinEntries(i));
			evtWs_BnonSl.push_back(prRjetb_BnonSl_s_m->GetBinEntries(i));
			evtWs_BCSl_BCbarSl.push_back(prRjetb_BCSl_BCbarSl_s_m->GetBinEntries(i));
			evtWs_BCSl_BSl.push_back(prRjetb_BCSl_BSl_s_m->GetBinEntries(i));
			evtWs_BCSl_BTauSl.push_back(prRjetb_BCSl_BTauSl_s_m->GetBinEntries(i));
			evtWs_BCbarSl_BSl.push_back(prRjetb_BCbarSl_BSl_s_m->GetBinEntries(i));
			evtWs_BCbarSl_BTauSl.push_back(prRjetb_BCbarSl_BTauSl_s_m->GetBinEntries(i));

			sem_BSl.push_back(prRjetb_BSl->GetBinError(i));
			sem_BTauSl.push_back(prRjetb_BTauSl->GetBinError(i));
			sem_BCSl.push_back(prRjetb_BCSl->GetBinError(i));
			sem_BCbarSl.push_back(prRjetb_BCbarSl->GetBinError(i));
			sem_BnonSl.push_back(prRjetb_BnonSl->GetBinError(i));
			sem_BCSl_BCbarSl.push_back(prRjetb_BCSl_BCbarSl->GetBinError(i));
			sem_BCSl_BSl.push_back(prRjetb_BCSl_BSl->GetBinError(i));
			sem_BCSl_BTauSl.push_back(prRjetb_BCSl_BTauSl->GetBinError(i));
			sem_BCbarSl_BSl.push_back(prRjetb_BCbarSl_BSl->GetBinError(i));
			sem_BCbarSl_BTauSl.push_back(prRjetb_BCbarSl_BTauSl->GetBinError(i));

			vals_BSl.push_back(prRjetb_BSl->GetBinContent(i));
			vals_BTauSl.push_back(prRjetb_BTauSl->GetBinContent(i));
			vals_BCSl.push_back(prRjetb_BCSl->GetBinContent(i));
			vals_BCbarSl.push_back(prRjetb_BCbarSl->GetBinContent(i));
			vals_BnonSl.push_back(prRjetb_BnonSl->GetBinContent(i));
			vals_BCSl_BCbarSl.push_back(prRjetb_BCSl_BCbarSl->GetBinContent(i));
			vals_BCSl_BSl.push_back(prRjetb_BCSl_BSl->GetBinContent(i));
			vals_BCSl_BTauSl.push_back(prRjetb_BCSl_BTauSl->GetBinContent(i));
			vals_BCbarSl_BSl.push_back(prRjetb_BCbarSl_BSl->GetBinContent(i));
			vals_BCbarSl_BTauSl.push_back(prRjetb_BCbarSl_BTauSl->GetBinContent(i));

			delete prRjetb_BSl;
			delete prRjetb_BTauSl;
			delete prRjetb_BCSl;
			delete prRjetb_BCbarSl;
			delete prRjetb_BnonSl;
			delete prRjetb_BCSl_BCbarSl;
			delete prRjetb_BCSl_BSl;
			delete prRjetb_BCSl_BTauSl;
			delete prRjetb_BCbarSl_BSl;
			delete prRjetb_BCbarSl_BTauSl;

			delete prRjetb_BSl_s_m;
			delete prRjetb_BTauSl_s_m;
			delete prRjetb_BCSl_s_m;
			delete prRjetb_BCbarSl_s_m;
			delete prRjetb_BnonSl_s_m;
			delete prRjetb_BCSl_BCbarSl_s_m;
			delete prRjetb_BCSl_BSl_s_m;
			delete prRjetb_BCSl_BTauSl_s_m;
			delete prRjetb_BCbarSl_BSl_s_m;
			delete prRjetb_BCbarSl_BTauSl_s_m;
		}

		 
		vector<vector<double>> vec_evtW{evtW_BSl,evtW_BTauSl,evtW_BCSl,evtW_BCbarSl,evtW_BnonSl,evtW_BCSl_BCbarSl,
																		evtW_BCSl_BSl,evtW_BCSl_BTauSl,evtW_BCbarSl_BSl,evtW_BCbarSl_BTauSl};

		vector<vector<double>> vec_evtWs{evtWs_BSl,evtWs_BTauSl,evtWs_BCSl,evtWs_BCbarSl,evtWs_BnonSl,evtWs_BCSl_BCbarSl,
																		evtWs_BCSl_BSl,evtWs_BCSl_BTauSl,evtWs_BCbarSl_BSl,evtWs_BCbarSl_BTauSl};

		vector<vector<double>> vec_sem{sem_BSl,sem_BTauSl,sem_BCSl,sem_BCbarSl,sem_BnonSl,sem_BCSl_BCbarSl,
																		sem_BCSl_BSl,sem_BCSl_BTauSl,sem_BCbarSl_BSl,sem_BCbarSl_BTauSl};

		vector<vector<double>> vec_vals{vals_BSl,vals_BTauSl,vals_BCSl,vals_BCbarSl,vals_BnonSl,vals_BCSl_BCbarSl,
																		vals_BCSl_BSl,vals_BCSl_BTauSl,vals_BCbarSl_BSl,vals_BCbarSl_BTauSl};

		//set small number so that we dont divide by zero 
		evtWtot_BSl = 1e-10;
		evtWtot_BTauSl = 1e-10;
		evtWtot_BCSl = 1e-10;
		evtWtot_BCbarSl = 1e-10;
		evtWtot_BnonSl = 1e-10;
		evtWtot_BCSl_BCbarSl = 1e-10;
		evtWtot_BCSl_BSl = 1e-10;
		evtWtot_BCSl_BTauSl = 1e-10;
		evtWtot_BCbarSl_BSl = 1e-10;
		evtWtot_BCbarSl_BTauSl = 1e-10;

		evtWtots_BSl = 1e-10;
		evtWtots_BTauSl = 1e-10;
		evtWtots_BCSl = 1e-10;
		evtWtots_BCbarSl = 1e-10;
		evtWtots_BnonSl = 1e-10;
		evtWtots_BCSl_BCbarSl = 1e-10;
		evtWtots_BCSl_BSl = 1e-10;
		evtWtots_BCSl_BTauSl = 1e-10;
		evtWtots_BCbarSl_BSl = 1e-10;
		evtWtots_BCbarSl_BTauSl = 1e-10;

		for (double x : evtW_BSl) evtWtot_BSl += x;
		for (double x : evtW_BTauSl) evtWtot_BTauSl += x;
		for (double x : evtW_BCSl) evtWtot_BCSl += x;
		for (double x : evtW_BCbarSl) evtWtot_BCbarSl += x;
		for (double x : evtW_BnonSl) evtWtot_BnonSl += x;
		for (double x : evtW_BCSl_BCbarSl) evtWtot_BCSl_BCbarSl += x;
		for (double x : evtW_BCSl_BSl) evtWtot_BCSl_BSl += x;
		for (double x : evtW_BCSl_BTauSl) evtWtot_BCSl_BTauSl += x;
		for (double x : evtW_BCbarSl_BSl) evtWtot_BCbarSl_BSl += x;
		for (double x : evtW_BCbarSl_BTauSl) evtWtot_BCbarSl_BTauSl += x;

		for (double x : evtWs_BSl) evtWtots_BSl += x;
		for (double x : evtWs_BTauSl) evtWtots_BTauSl += x;
		for (double x : evtWs_BCSl) evtWtots_BCSl += x;
		for (double x : evtWs_BCbarSl) evtWtots_BCbarSl += x;
		for (double x : evtWs_BnonSl) evtWtots_BnonSl += x;
		for (double x : evtWs_BCSl_BCbarSl) evtWtots_BCSl_BCbarSl += x;
		for (double x : evtWs_BCSl_BSl) evtWtots_BCSl_BSl += x;
		for (double x : evtWs_BCSl_BTauSl) evtWtots_BCSl_BTauSl += x;
		for (double x : evtWs_BCbarSl_BSl) evtWtots_BCbarSl_BSl += x;
		for (double x : evtWs_BCbarSl_BTauSl) evtWtots_BCbarSl_BTauSl += x;
		
		evtWtot = evtWtot_BSl + evtWtot_BTauSl + evtWtot_BCSl + evtWtot_BCbarSl + 
							evtWtot_BnonSl + evtWtot_BCSl_BCbarSl + evtWtot_BCSl_BSl +
							evtWtot_BCSl_BTauSl + evtWtot_BCbarSl_BSl + evtWtot_BCbarSl_BTauSl;

		evtWtots = evtWtots_BSl + evtWtots_BTauSl + evtWtots_BCSl + evtWtots_BCbarSl + 
							 evtWtots_BnonSl + evtWtots_BCSl_BCbarSl + evtWtots_BCSl_BSl +
							 evtWtots_BCSl_BTauSl + evtWtots_BCbarSl_BSl + evtWtots_BCbarSl_BTauSl;
		
		//loop over diff scale factors 
		errorb = 0;
		for (int j = 0; j < hRjetb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hRjetb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}
		
		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb_m->GetBinContent(i) == 0) diffb_m->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb_m->SetBinError(i,errorb);
  }

	//Scale to the percentages
  diffb->Scale(100.0);
  diffb_p->Scale(100.0);
  diffb_m->Scale(100.0);

	Rjetb_map["diffb"] = diffb;
	Rjetb_map["diffb_p"] = diffb_p;
	Rjetb_map["diffb_m"] = diffb_m;

	return Rjetb_map;
}

map<string, TH1D*> mpf_indvBtoSLBr() {
	map <string, TH1D*> MPFb_map;

  TFile* f_normal = TFile::Open(OpenName.c_str());
  TFile* f_scaled = TFile::Open(OpenName_indvBtoSLBr.c_str());
  TFile* f_scaled_p = TFile::Open(OpenName_indvBtoSLBr_p.c_str());
  TFile* f_scaled_m = TFile::Open(OpenName_indvBtoSLBr_m.c_str());

	TProfile *prMPFb;
	TProfile *prMPFb_s;
	TProfile *prMPFb_s_p;
	TProfile *prMPFb_s_m;

  //Read the normal sample
  f_normal->GetObject("prMPFb",   prMPFb);

  //Read the weighted sample
  f_scaled->GetObject("prMPFb",   prMPFb_s);
  f_scaled_p->GetObject("prMPFb",   prMPFb_s_p);
  f_scaled_m->GetObject("prMPFb",   prMPFb_s_m);

	//2D Tprofiles for the errorb calculations
	TProfile2D* pr2DMPFb;

	TProfile2D* pr2DMPFb_B0Sl;
	TProfile2D* pr2DMPFb_BpSl;
	TProfile2D* pr2DMPFb_B0sSl;
	TProfile2D* pr2DMPFb_LbSl;
	TProfile2D* pr2DMPFb_B0nonSl;
	TProfile2D* pr2DMPFb_BpnonSl;
	TProfile2D* pr2DMPFb_B0snonSl;
	TProfile2D* pr2DMPFb_LbnonSl;

  //The sample with the additional weighting (*_s)
	TProfile2D* pr2DMPFb_s;

	TProfile2D* pr2DMPFb_B0Sl_s;
	TProfile2D* pr2DMPFb_BpSl_s;
	TProfile2D* pr2DMPFb_B0sSl_s;
	TProfile2D* pr2DMPFb_LbSl_s;
	TProfile2D* pr2DMPFb_B0nonSl_s;
	TProfile2D* pr2DMPFb_BpnonSl_s;
	TProfile2D* pr2DMPFb_B0snonSl_s;
	TProfile2D* pr2DMPFb_LbnonSl_s;

	TProfile2D* pr2DMPFb_s_p;

	TProfile2D* pr2DMPFb_B0Sl_s_p;
	TProfile2D* pr2DMPFb_BpSl_s_p;
	TProfile2D* pr2DMPFb_B0sSl_s_p;
	TProfile2D* pr2DMPFb_LbSl_s_p;
	TProfile2D* pr2DMPFb_B0nonSl_s_p;
	TProfile2D* pr2DMPFb_BpnonSl_s_p;
	TProfile2D* pr2DMPFb_B0snonSl_s_p;
	TProfile2D* pr2DMPFb_LbnonSl_s_p;

	TProfile2D* pr2DMPFb_s_m;

	TProfile2D* pr2DMPFb_B0Sl_s_m;
	TProfile2D* pr2DMPFb_BpSl_s_m;
	TProfile2D* pr2DMPFb_B0sSl_s_m;
	TProfile2D* pr2DMPFb_LbSl_s_m;
	TProfile2D* pr2DMPFb_B0nonSl_s_m;
	TProfile2D* pr2DMPFb_BpnonSl_s_m;
	TProfile2D* pr2DMPFb_B0snonSl_s_m;
	TProfile2D* pr2DMPFb_LbnonSl_s_m;

	//Tprofiles for the errorb calculations
	TProfile* prMPFb_B0Sl;
	TProfile* prMPFb_BpSl;
	TProfile* prMPFb_B0sSl;
	TProfile* prMPFb_LbSl;
	TProfile* prMPFb_B0nonSl;
	TProfile* prMPFb_BpnonSl;
	TProfile* prMPFb_B0snonSl;
	TProfile* prMPFb_LbnonSl;

  //The sample with the additional weighting (*_s)
	TProfile* prMPFb_B0Sl_s;
	TProfile* prMPFb_BpSl_s;
	TProfile* prMPFb_B0sSl_s;
	TProfile* prMPFb_LbSl_s;
	TProfile* prMPFb_B0nonSl_s;
	TProfile* prMPFb_BpnonSl_s;
	TProfile* prMPFb_B0snonSl_s;
	TProfile* prMPFb_LbnonSl_s;

	TProfile* prMPFb_B0Sl_s_p;
	TProfile* prMPFb_BpSl_s_p;
	TProfile* prMPFb_B0sSl_s_p;
	TProfile* prMPFb_LbSl_s_p;
	TProfile* prMPFb_B0nonSl_s_p;
	TProfile* prMPFb_BpnonSl_s_p;
	TProfile* prMPFb_B0snonSl_s_p;
	TProfile* prMPFb_LbnonSl_s_p;

	TProfile* prMPFb_B0Sl_s_m;
	TProfile* prMPFb_BpSl_s_m;
	TProfile* prMPFb_B0sSl_s_m;
	TProfile* prMPFb_LbSl_s_m;
	TProfile* prMPFb_B0nonSl_s_m;
	TProfile* prMPFb_BpnonSl_s_m;
	TProfile* prMPFb_B0snonSl_s_m;
	TProfile* prMPFb_LbnonSl_s_m;

  //Read the normal sample
	f_normal->GetObject("pr2DMPFb",pr2DMPFb);

	f_normal->GetObject("pr2DMPFb_B0Sl",pr2DMPFb_B0Sl);
	f_normal->GetObject("pr2DMPFb_BpSl",pr2DMPFb_BpSl);
	f_normal->GetObject("pr2DMPFb_B0sSl",pr2DMPFb_B0sSl);
	f_normal->GetObject("pr2DMPFb_LbSl",pr2DMPFb_LbSl);
	f_normal->GetObject("pr2DMPFb_B0nonSl",pr2DMPFb_B0nonSl);
	f_normal->GetObject("pr2DMPFb_BpnonSl",pr2DMPFb_BpnonSl);
	f_normal->GetObject("pr2DMPFb_B0snonSl",pr2DMPFb_B0snonSl);
	f_normal->GetObject("pr2DMPFb_LbnonSl",pr2DMPFb_LbnonSl);

  //Read the weighted sample
	f_scaled->GetObject("pr2DMPFb",pr2DMPFb_s);

	f_scaled->GetObject("pr2DMPFb_B0Sl",pr2DMPFb_B0Sl_s);
	f_scaled->GetObject("pr2DMPFb_BpSl",pr2DMPFb_BpSl_s);
	f_scaled->GetObject("pr2DMPFb_B0sSl",pr2DMPFb_B0sSl_s);
	f_scaled->GetObject("pr2DMPFb_LbSl",pr2DMPFb_LbSl_s);
	f_scaled->GetObject("pr2DMPFb_B0nonSl",pr2DMPFb_B0nonSl_s);
	f_scaled->GetObject("pr2DMPFb_BpnonSl",pr2DMPFb_BpnonSl_s);
	f_scaled->GetObject("pr2DMPFb_B0snonSl",pr2DMPFb_B0snonSl_s);
	f_scaled->GetObject("pr2DMPFb_LbnonSl",pr2DMPFb_LbnonSl_s);

	f_scaled_p->GetObject("pr2DMPFb",pr2DMPFb_s_p);

	f_scaled_p->GetObject("pr2DMPFb_B0Sl",pr2DMPFb_B0Sl_s_p);
	f_scaled_p->GetObject("pr2DMPFb_BpSl",pr2DMPFb_BpSl_s_p);
	f_scaled_p->GetObject("pr2DMPFb_B0sSl",pr2DMPFb_B0sSl_s_p);
	f_scaled_p->GetObject("pr2DMPFb_LbSl",pr2DMPFb_LbSl_s_p);
	f_scaled_p->GetObject("pr2DMPFb_B0nonSl",pr2DMPFb_B0nonSl_s_p);
	f_scaled_p->GetObject("pr2DMPFb_BpnonSl",pr2DMPFb_BpnonSl_s_p);
	f_scaled_p->GetObject("pr2DMPFb_B0snonSl",pr2DMPFb_B0snonSl_s_p);
	f_scaled_p->GetObject("pr2DMPFb_LbnonSl",pr2DMPFb_LbnonSl_s_p);

	f_scaled_m->GetObject("pr2DMPFb",pr2DMPFb_s_m);

	f_scaled_m->GetObject("pr2DMPFb_B0Sl",pr2DMPFb_B0Sl_s_m);
	f_scaled_m->GetObject("pr2DMPFb_BpSl",pr2DMPFb_BpSl_s_m);
	f_scaled_m->GetObject("pr2DMPFb_B0sSl",pr2DMPFb_B0sSl_s_m);
	f_scaled_m->GetObject("pr2DMPFb_LbSl",pr2DMPFb_LbSl_s_m);
	f_scaled_m->GetObject("pr2DMPFb_B0nonSl",pr2DMPFb_B0nonSl_s_m);
	f_scaled_m->GetObject("pr2DMPFb_BpnonSl",pr2DMPFb_BpnonSl_s_m);
	f_scaled_m->GetObject("pr2DMPFb_B0snonSl",pr2DMPFb_B0snonSl_s_m);
	f_scaled_m->GetObject("pr2DMPFb_LbnonSl",pr2DMPFb_LbnonSl_s_m);

	//calculate the difference in response between normal and the rescaled sample 
	//the second ProjectionX has new name as argument, otherwise would have same name as the previous
	//histo and causes memory leak 
	TH1D* hMPFb     = prMPFb   ->ProjectionX();
	TH1D* hMPFb_s   = prMPFb_s ->ProjectionX("pr2DMPFb_s");
	TH1D* hMPFb_s_p = prMPFb_s_p ->ProjectionX("pr2DMPFb_s_p");
	TH1D* hMPFb_s_m = prMPFb_s_m ->ProjectionX("pr2DMPFb_s_m");

	TH1D* diffb 		= (TH1D*) hMPFb_s->Clone();
	TH1D* diffb_p 	= (TH1D*) hMPFb_s_p->Clone();
	TH1D* diffb_m 	= (TH1D*) hMPFb_s_m->Clone();

	diffb->Add(hMPFb,-1);
	diffb_p->Add(hMPFb,-1);
	diffb_m->Add(hMPFb,-1);

	//evtW_* = event weight, sem_* = standard errorb of the mean,   
	double errorb, evtWtot, evtWtots;

	double evtWtot_B0Sl;
	double evtWtot_BpSl;
	double evtWtot_B0sSl;
	double evtWtot_LbSl;
	double evtWtot_B0nonSl;
	double evtWtot_BpnonSl;
	double evtWtot_B0snonSl;
	double evtWtot_LbnonSl;

	double evtWtots_B0Sl;
	double evtWtots_BpSl;
	double evtWtots_B0sSl;
	double evtWtots_LbSl;
	double evtWtots_B0nonSl;
	double evtWtots_BpnonSl;
	double evtWtots_B0snonSl;
	double evtWtots_LbnonSl;

	vector<double> evtW_B0Sl;
	vector<double> evtW_BpSl;
	vector<double> evtW_B0sSl;
	vector<double> evtW_LbSl;
	vector<double> evtW_B0nonSl;
	vector<double> evtW_BpnonSl;
	vector<double> evtW_B0snonSl;
	vector<double> evtW_LbnonSl;

	vector<double> evtWs_B0Sl;
	vector<double> evtWs_BpSl;
	vector<double> evtWs_B0sSl;
	vector<double> evtWs_LbSl;
	vector<double> evtWs_B0nonSl;
	vector<double> evtWs_BpnonSl;
	vector<double> evtWs_B0snonSl;
	vector<double> evtWs_LbnonSl;

	vector<double> sem_B0Sl;
	vector<double> sem_BpSl;
	vector<double> sem_B0sSl;
	vector<double> sem_LbSl;
	vector<double> sem_B0nonSl;
	vector<double> sem_BpnonSl;
	vector<double> sem_B0snonSl;
	vector<double> sem_LbnonSl;

	vector<double> vals_B0Sl;
	vector<double> vals_BpSl;
	vector<double> vals_B0sSl;
	vector<double> vals_LbSl;
	vector<double> vals_B0nonSl;
	vector<double> vals_BpnonSl;
	vector<double> vals_B0snonSl;
	vector<double> vals_LbnonSl;

	vector<double> vec_B0Sl_stat, vec_BpSl_stat, vec_B0sSl_stat, vec_LbSl_stat,
	 							 vec_B0nonSl_stat, vec_BpnonSl_stat, vec_B0snonSl_stat,vec_LbnonSl_stat;

	 ReadFromFile(vec_B0Sl_stat,"/home/anpirtti/ultimate_generator/final_rescaling/indvBtoSLBr/calc_scale_factors/scale_factors/indvBtoSlBr/stat_err/B0Sl_err.txt");
	 ReadFromFile(vec_BpSl_stat,"/home/anpirtti/ultimate_generator/final_rescaling/indvBtoSLBr/calc_scale_factors/scale_factors/indvBtoSlBr/stat_err/BpSl_err.txt");
	 ReadFromFile(vec_B0sSl_stat,"/home/anpirtti/ultimate_generator/final_rescaling/indvBtoSLBr/calc_scale_factors/scale_factors/indvBtoSlBr/stat_err/B0sSl_err.txt");
	 ReadFromFile(vec_LbSl_stat,"/home/anpirtti/ultimate_generator/final_rescaling/indvBtoSLBr/calc_scale_factors/scale_factors/indvBtoSlBr/stat_err/LbSl_err.txt");
	 ReadFromFile(vec_B0nonSl_stat,"/home/anpirtti/ultimate_generator/final_rescaling/indvBtoSLBr/calc_scale_factors/scale_factors/indvBtoSlBr/stat_err/B0nonSl_err.txt");
	 ReadFromFile(vec_BpnonSl_stat,"/home/anpirtti/ultimate_generator/final_rescaling/indvBtoSLBr/calc_scale_factors/scale_factors/indvBtoSlBr/stat_err/BpnonSl_err.txt");
	 ReadFromFile(vec_B0snonSl_stat,"/home/anpirtti/ultimate_generator/final_rescaling/indvBtoSLBr/calc_scale_factors/scale_factors/indvBtoSlBr/stat_err/B0snonSl_err.txt");
	 ReadFromFile(vec_LbnonSl_stat,"/home/anpirtti/ultimate_generator/final_rescaling/indvBtoSLBr/calc_scale_factors/scale_factors/indvBtoSlBr/stat_err/LbnonSl_err.txt"); 

	vector<vector<double>> vec_sf_sem{vec_B0Sl_stat, vec_BpSl_stat, vec_B0sSl_stat, vec_LbSl_stat,
	 							 vec_B0nonSl_stat, vec_BpnonSl_stat, vec_B0snonSl_stat,vec_LbnonSl_stat};
	
	//removing the under- and overflow bin values
	for (int i = 0; i < vec_sf_sem.size(); i++) {
		vec_sf_sem[i].erase(vec_sf_sem[i].begin() + 0);
		vec_sf_sem[i].erase(vec_sf_sem[i].begin() + vec_sf_sem[i].size() - 1);
	}

  //Loop over the TProfile bins
  for (int i = 1; i < hMPFb->GetNbinsX()+1; i++){
		evtW_B0Sl.clear();
		evtW_BpSl.clear();
		evtW_B0sSl.clear();
		evtW_LbSl.clear();
		evtW_B0nonSl.clear();
		evtW_BpnonSl.clear();
		evtW_B0snonSl.clear();
		evtW_LbnonSl.clear();

		evtWs_B0Sl.clear();
		evtWs_BpSl.clear();
		evtWs_B0sSl.clear();
		evtWs_LbSl.clear();
		evtWs_B0nonSl.clear();
		evtWs_BpnonSl.clear();
		evtWs_B0snonSl.clear();
		evtWs_LbnonSl.clear();

		sem_B0Sl.clear();
		sem_BpSl.clear();
		sem_B0sSl.clear();
		sem_LbSl.clear();
		sem_B0nonSl.clear();
		sem_BpnonSl.clear();
		sem_B0snonSl.clear();
		sem_LbnonSl.clear();

		vals_B0Sl.clear();
		vals_BpSl.clear();
		vals_B0sSl.clear();
		vals_LbSl.clear();
		vals_B0nonSl.clear();
		vals_BpnonSl.clear();
		vals_B0snonSl.clear();
		vals_LbnonSl.clear();

		for (int j = 1; j < hMPFb->GetNbinsX()+1; j++) {
			string pr_ind = "_" + to_string(i) + "_" + to_string(j);

			prMPFb_B0Sl = pr2DMPFb_B0Sl->ProfileX(("B0Sl"+pr_ind).c_str(),j,j);
			prMPFb_BpSl = pr2DMPFb_BpSl->ProfileX(("BpSl"+pr_ind).c_str(),j,j);
			prMPFb_B0sSl = pr2DMPFb_B0sSl->ProfileX(("B0sSl"+pr_ind).c_str(),j,j);
			prMPFb_LbSl = pr2DMPFb_LbSl->ProfileX(("LbSl"+pr_ind).c_str(),j,j);
			prMPFb_B0nonSl = pr2DMPFb_B0nonSl->ProfileX(("B0nonSl"+pr_ind).c_str(),j,j);
			prMPFb_BpnonSl = pr2DMPFb_BpnonSl->ProfileX(("BpnonSl"+pr_ind).c_str(),j,j);
			prMPFb_B0snonSl = pr2DMPFb_B0snonSl->ProfileX(("B0snonSl"+pr_ind).c_str(),j,j);
			prMPFb_LbnonSl = pr2DMPFb_LbnonSl->ProfileX(("LbnonSl"+pr_ind).c_str(),j,j);

			prMPFb_B0Sl_s = pr2DMPFb_B0Sl_s->ProfileX(("B0Sl_s"+pr_ind).c_str(),j,j);
			prMPFb_BpSl_s = pr2DMPFb_BpSl_s->ProfileX(("BpSl_s"+pr_ind).c_str(),j,j);
			prMPFb_B0sSl_s = pr2DMPFb_B0sSl_s->ProfileX(("B0sSl_s"+pr_ind).c_str(),j,j);
			prMPFb_LbSl_s = pr2DMPFb_LbSl_s->ProfileX(("LbSl_s"+pr_ind).c_str(),j,j);
			prMPFb_B0nonSl_s = pr2DMPFb_B0nonSl_s->ProfileX(("B0nonSl_s"+pr_ind).c_str(),j,j);
			prMPFb_BpnonSl_s = pr2DMPFb_BpnonSl_s->ProfileX(("BpnonSl_s"+pr_ind).c_str(),j,j);
			prMPFb_B0snonSl_s = pr2DMPFb_B0snonSl_s->ProfileX(("B0snonSl_s"+pr_ind).c_str(),j,j);
			prMPFb_LbnonSl_s = pr2DMPFb_LbnonSl_s->ProfileX(("LbnonSl_s"+pr_ind).c_str(),j,j);

			evtW_B0Sl.push_back(prMPFb_B0Sl->GetBinEntries(i));
			evtW_BpSl.push_back(prMPFb_BpSl->GetBinEntries(i));
			evtW_B0sSl.push_back(prMPFb_B0sSl->GetBinEntries(i));
			evtW_LbSl.push_back(prMPFb_LbSl->GetBinEntries(i));
			evtW_B0nonSl.push_back(prMPFb_B0nonSl->GetBinEntries(i));
			evtW_BpnonSl.push_back(prMPFb_BpnonSl->GetBinEntries(i));
			evtW_B0snonSl.push_back(prMPFb_B0snonSl->GetBinEntries(i));
			evtW_LbnonSl.push_back(prMPFb_LbnonSl->GetBinEntries(i));

			evtWs_B0Sl.push_back(prMPFb_B0Sl_s->GetBinEntries(i));
			evtWs_BpSl.push_back(prMPFb_BpSl_s->GetBinEntries(i));
			evtWs_B0sSl.push_back(prMPFb_B0sSl_s->GetBinEntries(i));
			evtWs_LbSl.push_back(prMPFb_LbSl_s->GetBinEntries(i));
			evtWs_B0nonSl.push_back(prMPFb_B0nonSl_s->GetBinEntries(i));
			evtWs_BpnonSl.push_back(prMPFb_BpnonSl_s->GetBinEntries(i));
			evtWs_B0snonSl.push_back(prMPFb_B0snonSl_s->GetBinEntries(i));
			evtWs_LbnonSl.push_back(prMPFb_LbnonSl_s->GetBinEntries(i));

			sem_B0Sl.push_back(prMPFb_B0Sl->GetBinError(i));
			sem_BpSl.push_back(prMPFb_BpSl->GetBinError(i));
			sem_B0sSl.push_back(prMPFb_B0sSl->GetBinError(i));
			sem_LbSl.push_back(prMPFb_LbSl->GetBinError(i));
			sem_B0nonSl.push_back(prMPFb_B0nonSl->GetBinError(i));
			sem_BpnonSl.push_back(prMPFb_BpnonSl->GetBinError(i));
			sem_B0snonSl.push_back(prMPFb_B0snonSl->GetBinError(i));
			sem_LbnonSl.push_back(prMPFb_LbnonSl->GetBinError(i));

			vals_B0Sl.push_back(prMPFb_B0Sl->GetBinContent(i));
			vals_BpSl.push_back(prMPFb_BpSl->GetBinContent(i));
			vals_B0sSl.push_back(prMPFb_B0sSl->GetBinContent(i));
			vals_LbSl.push_back(prMPFb_LbSl->GetBinContent(i));
			vals_B0nonSl.push_back(prMPFb_B0nonSl->GetBinContent(i));
			vals_BpnonSl.push_back(prMPFb_BpnonSl->GetBinContent(i));
			vals_B0snonSl.push_back(prMPFb_B0snonSl->GetBinContent(i));
			vals_LbnonSl.push_back(prMPFb_LbnonSl->GetBinContent(i));

			delete prMPFb_B0Sl;
			delete prMPFb_BpSl;
			delete prMPFb_B0sSl;
			delete prMPFb_LbSl;
			delete prMPFb_B0nonSl;
			delete prMPFb_BpnonSl;
			delete prMPFb_B0snonSl; 
			delete prMPFb_LbnonSl;

			delete prMPFb_B0Sl_s;
			delete prMPFb_BpSl_s;
			delete prMPFb_B0sSl_s;
			delete prMPFb_LbSl_s;
			delete prMPFb_B0nonSl_s;
			delete prMPFb_BpnonSl_s;
			delete prMPFb_B0snonSl_s;
			delete prMPFb_LbnonSl_s;
		}

		 
		vector<vector<double>> vec_evtW{evtW_B0Sl, evtW_BpSl, evtW_B0sSl, evtW_LbSl,
																	  evtW_B0nonSl, evtW_BpnonSl, evtW_B0snonSl, evtW_LbnonSl};

		vector<vector<double>> vec_evtWs{evtWs_B0Sl, evtWs_BpSl, evtWs_B0sSl, evtWs_LbSl,
																	  evtWs_B0nonSl, evtWs_BpnonSl, evtWs_B0snonSl, evtWs_LbnonSl};

		vector<vector<double>> vec_sem{sem_B0Sl, sem_BpSl, sem_B0sSl, sem_LbSl,
																	  sem_B0nonSl, sem_BpnonSl, sem_B0snonSl, sem_LbnonSl};

		vector<vector<double>> vec_vals{vals_B0Sl, vals_BpSl, vals_B0sSl, vals_LbSl,
																	  vals_B0nonSl, vals_BpnonSl, vals_B0snonSl, vals_LbnonSl};

		//set small number so that we dont divide by zero 
		evtWtot_B0Sl = 1e-10;
		evtWtot_BpSl = 1e-10;
		evtWtot_B0sSl = 1e-10;
		evtWtot_LbSl = 1e-10;
		evtWtot_B0nonSl = 1e-10;
		evtWtot_BpnonSl = 1e-10;
		evtWtot_B0snonSl = 1e-10;
		evtWtot_LbnonSl = 1e-10;

		evtWtots_B0Sl = 1e-10;
		evtWtots_BpSl = 1e-10;
		evtWtots_B0sSl = 1e-10;
		evtWtots_LbSl = 1e-10;
		evtWtots_B0nonSl = 1e-10;
		evtWtots_BpnonSl = 1e-10;
		evtWtots_B0snonSl = 1e-10;
		evtWtots_LbnonSl = 1e-10;

		for (double x : evtW_B0Sl) evtWtot_B0Sl += x;	
		for (double x : evtW_BpSl) evtWtot_BpSl += x;
		for (double x : evtW_B0sSl) evtWtot_B0sSl += x;
		for (double x : evtW_LbSl) evtWtot_LbSl += x;
		for (double x : evtW_B0nonSl) evtWtot_B0nonSl += x;
		for (double x : evtW_BpnonSl) evtWtot_BpnonSl += x;
		for (double x : evtW_B0snonSl) evtWtot_B0snonSl += x;
		for (double x : evtW_LbnonSl) evtWtot_LbnonSl += x;

		for (double x : evtWs_B0Sl) evtWtots_B0Sl += x;
		for (double x : evtWs_BpSl) evtWtots_BpSl += x;
		for (double x : evtWs_B0sSl) evtWtots_B0sSl += x;
		for (double x : evtWs_LbSl) evtWtots_LbSl += x;
		for (double x : evtWs_B0nonSl) evtWtots_B0nonSl += x;
		for (double x : evtWs_BpnonSl) evtWtots_BpnonSl += x;
		for (double x : evtWs_B0snonSl) evtWtots_B0snonSl += x;
		for (double x : evtWs_LbnonSl) evtWtots_LbnonSl += x;

		evtWtot = evtWtot_B0Sl + evtWtot_BpSl + evtWtot_B0sSl + evtWtot_LbSl +
						  evtWtot_B0nonSl +evtWtot_BpnonSl + evtWtot_B0snonSl + evtWtot_LbnonSl;

		evtWtots = evtWtots_B0Sl + evtWtots_BpSl + evtWtots_B0sSl + evtWtots_LbSl +
							 evtWtots_B0nonSl +evtWtots_BpnonSl + evtWtots_B0snonSl + evtWtots_LbnonSl;

		errorb = 0;
		for (int j = 0; j < hMPFb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hMPFb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}

		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb->GetBinContent(i) == 0) diffb->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb->SetBinError(i,errorb);
  }

	//############### plus variation ####################
  //Loop over the TProfile bins
  for (int i = 1; i < hMPFb->GetNbinsX()+1; i++){
		evtW_B0Sl.clear();
		evtW_BpSl.clear();
		evtW_B0sSl.clear();
		evtW_LbSl.clear();
		evtW_B0nonSl.clear();
		evtW_BpnonSl.clear();
		evtW_B0snonSl.clear();
		evtW_LbnonSl.clear();

		evtWs_B0Sl.clear();
		evtWs_BpSl.clear();
		evtWs_B0sSl.clear();
		evtWs_LbSl.clear();
		evtWs_B0nonSl.clear();
		evtWs_BpnonSl.clear();
		evtWs_B0snonSl.clear();
		evtWs_LbnonSl.clear();

		sem_B0Sl.clear();
		sem_BpSl.clear();
		sem_B0sSl.clear();
		sem_LbSl.clear();
		sem_B0nonSl.clear();
		sem_BpnonSl.clear();
		sem_B0snonSl.clear();
		sem_LbnonSl.clear();

		vals_B0Sl.clear();
		vals_BpSl.clear();
		vals_B0sSl.clear();
		vals_LbSl.clear();
		vals_B0nonSl.clear();
		vals_BpnonSl.clear();
		vals_B0snonSl.clear();
		vals_LbnonSl.clear();

		for (int j = 1; j < hMPFb->GetNbinsX()+1; j++) {
			string pr_ind = "_" + to_string(i) + "_" + to_string(j);

			prMPFb_B0Sl = pr2DMPFb_B0Sl->ProfileX(("B0Sl"+pr_ind).c_str(),j,j);
			prMPFb_BpSl = pr2DMPFb_BpSl->ProfileX(("BpSl"+pr_ind).c_str(),j,j);
			prMPFb_B0sSl = pr2DMPFb_B0sSl->ProfileX(("B0sSl"+pr_ind).c_str(),j,j);
			prMPFb_LbSl = pr2DMPFb_LbSl->ProfileX(("LbSl"+pr_ind).c_str(),j,j);
			prMPFb_B0nonSl = pr2DMPFb_B0nonSl->ProfileX(("B0nonSl"+pr_ind).c_str(),j,j);
			prMPFb_BpnonSl = pr2DMPFb_BpnonSl->ProfileX(("BpnonSl"+pr_ind).c_str(),j,j);
			prMPFb_B0snonSl = pr2DMPFb_B0snonSl->ProfileX(("B0snonSl"+pr_ind).c_str(),j,j);
			prMPFb_LbnonSl = pr2DMPFb_LbnonSl->ProfileX(("LbnonSl"+pr_ind).c_str(),j,j);

			prMPFb_B0Sl_s_p = pr2DMPFb_B0Sl_s_p->ProfileX(("B0Sl_s"+pr_ind).c_str(),j,j);
			prMPFb_BpSl_s_p = pr2DMPFb_BpSl_s_p->ProfileX(("BpSl_s"+pr_ind).c_str(),j,j);
			prMPFb_B0sSl_s_p = pr2DMPFb_B0sSl_s_p->ProfileX(("B0sSl_s"+pr_ind).c_str(),j,j);
			prMPFb_LbSl_s_p = pr2DMPFb_LbSl_s_p->ProfileX(("LbSl_s"+pr_ind).c_str(),j,j);
			prMPFb_B0nonSl_s_p = pr2DMPFb_B0nonSl_s_p->ProfileX(("B0nonSl_s"+pr_ind).c_str(),j,j);
			prMPFb_BpnonSl_s_p = pr2DMPFb_BpnonSl_s_p->ProfileX(("BpnonSl_s"+pr_ind).c_str(),j,j);
			prMPFb_B0snonSl_s_p = pr2DMPFb_B0snonSl_s_p->ProfileX(("B0snonSl_s"+pr_ind).c_str(),j,j);
			prMPFb_LbnonSl_s_p = pr2DMPFb_LbnonSl_s_p->ProfileX(("LbnonSl_s"+pr_ind).c_str(),j,j);

			evtW_B0Sl.push_back(prMPFb_B0Sl->GetBinEntries(i));
			evtW_BpSl.push_back(prMPFb_BpSl->GetBinEntries(i));
			evtW_B0sSl.push_back(prMPFb_B0sSl->GetBinEntries(i));
			evtW_LbSl.push_back(prMPFb_LbSl->GetBinEntries(i));
			evtW_B0nonSl.push_back(prMPFb_B0nonSl->GetBinEntries(i));
			evtW_BpnonSl.push_back(prMPFb_BpnonSl->GetBinEntries(i));
			evtW_B0snonSl.push_back(prMPFb_B0snonSl->GetBinEntries(i));
			evtW_LbnonSl.push_back(prMPFb_LbnonSl->GetBinEntries(i));

			evtWs_B0Sl.push_back(prMPFb_B0Sl_s_p->GetBinEntries(i));
			evtWs_BpSl.push_back(prMPFb_BpSl_s_p->GetBinEntries(i));
			evtWs_B0sSl.push_back(prMPFb_B0sSl_s_p->GetBinEntries(i));
			evtWs_LbSl.push_back(prMPFb_LbSl_s_p->GetBinEntries(i));
			evtWs_B0nonSl.push_back(prMPFb_B0nonSl_s_p->GetBinEntries(i));
			evtWs_BpnonSl.push_back(prMPFb_BpnonSl_s_p->GetBinEntries(i));
			evtWs_B0snonSl.push_back(prMPFb_B0snonSl_s_p->GetBinEntries(i));
			evtWs_LbnonSl.push_back(prMPFb_LbnonSl_s_p->GetBinEntries(i));

			sem_B0Sl.push_back(prMPFb_B0Sl->GetBinError(i));
			sem_BpSl.push_back(prMPFb_BpSl->GetBinError(i));
			sem_B0sSl.push_back(prMPFb_B0sSl->GetBinError(i));
			sem_LbSl.push_back(prMPFb_LbSl->GetBinError(i));
			sem_B0nonSl.push_back(prMPFb_B0nonSl->GetBinError(i));
			sem_BpnonSl.push_back(prMPFb_BpnonSl->GetBinError(i));
			sem_B0snonSl.push_back(prMPFb_B0snonSl->GetBinError(i));
			sem_LbnonSl.push_back(prMPFb_LbnonSl->GetBinError(i));

			vals_B0Sl.push_back(prMPFb_B0Sl->GetBinContent(i));
			vals_BpSl.push_back(prMPFb_BpSl->GetBinContent(i));
			vals_B0sSl.push_back(prMPFb_B0sSl->GetBinContent(i));
			vals_LbSl.push_back(prMPFb_LbSl->GetBinContent(i));
			vals_B0nonSl.push_back(prMPFb_B0nonSl->GetBinContent(i));
			vals_BpnonSl.push_back(prMPFb_BpnonSl->GetBinContent(i));
			vals_B0snonSl.push_back(prMPFb_B0snonSl->GetBinContent(i));
			vals_LbnonSl.push_back(prMPFb_LbnonSl->GetBinContent(i));

			delete prMPFb_B0Sl;
			delete prMPFb_BpSl;
			delete prMPFb_B0sSl;
			delete prMPFb_LbSl;
			delete prMPFb_B0nonSl;
			delete prMPFb_BpnonSl;
			delete prMPFb_B0snonSl; 
			delete prMPFb_LbnonSl;

			delete prMPFb_B0Sl_s_p;
			delete prMPFb_BpSl_s_p;
			delete prMPFb_B0sSl_s_p;
			delete prMPFb_LbSl_s_p;
			delete prMPFb_B0nonSl_s_p;
			delete prMPFb_BpnonSl_s_p;
			delete prMPFb_B0snonSl_s_p;
			delete prMPFb_LbnonSl_s_p;
		}

		 
		vector<vector<double>> vec_evtW{evtW_B0Sl, evtW_BpSl, evtW_B0sSl, evtW_LbSl,
																	  evtW_B0nonSl, evtW_BpnonSl, evtW_B0snonSl, evtW_LbnonSl};

		vector<vector<double>> vec_evtWs{evtWs_B0Sl, evtWs_BpSl, evtWs_B0sSl, evtWs_LbSl,
																	  evtWs_B0nonSl, evtWs_BpnonSl, evtWs_B0snonSl, evtWs_LbnonSl};

		vector<vector<double>> vec_sem{sem_B0Sl, sem_BpSl, sem_B0sSl, sem_LbSl,
																	  sem_B0nonSl, sem_BpnonSl, sem_B0snonSl, sem_LbnonSl};

		vector<vector<double>> vec_vals{vals_B0Sl, vals_BpSl, vals_B0sSl, vals_LbSl,
																	  vals_B0nonSl, vals_BpnonSl, vals_B0snonSl, vals_LbnonSl};

		//set small number so that we dont divide by zero 
		evtWtot_B0Sl = 1e-10;
		evtWtot_BpSl = 1e-10;
		evtWtot_B0sSl = 1e-10;
		evtWtot_LbSl = 1e-10;
		evtWtot_B0nonSl = 1e-10;
		evtWtot_BpnonSl = 1e-10;
		evtWtot_B0snonSl = 1e-10;
		evtWtot_LbnonSl = 1e-10;

		evtWtots_B0Sl = 1e-10;
		evtWtots_BpSl = 1e-10;
		evtWtots_B0sSl = 1e-10;
		evtWtots_LbSl = 1e-10;
		evtWtots_B0nonSl = 1e-10;
		evtWtots_BpnonSl = 1e-10;
		evtWtots_B0snonSl = 1e-10;
		evtWtots_LbnonSl = 1e-10;

		for (double x : evtW_B0Sl) evtWtot_B0Sl += x;	
		for (double x : evtW_BpSl) evtWtot_BpSl += x;
		for (double x : evtW_B0sSl) evtWtot_B0sSl += x;
		for (double x : evtW_LbSl) evtWtot_LbSl += x;
		for (double x : evtW_B0nonSl) evtWtot_B0nonSl += x;
		for (double x : evtW_BpnonSl) evtWtot_BpnonSl += x;
		for (double x : evtW_B0snonSl) evtWtot_B0snonSl += x;
		for (double x : evtW_LbnonSl) evtWtot_LbnonSl += x;

		for (double x : evtWs_B0Sl) evtWtots_B0Sl += x;
		for (double x : evtWs_BpSl) evtWtots_BpSl += x;
		for (double x : evtWs_B0sSl) evtWtots_B0sSl += x;
		for (double x : evtWs_LbSl) evtWtots_LbSl += x;
		for (double x : evtWs_B0nonSl) evtWtots_B0nonSl += x;
		for (double x : evtWs_BpnonSl) evtWtots_BpnonSl += x;
		for (double x : evtWs_B0snonSl) evtWtots_B0snonSl += x;
		for (double x : evtWs_LbnonSl) evtWtots_LbnonSl += x;

		evtWtot = evtWtot_B0Sl + evtWtot_BpSl + evtWtot_B0sSl + evtWtot_LbSl +
						  evtWtot_B0nonSl +evtWtot_BpnonSl + evtWtot_B0snonSl + evtWtot_LbnonSl;

		evtWtots = evtWtots_B0Sl + evtWtots_BpSl + evtWtots_B0sSl + evtWtots_LbSl +
							 evtWtots_B0nonSl +evtWtots_BpnonSl + evtWtots_B0snonSl + evtWtots_LbnonSl;

		errorb = 0;
		for (int j = 0; j < hMPFb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hMPFb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}

		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb_p->GetBinContent(i) == 0) diffb_p->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb_p->SetBinError(i,errorb);
  }

	//############### minus variation ####################
  //Loop over the TProfile bins
  for (int i = 1; i < hMPFb->GetNbinsX()+1; i++){
		evtW_B0Sl.clear();
		evtW_BpSl.clear();
		evtW_B0sSl.clear();
		evtW_LbSl.clear();
		evtW_B0nonSl.clear();
		evtW_BpnonSl.clear();
		evtW_B0snonSl.clear();
		evtW_LbnonSl.clear();

		evtWs_B0Sl.clear();
		evtWs_BpSl.clear();
		evtWs_B0sSl.clear();
		evtWs_LbSl.clear();
		evtWs_B0nonSl.clear();
		evtWs_BpnonSl.clear();
		evtWs_B0snonSl.clear();
		evtWs_LbnonSl.clear();

		sem_B0Sl.clear();
		sem_BpSl.clear();
		sem_B0sSl.clear();
		sem_LbSl.clear();
		sem_B0nonSl.clear();
		sem_BpnonSl.clear();
		sem_B0snonSl.clear();
		sem_LbnonSl.clear();

		vals_B0Sl.clear();
		vals_BpSl.clear();
		vals_B0sSl.clear();
		vals_LbSl.clear();
		vals_B0nonSl.clear();
		vals_BpnonSl.clear();
		vals_B0snonSl.clear();
		vals_LbnonSl.clear();

		for (int j = 1; j < hMPFb->GetNbinsX()+1; j++) {
			string pr_ind = "_" + to_string(i) + "_" + to_string(j);

			prMPFb_B0Sl = pr2DMPFb_B0Sl->ProfileX(("B0Sl"+pr_ind).c_str(),j,j);
			prMPFb_BpSl = pr2DMPFb_BpSl->ProfileX(("BpSl"+pr_ind).c_str(),j,j);
			prMPFb_B0sSl = pr2DMPFb_B0sSl->ProfileX(("B0sSl"+pr_ind).c_str(),j,j);
			prMPFb_LbSl = pr2DMPFb_LbSl->ProfileX(("LbSl"+pr_ind).c_str(),j,j);
			prMPFb_B0nonSl = pr2DMPFb_B0nonSl->ProfileX(("B0nonSl"+pr_ind).c_str(),j,j);
			prMPFb_BpnonSl = pr2DMPFb_BpnonSl->ProfileX(("BpnonSl"+pr_ind).c_str(),j,j);
			prMPFb_B0snonSl = pr2DMPFb_B0snonSl->ProfileX(("B0snonSl"+pr_ind).c_str(),j,j);
			prMPFb_LbnonSl = pr2DMPFb_LbnonSl->ProfileX(("LbnonSl"+pr_ind).c_str(),j,j);

			prMPFb_B0Sl_s_m = pr2DMPFb_B0Sl_s_m->ProfileX(("B0Sl_s"+pr_ind).c_str(),j,j);
			prMPFb_BpSl_s_m = pr2DMPFb_BpSl_s_m->ProfileX(("BpSl_s"+pr_ind).c_str(),j,j);
			prMPFb_B0sSl_s_m = pr2DMPFb_B0sSl_s_m->ProfileX(("B0sSl_s"+pr_ind).c_str(),j,j);
			prMPFb_LbSl_s_m = pr2DMPFb_LbSl_s_m->ProfileX(("LbSl_s"+pr_ind).c_str(),j,j);
			prMPFb_B0nonSl_s_m = pr2DMPFb_B0nonSl_s_m->ProfileX(("B0nonSl_s"+pr_ind).c_str(),j,j);
			prMPFb_BpnonSl_s_m = pr2DMPFb_BpnonSl_s_m->ProfileX(("BpnonSl_s"+pr_ind).c_str(),j,j);
			prMPFb_B0snonSl_s_m = pr2DMPFb_B0snonSl_s_m->ProfileX(("B0snonSl_s"+pr_ind).c_str(),j,j);
			prMPFb_LbnonSl_s_m = pr2DMPFb_LbnonSl_s_m->ProfileX(("LbnonSl_s"+pr_ind).c_str(),j,j);

			evtW_B0Sl.push_back(prMPFb_B0Sl->GetBinEntries(i));
			evtW_BpSl.push_back(prMPFb_BpSl->GetBinEntries(i));
			evtW_B0sSl.push_back(prMPFb_B0sSl->GetBinEntries(i));
			evtW_LbSl.push_back(prMPFb_LbSl->GetBinEntries(i));
			evtW_B0nonSl.push_back(prMPFb_B0nonSl->GetBinEntries(i));
			evtW_BpnonSl.push_back(prMPFb_BpnonSl->GetBinEntries(i));
			evtW_B0snonSl.push_back(prMPFb_B0snonSl->GetBinEntries(i));
			evtW_LbnonSl.push_back(prMPFb_LbnonSl->GetBinEntries(i));

			evtWs_B0Sl.push_back(prMPFb_B0Sl_s_m->GetBinEntries(i));
			evtWs_BpSl.push_back(prMPFb_BpSl_s_m->GetBinEntries(i));
			evtWs_B0sSl.push_back(prMPFb_B0sSl_s_m->GetBinEntries(i));
			evtWs_LbSl.push_back(prMPFb_LbSl_s_m->GetBinEntries(i));
			evtWs_B0nonSl.push_back(prMPFb_B0nonSl_s_m->GetBinEntries(i));
			evtWs_BpnonSl.push_back(prMPFb_BpnonSl_s_m->GetBinEntries(i));
			evtWs_B0snonSl.push_back(prMPFb_B0snonSl_s_m->GetBinEntries(i));
			evtWs_LbnonSl.push_back(prMPFb_LbnonSl_s_m->GetBinEntries(i));

			sem_B0Sl.push_back(prMPFb_B0Sl->GetBinError(i));
			sem_BpSl.push_back(prMPFb_BpSl->GetBinError(i));
			sem_B0sSl.push_back(prMPFb_B0sSl->GetBinError(i));
			sem_LbSl.push_back(prMPFb_LbSl->GetBinError(i));
			sem_B0nonSl.push_back(prMPFb_B0nonSl->GetBinError(i));
			sem_BpnonSl.push_back(prMPFb_BpnonSl->GetBinError(i));
			sem_B0snonSl.push_back(prMPFb_B0snonSl->GetBinError(i));
			sem_LbnonSl.push_back(prMPFb_LbnonSl->GetBinError(i));

			vals_B0Sl.push_back(prMPFb_B0Sl->GetBinContent(i));
			vals_BpSl.push_back(prMPFb_BpSl->GetBinContent(i));
			vals_B0sSl.push_back(prMPFb_B0sSl->GetBinContent(i));
			vals_LbSl.push_back(prMPFb_LbSl->GetBinContent(i));
			vals_B0nonSl.push_back(prMPFb_B0nonSl->GetBinContent(i));
			vals_BpnonSl.push_back(prMPFb_BpnonSl->GetBinContent(i));
			vals_B0snonSl.push_back(prMPFb_B0snonSl->GetBinContent(i));
			vals_LbnonSl.push_back(prMPFb_LbnonSl->GetBinContent(i));

			delete prMPFb_B0Sl;
			delete prMPFb_BpSl;
			delete prMPFb_B0sSl;
			delete prMPFb_LbSl;
			delete prMPFb_B0nonSl;
			delete prMPFb_BpnonSl;
			delete prMPFb_B0snonSl; 
			delete prMPFb_LbnonSl;

			delete prMPFb_B0Sl_s_m;
			delete prMPFb_BpSl_s_m;
			delete prMPFb_B0sSl_s_m;
			delete prMPFb_LbSl_s_m;
			delete prMPFb_B0nonSl_s_m;
			delete prMPFb_BpnonSl_s_m;
			delete prMPFb_B0snonSl_s_m;
			delete prMPFb_LbnonSl_s_m;
		}

		 
		vector<vector<double>> vec_evtW{evtW_B0Sl, evtW_BpSl, evtW_B0sSl, evtW_LbSl,
																	  evtW_B0nonSl, evtW_BpnonSl, evtW_B0snonSl, evtW_LbnonSl};

		vector<vector<double>> vec_evtWs{evtWs_B0Sl, evtWs_BpSl, evtWs_B0sSl, evtWs_LbSl,
																	  evtWs_B0nonSl, evtWs_BpnonSl, evtWs_B0snonSl, evtWs_LbnonSl};

		vector<vector<double>> vec_sem{sem_B0Sl, sem_BpSl, sem_B0sSl, sem_LbSl,
																	  sem_B0nonSl, sem_BpnonSl, sem_B0snonSl, sem_LbnonSl};

		vector<vector<double>> vec_vals{vals_B0Sl, vals_BpSl, vals_B0sSl, vals_LbSl,
																	  vals_B0nonSl, vals_BpnonSl, vals_B0snonSl, vals_LbnonSl};

		//set small number so that we dont divide by zero 
		evtWtot_B0Sl = 1e-10;
		evtWtot_BpSl = 1e-10;
		evtWtot_B0sSl = 1e-10;
		evtWtot_LbSl = 1e-10;
		evtWtot_B0nonSl = 1e-10;
		evtWtot_BpnonSl = 1e-10;
		evtWtot_B0snonSl = 1e-10;
		evtWtot_LbnonSl = 1e-10;

		evtWtots_B0Sl = 1e-10;
		evtWtots_BpSl = 1e-10;
		evtWtots_B0sSl = 1e-10;
		evtWtots_LbSl = 1e-10;
		evtWtots_B0nonSl = 1e-10;
		evtWtots_BpnonSl = 1e-10;
		evtWtots_B0snonSl = 1e-10;
		evtWtots_LbnonSl = 1e-10;

		for (double x : evtW_B0Sl) evtWtot_B0Sl += x;	
		for (double x : evtW_BpSl) evtWtot_BpSl += x;
		for (double x : evtW_B0sSl) evtWtot_B0sSl += x;
		for (double x : evtW_LbSl) evtWtot_LbSl += x;
		for (double x : evtW_B0nonSl) evtWtot_B0nonSl += x;
		for (double x : evtW_BpnonSl) evtWtot_BpnonSl += x;
		for (double x : evtW_B0snonSl) evtWtot_B0snonSl += x;
		for (double x : evtW_LbnonSl) evtWtot_LbnonSl += x;

		for (double x : evtWs_B0Sl) evtWtots_B0Sl += x;
		for (double x : evtWs_BpSl) evtWtots_BpSl += x;
		for (double x : evtWs_B0sSl) evtWtots_B0sSl += x;
		for (double x : evtWs_LbSl) evtWtots_LbSl += x;
		for (double x : evtWs_B0nonSl) evtWtots_B0nonSl += x;
		for (double x : evtWs_BpnonSl) evtWtots_BpnonSl += x;
		for (double x : evtWs_B0snonSl) evtWtots_B0snonSl += x;
		for (double x : evtWs_LbnonSl) evtWtots_LbnonSl += x;

		evtWtot = evtWtot_B0Sl + evtWtot_BpSl + evtWtot_B0sSl + evtWtot_LbSl +
						  evtWtot_B0nonSl +evtWtot_BpnonSl + evtWtot_B0snonSl + evtWtot_LbnonSl;

		evtWtots = evtWtots_B0Sl + evtWtots_BpSl + evtWtots_B0sSl + evtWtots_LbSl +
							 evtWtots_B0nonSl +evtWtots_BpnonSl + evtWtots_B0snonSl + evtWtots_LbnonSl;

		errorb = 0;
		for (int j = 0; j < hMPFb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hMPFb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}

		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb_m->GetBinContent(i) == 0) diffb_m->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb_m->SetBinError(i,errorb);
  }  

	//Scale to the percentages
  diffb->Scale(100.0);
  diffb_p->Scale(100.0);
  diffb_m->Scale(100.0);

	MPFb_map["diffb"] = diffb;
	MPFb_map["diffb_p"] = diffb_p;
	MPFb_map["diffb_m"] = diffb_m;

	return MPFb_map;
}

map<string, TH1D*> pTbal_indvBtoSLBr() {
	map <string, TH1D*> pTbalb_map;

  TFile* f_normal = TFile::Open(OpenName.c_str());
  TFile* f_scaled = TFile::Open(OpenName_indvBtoSLBr.c_str());
  TFile* f_scaled_p = TFile::Open(OpenName_indvBtoSLBr_p.c_str());
  TFile* f_scaled_m = TFile::Open(OpenName_indvBtoSLBr_m.c_str());

	TProfile *prpTbalb;
	TProfile *prpTbalb_s;
	TProfile *prpTbalb_s_p;
	TProfile *prpTbalb_s_m;

  //Read the normal sample
  f_normal->GetObject("prpTbalb",   prpTbalb);

  //Read the weighted sample
  f_scaled->GetObject("prpTbalb",   prpTbalb_s);
  f_scaled_p->GetObject("prpTbalb",   prpTbalb_s_p);
  f_scaled_m->GetObject("prpTbalb",   prpTbalb_s_m);

	//2D Tprofiles for the errorb calculations
	TProfile2D* pr2DpTbalb;

	TProfile2D* pr2DpTbalb_B0Sl;
	TProfile2D* pr2DpTbalb_BpSl;
	TProfile2D* pr2DpTbalb_B0sSl;
	TProfile2D* pr2DpTbalb_LbSl;
	TProfile2D* pr2DpTbalb_B0nonSl;
	TProfile2D* pr2DpTbalb_BpnonSl;
	TProfile2D* pr2DpTbalb_B0snonSl;
	TProfile2D* pr2DpTbalb_LbnonSl;

  //The sample with the additional weighting (*_s)
	TProfile2D* pr2DpTbalb_s;

	TProfile2D* pr2DpTbalb_B0Sl_s;
	TProfile2D* pr2DpTbalb_BpSl_s;
	TProfile2D* pr2DpTbalb_B0sSl_s;
	TProfile2D* pr2DpTbalb_LbSl_s;
	TProfile2D* pr2DpTbalb_B0nonSl_s;
	TProfile2D* pr2DpTbalb_BpnonSl_s;
	TProfile2D* pr2DpTbalb_B0snonSl_s;
	TProfile2D* pr2DpTbalb_LbnonSl_s;

	TProfile2D* pr2DpTbalb_s_p;

	TProfile2D* pr2DpTbalb_B0Sl_s_p;
	TProfile2D* pr2DpTbalb_BpSl_s_p;
	TProfile2D* pr2DpTbalb_B0sSl_s_p;
	TProfile2D* pr2DpTbalb_LbSl_s_p;
	TProfile2D* pr2DpTbalb_B0nonSl_s_p;
	TProfile2D* pr2DpTbalb_BpnonSl_s_p;
	TProfile2D* pr2DpTbalb_B0snonSl_s_p;
	TProfile2D* pr2DpTbalb_LbnonSl_s_p;

	TProfile2D* pr2DpTbalb_s_m;

	TProfile2D* pr2DpTbalb_B0Sl_s_m;
	TProfile2D* pr2DpTbalb_BpSl_s_m;
	TProfile2D* pr2DpTbalb_B0sSl_s_m;
	TProfile2D* pr2DpTbalb_LbSl_s_m;
	TProfile2D* pr2DpTbalb_B0nonSl_s_m;
	TProfile2D* pr2DpTbalb_BpnonSl_s_m;
	TProfile2D* pr2DpTbalb_B0snonSl_s_m;
	TProfile2D* pr2DpTbalb_LbnonSl_s_m;

	//Tprofiles for the errorb calculations
	TProfile* prpTbalb_B0Sl;
	TProfile* prpTbalb_BpSl;
	TProfile* prpTbalb_B0sSl;
	TProfile* prpTbalb_LbSl;
	TProfile* prpTbalb_B0nonSl;
	TProfile* prpTbalb_BpnonSl;
	TProfile* prpTbalb_B0snonSl;
	TProfile* prpTbalb_LbnonSl;

  //The sample with the additional weighting (*_s)
	TProfile* prpTbalb_B0Sl_s;
	TProfile* prpTbalb_BpSl_s;
	TProfile* prpTbalb_B0sSl_s;
	TProfile* prpTbalb_LbSl_s;
	TProfile* prpTbalb_B0nonSl_s;
	TProfile* prpTbalb_BpnonSl_s;
	TProfile* prpTbalb_B0snonSl_s;
	TProfile* prpTbalb_LbnonSl_s;

	TProfile* prpTbalb_B0Sl_s_p;
	TProfile* prpTbalb_BpSl_s_p;
	TProfile* prpTbalb_B0sSl_s_p;
	TProfile* prpTbalb_LbSl_s_p;
	TProfile* prpTbalb_B0nonSl_s_p;
	TProfile* prpTbalb_BpnonSl_s_p;
	TProfile* prpTbalb_B0snonSl_s_p;
	TProfile* prpTbalb_LbnonSl_s_p;

	TProfile* prpTbalb_B0Sl_s_m;
	TProfile* prpTbalb_BpSl_s_m;
	TProfile* prpTbalb_B0sSl_s_m;
	TProfile* prpTbalb_LbSl_s_m;
	TProfile* prpTbalb_B0nonSl_s_m;
	TProfile* prpTbalb_BpnonSl_s_m;
	TProfile* prpTbalb_B0snonSl_s_m;
	TProfile* prpTbalb_LbnonSl_s_m;

  //Read the normal sample
	f_normal->GetObject("pr2DpTbalb",pr2DpTbalb);

	f_normal->GetObject("pr2DpTbalb_B0Sl",pr2DpTbalb_B0Sl);
	f_normal->GetObject("pr2DpTbalb_BpSl",pr2DpTbalb_BpSl);
	f_normal->GetObject("pr2DpTbalb_B0sSl",pr2DpTbalb_B0sSl);
	f_normal->GetObject("pr2DpTbalb_LbSl",pr2DpTbalb_LbSl);
	f_normal->GetObject("pr2DpTbalb_B0nonSl",pr2DpTbalb_B0nonSl);
	f_normal->GetObject("pr2DpTbalb_BpnonSl",pr2DpTbalb_BpnonSl);
	f_normal->GetObject("pr2DpTbalb_B0snonSl",pr2DpTbalb_B0snonSl);
	f_normal->GetObject("pr2DpTbalb_LbnonSl",pr2DpTbalb_LbnonSl);

  //Read the weighted sample
	f_scaled->GetObject("pr2DpTbalb",pr2DpTbalb_s);

	f_scaled->GetObject("pr2DpTbalb_B0Sl",pr2DpTbalb_B0Sl_s);
	f_scaled->GetObject("pr2DpTbalb_BpSl",pr2DpTbalb_BpSl_s);
	f_scaled->GetObject("pr2DpTbalb_B0sSl",pr2DpTbalb_B0sSl_s);
	f_scaled->GetObject("pr2DpTbalb_LbSl",pr2DpTbalb_LbSl_s);
	f_scaled->GetObject("pr2DpTbalb_B0nonSl",pr2DpTbalb_B0nonSl_s);
	f_scaled->GetObject("pr2DpTbalb_BpnonSl",pr2DpTbalb_BpnonSl_s);
	f_scaled->GetObject("pr2DpTbalb_B0snonSl",pr2DpTbalb_B0snonSl_s);
	f_scaled->GetObject("pr2DpTbalb_LbnonSl",pr2DpTbalb_LbnonSl_s);

	f_scaled_p->GetObject("pr2DpTbalb",pr2DpTbalb_s_p);

	f_scaled_p->GetObject("pr2DpTbalb_B0Sl",pr2DpTbalb_B0Sl_s_p);
	f_scaled_p->GetObject("pr2DpTbalb_BpSl",pr2DpTbalb_BpSl_s_p);
	f_scaled_p->GetObject("pr2DpTbalb_B0sSl",pr2DpTbalb_B0sSl_s_p);
	f_scaled_p->GetObject("pr2DpTbalb_LbSl",pr2DpTbalb_LbSl_s_p);
	f_scaled_p->GetObject("pr2DpTbalb_B0nonSl",pr2DpTbalb_B0nonSl_s_p);
	f_scaled_p->GetObject("pr2DpTbalb_BpnonSl",pr2DpTbalb_BpnonSl_s_p);
	f_scaled_p->GetObject("pr2DpTbalb_B0snonSl",pr2DpTbalb_B0snonSl_s_p);
	f_scaled_p->GetObject("pr2DpTbalb_LbnonSl",pr2DpTbalb_LbnonSl_s_p);

	f_scaled_m->GetObject("pr2DpTbalb",pr2DpTbalb_s_m);

	f_scaled_m->GetObject("pr2DpTbalb_B0Sl",pr2DpTbalb_B0Sl_s_m);
	f_scaled_m->GetObject("pr2DpTbalb_BpSl",pr2DpTbalb_BpSl_s_m);
	f_scaled_m->GetObject("pr2DpTbalb_B0sSl",pr2DpTbalb_B0sSl_s_m);
	f_scaled_m->GetObject("pr2DpTbalb_LbSl",pr2DpTbalb_LbSl_s_m);
	f_scaled_m->GetObject("pr2DpTbalb_B0nonSl",pr2DpTbalb_B0nonSl_s_m);
	f_scaled_m->GetObject("pr2DpTbalb_BpnonSl",pr2DpTbalb_BpnonSl_s_m);
	f_scaled_m->GetObject("pr2DpTbalb_B0snonSl",pr2DpTbalb_B0snonSl_s_m);
	f_scaled_m->GetObject("pr2DpTbalb_LbnonSl",pr2DpTbalb_LbnonSl_s_m);

	//calculate the difference in response between normal and the rescaled sample 
	//the second ProjectionX has new name as argument, otherwise would have same name as the previous
	//histo and causes memory leak 
	TH1D* hpTbalb     = prpTbalb   ->ProjectionX();
	TH1D* hpTbalb_s   = prpTbalb_s ->ProjectionX("pr2DpTbalb_s");
	TH1D* hpTbalb_s_p = prpTbalb_s_p ->ProjectionX("pr2DpTbalb_s_p");
	TH1D* hpTbalb_s_m = prpTbalb_s_m ->ProjectionX("pr2DpTbalb_s_m");

	TH1D* diffb 		= (TH1D*) hpTbalb_s->Clone();
	TH1D* diffb_p 	= (TH1D*) hpTbalb_s_p->Clone();
	TH1D* diffb_m 	= (TH1D*) hpTbalb_s_m->Clone();

	diffb->Add(hpTbalb,-1);
	diffb_p->Add(hpTbalb,-1);
	diffb_m->Add(hpTbalb,-1);

	//evtW_* = event weight, sem_* = standard errorb of the mean,   
	double errorb, evtWtot, evtWtots;

	double evtWtot_B0Sl;
	double evtWtot_BpSl;
	double evtWtot_B0sSl;
	double evtWtot_LbSl;
	double evtWtot_B0nonSl;
	double evtWtot_BpnonSl;
	double evtWtot_B0snonSl;
	double evtWtot_LbnonSl;

	double evtWtots_B0Sl;
	double evtWtots_BpSl;
	double evtWtots_B0sSl;
	double evtWtots_LbSl;
	double evtWtots_B0nonSl;
	double evtWtots_BpnonSl;
	double evtWtots_B0snonSl;
	double evtWtots_LbnonSl;

	vector<double> evtW_B0Sl;
	vector<double> evtW_BpSl;
	vector<double> evtW_B0sSl;
	vector<double> evtW_LbSl;
	vector<double> evtW_B0nonSl;
	vector<double> evtW_BpnonSl;
	vector<double> evtW_B0snonSl;
	vector<double> evtW_LbnonSl;

	vector<double> evtWs_B0Sl;
	vector<double> evtWs_BpSl;
	vector<double> evtWs_B0sSl;
	vector<double> evtWs_LbSl;
	vector<double> evtWs_B0nonSl;
	vector<double> evtWs_BpnonSl;
	vector<double> evtWs_B0snonSl;
	vector<double> evtWs_LbnonSl;

	vector<double> sem_B0Sl;
	vector<double> sem_BpSl;
	vector<double> sem_B0sSl;
	vector<double> sem_LbSl;
	vector<double> sem_B0nonSl;
	vector<double> sem_BpnonSl;
	vector<double> sem_B0snonSl;
	vector<double> sem_LbnonSl;

	vector<double> vals_B0Sl;
	vector<double> vals_BpSl;
	vector<double> vals_B0sSl;
	vector<double> vals_LbSl;
	vector<double> vals_B0nonSl;
	vector<double> vals_BpnonSl;
	vector<double> vals_B0snonSl;
	vector<double> vals_LbnonSl;

	vector<double> vec_B0Sl_stat, vec_BpSl_stat, vec_B0sSl_stat, vec_LbSl_stat,
	 							 vec_B0nonSl_stat, vec_BpnonSl_stat, vec_B0snonSl_stat,vec_LbnonSl_stat;

	 ReadFromFile(vec_B0Sl_stat,"/home/anpirtti/ultimate_generator/final_rescaling/indvBtoSLBr/calc_scale_factors/scale_factors/indvBtoSlBr/stat_err/B0Sl_err.txt");
	 ReadFromFile(vec_BpSl_stat,"/home/anpirtti/ultimate_generator/final_rescaling/indvBtoSLBr/calc_scale_factors/scale_factors/indvBtoSlBr/stat_err/BpSl_err.txt");
	 ReadFromFile(vec_B0sSl_stat,"/home/anpirtti/ultimate_generator/final_rescaling/indvBtoSLBr/calc_scale_factors/scale_factors/indvBtoSlBr/stat_err/B0sSl_err.txt");
	 ReadFromFile(vec_LbSl_stat,"/home/anpirtti/ultimate_generator/final_rescaling/indvBtoSLBr/calc_scale_factors/scale_factors/indvBtoSlBr/stat_err/LbSl_err.txt");
	 ReadFromFile(vec_B0nonSl_stat,"/home/anpirtti/ultimate_generator/final_rescaling/indvBtoSLBr/calc_scale_factors/scale_factors/indvBtoSlBr/stat_err/B0nonSl_err.txt");
	 ReadFromFile(vec_BpnonSl_stat,"/home/anpirtti/ultimate_generator/final_rescaling/indvBtoSLBr/calc_scale_factors/scale_factors/indvBtoSlBr/stat_err/BpnonSl_err.txt");
	 ReadFromFile(vec_B0snonSl_stat,"/home/anpirtti/ultimate_generator/final_rescaling/indvBtoSLBr/calc_scale_factors/scale_factors/indvBtoSlBr/stat_err/B0snonSl_err.txt");
	 ReadFromFile(vec_LbnonSl_stat,"/home/anpirtti/ultimate_generator/final_rescaling/indvBtoSLBr/calc_scale_factors/scale_factors/indvBtoSlBr/stat_err/LbnonSl_err.txt"); 

	vector<vector<double>> vec_sf_sem{vec_B0Sl_stat, vec_BpSl_stat, vec_B0sSl_stat, vec_LbSl_stat,
	 							 vec_B0nonSl_stat, vec_BpnonSl_stat, vec_B0snonSl_stat,vec_LbnonSl_stat};
	
	//removing the under- and overflow bin values
	for (int i = 0; i < vec_sf_sem.size(); i++) {
		vec_sf_sem[i].erase(vec_sf_sem[i].begin() + 0);
		vec_sf_sem[i].erase(vec_sf_sem[i].begin() + vec_sf_sem[i].size() - 1);
	}

  //Loop over the TProfile bins
  for (int i = 1; i < hpTbalb->GetNbinsX()+1; i++){
		evtW_B0Sl.clear();
		evtW_BpSl.clear();
		evtW_B0sSl.clear();
		evtW_LbSl.clear();
		evtW_B0nonSl.clear();
		evtW_BpnonSl.clear();
		evtW_B0snonSl.clear();
		evtW_LbnonSl.clear();

		evtWs_B0Sl.clear();
		evtWs_BpSl.clear();
		evtWs_B0sSl.clear();
		evtWs_LbSl.clear();
		evtWs_B0nonSl.clear();
		evtWs_BpnonSl.clear();
		evtWs_B0snonSl.clear();
		evtWs_LbnonSl.clear();

		sem_B0Sl.clear();
		sem_BpSl.clear();
		sem_B0sSl.clear();
		sem_LbSl.clear();
		sem_B0nonSl.clear();
		sem_BpnonSl.clear();
		sem_B0snonSl.clear();
		sem_LbnonSl.clear();

		vals_B0Sl.clear();
		vals_BpSl.clear();
		vals_B0sSl.clear();
		vals_LbSl.clear();
		vals_B0nonSl.clear();
		vals_BpnonSl.clear();
		vals_B0snonSl.clear();
		vals_LbnonSl.clear();

		for (int j = 1; j < hpTbalb->GetNbinsX()+1; j++) {
			string pr_ind = "_" + to_string(i) + "_" + to_string(j);

			prpTbalb_B0Sl = pr2DpTbalb_B0Sl->ProfileX(("B0Sl"+pr_ind).c_str(),j,j);
			prpTbalb_BpSl = pr2DpTbalb_BpSl->ProfileX(("BpSl"+pr_ind).c_str(),j,j);
			prpTbalb_B0sSl = pr2DpTbalb_B0sSl->ProfileX(("B0sSl"+pr_ind).c_str(),j,j);
			prpTbalb_LbSl = pr2DpTbalb_LbSl->ProfileX(("LbSl"+pr_ind).c_str(),j,j);
			prpTbalb_B0nonSl = pr2DpTbalb_B0nonSl->ProfileX(("B0nonSl"+pr_ind).c_str(),j,j);
			prpTbalb_BpnonSl = pr2DpTbalb_BpnonSl->ProfileX(("BpnonSl"+pr_ind).c_str(),j,j);
			prpTbalb_B0snonSl = pr2DpTbalb_B0snonSl->ProfileX(("B0snonSl"+pr_ind).c_str(),j,j);
			prpTbalb_LbnonSl = pr2DpTbalb_LbnonSl->ProfileX(("LbnonSl"+pr_ind).c_str(),j,j);

			prpTbalb_B0Sl_s = pr2DpTbalb_B0Sl_s->ProfileX(("B0Sl_s"+pr_ind).c_str(),j,j);
			prpTbalb_BpSl_s = pr2DpTbalb_BpSl_s->ProfileX(("BpSl_s"+pr_ind).c_str(),j,j);
			prpTbalb_B0sSl_s = pr2DpTbalb_B0sSl_s->ProfileX(("B0sSl_s"+pr_ind).c_str(),j,j);
			prpTbalb_LbSl_s = pr2DpTbalb_LbSl_s->ProfileX(("LbSl_s"+pr_ind).c_str(),j,j);
			prpTbalb_B0nonSl_s = pr2DpTbalb_B0nonSl_s->ProfileX(("B0nonSl_s"+pr_ind).c_str(),j,j);
			prpTbalb_BpnonSl_s = pr2DpTbalb_BpnonSl_s->ProfileX(("BpnonSl_s"+pr_ind).c_str(),j,j);
			prpTbalb_B0snonSl_s = pr2DpTbalb_B0snonSl_s->ProfileX(("B0snonSl_s"+pr_ind).c_str(),j,j);
			prpTbalb_LbnonSl_s = pr2DpTbalb_LbnonSl_s->ProfileX(("LbnonSl_s"+pr_ind).c_str(),j,j);

			evtW_B0Sl.push_back(prpTbalb_B0Sl->GetBinEntries(i));
			evtW_BpSl.push_back(prpTbalb_BpSl->GetBinEntries(i));
			evtW_B0sSl.push_back(prpTbalb_B0sSl->GetBinEntries(i));
			evtW_LbSl.push_back(prpTbalb_LbSl->GetBinEntries(i));
			evtW_B0nonSl.push_back(prpTbalb_B0nonSl->GetBinEntries(i));
			evtW_BpnonSl.push_back(prpTbalb_BpnonSl->GetBinEntries(i));
			evtW_B0snonSl.push_back(prpTbalb_B0snonSl->GetBinEntries(i));
			evtW_LbnonSl.push_back(prpTbalb_LbnonSl->GetBinEntries(i));

			evtWs_B0Sl.push_back(prpTbalb_B0Sl_s->GetBinEntries(i));
			evtWs_BpSl.push_back(prpTbalb_BpSl_s->GetBinEntries(i));
			evtWs_B0sSl.push_back(prpTbalb_B0sSl_s->GetBinEntries(i));
			evtWs_LbSl.push_back(prpTbalb_LbSl_s->GetBinEntries(i));
			evtWs_B0nonSl.push_back(prpTbalb_B0nonSl_s->GetBinEntries(i));
			evtWs_BpnonSl.push_back(prpTbalb_BpnonSl_s->GetBinEntries(i));
			evtWs_B0snonSl.push_back(prpTbalb_B0snonSl_s->GetBinEntries(i));
			evtWs_LbnonSl.push_back(prpTbalb_LbnonSl_s->GetBinEntries(i));

			sem_B0Sl.push_back(prpTbalb_B0Sl->GetBinError(i));
			sem_BpSl.push_back(prpTbalb_BpSl->GetBinError(i));
			sem_B0sSl.push_back(prpTbalb_B0sSl->GetBinError(i));
			sem_LbSl.push_back(prpTbalb_LbSl->GetBinError(i));
			sem_B0nonSl.push_back(prpTbalb_B0nonSl->GetBinError(i));
			sem_BpnonSl.push_back(prpTbalb_BpnonSl->GetBinError(i));
			sem_B0snonSl.push_back(prpTbalb_B0snonSl->GetBinError(i));
			sem_LbnonSl.push_back(prpTbalb_LbnonSl->GetBinError(i));

			vals_B0Sl.push_back(prpTbalb_B0Sl->GetBinContent(i));
			vals_BpSl.push_back(prpTbalb_BpSl->GetBinContent(i));
			vals_B0sSl.push_back(prpTbalb_B0sSl->GetBinContent(i));
			vals_LbSl.push_back(prpTbalb_LbSl->GetBinContent(i));
			vals_B0nonSl.push_back(prpTbalb_B0nonSl->GetBinContent(i));
			vals_BpnonSl.push_back(prpTbalb_BpnonSl->GetBinContent(i));
			vals_B0snonSl.push_back(prpTbalb_B0snonSl->GetBinContent(i));
			vals_LbnonSl.push_back(prpTbalb_LbnonSl->GetBinContent(i));

			delete prpTbalb_B0Sl;
			delete prpTbalb_BpSl;
			delete prpTbalb_B0sSl;
			delete prpTbalb_LbSl;
			delete prpTbalb_B0nonSl;
			delete prpTbalb_BpnonSl;
			delete prpTbalb_B0snonSl; 
			delete prpTbalb_LbnonSl;

			delete prpTbalb_B0Sl_s;
			delete prpTbalb_BpSl_s;
			delete prpTbalb_B0sSl_s;
			delete prpTbalb_LbSl_s;
			delete prpTbalb_B0nonSl_s;
			delete prpTbalb_BpnonSl_s;
			delete prpTbalb_B0snonSl_s;
			delete prpTbalb_LbnonSl_s;
		}

		 
		vector<vector<double>> vec_evtW{evtW_B0Sl, evtW_BpSl, evtW_B0sSl, evtW_LbSl,
																	  evtW_B0nonSl, evtW_BpnonSl, evtW_B0snonSl, evtW_LbnonSl};

		vector<vector<double>> vec_evtWs{evtWs_B0Sl, evtWs_BpSl, evtWs_B0sSl, evtWs_LbSl,
																	  evtWs_B0nonSl, evtWs_BpnonSl, evtWs_B0snonSl, evtWs_LbnonSl};

		vector<vector<double>> vec_sem{sem_B0Sl, sem_BpSl, sem_B0sSl, sem_LbSl,
																	  sem_B0nonSl, sem_BpnonSl, sem_B0snonSl, sem_LbnonSl};

		vector<vector<double>> vec_vals{vals_B0Sl, vals_BpSl, vals_B0sSl, vals_LbSl,
																	  vals_B0nonSl, vals_BpnonSl, vals_B0snonSl, vals_LbnonSl};

		//set small number so that we dont divide by zero 
		evtWtot_B0Sl = 1e-10;
		evtWtot_BpSl = 1e-10;
		evtWtot_B0sSl = 1e-10;
		evtWtot_LbSl = 1e-10;
		evtWtot_B0nonSl = 1e-10;
		evtWtot_BpnonSl = 1e-10;
		evtWtot_B0snonSl = 1e-10;
		evtWtot_LbnonSl = 1e-10;

		evtWtots_B0Sl = 1e-10;
		evtWtots_BpSl = 1e-10;
		evtWtots_B0sSl = 1e-10;
		evtWtots_LbSl = 1e-10;
		evtWtots_B0nonSl = 1e-10;
		evtWtots_BpnonSl = 1e-10;
		evtWtots_B0snonSl = 1e-10;
		evtWtots_LbnonSl = 1e-10;

		for (double x : evtW_B0Sl) evtWtot_B0Sl += x;	
		for (double x : evtW_BpSl) evtWtot_BpSl += x;
		for (double x : evtW_B0sSl) evtWtot_B0sSl += x;
		for (double x : evtW_LbSl) evtWtot_LbSl += x;
		for (double x : evtW_B0nonSl) evtWtot_B0nonSl += x;
		for (double x : evtW_BpnonSl) evtWtot_BpnonSl += x;
		for (double x : evtW_B0snonSl) evtWtot_B0snonSl += x;
		for (double x : evtW_LbnonSl) evtWtot_LbnonSl += x;

		for (double x : evtWs_B0Sl) evtWtots_B0Sl += x;
		for (double x : evtWs_BpSl) evtWtots_BpSl += x;
		for (double x : evtWs_B0sSl) evtWtots_B0sSl += x;
		for (double x : evtWs_LbSl) evtWtots_LbSl += x;
		for (double x : evtWs_B0nonSl) evtWtots_B0nonSl += x;
		for (double x : evtWs_BpnonSl) evtWtots_BpnonSl += x;
		for (double x : evtWs_B0snonSl) evtWtots_B0snonSl += x;
		for (double x : evtWs_LbnonSl) evtWtots_LbnonSl += x;

		evtWtot = evtWtot_B0Sl + evtWtot_BpSl + evtWtot_B0sSl + evtWtot_LbSl +
						  evtWtot_B0nonSl +evtWtot_BpnonSl + evtWtot_B0snonSl + evtWtot_LbnonSl;

		evtWtots = evtWtots_B0Sl + evtWtots_BpSl + evtWtots_B0sSl + evtWtots_LbSl +
							 evtWtots_B0nonSl +evtWtots_BpnonSl + evtWtots_B0snonSl + evtWtots_LbnonSl;

		errorb = 0;
		for (int j = 0; j < hpTbalb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hpTbalb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}

		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb->GetBinContent(i) == 0) diffb->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb->SetBinError(i,errorb);
  }

	//############### plus variation ####################
  //Loop over the TProfile bins
  for (int i = 1; i < hpTbalb->GetNbinsX()+1; i++){
		evtW_B0Sl.clear();
		evtW_BpSl.clear();
		evtW_B0sSl.clear();
		evtW_LbSl.clear();
		evtW_B0nonSl.clear();
		evtW_BpnonSl.clear();
		evtW_B0snonSl.clear();
		evtW_LbnonSl.clear();

		evtWs_B0Sl.clear();
		evtWs_BpSl.clear();
		evtWs_B0sSl.clear();
		evtWs_LbSl.clear();
		evtWs_B0nonSl.clear();
		evtWs_BpnonSl.clear();
		evtWs_B0snonSl.clear();
		evtWs_LbnonSl.clear();

		sem_B0Sl.clear();
		sem_BpSl.clear();
		sem_B0sSl.clear();
		sem_LbSl.clear();
		sem_B0nonSl.clear();
		sem_BpnonSl.clear();
		sem_B0snonSl.clear();
		sem_LbnonSl.clear();

		vals_B0Sl.clear();
		vals_BpSl.clear();
		vals_B0sSl.clear();
		vals_LbSl.clear();
		vals_B0nonSl.clear();
		vals_BpnonSl.clear();
		vals_B0snonSl.clear();
		vals_LbnonSl.clear();

		for (int j = 1; j < hpTbalb->GetNbinsX()+1; j++) {
			string pr_ind = "_" + to_string(i) + "_" + to_string(j);

			prpTbalb_B0Sl = pr2DpTbalb_B0Sl->ProfileX(("B0Sl"+pr_ind).c_str(),j,j);
			prpTbalb_BpSl = pr2DpTbalb_BpSl->ProfileX(("BpSl"+pr_ind).c_str(),j,j);
			prpTbalb_B0sSl = pr2DpTbalb_B0sSl->ProfileX(("B0sSl"+pr_ind).c_str(),j,j);
			prpTbalb_LbSl = pr2DpTbalb_LbSl->ProfileX(("LbSl"+pr_ind).c_str(),j,j);
			prpTbalb_B0nonSl = pr2DpTbalb_B0nonSl->ProfileX(("B0nonSl"+pr_ind).c_str(),j,j);
			prpTbalb_BpnonSl = pr2DpTbalb_BpnonSl->ProfileX(("BpnonSl"+pr_ind).c_str(),j,j);
			prpTbalb_B0snonSl = pr2DpTbalb_B0snonSl->ProfileX(("B0snonSl"+pr_ind).c_str(),j,j);
			prpTbalb_LbnonSl = pr2DpTbalb_LbnonSl->ProfileX(("LbnonSl"+pr_ind).c_str(),j,j);

			prpTbalb_B0Sl_s_p = pr2DpTbalb_B0Sl_s_p->ProfileX(("B0Sl_s"+pr_ind).c_str(),j,j);
			prpTbalb_BpSl_s_p = pr2DpTbalb_BpSl_s_p->ProfileX(("BpSl_s"+pr_ind).c_str(),j,j);
			prpTbalb_B0sSl_s_p = pr2DpTbalb_B0sSl_s_p->ProfileX(("B0sSl_s"+pr_ind).c_str(),j,j);
			prpTbalb_LbSl_s_p = pr2DpTbalb_LbSl_s_p->ProfileX(("LbSl_s"+pr_ind).c_str(),j,j);
			prpTbalb_B0nonSl_s_p = pr2DpTbalb_B0nonSl_s_p->ProfileX(("B0nonSl_s"+pr_ind).c_str(),j,j);
			prpTbalb_BpnonSl_s_p = pr2DpTbalb_BpnonSl_s_p->ProfileX(("BpnonSl_s"+pr_ind).c_str(),j,j);
			prpTbalb_B0snonSl_s_p = pr2DpTbalb_B0snonSl_s_p->ProfileX(("B0snonSl_s"+pr_ind).c_str(),j,j);
			prpTbalb_LbnonSl_s_p = pr2DpTbalb_LbnonSl_s_p->ProfileX(("LbnonSl_s"+pr_ind).c_str(),j,j);

			evtW_B0Sl.push_back(prpTbalb_B0Sl->GetBinEntries(i));
			evtW_BpSl.push_back(prpTbalb_BpSl->GetBinEntries(i));
			evtW_B0sSl.push_back(prpTbalb_B0sSl->GetBinEntries(i));
			evtW_LbSl.push_back(prpTbalb_LbSl->GetBinEntries(i));
			evtW_B0nonSl.push_back(prpTbalb_B0nonSl->GetBinEntries(i));
			evtW_BpnonSl.push_back(prpTbalb_BpnonSl->GetBinEntries(i));
			evtW_B0snonSl.push_back(prpTbalb_B0snonSl->GetBinEntries(i));
			evtW_LbnonSl.push_back(prpTbalb_LbnonSl->GetBinEntries(i));

			evtWs_B0Sl.push_back(prpTbalb_B0Sl_s_p->GetBinEntries(i));
			evtWs_BpSl.push_back(prpTbalb_BpSl_s_p->GetBinEntries(i));
			evtWs_B0sSl.push_back(prpTbalb_B0sSl_s_p->GetBinEntries(i));
			evtWs_LbSl.push_back(prpTbalb_LbSl_s_p->GetBinEntries(i));
			evtWs_B0nonSl.push_back(prpTbalb_B0nonSl_s_p->GetBinEntries(i));
			evtWs_BpnonSl.push_back(prpTbalb_BpnonSl_s_p->GetBinEntries(i));
			evtWs_B0snonSl.push_back(prpTbalb_B0snonSl_s_p->GetBinEntries(i));
			evtWs_LbnonSl.push_back(prpTbalb_LbnonSl_s_p->GetBinEntries(i));

			sem_B0Sl.push_back(prpTbalb_B0Sl->GetBinError(i));
			sem_BpSl.push_back(prpTbalb_BpSl->GetBinError(i));
			sem_B0sSl.push_back(prpTbalb_B0sSl->GetBinError(i));
			sem_LbSl.push_back(prpTbalb_LbSl->GetBinError(i));
			sem_B0nonSl.push_back(prpTbalb_B0nonSl->GetBinError(i));
			sem_BpnonSl.push_back(prpTbalb_BpnonSl->GetBinError(i));
			sem_B0snonSl.push_back(prpTbalb_B0snonSl->GetBinError(i));
			sem_LbnonSl.push_back(prpTbalb_LbnonSl->GetBinError(i));

			vals_B0Sl.push_back(prpTbalb_B0Sl->GetBinContent(i));
			vals_BpSl.push_back(prpTbalb_BpSl->GetBinContent(i));
			vals_B0sSl.push_back(prpTbalb_B0sSl->GetBinContent(i));
			vals_LbSl.push_back(prpTbalb_LbSl->GetBinContent(i));
			vals_B0nonSl.push_back(prpTbalb_B0nonSl->GetBinContent(i));
			vals_BpnonSl.push_back(prpTbalb_BpnonSl->GetBinContent(i));
			vals_B0snonSl.push_back(prpTbalb_B0snonSl->GetBinContent(i));
			vals_LbnonSl.push_back(prpTbalb_LbnonSl->GetBinContent(i));

			delete prpTbalb_B0Sl;
			delete prpTbalb_BpSl;
			delete prpTbalb_B0sSl;
			delete prpTbalb_LbSl;
			delete prpTbalb_B0nonSl;
			delete prpTbalb_BpnonSl;
			delete prpTbalb_B0snonSl; 
			delete prpTbalb_LbnonSl;

			delete prpTbalb_B0Sl_s_p;
			delete prpTbalb_BpSl_s_p;
			delete prpTbalb_B0sSl_s_p;
			delete prpTbalb_LbSl_s_p;
			delete prpTbalb_B0nonSl_s_p;
			delete prpTbalb_BpnonSl_s_p;
			delete prpTbalb_B0snonSl_s_p;
			delete prpTbalb_LbnonSl_s_p;
		}

		 
		vector<vector<double>> vec_evtW{evtW_B0Sl, evtW_BpSl, evtW_B0sSl, evtW_LbSl,
																	  evtW_B0nonSl, evtW_BpnonSl, evtW_B0snonSl, evtW_LbnonSl};

		vector<vector<double>> vec_evtWs{evtWs_B0Sl, evtWs_BpSl, evtWs_B0sSl, evtWs_LbSl,
																	  evtWs_B0nonSl, evtWs_BpnonSl, evtWs_B0snonSl, evtWs_LbnonSl};

		vector<vector<double>> vec_sem{sem_B0Sl, sem_BpSl, sem_B0sSl, sem_LbSl,
																	  sem_B0nonSl, sem_BpnonSl, sem_B0snonSl, sem_LbnonSl};

		vector<vector<double>> vec_vals{vals_B0Sl, vals_BpSl, vals_B0sSl, vals_LbSl,
																	  vals_B0nonSl, vals_BpnonSl, vals_B0snonSl, vals_LbnonSl};

		//set small number so that we dont divide by zero 
		evtWtot_B0Sl = 1e-10;
		evtWtot_BpSl = 1e-10;
		evtWtot_B0sSl = 1e-10;
		evtWtot_LbSl = 1e-10;
		evtWtot_B0nonSl = 1e-10;
		evtWtot_BpnonSl = 1e-10;
		evtWtot_B0snonSl = 1e-10;
		evtWtot_LbnonSl = 1e-10;

		evtWtots_B0Sl = 1e-10;
		evtWtots_BpSl = 1e-10;
		evtWtots_B0sSl = 1e-10;
		evtWtots_LbSl = 1e-10;
		evtWtots_B0nonSl = 1e-10;
		evtWtots_BpnonSl = 1e-10;
		evtWtots_B0snonSl = 1e-10;
		evtWtots_LbnonSl = 1e-10;

		for (double x : evtW_B0Sl) evtWtot_B0Sl += x;	
		for (double x : evtW_BpSl) evtWtot_BpSl += x;
		for (double x : evtW_B0sSl) evtWtot_B0sSl += x;
		for (double x : evtW_LbSl) evtWtot_LbSl += x;
		for (double x : evtW_B0nonSl) evtWtot_B0nonSl += x;
		for (double x : evtW_BpnonSl) evtWtot_BpnonSl += x;
		for (double x : evtW_B0snonSl) evtWtot_B0snonSl += x;
		for (double x : evtW_LbnonSl) evtWtot_LbnonSl += x;

		for (double x : evtWs_B0Sl) evtWtots_B0Sl += x;
		for (double x : evtWs_BpSl) evtWtots_BpSl += x;
		for (double x : evtWs_B0sSl) evtWtots_B0sSl += x;
		for (double x : evtWs_LbSl) evtWtots_LbSl += x;
		for (double x : evtWs_B0nonSl) evtWtots_B0nonSl += x;
		for (double x : evtWs_BpnonSl) evtWtots_BpnonSl += x;
		for (double x : evtWs_B0snonSl) evtWtots_B0snonSl += x;
		for (double x : evtWs_LbnonSl) evtWtots_LbnonSl += x;

		evtWtot = evtWtot_B0Sl + evtWtot_BpSl + evtWtot_B0sSl + evtWtot_LbSl +
						  evtWtot_B0nonSl +evtWtot_BpnonSl + evtWtot_B0snonSl + evtWtot_LbnonSl;

		evtWtots = evtWtots_B0Sl + evtWtots_BpSl + evtWtots_B0sSl + evtWtots_LbSl +
							 evtWtots_B0nonSl +evtWtots_BpnonSl + evtWtots_B0snonSl + evtWtots_LbnonSl;

		errorb = 0;
		for (int j = 0; j < hpTbalb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hpTbalb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}

		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb_p->GetBinContent(i) == 0) diffb_p->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb_p->SetBinError(i,errorb);
  }

	//############### minus variation ####################
  //Loop over the TProfile bins
  for (int i = 1; i < hpTbalb->GetNbinsX()+1; i++){
		evtW_B0Sl.clear();
		evtW_BpSl.clear();
		evtW_B0sSl.clear();
		evtW_LbSl.clear();
		evtW_B0nonSl.clear();
		evtW_BpnonSl.clear();
		evtW_B0snonSl.clear();
		evtW_LbnonSl.clear();

		evtWs_B0Sl.clear();
		evtWs_BpSl.clear();
		evtWs_B0sSl.clear();
		evtWs_LbSl.clear();
		evtWs_B0nonSl.clear();
		evtWs_BpnonSl.clear();
		evtWs_B0snonSl.clear();
		evtWs_LbnonSl.clear();

		sem_B0Sl.clear();
		sem_BpSl.clear();
		sem_B0sSl.clear();
		sem_LbSl.clear();
		sem_B0nonSl.clear();
		sem_BpnonSl.clear();
		sem_B0snonSl.clear();
		sem_LbnonSl.clear();

		vals_B0Sl.clear();
		vals_BpSl.clear();
		vals_B0sSl.clear();
		vals_LbSl.clear();
		vals_B0nonSl.clear();
		vals_BpnonSl.clear();
		vals_B0snonSl.clear();
		vals_LbnonSl.clear();

		for (int j = 1; j < hpTbalb->GetNbinsX()+1; j++) {
			string pr_ind = "_" + to_string(i) + "_" + to_string(j);

			prpTbalb_B0Sl = pr2DpTbalb_B0Sl->ProfileX(("B0Sl"+pr_ind).c_str(),j,j);
			prpTbalb_BpSl = pr2DpTbalb_BpSl->ProfileX(("BpSl"+pr_ind).c_str(),j,j);
			prpTbalb_B0sSl = pr2DpTbalb_B0sSl->ProfileX(("B0sSl"+pr_ind).c_str(),j,j);
			prpTbalb_LbSl = pr2DpTbalb_LbSl->ProfileX(("LbSl"+pr_ind).c_str(),j,j);
			prpTbalb_B0nonSl = pr2DpTbalb_B0nonSl->ProfileX(("B0nonSl"+pr_ind).c_str(),j,j);
			prpTbalb_BpnonSl = pr2DpTbalb_BpnonSl->ProfileX(("BpnonSl"+pr_ind).c_str(),j,j);
			prpTbalb_B0snonSl = pr2DpTbalb_B0snonSl->ProfileX(("B0snonSl"+pr_ind).c_str(),j,j);
			prpTbalb_LbnonSl = pr2DpTbalb_LbnonSl->ProfileX(("LbnonSl"+pr_ind).c_str(),j,j);

			prpTbalb_B0Sl_s_m = pr2DpTbalb_B0Sl_s_m->ProfileX(("B0Sl_s"+pr_ind).c_str(),j,j);
			prpTbalb_BpSl_s_m = pr2DpTbalb_BpSl_s_m->ProfileX(("BpSl_s"+pr_ind).c_str(),j,j);
			prpTbalb_B0sSl_s_m = pr2DpTbalb_B0sSl_s_m->ProfileX(("B0sSl_s"+pr_ind).c_str(),j,j);
			prpTbalb_LbSl_s_m = pr2DpTbalb_LbSl_s_m->ProfileX(("LbSl_s"+pr_ind).c_str(),j,j);
			prpTbalb_B0nonSl_s_m = pr2DpTbalb_B0nonSl_s_m->ProfileX(("B0nonSl_s"+pr_ind).c_str(),j,j);
			prpTbalb_BpnonSl_s_m = pr2DpTbalb_BpnonSl_s_m->ProfileX(("BpnonSl_s"+pr_ind).c_str(),j,j);
			prpTbalb_B0snonSl_s_m = pr2DpTbalb_B0snonSl_s_m->ProfileX(("B0snonSl_s"+pr_ind).c_str(),j,j);
			prpTbalb_LbnonSl_s_m = pr2DpTbalb_LbnonSl_s_m->ProfileX(("LbnonSl_s"+pr_ind).c_str(),j,j);

			evtW_B0Sl.push_back(prpTbalb_B0Sl->GetBinEntries(i));
			evtW_BpSl.push_back(prpTbalb_BpSl->GetBinEntries(i));
			evtW_B0sSl.push_back(prpTbalb_B0sSl->GetBinEntries(i));
			evtW_LbSl.push_back(prpTbalb_LbSl->GetBinEntries(i));
			evtW_B0nonSl.push_back(prpTbalb_B0nonSl->GetBinEntries(i));
			evtW_BpnonSl.push_back(prpTbalb_BpnonSl->GetBinEntries(i));
			evtW_B0snonSl.push_back(prpTbalb_B0snonSl->GetBinEntries(i));
			evtW_LbnonSl.push_back(prpTbalb_LbnonSl->GetBinEntries(i));

			evtWs_B0Sl.push_back(prpTbalb_B0Sl_s_m->GetBinEntries(i));
			evtWs_BpSl.push_back(prpTbalb_BpSl_s_m->GetBinEntries(i));
			evtWs_B0sSl.push_back(prpTbalb_B0sSl_s_m->GetBinEntries(i));
			evtWs_LbSl.push_back(prpTbalb_LbSl_s_m->GetBinEntries(i));
			evtWs_B0nonSl.push_back(prpTbalb_B0nonSl_s_m->GetBinEntries(i));
			evtWs_BpnonSl.push_back(prpTbalb_BpnonSl_s_m->GetBinEntries(i));
			evtWs_B0snonSl.push_back(prpTbalb_B0snonSl_s_m->GetBinEntries(i));
			evtWs_LbnonSl.push_back(prpTbalb_LbnonSl_s_m->GetBinEntries(i));

			sem_B0Sl.push_back(prpTbalb_B0Sl->GetBinError(i));
			sem_BpSl.push_back(prpTbalb_BpSl->GetBinError(i));
			sem_B0sSl.push_back(prpTbalb_B0sSl->GetBinError(i));
			sem_LbSl.push_back(prpTbalb_LbSl->GetBinError(i));
			sem_B0nonSl.push_back(prpTbalb_B0nonSl->GetBinError(i));
			sem_BpnonSl.push_back(prpTbalb_BpnonSl->GetBinError(i));
			sem_B0snonSl.push_back(prpTbalb_B0snonSl->GetBinError(i));
			sem_LbnonSl.push_back(prpTbalb_LbnonSl->GetBinError(i));

			vals_B0Sl.push_back(prpTbalb_B0Sl->GetBinContent(i));
			vals_BpSl.push_back(prpTbalb_BpSl->GetBinContent(i));
			vals_B0sSl.push_back(prpTbalb_B0sSl->GetBinContent(i));
			vals_LbSl.push_back(prpTbalb_LbSl->GetBinContent(i));
			vals_B0nonSl.push_back(prpTbalb_B0nonSl->GetBinContent(i));
			vals_BpnonSl.push_back(prpTbalb_BpnonSl->GetBinContent(i));
			vals_B0snonSl.push_back(prpTbalb_B0snonSl->GetBinContent(i));
			vals_LbnonSl.push_back(prpTbalb_LbnonSl->GetBinContent(i));

			delete prpTbalb_B0Sl;
			delete prpTbalb_BpSl;
			delete prpTbalb_B0sSl;
			delete prpTbalb_LbSl;
			delete prpTbalb_B0nonSl;
			delete prpTbalb_BpnonSl;
			delete prpTbalb_B0snonSl; 
			delete prpTbalb_LbnonSl;

			delete prpTbalb_B0Sl_s_m;
			delete prpTbalb_BpSl_s_m;
			delete prpTbalb_B0sSl_s_m;
			delete prpTbalb_LbSl_s_m;
			delete prpTbalb_B0nonSl_s_m;
			delete prpTbalb_BpnonSl_s_m;
			delete prpTbalb_B0snonSl_s_m;
			delete prpTbalb_LbnonSl_s_m;
		}

		 
		vector<vector<double>> vec_evtW{evtW_B0Sl, evtW_BpSl, evtW_B0sSl, evtW_LbSl,
																	  evtW_B0nonSl, evtW_BpnonSl, evtW_B0snonSl, evtW_LbnonSl};

		vector<vector<double>> vec_evtWs{evtWs_B0Sl, evtWs_BpSl, evtWs_B0sSl, evtWs_LbSl,
																	  evtWs_B0nonSl, evtWs_BpnonSl, evtWs_B0snonSl, evtWs_LbnonSl};

		vector<vector<double>> vec_sem{sem_B0Sl, sem_BpSl, sem_B0sSl, sem_LbSl,
																	  sem_B0nonSl, sem_BpnonSl, sem_B0snonSl, sem_LbnonSl};

		vector<vector<double>> vec_vals{vals_B0Sl, vals_BpSl, vals_B0sSl, vals_LbSl,
																	  vals_B0nonSl, vals_BpnonSl, vals_B0snonSl, vals_LbnonSl};

		//set small number so that we dont divide by zero 
		evtWtot_B0Sl = 1e-10;
		evtWtot_BpSl = 1e-10;
		evtWtot_B0sSl = 1e-10;
		evtWtot_LbSl = 1e-10;
		evtWtot_B0nonSl = 1e-10;
		evtWtot_BpnonSl = 1e-10;
		evtWtot_B0snonSl = 1e-10;
		evtWtot_LbnonSl = 1e-10;

		evtWtots_B0Sl = 1e-10;
		evtWtots_BpSl = 1e-10;
		evtWtots_B0sSl = 1e-10;
		evtWtots_LbSl = 1e-10;
		evtWtots_B0nonSl = 1e-10;
		evtWtots_BpnonSl = 1e-10;
		evtWtots_B0snonSl = 1e-10;
		evtWtots_LbnonSl = 1e-10;

		for (double x : evtW_B0Sl) evtWtot_B0Sl += x;	
		for (double x : evtW_BpSl) evtWtot_BpSl += x;
		for (double x : evtW_B0sSl) evtWtot_B0sSl += x;
		for (double x : evtW_LbSl) evtWtot_LbSl += x;
		for (double x : evtW_B0nonSl) evtWtot_B0nonSl += x;
		for (double x : evtW_BpnonSl) evtWtot_BpnonSl += x;
		for (double x : evtW_B0snonSl) evtWtot_B0snonSl += x;
		for (double x : evtW_LbnonSl) evtWtot_LbnonSl += x;

		for (double x : evtWs_B0Sl) evtWtots_B0Sl += x;
		for (double x : evtWs_BpSl) evtWtots_BpSl += x;
		for (double x : evtWs_B0sSl) evtWtots_B0sSl += x;
		for (double x : evtWs_LbSl) evtWtots_LbSl += x;
		for (double x : evtWs_B0nonSl) evtWtots_B0nonSl += x;
		for (double x : evtWs_BpnonSl) evtWtots_BpnonSl += x;
		for (double x : evtWs_B0snonSl) evtWtots_B0snonSl += x;
		for (double x : evtWs_LbnonSl) evtWtots_LbnonSl += x;

		evtWtot = evtWtot_B0Sl + evtWtot_BpSl + evtWtot_B0sSl + evtWtot_LbSl +
						  evtWtot_B0nonSl +evtWtot_BpnonSl + evtWtot_B0snonSl + evtWtot_LbnonSl;

		evtWtots = evtWtots_B0Sl + evtWtots_BpSl + evtWtots_B0sSl + evtWtots_LbSl +
							 evtWtots_B0nonSl +evtWtots_BpnonSl + evtWtots_B0snonSl + evtWtots_LbnonSl;

		errorb = 0;
		for (int j = 0; j < hpTbalb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hpTbalb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}

		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb_m->GetBinContent(i) == 0) diffb_m->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb_m->SetBinError(i,errorb);
  }  

	//Scale to the percentages
  diffb->Scale(100.0);
  diffb_p->Scale(100.0);
  diffb_m->Scale(100.0);

	pTbalb_map["diffb"] = diffb;
	pTbalb_map["diffb_p"] = diffb_p;
	pTbalb_map["diffb_m"] = diffb_m;

	return pTbalb_map;
}

map<string, TH1D*> Rjet_indvBtoSLBr() {
	map <string, TH1D*> Rjetb_map;

  TFile* f_normal = TFile::Open(OpenName.c_str());
  TFile* f_scaled = TFile::Open(OpenName_indvBtoSLBr.c_str());
  TFile* f_scaled_p = TFile::Open(OpenName_indvBtoSLBr_p.c_str());
  TFile* f_scaled_m = TFile::Open(OpenName_indvBtoSLBr_m.c_str());

	TProfile *prRjetb;
	TProfile *prRjetb_s;
	TProfile *prRjetb_s_p;
	TProfile *prRjetb_s_m;

  //Read the normal sample
  f_normal->GetObject("prRjetb",   prRjetb);

  //Read the weighted sample
  f_scaled->GetObject("prRjetb",   prRjetb_s);
  f_scaled_p->GetObject("prRjetb",   prRjetb_s_p);
  f_scaled_m->GetObject("prRjetb",   prRjetb_s_m);

	//2D Tprofiles for the errorb calculations
	TProfile2D* pr2DRjetb;

	TProfile2D* pr2DRjetb_B0Sl;
	TProfile2D* pr2DRjetb_BpSl;
	TProfile2D* pr2DRjetb_B0sSl;
	TProfile2D* pr2DRjetb_LbSl;
	TProfile2D* pr2DRjetb_B0nonSl;
	TProfile2D* pr2DRjetb_BpnonSl;
	TProfile2D* pr2DRjetb_B0snonSl;
	TProfile2D* pr2DRjetb_LbnonSl;

  //The sample with the additional weighting (*_s)
	TProfile2D* pr2DRjetb_s;

	TProfile2D* pr2DRjetb_B0Sl_s;
	TProfile2D* pr2DRjetb_BpSl_s;
	TProfile2D* pr2DRjetb_B0sSl_s;
	TProfile2D* pr2DRjetb_LbSl_s;
	TProfile2D* pr2DRjetb_B0nonSl_s;
	TProfile2D* pr2DRjetb_BpnonSl_s;
	TProfile2D* pr2DRjetb_B0snonSl_s;
	TProfile2D* pr2DRjetb_LbnonSl_s;

	TProfile2D* pr2DRjetb_s_p;

	TProfile2D* pr2DRjetb_B0Sl_s_p;
	TProfile2D* pr2DRjetb_BpSl_s_p;
	TProfile2D* pr2DRjetb_B0sSl_s_p;
	TProfile2D* pr2DRjetb_LbSl_s_p;
	TProfile2D* pr2DRjetb_B0nonSl_s_p;
	TProfile2D* pr2DRjetb_BpnonSl_s_p;
	TProfile2D* pr2DRjetb_B0snonSl_s_p;
	TProfile2D* pr2DRjetb_LbnonSl_s_p;

	TProfile2D* pr2DRjetb_s_m;

	TProfile2D* pr2DRjetb_B0Sl_s_m;
	TProfile2D* pr2DRjetb_BpSl_s_m;
	TProfile2D* pr2DRjetb_B0sSl_s_m;
	TProfile2D* pr2DRjetb_LbSl_s_m;
	TProfile2D* pr2DRjetb_B0nonSl_s_m;
	TProfile2D* pr2DRjetb_BpnonSl_s_m;
	TProfile2D* pr2DRjetb_B0snonSl_s_m;
	TProfile2D* pr2DRjetb_LbnonSl_s_m;

	//Tprofiles for the errorb calculations
	TProfile* prRjetb_B0Sl;
	TProfile* prRjetb_BpSl;
	TProfile* prRjetb_B0sSl;
	TProfile* prRjetb_LbSl;
	TProfile* prRjetb_B0nonSl;
	TProfile* prRjetb_BpnonSl;
	TProfile* prRjetb_B0snonSl;
	TProfile* prRjetb_LbnonSl;

  //The sample with the additional weighting (*_s)
	TProfile* prRjetb_B0Sl_s;
	TProfile* prRjetb_BpSl_s;
	TProfile* prRjetb_B0sSl_s;
	TProfile* prRjetb_LbSl_s;
	TProfile* prRjetb_B0nonSl_s;
	TProfile* prRjetb_BpnonSl_s;
	TProfile* prRjetb_B0snonSl_s;
	TProfile* prRjetb_LbnonSl_s;

	TProfile* prRjetb_B0Sl_s_p;
	TProfile* prRjetb_BpSl_s_p;
	TProfile* prRjetb_B0sSl_s_p;
	TProfile* prRjetb_LbSl_s_p;
	TProfile* prRjetb_B0nonSl_s_p;
	TProfile* prRjetb_BpnonSl_s_p;
	TProfile* prRjetb_B0snonSl_s_p;
	TProfile* prRjetb_LbnonSl_s_p;

	TProfile* prRjetb_B0Sl_s_m;
	TProfile* prRjetb_BpSl_s_m;
	TProfile* prRjetb_B0sSl_s_m;
	TProfile* prRjetb_LbSl_s_m;
	TProfile* prRjetb_B0nonSl_s_m;
	TProfile* prRjetb_BpnonSl_s_m;
	TProfile* prRjetb_B0snonSl_s_m;
	TProfile* prRjetb_LbnonSl_s_m;

  //Read the normal sample
	f_normal->GetObject("pr2DRjetb",pr2DRjetb);

	f_normal->GetObject("pr2DRjetb_B0Sl",pr2DRjetb_B0Sl);
	f_normal->GetObject("pr2DRjetb_BpSl",pr2DRjetb_BpSl);
	f_normal->GetObject("pr2DRjetb_B0sSl",pr2DRjetb_B0sSl);
	f_normal->GetObject("pr2DRjetb_LbSl",pr2DRjetb_LbSl);
	f_normal->GetObject("pr2DRjetb_B0nonSl",pr2DRjetb_B0nonSl);
	f_normal->GetObject("pr2DRjetb_BpnonSl",pr2DRjetb_BpnonSl);
	f_normal->GetObject("pr2DRjetb_B0snonSl",pr2DRjetb_B0snonSl);
	f_normal->GetObject("pr2DRjetb_LbnonSl",pr2DRjetb_LbnonSl);

  //Read the weighted sample
	f_scaled->GetObject("pr2DRjetb",pr2DRjetb_s);

	f_scaled->GetObject("pr2DRjetb_B0Sl",pr2DRjetb_B0Sl_s);
	f_scaled->GetObject("pr2DRjetb_BpSl",pr2DRjetb_BpSl_s);
	f_scaled->GetObject("pr2DRjetb_B0sSl",pr2DRjetb_B0sSl_s);
	f_scaled->GetObject("pr2DRjetb_LbSl",pr2DRjetb_LbSl_s);
	f_scaled->GetObject("pr2DRjetb_B0nonSl",pr2DRjetb_B0nonSl_s);
	f_scaled->GetObject("pr2DRjetb_BpnonSl",pr2DRjetb_BpnonSl_s);
	f_scaled->GetObject("pr2DRjetb_B0snonSl",pr2DRjetb_B0snonSl_s);
	f_scaled->GetObject("pr2DRjetb_LbnonSl",pr2DRjetb_LbnonSl_s);

	f_scaled_p->GetObject("pr2DRjetb",pr2DRjetb_s_p);

	f_scaled_p->GetObject("pr2DRjetb_B0Sl",pr2DRjetb_B0Sl_s_p);
	f_scaled_p->GetObject("pr2DRjetb_BpSl",pr2DRjetb_BpSl_s_p);
	f_scaled_p->GetObject("pr2DRjetb_B0sSl",pr2DRjetb_B0sSl_s_p);
	f_scaled_p->GetObject("pr2DRjetb_LbSl",pr2DRjetb_LbSl_s_p);
	f_scaled_p->GetObject("pr2DRjetb_B0nonSl",pr2DRjetb_B0nonSl_s_p);
	f_scaled_p->GetObject("pr2DRjetb_BpnonSl",pr2DRjetb_BpnonSl_s_p);
	f_scaled_p->GetObject("pr2DRjetb_B0snonSl",pr2DRjetb_B0snonSl_s_p);
	f_scaled_p->GetObject("pr2DRjetb_LbnonSl",pr2DRjetb_LbnonSl_s_p);

	f_scaled_m->GetObject("pr2DRjetb",pr2DRjetb_s_m);

	f_scaled_m->GetObject("pr2DRjetb_B0Sl",pr2DRjetb_B0Sl_s_m);
	f_scaled_m->GetObject("pr2DRjetb_BpSl",pr2DRjetb_BpSl_s_m);
	f_scaled_m->GetObject("pr2DRjetb_B0sSl",pr2DRjetb_B0sSl_s_m);
	f_scaled_m->GetObject("pr2DRjetb_LbSl",pr2DRjetb_LbSl_s_m);
	f_scaled_m->GetObject("pr2DRjetb_B0nonSl",pr2DRjetb_B0nonSl_s_m);
	f_scaled_m->GetObject("pr2DRjetb_BpnonSl",pr2DRjetb_BpnonSl_s_m);
	f_scaled_m->GetObject("pr2DRjetb_B0snonSl",pr2DRjetb_B0snonSl_s_m);
	f_scaled_m->GetObject("pr2DRjetb_LbnonSl",pr2DRjetb_LbnonSl_s_m);

	//calculate the difference in response between normal and the rescaled sample 
	//the second ProjectionX has new name as argument, otherwise would have same name as the previous
	//histo and causes memory leak 
	TH1D* hRjetb     = prRjetb   ->ProjectionX();
	TH1D* hRjetb_s   = prRjetb_s ->ProjectionX("pr2DRjetb_s");
	TH1D* hRjetb_s_p = prRjetb_s_p ->ProjectionX("pr2DRjetb_s_p");
	TH1D* hRjetb_s_m = prRjetb_s_m ->ProjectionX("pr2DRjetb_s_m");

	TH1D* diffb 		= (TH1D*) hRjetb_s->Clone();
	TH1D* diffb_p 	= (TH1D*) hRjetb_s_p->Clone();
	TH1D* diffb_m 	= (TH1D*) hRjetb_s_m->Clone();

	diffb->Add(hRjetb,-1);
	diffb_p->Add(hRjetb,-1);
	diffb_m->Add(hRjetb,-1);

	//evtW_* = event weight, sem_* = standard errorb of the mean,   
	double errorb, evtWtot, evtWtots;

	double evtWtot_B0Sl;
	double evtWtot_BpSl;
	double evtWtot_B0sSl;
	double evtWtot_LbSl;
	double evtWtot_B0nonSl;
	double evtWtot_BpnonSl;
	double evtWtot_B0snonSl;
	double evtWtot_LbnonSl;

	double evtWtots_B0Sl;
	double evtWtots_BpSl;
	double evtWtots_B0sSl;
	double evtWtots_LbSl;
	double evtWtots_B0nonSl;
	double evtWtots_BpnonSl;
	double evtWtots_B0snonSl;
	double evtWtots_LbnonSl;

	vector<double> evtW_B0Sl;
	vector<double> evtW_BpSl;
	vector<double> evtW_B0sSl;
	vector<double> evtW_LbSl;
	vector<double> evtW_B0nonSl;
	vector<double> evtW_BpnonSl;
	vector<double> evtW_B0snonSl;
	vector<double> evtW_LbnonSl;

	vector<double> evtWs_B0Sl;
	vector<double> evtWs_BpSl;
	vector<double> evtWs_B0sSl;
	vector<double> evtWs_LbSl;
	vector<double> evtWs_B0nonSl;
	vector<double> evtWs_BpnonSl;
	vector<double> evtWs_B0snonSl;
	vector<double> evtWs_LbnonSl;

	vector<double> sem_B0Sl;
	vector<double> sem_BpSl;
	vector<double> sem_B0sSl;
	vector<double> sem_LbSl;
	vector<double> sem_B0nonSl;
	vector<double> sem_BpnonSl;
	vector<double> sem_B0snonSl;
	vector<double> sem_LbnonSl;

	vector<double> vals_B0Sl;
	vector<double> vals_BpSl;
	vector<double> vals_B0sSl;
	vector<double> vals_LbSl;
	vector<double> vals_B0nonSl;
	vector<double> vals_BpnonSl;
	vector<double> vals_B0snonSl;
	vector<double> vals_LbnonSl;

	vector<double> vec_B0Sl_stat, vec_BpSl_stat, vec_B0sSl_stat, vec_LbSl_stat,
	 							 vec_B0nonSl_stat, vec_BpnonSl_stat, vec_B0snonSl_stat,vec_LbnonSl_stat;

	 ReadFromFile(vec_B0Sl_stat,"/home/anpirtti/ultimate_generator/final_rescaling/indvBtoSLBr/calc_scale_factors/scale_factors/indvBtoSlBr/stat_err/B0Sl_err.txt");
	 ReadFromFile(vec_BpSl_stat,"/home/anpirtti/ultimate_generator/final_rescaling/indvBtoSLBr/calc_scale_factors/scale_factors/indvBtoSlBr/stat_err/BpSl_err.txt");
	 ReadFromFile(vec_B0sSl_stat,"/home/anpirtti/ultimate_generator/final_rescaling/indvBtoSLBr/calc_scale_factors/scale_factors/indvBtoSlBr/stat_err/B0sSl_err.txt");
	 ReadFromFile(vec_LbSl_stat,"/home/anpirtti/ultimate_generator/final_rescaling/indvBtoSLBr/calc_scale_factors/scale_factors/indvBtoSlBr/stat_err/LbSl_err.txt");
	 ReadFromFile(vec_B0nonSl_stat,"/home/anpirtti/ultimate_generator/final_rescaling/indvBtoSLBr/calc_scale_factors/scale_factors/indvBtoSlBr/stat_err/B0nonSl_err.txt");
	 ReadFromFile(vec_BpnonSl_stat,"/home/anpirtti/ultimate_generator/final_rescaling/indvBtoSLBr/calc_scale_factors/scale_factors/indvBtoSlBr/stat_err/BpnonSl_err.txt");
	 ReadFromFile(vec_B0snonSl_stat,"/home/anpirtti/ultimate_generator/final_rescaling/indvBtoSLBr/calc_scale_factors/scale_factors/indvBtoSlBr/stat_err/B0snonSl_err.txt");
	 ReadFromFile(vec_LbnonSl_stat,"/home/anpirtti/ultimate_generator/final_rescaling/indvBtoSLBr/calc_scale_factors/scale_factors/indvBtoSlBr/stat_err/LbnonSl_err.txt"); 

	vector<vector<double>> vec_sf_sem{vec_B0Sl_stat, vec_BpSl_stat, vec_B0sSl_stat, vec_LbSl_stat,
	 							 vec_B0nonSl_stat, vec_BpnonSl_stat, vec_B0snonSl_stat,vec_LbnonSl_stat};
	
	//removing the under- and overflow bin values
	for (int i = 0; i < vec_sf_sem.size(); i++) {
		vec_sf_sem[i].erase(vec_sf_sem[i].begin() + 0);
		vec_sf_sem[i].erase(vec_sf_sem[i].begin() + vec_sf_sem[i].size() - 1);
	}

  //Loop over the TProfile bins
  for (int i = 1; i < hRjetb->GetNbinsX()+1; i++){
		evtW_B0Sl.clear();
		evtW_BpSl.clear();
		evtW_B0sSl.clear();
		evtW_LbSl.clear();
		evtW_B0nonSl.clear();
		evtW_BpnonSl.clear();
		evtW_B0snonSl.clear();
		evtW_LbnonSl.clear();

		evtWs_B0Sl.clear();
		evtWs_BpSl.clear();
		evtWs_B0sSl.clear();
		evtWs_LbSl.clear();
		evtWs_B0nonSl.clear();
		evtWs_BpnonSl.clear();
		evtWs_B0snonSl.clear();
		evtWs_LbnonSl.clear();

		sem_B0Sl.clear();
		sem_BpSl.clear();
		sem_B0sSl.clear();
		sem_LbSl.clear();
		sem_B0nonSl.clear();
		sem_BpnonSl.clear();
		sem_B0snonSl.clear();
		sem_LbnonSl.clear();

		vals_B0Sl.clear();
		vals_BpSl.clear();
		vals_B0sSl.clear();
		vals_LbSl.clear();
		vals_B0nonSl.clear();
		vals_BpnonSl.clear();
		vals_B0snonSl.clear();
		vals_LbnonSl.clear();

		for (int j = 1; j < hRjetb->GetNbinsX()+1; j++) {
			string pr_ind = "_" + to_string(i) + "_" + to_string(j);

			prRjetb_B0Sl = pr2DRjetb_B0Sl->ProfileX(("B0Sl"+pr_ind).c_str(),j,j);
			prRjetb_BpSl = pr2DRjetb_BpSl->ProfileX(("BpSl"+pr_ind).c_str(),j,j);
			prRjetb_B0sSl = pr2DRjetb_B0sSl->ProfileX(("B0sSl"+pr_ind).c_str(),j,j);
			prRjetb_LbSl = pr2DRjetb_LbSl->ProfileX(("LbSl"+pr_ind).c_str(),j,j);
			prRjetb_B0nonSl = pr2DRjetb_B0nonSl->ProfileX(("B0nonSl"+pr_ind).c_str(),j,j);
			prRjetb_BpnonSl = pr2DRjetb_BpnonSl->ProfileX(("BpnonSl"+pr_ind).c_str(),j,j);
			prRjetb_B0snonSl = pr2DRjetb_B0snonSl->ProfileX(("B0snonSl"+pr_ind).c_str(),j,j);
			prRjetb_LbnonSl = pr2DRjetb_LbnonSl->ProfileX(("LbnonSl"+pr_ind).c_str(),j,j);

			prRjetb_B0Sl_s = pr2DRjetb_B0Sl_s->ProfileX(("B0Sl_s"+pr_ind).c_str(),j,j);
			prRjetb_BpSl_s = pr2DRjetb_BpSl_s->ProfileX(("BpSl_s"+pr_ind).c_str(),j,j);
			prRjetb_B0sSl_s = pr2DRjetb_B0sSl_s->ProfileX(("B0sSl_s"+pr_ind).c_str(),j,j);
			prRjetb_LbSl_s = pr2DRjetb_LbSl_s->ProfileX(("LbSl_s"+pr_ind).c_str(),j,j);
			prRjetb_B0nonSl_s = pr2DRjetb_B0nonSl_s->ProfileX(("B0nonSl_s"+pr_ind).c_str(),j,j);
			prRjetb_BpnonSl_s = pr2DRjetb_BpnonSl_s->ProfileX(("BpnonSl_s"+pr_ind).c_str(),j,j);
			prRjetb_B0snonSl_s = pr2DRjetb_B0snonSl_s->ProfileX(("B0snonSl_s"+pr_ind).c_str(),j,j);
			prRjetb_LbnonSl_s = pr2DRjetb_LbnonSl_s->ProfileX(("LbnonSl_s"+pr_ind).c_str(),j,j);

			evtW_B0Sl.push_back(prRjetb_B0Sl->GetBinEntries(i));
			evtW_BpSl.push_back(prRjetb_BpSl->GetBinEntries(i));
			evtW_B0sSl.push_back(prRjetb_B0sSl->GetBinEntries(i));
			evtW_LbSl.push_back(prRjetb_LbSl->GetBinEntries(i));
			evtW_B0nonSl.push_back(prRjetb_B0nonSl->GetBinEntries(i));
			evtW_BpnonSl.push_back(prRjetb_BpnonSl->GetBinEntries(i));
			evtW_B0snonSl.push_back(prRjetb_B0snonSl->GetBinEntries(i));
			evtW_LbnonSl.push_back(prRjetb_LbnonSl->GetBinEntries(i));

			evtWs_B0Sl.push_back(prRjetb_B0Sl_s->GetBinEntries(i));
			evtWs_BpSl.push_back(prRjetb_BpSl_s->GetBinEntries(i));
			evtWs_B0sSl.push_back(prRjetb_B0sSl_s->GetBinEntries(i));
			evtWs_LbSl.push_back(prRjetb_LbSl_s->GetBinEntries(i));
			evtWs_B0nonSl.push_back(prRjetb_B0nonSl_s->GetBinEntries(i));
			evtWs_BpnonSl.push_back(prRjetb_BpnonSl_s->GetBinEntries(i));
			evtWs_B0snonSl.push_back(prRjetb_B0snonSl_s->GetBinEntries(i));
			evtWs_LbnonSl.push_back(prRjetb_LbnonSl_s->GetBinEntries(i));

			sem_B0Sl.push_back(prRjetb_B0Sl->GetBinError(i));
			sem_BpSl.push_back(prRjetb_BpSl->GetBinError(i));
			sem_B0sSl.push_back(prRjetb_B0sSl->GetBinError(i));
			sem_LbSl.push_back(prRjetb_LbSl->GetBinError(i));
			sem_B0nonSl.push_back(prRjetb_B0nonSl->GetBinError(i));
			sem_BpnonSl.push_back(prRjetb_BpnonSl->GetBinError(i));
			sem_B0snonSl.push_back(prRjetb_B0snonSl->GetBinError(i));
			sem_LbnonSl.push_back(prRjetb_LbnonSl->GetBinError(i));

			vals_B0Sl.push_back(prRjetb_B0Sl->GetBinContent(i));
			vals_BpSl.push_back(prRjetb_BpSl->GetBinContent(i));
			vals_B0sSl.push_back(prRjetb_B0sSl->GetBinContent(i));
			vals_LbSl.push_back(prRjetb_LbSl->GetBinContent(i));
			vals_B0nonSl.push_back(prRjetb_B0nonSl->GetBinContent(i));
			vals_BpnonSl.push_back(prRjetb_BpnonSl->GetBinContent(i));
			vals_B0snonSl.push_back(prRjetb_B0snonSl->GetBinContent(i));
			vals_LbnonSl.push_back(prRjetb_LbnonSl->GetBinContent(i));

			delete prRjetb_B0Sl;
			delete prRjetb_BpSl;
			delete prRjetb_B0sSl;
			delete prRjetb_LbSl;
			delete prRjetb_B0nonSl;
			delete prRjetb_BpnonSl;
			delete prRjetb_B0snonSl; 
			delete prRjetb_LbnonSl;

			delete prRjetb_B0Sl_s;
			delete prRjetb_BpSl_s;
			delete prRjetb_B0sSl_s;
			delete prRjetb_LbSl_s;
			delete prRjetb_B0nonSl_s;
			delete prRjetb_BpnonSl_s;
			delete prRjetb_B0snonSl_s;
			delete prRjetb_LbnonSl_s;
		}

		 
		vector<vector<double>> vec_evtW{evtW_B0Sl, evtW_BpSl, evtW_B0sSl, evtW_LbSl,
																	  evtW_B0nonSl, evtW_BpnonSl, evtW_B0snonSl, evtW_LbnonSl};

		vector<vector<double>> vec_evtWs{evtWs_B0Sl, evtWs_BpSl, evtWs_B0sSl, evtWs_LbSl,
																	  evtWs_B0nonSl, evtWs_BpnonSl, evtWs_B0snonSl, evtWs_LbnonSl};

		vector<vector<double>> vec_sem{sem_B0Sl, sem_BpSl, sem_B0sSl, sem_LbSl,
																	  sem_B0nonSl, sem_BpnonSl, sem_B0snonSl, sem_LbnonSl};

		vector<vector<double>> vec_vals{vals_B0Sl, vals_BpSl, vals_B0sSl, vals_LbSl,
																	  vals_B0nonSl, vals_BpnonSl, vals_B0snonSl, vals_LbnonSl};

		//set small number so that we dont divide by zero 
		evtWtot_B0Sl = 1e-10;
		evtWtot_BpSl = 1e-10;
		evtWtot_B0sSl = 1e-10;
		evtWtot_LbSl = 1e-10;
		evtWtot_B0nonSl = 1e-10;
		evtWtot_BpnonSl = 1e-10;
		evtWtot_B0snonSl = 1e-10;
		evtWtot_LbnonSl = 1e-10;

		evtWtots_B0Sl = 1e-10;
		evtWtots_BpSl = 1e-10;
		evtWtots_B0sSl = 1e-10;
		evtWtots_LbSl = 1e-10;
		evtWtots_B0nonSl = 1e-10;
		evtWtots_BpnonSl = 1e-10;
		evtWtots_B0snonSl = 1e-10;
		evtWtots_LbnonSl = 1e-10;

		for (double x : evtW_B0Sl) evtWtot_B0Sl += x;	
		for (double x : evtW_BpSl) evtWtot_BpSl += x;
		for (double x : evtW_B0sSl) evtWtot_B0sSl += x;
		for (double x : evtW_LbSl) evtWtot_LbSl += x;
		for (double x : evtW_B0nonSl) evtWtot_B0nonSl += x;
		for (double x : evtW_BpnonSl) evtWtot_BpnonSl += x;
		for (double x : evtW_B0snonSl) evtWtot_B0snonSl += x;
		for (double x : evtW_LbnonSl) evtWtot_LbnonSl += x;

		for (double x : evtWs_B0Sl) evtWtots_B0Sl += x;
		for (double x : evtWs_BpSl) evtWtots_BpSl += x;
		for (double x : evtWs_B0sSl) evtWtots_B0sSl += x;
		for (double x : evtWs_LbSl) evtWtots_LbSl += x;
		for (double x : evtWs_B0nonSl) evtWtots_B0nonSl += x;
		for (double x : evtWs_BpnonSl) evtWtots_BpnonSl += x;
		for (double x : evtWs_B0snonSl) evtWtots_B0snonSl += x;
		for (double x : evtWs_LbnonSl) evtWtots_LbnonSl += x;

		evtWtot = evtWtot_B0Sl + evtWtot_BpSl + evtWtot_B0sSl + evtWtot_LbSl +
						  evtWtot_B0nonSl +evtWtot_BpnonSl + evtWtot_B0snonSl + evtWtot_LbnonSl;

		evtWtots = evtWtots_B0Sl + evtWtots_BpSl + evtWtots_B0sSl + evtWtots_LbSl +
							 evtWtots_B0nonSl +evtWtots_BpnonSl + evtWtots_B0snonSl + evtWtots_LbnonSl;

		errorb = 0;
		for (int j = 0; j < hRjetb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hRjetb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}

		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb->GetBinContent(i) == 0) diffb->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb->SetBinError(i,errorb);
  }

	//############### plus variation ####################
  //Loop over the TProfile bins
  for (int i = 1; i < hRjetb->GetNbinsX()+1; i++){
		evtW_B0Sl.clear();
		evtW_BpSl.clear();
		evtW_B0sSl.clear();
		evtW_LbSl.clear();
		evtW_B0nonSl.clear();
		evtW_BpnonSl.clear();
		evtW_B0snonSl.clear();
		evtW_LbnonSl.clear();

		evtWs_B0Sl.clear();
		evtWs_BpSl.clear();
		evtWs_B0sSl.clear();
		evtWs_LbSl.clear();
		evtWs_B0nonSl.clear();
		evtWs_BpnonSl.clear();
		evtWs_B0snonSl.clear();
		evtWs_LbnonSl.clear();

		sem_B0Sl.clear();
		sem_BpSl.clear();
		sem_B0sSl.clear();
		sem_LbSl.clear();
		sem_B0nonSl.clear();
		sem_BpnonSl.clear();
		sem_B0snonSl.clear();
		sem_LbnonSl.clear();

		vals_B0Sl.clear();
		vals_BpSl.clear();
		vals_B0sSl.clear();
		vals_LbSl.clear();
		vals_B0nonSl.clear();
		vals_BpnonSl.clear();
		vals_B0snonSl.clear();
		vals_LbnonSl.clear();

		for (int j = 1; j < hRjetb->GetNbinsX()+1; j++) {
			string pr_ind = "_" + to_string(i) + "_" + to_string(j);

			prRjetb_B0Sl = pr2DRjetb_B0Sl->ProfileX(("B0Sl"+pr_ind).c_str(),j,j);
			prRjetb_BpSl = pr2DRjetb_BpSl->ProfileX(("BpSl"+pr_ind).c_str(),j,j);
			prRjetb_B0sSl = pr2DRjetb_B0sSl->ProfileX(("B0sSl"+pr_ind).c_str(),j,j);
			prRjetb_LbSl = pr2DRjetb_LbSl->ProfileX(("LbSl"+pr_ind).c_str(),j,j);
			prRjetb_B0nonSl = pr2DRjetb_B0nonSl->ProfileX(("B0nonSl"+pr_ind).c_str(),j,j);
			prRjetb_BpnonSl = pr2DRjetb_BpnonSl->ProfileX(("BpnonSl"+pr_ind).c_str(),j,j);
			prRjetb_B0snonSl = pr2DRjetb_B0snonSl->ProfileX(("B0snonSl"+pr_ind).c_str(),j,j);
			prRjetb_LbnonSl = pr2DRjetb_LbnonSl->ProfileX(("LbnonSl"+pr_ind).c_str(),j,j);

			prRjetb_B0Sl_s_p = pr2DRjetb_B0Sl_s_p->ProfileX(("B0Sl_s"+pr_ind).c_str(),j,j);
			prRjetb_BpSl_s_p = pr2DRjetb_BpSl_s_p->ProfileX(("BpSl_s"+pr_ind).c_str(),j,j);
			prRjetb_B0sSl_s_p = pr2DRjetb_B0sSl_s_p->ProfileX(("B0sSl_s"+pr_ind).c_str(),j,j);
			prRjetb_LbSl_s_p = pr2DRjetb_LbSl_s_p->ProfileX(("LbSl_s"+pr_ind).c_str(),j,j);
			prRjetb_B0nonSl_s_p = pr2DRjetb_B0nonSl_s_p->ProfileX(("B0nonSl_s"+pr_ind).c_str(),j,j);
			prRjetb_BpnonSl_s_p = pr2DRjetb_BpnonSl_s_p->ProfileX(("BpnonSl_s"+pr_ind).c_str(),j,j);
			prRjetb_B0snonSl_s_p = pr2DRjetb_B0snonSl_s_p->ProfileX(("B0snonSl_s"+pr_ind).c_str(),j,j);
			prRjetb_LbnonSl_s_p = pr2DRjetb_LbnonSl_s_p->ProfileX(("LbnonSl_s"+pr_ind).c_str(),j,j);

			evtW_B0Sl.push_back(prRjetb_B0Sl->GetBinEntries(i));
			evtW_BpSl.push_back(prRjetb_BpSl->GetBinEntries(i));
			evtW_B0sSl.push_back(prRjetb_B0sSl->GetBinEntries(i));
			evtW_LbSl.push_back(prRjetb_LbSl->GetBinEntries(i));
			evtW_B0nonSl.push_back(prRjetb_B0nonSl->GetBinEntries(i));
			evtW_BpnonSl.push_back(prRjetb_BpnonSl->GetBinEntries(i));
			evtW_B0snonSl.push_back(prRjetb_B0snonSl->GetBinEntries(i));
			evtW_LbnonSl.push_back(prRjetb_LbnonSl->GetBinEntries(i));

			evtWs_B0Sl.push_back(prRjetb_B0Sl_s_p->GetBinEntries(i));
			evtWs_BpSl.push_back(prRjetb_BpSl_s_p->GetBinEntries(i));
			evtWs_B0sSl.push_back(prRjetb_B0sSl_s_p->GetBinEntries(i));
			evtWs_LbSl.push_back(prRjetb_LbSl_s_p->GetBinEntries(i));
			evtWs_B0nonSl.push_back(prRjetb_B0nonSl_s_p->GetBinEntries(i));
			evtWs_BpnonSl.push_back(prRjetb_BpnonSl_s_p->GetBinEntries(i));
			evtWs_B0snonSl.push_back(prRjetb_B0snonSl_s_p->GetBinEntries(i));
			evtWs_LbnonSl.push_back(prRjetb_LbnonSl_s_p->GetBinEntries(i));

			sem_B0Sl.push_back(prRjetb_B0Sl->GetBinError(i));
			sem_BpSl.push_back(prRjetb_BpSl->GetBinError(i));
			sem_B0sSl.push_back(prRjetb_B0sSl->GetBinError(i));
			sem_LbSl.push_back(prRjetb_LbSl->GetBinError(i));
			sem_B0nonSl.push_back(prRjetb_B0nonSl->GetBinError(i));
			sem_BpnonSl.push_back(prRjetb_BpnonSl->GetBinError(i));
			sem_B0snonSl.push_back(prRjetb_B0snonSl->GetBinError(i));
			sem_LbnonSl.push_back(prRjetb_LbnonSl->GetBinError(i));

			vals_B0Sl.push_back(prRjetb_B0Sl->GetBinContent(i));
			vals_BpSl.push_back(prRjetb_BpSl->GetBinContent(i));
			vals_B0sSl.push_back(prRjetb_B0sSl->GetBinContent(i));
			vals_LbSl.push_back(prRjetb_LbSl->GetBinContent(i));
			vals_B0nonSl.push_back(prRjetb_B0nonSl->GetBinContent(i));
			vals_BpnonSl.push_back(prRjetb_BpnonSl->GetBinContent(i));
			vals_B0snonSl.push_back(prRjetb_B0snonSl->GetBinContent(i));
			vals_LbnonSl.push_back(prRjetb_LbnonSl->GetBinContent(i));

			delete prRjetb_B0Sl;
			delete prRjetb_BpSl;
			delete prRjetb_B0sSl;
			delete prRjetb_LbSl;
			delete prRjetb_B0nonSl;
			delete prRjetb_BpnonSl;
			delete prRjetb_B0snonSl; 
			delete prRjetb_LbnonSl;

			delete prRjetb_B0Sl_s_p;
			delete prRjetb_BpSl_s_p;
			delete prRjetb_B0sSl_s_p;
			delete prRjetb_LbSl_s_p;
			delete prRjetb_B0nonSl_s_p;
			delete prRjetb_BpnonSl_s_p;
			delete prRjetb_B0snonSl_s_p;
			delete prRjetb_LbnonSl_s_p;
		}

		 
		vector<vector<double>> vec_evtW{evtW_B0Sl, evtW_BpSl, evtW_B0sSl, evtW_LbSl,
																	  evtW_B0nonSl, evtW_BpnonSl, evtW_B0snonSl, evtW_LbnonSl};

		vector<vector<double>> vec_evtWs{evtWs_B0Sl, evtWs_BpSl, evtWs_B0sSl, evtWs_LbSl,
																	  evtWs_B0nonSl, evtWs_BpnonSl, evtWs_B0snonSl, evtWs_LbnonSl};

		vector<vector<double>> vec_sem{sem_B0Sl, sem_BpSl, sem_B0sSl, sem_LbSl,
																	  sem_B0nonSl, sem_BpnonSl, sem_B0snonSl, sem_LbnonSl};

		vector<vector<double>> vec_vals{vals_B0Sl, vals_BpSl, vals_B0sSl, vals_LbSl,
																	  vals_B0nonSl, vals_BpnonSl, vals_B0snonSl, vals_LbnonSl};

		//set small number so that we dont divide by zero 
		evtWtot_B0Sl = 1e-10;
		evtWtot_BpSl = 1e-10;
		evtWtot_B0sSl = 1e-10;
		evtWtot_LbSl = 1e-10;
		evtWtot_B0nonSl = 1e-10;
		evtWtot_BpnonSl = 1e-10;
		evtWtot_B0snonSl = 1e-10;
		evtWtot_LbnonSl = 1e-10;

		evtWtots_B0Sl = 1e-10;
		evtWtots_BpSl = 1e-10;
		evtWtots_B0sSl = 1e-10;
		evtWtots_LbSl = 1e-10;
		evtWtots_B0nonSl = 1e-10;
		evtWtots_BpnonSl = 1e-10;
		evtWtots_B0snonSl = 1e-10;
		evtWtots_LbnonSl = 1e-10;

		for (double x : evtW_B0Sl) evtWtot_B0Sl += x;	
		for (double x : evtW_BpSl) evtWtot_BpSl += x;
		for (double x : evtW_B0sSl) evtWtot_B0sSl += x;
		for (double x : evtW_LbSl) evtWtot_LbSl += x;
		for (double x : evtW_B0nonSl) evtWtot_B0nonSl += x;
		for (double x : evtW_BpnonSl) evtWtot_BpnonSl += x;
		for (double x : evtW_B0snonSl) evtWtot_B0snonSl += x;
		for (double x : evtW_LbnonSl) evtWtot_LbnonSl += x;

		for (double x : evtWs_B0Sl) evtWtots_B0Sl += x;
		for (double x : evtWs_BpSl) evtWtots_BpSl += x;
		for (double x : evtWs_B0sSl) evtWtots_B0sSl += x;
		for (double x : evtWs_LbSl) evtWtots_LbSl += x;
		for (double x : evtWs_B0nonSl) evtWtots_B0nonSl += x;
		for (double x : evtWs_BpnonSl) evtWtots_BpnonSl += x;
		for (double x : evtWs_B0snonSl) evtWtots_B0snonSl += x;
		for (double x : evtWs_LbnonSl) evtWtots_LbnonSl += x;

		evtWtot = evtWtot_B0Sl + evtWtot_BpSl + evtWtot_B0sSl + evtWtot_LbSl +
						  evtWtot_B0nonSl +evtWtot_BpnonSl + evtWtot_B0snonSl + evtWtot_LbnonSl;

		evtWtots = evtWtots_B0Sl + evtWtots_BpSl + evtWtots_B0sSl + evtWtots_LbSl +
							 evtWtots_B0nonSl +evtWtots_BpnonSl + evtWtots_B0snonSl + evtWtots_LbnonSl;

		errorb = 0;
		for (int j = 0; j < hRjetb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hRjetb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}

		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb_p->GetBinContent(i) == 0) diffb_p->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb_p->SetBinError(i,errorb);
  }

	//############### minus variation ####################
  //Loop over the TProfile bins
  for (int i = 1; i < hRjetb->GetNbinsX()+1; i++){
		evtW_B0Sl.clear();
		evtW_BpSl.clear();
		evtW_B0sSl.clear();
		evtW_LbSl.clear();
		evtW_B0nonSl.clear();
		evtW_BpnonSl.clear();
		evtW_B0snonSl.clear();
		evtW_LbnonSl.clear();

		evtWs_B0Sl.clear();
		evtWs_BpSl.clear();
		evtWs_B0sSl.clear();
		evtWs_LbSl.clear();
		evtWs_B0nonSl.clear();
		evtWs_BpnonSl.clear();
		evtWs_B0snonSl.clear();
		evtWs_LbnonSl.clear();

		sem_B0Sl.clear();
		sem_BpSl.clear();
		sem_B0sSl.clear();
		sem_LbSl.clear();
		sem_B0nonSl.clear();
		sem_BpnonSl.clear();
		sem_B0snonSl.clear();
		sem_LbnonSl.clear();

		vals_B0Sl.clear();
		vals_BpSl.clear();
		vals_B0sSl.clear();
		vals_LbSl.clear();
		vals_B0nonSl.clear();
		vals_BpnonSl.clear();
		vals_B0snonSl.clear();
		vals_LbnonSl.clear();

		for (int j = 1; j < hRjetb->GetNbinsX()+1; j++) {
			string pr_ind = "_" + to_string(i) + "_" + to_string(j);

			prRjetb_B0Sl = pr2DRjetb_B0Sl->ProfileX(("B0Sl"+pr_ind).c_str(),j,j);
			prRjetb_BpSl = pr2DRjetb_BpSl->ProfileX(("BpSl"+pr_ind).c_str(),j,j);
			prRjetb_B0sSl = pr2DRjetb_B0sSl->ProfileX(("B0sSl"+pr_ind).c_str(),j,j);
			prRjetb_LbSl = pr2DRjetb_LbSl->ProfileX(("LbSl"+pr_ind).c_str(),j,j);
			prRjetb_B0nonSl = pr2DRjetb_B0nonSl->ProfileX(("B0nonSl"+pr_ind).c_str(),j,j);
			prRjetb_BpnonSl = pr2DRjetb_BpnonSl->ProfileX(("BpnonSl"+pr_ind).c_str(),j,j);
			prRjetb_B0snonSl = pr2DRjetb_B0snonSl->ProfileX(("B0snonSl"+pr_ind).c_str(),j,j);
			prRjetb_LbnonSl = pr2DRjetb_LbnonSl->ProfileX(("LbnonSl"+pr_ind).c_str(),j,j);

			prRjetb_B0Sl_s_m = pr2DRjetb_B0Sl_s_m->ProfileX(("B0Sl_s"+pr_ind).c_str(),j,j);
			prRjetb_BpSl_s_m = pr2DRjetb_BpSl_s_m->ProfileX(("BpSl_s"+pr_ind).c_str(),j,j);
			prRjetb_B0sSl_s_m = pr2DRjetb_B0sSl_s_m->ProfileX(("B0sSl_s"+pr_ind).c_str(),j,j);
			prRjetb_LbSl_s_m = pr2DRjetb_LbSl_s_m->ProfileX(("LbSl_s"+pr_ind).c_str(),j,j);
			prRjetb_B0nonSl_s_m = pr2DRjetb_B0nonSl_s_m->ProfileX(("B0nonSl_s"+pr_ind).c_str(),j,j);
			prRjetb_BpnonSl_s_m = pr2DRjetb_BpnonSl_s_m->ProfileX(("BpnonSl_s"+pr_ind).c_str(),j,j);
			prRjetb_B0snonSl_s_m = pr2DRjetb_B0snonSl_s_m->ProfileX(("B0snonSl_s"+pr_ind).c_str(),j,j);
			prRjetb_LbnonSl_s_m = pr2DRjetb_LbnonSl_s_m->ProfileX(("LbnonSl_s"+pr_ind).c_str(),j,j);

			evtW_B0Sl.push_back(prRjetb_B0Sl->GetBinEntries(i));
			evtW_BpSl.push_back(prRjetb_BpSl->GetBinEntries(i));
			evtW_B0sSl.push_back(prRjetb_B0sSl->GetBinEntries(i));
			evtW_LbSl.push_back(prRjetb_LbSl->GetBinEntries(i));
			evtW_B0nonSl.push_back(prRjetb_B0nonSl->GetBinEntries(i));
			evtW_BpnonSl.push_back(prRjetb_BpnonSl->GetBinEntries(i));
			evtW_B0snonSl.push_back(prRjetb_B0snonSl->GetBinEntries(i));
			evtW_LbnonSl.push_back(prRjetb_LbnonSl->GetBinEntries(i));

			evtWs_B0Sl.push_back(prRjetb_B0Sl_s_m->GetBinEntries(i));
			evtWs_BpSl.push_back(prRjetb_BpSl_s_m->GetBinEntries(i));
			evtWs_B0sSl.push_back(prRjetb_B0sSl_s_m->GetBinEntries(i));
			evtWs_LbSl.push_back(prRjetb_LbSl_s_m->GetBinEntries(i));
			evtWs_B0nonSl.push_back(prRjetb_B0nonSl_s_m->GetBinEntries(i));
			evtWs_BpnonSl.push_back(prRjetb_BpnonSl_s_m->GetBinEntries(i));
			evtWs_B0snonSl.push_back(prRjetb_B0snonSl_s_m->GetBinEntries(i));
			evtWs_LbnonSl.push_back(prRjetb_LbnonSl_s_m->GetBinEntries(i));

			sem_B0Sl.push_back(prRjetb_B0Sl->GetBinError(i));
			sem_BpSl.push_back(prRjetb_BpSl->GetBinError(i));
			sem_B0sSl.push_back(prRjetb_B0sSl->GetBinError(i));
			sem_LbSl.push_back(prRjetb_LbSl->GetBinError(i));
			sem_B0nonSl.push_back(prRjetb_B0nonSl->GetBinError(i));
			sem_BpnonSl.push_back(prRjetb_BpnonSl->GetBinError(i));
			sem_B0snonSl.push_back(prRjetb_B0snonSl->GetBinError(i));
			sem_LbnonSl.push_back(prRjetb_LbnonSl->GetBinError(i));

			vals_B0Sl.push_back(prRjetb_B0Sl->GetBinContent(i));
			vals_BpSl.push_back(prRjetb_BpSl->GetBinContent(i));
			vals_B0sSl.push_back(prRjetb_B0sSl->GetBinContent(i));
			vals_LbSl.push_back(prRjetb_LbSl->GetBinContent(i));
			vals_B0nonSl.push_back(prRjetb_B0nonSl->GetBinContent(i));
			vals_BpnonSl.push_back(prRjetb_BpnonSl->GetBinContent(i));
			vals_B0snonSl.push_back(prRjetb_B0snonSl->GetBinContent(i));
			vals_LbnonSl.push_back(prRjetb_LbnonSl->GetBinContent(i));

			delete prRjetb_B0Sl;
			delete prRjetb_BpSl;
			delete prRjetb_B0sSl;
			delete prRjetb_LbSl;
			delete prRjetb_B0nonSl;
			delete prRjetb_BpnonSl;
			delete prRjetb_B0snonSl; 
			delete prRjetb_LbnonSl;

			delete prRjetb_B0Sl_s_m;
			delete prRjetb_BpSl_s_m;
			delete prRjetb_B0sSl_s_m;
			delete prRjetb_LbSl_s_m;
			delete prRjetb_B0nonSl_s_m;
			delete prRjetb_BpnonSl_s_m;
			delete prRjetb_B0snonSl_s_m;
			delete prRjetb_LbnonSl_s_m;
		}

		 
		vector<vector<double>> vec_evtW{evtW_B0Sl, evtW_BpSl, evtW_B0sSl, evtW_LbSl,
																	  evtW_B0nonSl, evtW_BpnonSl, evtW_B0snonSl, evtW_LbnonSl};

		vector<vector<double>> vec_evtWs{evtWs_B0Sl, evtWs_BpSl, evtWs_B0sSl, evtWs_LbSl,
																	  evtWs_B0nonSl, evtWs_BpnonSl, evtWs_B0snonSl, evtWs_LbnonSl};

		vector<vector<double>> vec_sem{sem_B0Sl, sem_BpSl, sem_B0sSl, sem_LbSl,
																	  sem_B0nonSl, sem_BpnonSl, sem_B0snonSl, sem_LbnonSl};

		vector<vector<double>> vec_vals{vals_B0Sl, vals_BpSl, vals_B0sSl, vals_LbSl,
																	  vals_B0nonSl, vals_BpnonSl, vals_B0snonSl, vals_LbnonSl};

		//set small number so that we dont divide by zero 
		evtWtot_B0Sl = 1e-10;
		evtWtot_BpSl = 1e-10;
		evtWtot_B0sSl = 1e-10;
		evtWtot_LbSl = 1e-10;
		evtWtot_B0nonSl = 1e-10;
		evtWtot_BpnonSl = 1e-10;
		evtWtot_B0snonSl = 1e-10;
		evtWtot_LbnonSl = 1e-10;

		evtWtots_B0Sl = 1e-10;
		evtWtots_BpSl = 1e-10;
		evtWtots_B0sSl = 1e-10;
		evtWtots_LbSl = 1e-10;
		evtWtots_B0nonSl = 1e-10;
		evtWtots_BpnonSl = 1e-10;
		evtWtots_B0snonSl = 1e-10;
		evtWtots_LbnonSl = 1e-10;

		for (double x : evtW_B0Sl) evtWtot_B0Sl += x;	
		for (double x : evtW_BpSl) evtWtot_BpSl += x;
		for (double x : evtW_B0sSl) evtWtot_B0sSl += x;
		for (double x : evtW_LbSl) evtWtot_LbSl += x;
		for (double x : evtW_B0nonSl) evtWtot_B0nonSl += x;
		for (double x : evtW_BpnonSl) evtWtot_BpnonSl += x;
		for (double x : evtW_B0snonSl) evtWtot_B0snonSl += x;
		for (double x : evtW_LbnonSl) evtWtot_LbnonSl += x;

		for (double x : evtWs_B0Sl) evtWtots_B0Sl += x;
		for (double x : evtWs_BpSl) evtWtots_BpSl += x;
		for (double x : evtWs_B0sSl) evtWtots_B0sSl += x;
		for (double x : evtWs_LbSl) evtWtots_LbSl += x;
		for (double x : evtWs_B0nonSl) evtWtots_B0nonSl += x;
		for (double x : evtWs_BpnonSl) evtWtots_BpnonSl += x;
		for (double x : evtWs_B0snonSl) evtWtots_B0snonSl += x;
		for (double x : evtWs_LbnonSl) evtWtots_LbnonSl += x;

		evtWtot = evtWtot_B0Sl + evtWtot_BpSl + evtWtot_B0sSl + evtWtot_LbSl +
						  evtWtot_B0nonSl +evtWtot_BpnonSl + evtWtot_B0snonSl + evtWtot_LbnonSl;

		evtWtots = evtWtots_B0Sl + evtWtots_BpSl + evtWtots_B0sSl + evtWtots_LbSl +
							 evtWtots_B0nonSl +evtWtots_BpnonSl + evtWtots_B0snonSl + evtWtots_LbnonSl;

		errorb = 0;
		for (int j = 0; j < hRjetb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hRjetb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}

		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb_m->GetBinContent(i) == 0) diffb_m->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb_m->SetBinError(i,errorb);
  }  

	//Scale to the percentages
  diffb->Scale(100.0);
  diffb_p->Scale(100.0);
  diffb_m->Scale(100.0);

	Rjetb_map["diffb"] = diffb;
	Rjetb_map["diffb_p"] = diffb_p;
	Rjetb_map["diffb_m"] = diffb_m;

	return Rjetb_map;
}

map<string, TH1D*> mpf_BtoCBr() {
	map <string, TH1D*> MPFb_map;

  TFile* f_normal = TFile::Open(OpenName.c_str());
  TFile* f_scaled = TFile::Open(OpenName_BtoCBr.c_str());
  TFile* f_scaled_p = TFile::Open(OpenName_BtoCBr_p.c_str());
  TFile* f_scaled_m = TFile::Open(OpenName_BtoCBr_m.c_str());

	TProfile *prMPFb;
	TProfile *prMPFb_s;
	TProfile *prMPFb_s_p;
	TProfile *prMPFb_s_m;

  //Read the normal sample
  f_normal->GetObject("prMPFb",   prMPFb);

  //Read the weighted sample
  f_scaled->GetObject("prMPFb",   prMPFb_s);
  f_scaled_p->GetObject("prMPFb",   prMPFb_s_p);
  f_scaled_m->GetObject("prMPFb",   prMPFb_s_m);

	//2D Tprofiles for the errorb calculations
	TProfile2D* pr2DMPFb;

	TProfile2D* pr2DMPFb_B0Dp;
	TProfile2D* pr2DMPFb_B0D0;
	TProfile2D* pr2DMPFb_B0Dps;
	TProfile2D* pr2DMPFb_BpDp;
	TProfile2D* pr2DMPFb_BpD0;
	TProfile2D* pr2DMPFb_BpDps;
	TProfile2D* pr2DMPFb_B0sDps;
	TProfile2D* pr2DMPFb_LbLc;
	TProfile2D* pr2DMPFb_B0nonC;
	TProfile2D* pr2DMPFb_BpnonC;
	TProfile2D* pr2DMPFb_B0snonC;
	TProfile2D* pr2DMPFb_LbnonC;

	TProfile2D* pr2DMPFb_B0DpD0;
	TProfile2D* pr2DMPFb_B0DpDps;
	TProfile2D* pr2DMPFb_B0D0Dps;
	TProfile2D* pr2DMPFb_BpDpD0;
	TProfile2D* pr2DMPFb_BpDpDps;
	TProfile2D* pr2DMPFb_BpD0Dps;

  //The sample with the additional weighting (*_s)
	TProfile2D* pr2DMPFb_s;

	TProfile2D* pr2DMPFb_B0Dp_s;
	TProfile2D* pr2DMPFb_B0D0_s;
	TProfile2D* pr2DMPFb_B0Dps_s;
	TProfile2D* pr2DMPFb_BpDp_s;
	TProfile2D* pr2DMPFb_BpD0_s;
	TProfile2D* pr2DMPFb_BpDps_s;
	TProfile2D* pr2DMPFb_B0sDps_s;
	TProfile2D* pr2DMPFb_LbLc_s;
	TProfile2D* pr2DMPFb_B0nonC_s;
	TProfile2D* pr2DMPFb_BpnonC_s;
	TProfile2D* pr2DMPFb_B0snonC_s;
	TProfile2D* pr2DMPFb_LbnonC_s;

	TProfile2D* pr2DMPFb_B0DpD0_s;
	TProfile2D* pr2DMPFb_B0DpDps_s;
	TProfile2D* pr2DMPFb_B0D0Dps_s;
	TProfile2D* pr2DMPFb_BpDpD0_s;
	TProfile2D* pr2DMPFb_BpDpDps_s;
	TProfile2D* pr2DMPFb_BpD0Dps_s;

	TProfile2D* pr2DMPFb_s_p;

	TProfile2D* pr2DMPFb_B0Dp_s_p;
	TProfile2D* pr2DMPFb_B0D0_s_p;
	TProfile2D* pr2DMPFb_B0Dps_s_p;
	TProfile2D* pr2DMPFb_BpDp_s_p;
	TProfile2D* pr2DMPFb_BpD0_s_p;
	TProfile2D* pr2DMPFb_BpDps_s_p;
	TProfile2D* pr2DMPFb_B0sDps_s_p;
	TProfile2D* pr2DMPFb_LbLc_s_p;
	TProfile2D* pr2DMPFb_B0nonC_s_p;
	TProfile2D* pr2DMPFb_BpnonC_s_p;
	TProfile2D* pr2DMPFb_B0snonC_s_p;
	TProfile2D* pr2DMPFb_LbnonC_s_p;

	TProfile2D* pr2DMPFb_B0DpD0_s_p;
	TProfile2D* pr2DMPFb_B0DpDps_s_p;
	TProfile2D* pr2DMPFb_B0D0Dps_s_p;
	TProfile2D* pr2DMPFb_BpDpD0_s_p;
	TProfile2D* pr2DMPFb_BpDpDps_s_p;
	TProfile2D* pr2DMPFb_BpD0Dps_s_p;

	TProfile2D* pr2DMPFb_s_m;

	TProfile2D* pr2DMPFb_B0Dp_s_m;
	TProfile2D* pr2DMPFb_B0D0_s_m;
	TProfile2D* pr2DMPFb_B0Dps_s_m;
	TProfile2D* pr2DMPFb_BpDp_s_m;
	TProfile2D* pr2DMPFb_BpD0_s_m;
	TProfile2D* pr2DMPFb_BpDps_s_m;
	TProfile2D* pr2DMPFb_B0sDps_s_m;
	TProfile2D* pr2DMPFb_LbLc_s_m;
	TProfile2D* pr2DMPFb_B0nonC_s_m;
	TProfile2D* pr2DMPFb_BpnonC_s_m;
	TProfile2D* pr2DMPFb_B0snonC_s_m;
	TProfile2D* pr2DMPFb_LbnonC_s_m;

	TProfile2D* pr2DMPFb_B0DpD0_s_m;
	TProfile2D* pr2DMPFb_B0DpDps_s_m;
	TProfile2D* pr2DMPFb_B0D0Dps_s_m;
	TProfile2D* pr2DMPFb_BpDpD0_s_m;
	TProfile2D* pr2DMPFb_BpDpDps_s_m;
	TProfile2D* pr2DMPFb_BpD0Dps_s_m;

	//Tprofiles for the errorb calculations
	TProfile* prMPFb_B0Dp;
	TProfile* prMPFb_B0D0;
	TProfile* prMPFb_B0Dps;
	TProfile* prMPFb_BpDp;
	TProfile* prMPFb_BpD0;
	TProfile* prMPFb_BpDps;
	TProfile* prMPFb_B0sDps;
	TProfile* prMPFb_LbLc;
	TProfile* prMPFb_B0nonC;
	TProfile* prMPFb_BpnonC;
	TProfile* prMPFb_B0snonC;
	TProfile* prMPFb_LbnonC;

	TProfile* prMPFb_B0DpD0;
	TProfile* prMPFb_B0DpDps;
	TProfile* prMPFb_B0D0Dps;
	TProfile* prMPFb_BpDpD0;
	TProfile* prMPFb_BpDpDps;
	TProfile* prMPFb_BpD0Dps;

  //The sample with the additional weighting (*_s)
	TProfile* prMPFb_B0Dp_s;
	TProfile* prMPFb_B0D0_s;
	TProfile* prMPFb_B0Dps_s;
	TProfile* prMPFb_BpDp_s;
	TProfile* prMPFb_BpD0_s;
	TProfile* prMPFb_BpDps_s;
	TProfile* prMPFb_B0sDps_s;
	TProfile* prMPFb_LbLc_s;
	TProfile* prMPFb_B0nonC_s;
	TProfile* prMPFb_BpnonC_s;
	TProfile* prMPFb_B0snonC_s;
	TProfile* prMPFb_LbnonC_s;

	TProfile* prMPFb_B0DpD0_s;
	TProfile* prMPFb_B0DpDps_s;
	TProfile* prMPFb_B0D0Dps_s;
	TProfile* prMPFb_BpDpD0_s;
	TProfile* prMPFb_BpDpDps_s;
	TProfile* prMPFb_BpD0Dps_s;

	TProfile* prMPFb_B0Dp_s_p;
	TProfile* prMPFb_B0D0_s_p;
	TProfile* prMPFb_B0Dps_s_p;
	TProfile* prMPFb_BpDp_s_p;
	TProfile* prMPFb_BpD0_s_p;
	TProfile* prMPFb_BpDps_s_p;
	TProfile* prMPFb_B0sDps_s_p;
	TProfile* prMPFb_LbLc_s_p;
	TProfile* prMPFb_B0nonC_s_p;
	TProfile* prMPFb_BpnonC_s_p;
	TProfile* prMPFb_B0snonC_s_p;
	TProfile* prMPFb_LbnonC_s_p;

	TProfile* prMPFb_B0DpD0_s_p;
	TProfile* prMPFb_B0DpDps_s_p;
	TProfile* prMPFb_B0D0Dps_s_p;
	TProfile* prMPFb_BpDpD0_s_p;
	TProfile* prMPFb_BpDpDps_s_p;
	TProfile* prMPFb_BpD0Dps_s_p;

	TProfile* prMPFb_B0Dp_s_m;
	TProfile* prMPFb_B0D0_s_m;
	TProfile* prMPFb_B0Dps_s_m;
	TProfile* prMPFb_BpDp_s_m;
	TProfile* prMPFb_BpD0_s_m;
	TProfile* prMPFb_BpDps_s_m;
	TProfile* prMPFb_B0sDps_s_m;
	TProfile* prMPFb_LbLc_s_m;
	TProfile* prMPFb_B0nonC_s_m;
	TProfile* prMPFb_BpnonC_s_m;
	TProfile* prMPFb_B0snonC_s_m;
	TProfile* prMPFb_LbnonC_s_m;

	TProfile* prMPFb_B0DpD0_s_m;
	TProfile* prMPFb_B0DpDps_s_m;
	TProfile* prMPFb_B0D0Dps_s_m;
	TProfile* prMPFb_BpDpD0_s_m;
	TProfile* prMPFb_BpDpDps_s_m;
	TProfile* prMPFb_BpD0Dps_s_m;

  //Read the normal sample
	f_normal->GetObject("pr2DMPFb",pr2DMPFb);

	f_normal->GetObject("pr2DMPFb_B0Dp",pr2DMPFb_B0Dp);
	f_normal->GetObject("pr2DMPFb_B0D0",pr2DMPFb_B0D0);
	f_normal->GetObject("pr2DMPFb_B0Dps",pr2DMPFb_B0Dps);
	f_normal->GetObject("pr2DMPFb_BpDp",pr2DMPFb_BpDp);
	f_normal->GetObject("pr2DMPFb_BpD0",pr2DMPFb_BpD0);
	f_normal->GetObject("pr2DMPFb_BpDps",pr2DMPFb_BpDps);
	f_normal->GetObject("pr2DMPFb_B0sDps",pr2DMPFb_B0sDps);
	f_normal->GetObject("pr2DMPFb_LbLc",pr2DMPFb_LbLc);
	f_normal->GetObject("pr2DMPFb_B0nonC",pr2DMPFb_B0nonC);
	f_normal->GetObject("pr2DMPFb_BpnonC",pr2DMPFb_BpnonC);
	f_normal->GetObject("pr2DMPFb_B0snonC",pr2DMPFb_B0snonC);
	f_normal->GetObject("pr2DMPFb_LbnonC",pr2DMPFb_LbnonC);

	f_normal->GetObject("pr2DMPFb_B0DpD0",pr2DMPFb_B0DpD0);
	f_normal->GetObject("pr2DMPFb_B0DpDps",pr2DMPFb_B0DpDps);
	f_normal->GetObject("pr2DMPFb_B0D0Dps",pr2DMPFb_B0D0Dps);
	f_normal->GetObject("pr2DMPFb_BpDpD0",pr2DMPFb_BpDpD0);
	f_normal->GetObject("pr2DMPFb_BpDpDps",pr2DMPFb_BpDpDps);
	f_normal->GetObject("pr2DMPFb_BpD0Dps",pr2DMPFb_BpD0Dps);

  //Read the weighted sample
	f_scaled->GetObject("pr2DMPFb",pr2DMPFb_s);

	f_scaled->GetObject("pr2DMPFb_B0Dp",pr2DMPFb_B0Dp_s);
	f_scaled->GetObject("pr2DMPFb_B0D0",pr2DMPFb_B0D0_s);
	f_scaled->GetObject("pr2DMPFb_B0Dps",pr2DMPFb_B0Dps_s);
	f_scaled->GetObject("pr2DMPFb_BpDp",pr2DMPFb_BpDp_s);
	f_scaled->GetObject("pr2DMPFb_BpD0",pr2DMPFb_BpD0_s);
	f_scaled->GetObject("pr2DMPFb_BpDps",pr2DMPFb_BpDps_s);
	f_scaled->GetObject("pr2DMPFb_B0sDps",pr2DMPFb_B0sDps_s);
	f_scaled->GetObject("pr2DMPFb_LbLc",pr2DMPFb_LbLc_s);
	f_scaled->GetObject("pr2DMPFb_B0nonC",pr2DMPFb_B0nonC_s);
	f_scaled->GetObject("pr2DMPFb_BpnonC",pr2DMPFb_BpnonC_s);
	f_scaled->GetObject("pr2DMPFb_B0snonC",pr2DMPFb_B0snonC_s);
	f_scaled->GetObject("pr2DMPFb_LbnonC",pr2DMPFb_LbnonC_s);

	f_scaled->GetObject("pr2DMPFb_B0DpD0",pr2DMPFb_B0DpD0_s);
	f_scaled->GetObject("pr2DMPFb_B0DpDps",pr2DMPFb_B0DpDps_s);
	f_scaled->GetObject("pr2DMPFb_B0D0Dps",pr2DMPFb_B0D0Dps_s);
	f_scaled->GetObject("pr2DMPFb_BpDpD0",pr2DMPFb_BpDpD0_s);
	f_scaled->GetObject("pr2DMPFb_BpDpDps",pr2DMPFb_BpDpDps_s);
	f_scaled->GetObject("pr2DMPFb_BpD0Dps",pr2DMPFb_BpD0Dps_s);

	f_scaled_p->GetObject("pr2DMPFb",pr2DMPFb_s_p);

	f_scaled_p->GetObject("pr2DMPFb_B0Dp",pr2DMPFb_B0Dp_s_p);
	f_scaled_p->GetObject("pr2DMPFb_B0D0",pr2DMPFb_B0D0_s_p);
	f_scaled_p->GetObject("pr2DMPFb_B0Dps",pr2DMPFb_B0Dps_s_p);
	f_scaled_p->GetObject("pr2DMPFb_BpDp",pr2DMPFb_BpDp_s_p);
	f_scaled_p->GetObject("pr2DMPFb_BpD0",pr2DMPFb_BpD0_s_p);
	f_scaled_p->GetObject("pr2DMPFb_BpDps",pr2DMPFb_BpDps_s_p);
	f_scaled_p->GetObject("pr2DMPFb_B0sDps",pr2DMPFb_B0sDps_s_p);
	f_scaled_p->GetObject("pr2DMPFb_LbLc",pr2DMPFb_LbLc_s_p);
	f_scaled_p->GetObject("pr2DMPFb_B0nonC",pr2DMPFb_B0nonC_s_p);
	f_scaled_p->GetObject("pr2DMPFb_BpnonC",pr2DMPFb_BpnonC_s_p);
	f_scaled_p->GetObject("pr2DMPFb_B0snonC",pr2DMPFb_B0snonC_s_p);
	f_scaled_p->GetObject("pr2DMPFb_LbnonC",pr2DMPFb_LbnonC_s_p);

	f_scaled_p->GetObject("pr2DMPFb_B0DpD0",pr2DMPFb_B0DpD0_s_p);
	f_scaled_p->GetObject("pr2DMPFb_B0DpDps",pr2DMPFb_B0DpDps_s_p);
	f_scaled_p->GetObject("pr2DMPFb_B0D0Dps",pr2DMPFb_B0D0Dps_s_p);
	f_scaled_p->GetObject("pr2DMPFb_BpDpD0",pr2DMPFb_BpDpD0_s_p);
	f_scaled_p->GetObject("pr2DMPFb_BpDpDps",pr2DMPFb_BpDpDps_s_p);
	f_scaled_p->GetObject("pr2DMPFb_BpD0Dps",pr2DMPFb_BpD0Dps_s_p);

	f_scaled_m->GetObject("pr2DMPFb",pr2DMPFb_s_m);

	f_scaled_m->GetObject("pr2DMPFb_B0Dp",pr2DMPFb_B0Dp_s_m);
	f_scaled_m->GetObject("pr2DMPFb_B0D0",pr2DMPFb_B0D0_s_m);
	f_scaled_m->GetObject("pr2DMPFb_B0Dps",pr2DMPFb_B0Dps_s_m);
	f_scaled_m->GetObject("pr2DMPFb_BpDp",pr2DMPFb_BpDp_s_m);
	f_scaled_m->GetObject("pr2DMPFb_BpD0",pr2DMPFb_BpD0_s_m);
	f_scaled_m->GetObject("pr2DMPFb_BpDps",pr2DMPFb_BpDps_s_m);
	f_scaled_m->GetObject("pr2DMPFb_B0sDps",pr2DMPFb_B0sDps_s_m);
	f_scaled_m->GetObject("pr2DMPFb_LbLc",pr2DMPFb_LbLc_s_m);
	f_scaled_m->GetObject("pr2DMPFb_B0nonC",pr2DMPFb_B0nonC_s_m);
	f_scaled_m->GetObject("pr2DMPFb_BpnonC",pr2DMPFb_BpnonC_s_m);
	f_scaled_m->GetObject("pr2DMPFb_B0snonC",pr2DMPFb_B0snonC_s_m);
	f_scaled_m->GetObject("pr2DMPFb_LbnonC",pr2DMPFb_LbnonC_s_m);

	f_scaled_m->GetObject("pr2DMPFb_B0DpD0",pr2DMPFb_B0DpD0_s_m);
	f_scaled_m->GetObject("pr2DMPFb_B0DpDps",pr2DMPFb_B0DpDps_s_m);
	f_scaled_m->GetObject("pr2DMPFb_B0D0Dps",pr2DMPFb_B0D0Dps_s_m);
	f_scaled_m->GetObject("pr2DMPFb_BpDpD0",pr2DMPFb_BpDpD0_s_m);
	f_scaled_m->GetObject("pr2DMPFb_BpDpDps",pr2DMPFb_BpDpDps_s_m);
	f_scaled_m->GetObject("pr2DMPFb_BpD0Dps",pr2DMPFb_BpD0Dps_s_m);

	//calculate the difference in response between normal and the rescaled sample 
	//the second ProjectionX has new name as argument, otherwise would have same name as the previous
	//histo and causes memory leak 
	TH1D* hMPFb     = prMPFb   ->ProjectionX();
	TH1D* hMPFb_s   = prMPFb_s ->ProjectionX("pr2DMPFb_s");
	TH1D* hMPFb_s_p = prMPFb_s_p ->ProjectionX("pr2DMPFb_s_p");
	TH1D* hMPFb_s_m = prMPFb_s_m ->ProjectionX("pr2DMPFb_s_m");

	TH1D* diffb 		= (TH1D*) hMPFb_s->Clone();
	TH1D* diffb_p 	= (TH1D*) hMPFb_s_p->Clone();
	TH1D* diffb_m 	= (TH1D*) hMPFb_s_m->Clone();

	diffb->Add(hMPFb,-1);
	diffb_p->Add(hMPFb,-1);
	diffb_m->Add(hMPFb,-1);

	//evtW_* = event weight, sem_* = standard errorb of the mean,   
	double errorb, evtWtot, evtWtots;

	double evtWtot_B0Dp;
	double evtWtot_B0D0;
	double evtWtot_B0Dps;
	double evtWtot_BpDp;
	double evtWtot_BpD0;
	double evtWtot_BpDps;
	double evtWtot_B0sDps;
	double evtWtot_LbLc;
	double evtWtot_B0nonC;
	double evtWtot_BpnonC;
	double evtWtot_B0snonC;
	double evtWtot_LbnonC;

	double evtWtot_B0DpD0;
	double evtWtot_B0DpDps;
	double evtWtot_B0D0Dps;
	double evtWtot_BpDpD0;
	double evtWtot_BpDpDps;
	double evtWtot_BpD0Dps;

	double evtWtots_B0Dp;
	double evtWtots_B0D0;
	double evtWtots_B0Dps;
	double evtWtots_BpDp;
	double evtWtots_BpD0;
	double evtWtots_BpDps;
	double evtWtots_B0sDps;
	double evtWtots_LbLc;
	double evtWtots_B0nonC;
	double evtWtots_BpnonC;
	double evtWtots_B0snonC;
	double evtWtots_LbnonC;

	double evtWtots_B0DpD0;
	double evtWtots_B0DpDps;
	double evtWtots_B0D0Dps;
	double evtWtots_BpDpD0;
	double evtWtots_BpDpDps;
	double evtWtots_BpD0Dps;

	vector<double> evtW_B0Dp;
	vector<double> evtW_B0D0;
	vector<double> evtW_B0Dps;
	vector<double> evtW_BpDp;
	vector<double> evtW_BpD0;
	vector<double> evtW_BpDps;
	vector<double> evtW_B0sDps;
	vector<double> evtW_LbLc;
	vector<double> evtW_B0nonC;
	vector<double> evtW_BpnonC;
	vector<double> evtW_B0snonC;
	vector<double> evtW_LbnonC;

	vector<double> evtW_B0DpD0;
	vector<double> evtW_B0DpDps;
	vector<double> evtW_B0D0Dps;
	vector<double> evtW_BpDpD0;
	vector<double> evtW_BpDpDps;
	vector<double> evtW_BpD0Dps;

	vector<double> evtWs_B0Dp;
	vector<double> evtWs_B0D0;
	vector<double> evtWs_B0Dps;
	vector<double> evtWs_BpDp;
	vector<double> evtWs_BpD0;
	vector<double> evtWs_BpDps;
	vector<double> evtWs_B0sDps;
	vector<double> evtWs_LbLc;
	vector<double> evtWs_B0nonC;
	vector<double> evtWs_BpnonC;
	vector<double> evtWs_B0snonC;
	vector<double> evtWs_LbnonC;

	vector<double> evtWs_B0DpD0;
	vector<double> evtWs_B0DpDps;
	vector<double> evtWs_B0D0Dps;
	vector<double> evtWs_BpDpD0;
	vector<double> evtWs_BpDpDps;
	vector<double> evtWs_BpD0Dps;

	vector<double> sem_B0Dp;
	vector<double> sem_B0D0;
	vector<double> sem_B0Dps;
	vector<double> sem_BpDp;
	vector<double> sem_BpD0;
	vector<double> sem_BpDps;
	vector<double> sem_B0sDps;
	vector<double> sem_LbLc;
	vector<double> sem_B0nonC;
	vector<double> sem_BpnonC;
	vector<double> sem_B0snonC;
	vector<double> sem_LbnonC;

	vector<double> sem_B0DpD0;
	vector<double> sem_B0DpDps;
	vector<double> sem_B0D0Dps;
	vector<double> sem_BpDpD0;
	vector<double> sem_BpDpDps;
	vector<double> sem_BpD0Dps;

	vector<double> vals_B0Dp;
	vector<double> vals_B0D0;
	vector<double> vals_B0Dps;
	vector<double> vals_BpDp;
	vector<double> vals_BpD0;
	vector<double> vals_BpDps;
	vector<double> vals_B0sDps;
	vector<double> vals_LbLc;
	vector<double> vals_B0nonC;
	vector<double> vals_BpnonC;
	vector<double> vals_B0snonC;
	vector<double> vals_LbnonC;

	vector<double> vals_B0DpD0;
	vector<double> vals_B0DpDps;
	vector<double> vals_B0D0Dps;
	vector<double> vals_BpDpD0;
	vector<double> vals_BpDpDps;
	vector<double> vals_BpD0Dps;


	vector<double> vec_B0Dp_stat, vec_B0D0_stat, vec_B0Dps_stat, vec_BpDp_stat,
								 vec_BpD0_stat, vec_BpDps_stat, vec_B0sDps_stat, vec_LbLc_stat,
								 vec_B0nonC_stat, vec_BpnonC_stat, vec_B0snonC_stat, vec_LbnonC_stat,
								 vec_B0DpD0_stat,	vec_B0DpDps_stat,	vec_B0D0Dps_stat,	vec_BpDpD0_stat,
								 vec_BpDpDps_stat, vec_BpD0Dps_stat;

	ReadFromFile(vec_B0Dp_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/stat_err/B0Dp_sf_stat.txt");
	ReadFromFile(vec_B0D0_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/stat_err/B0D0_sf_stat.txt");
	ReadFromFile(vec_B0Dps_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/stat_err/B0Dps_sf_stat.txt");
	ReadFromFile(vec_BpDp_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/stat_err/BpDp_sf_stat.txt");
	ReadFromFile(vec_BpD0_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/stat_err/BpD0_sf_stat.txt");
	ReadFromFile(vec_BpDps_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/stat_err/BpDps_sf_stat.txt");
	ReadFromFile(vec_B0sDps_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/stat_err/B0sDps_sf_stat.txt");
	ReadFromFile(vec_LbLc_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/stat_err/LbLc_sf_stat.txt");
	ReadFromFile(vec_B0nonC_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/stat_err/B0nonC_sf_stat.txt");
	ReadFromFile(vec_BpnonC_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/stat_err/BpnonC_sf_stat.txt");
	ReadFromFile(vec_B0snonC_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/stat_err/B0snonC_sf_stat.txt");
	ReadFromFile(vec_LbnonC_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/stat_err/LbnonC_sf_stat.txt");

	//calculating the stat error of the categories with two scaling factors 
	vector<double> vec_B0Dp_sf, vec_B0D0_sf, vec_B0Dps_sf, vec_BpDp_sf,
								 vec_BpD0_sf, vec_BpDps_sf, vec_B0sDps_sf, vec_LbLc_sf,
								 vec_B0nonC_sf, vec_BpnonC_sf, vec_B0snonC_sf, vec_LbnonC_sf;

	ReadFromFile(vec_B0Dp_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/normal/B0Dp_sf.txt");
	ReadFromFile(vec_B0D0_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/normal/B0D0_sf.txt");
	ReadFromFile(vec_B0Dps_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/normal/B0Dps_sf.txt");
	ReadFromFile(vec_BpDp_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/normal/BpDp_sf.txt");
	ReadFromFile(vec_BpD0_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/normal/BpD0_sf.txt");
	ReadFromFile(vec_BpDps_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/normal/BpDps_sf.txt");
	ReadFromFile(vec_B0sDps_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/normal/B0sDps_sf.txt");
	ReadFromFile(vec_LbLc_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/normal/LbLc_sf.txt");
	ReadFromFile(vec_B0nonC_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/normal/B0nonC_sf.txt");
	ReadFromFile(vec_BpnonC_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/normal/BpnonC_sf.txt");
	ReadFromFile(vec_B0snonC_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/normal/B0snonC_sf.txt");
	ReadFromFile(vec_LbnonC_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/normal/LbnonC_sf.txt");

	for (int i = 0; i < vec_B0Dp_sf.size(); i++) {
		vec_B0DpD0_stat.push_back(sqrt(pow(vec_B0D0_sf[i]*vec_B0Dp_stat[i],2) + pow(vec_B0Dp_sf[i]*vec_B0D0_stat[i],2)));
		vec_B0DpDps_stat.push_back(sqrt(pow(vec_B0Dps_sf[i]*vec_B0Dp_stat[i],2) + pow(vec_B0Dp_sf[i]*vec_B0Dps_stat[i],2)));
		vec_B0D0Dps_stat.push_back(sqrt(pow(vec_B0Dps_sf[i]*vec_B0D0_stat[i],2) + pow(vec_B0D0_sf[i]*vec_B0Dps_stat[i],2)));

		vec_BpDpD0_stat.push_back(sqrt(pow(vec_BpD0_sf[i]*vec_BpDp_stat[i],2) + pow(vec_BpDp_sf[i]*vec_BpD0_stat[i],2)));
		vec_BpDpDps_stat.push_back(sqrt(pow(vec_BpDps_sf[i]*vec_BpDp_stat[i],2) + pow(vec_BpDp_sf[i]*vec_BpDps_stat[i],2)));
		vec_BpD0Dps_stat.push_back(sqrt(pow(vec_BpDps_sf[i]*vec_BpD0_stat[i],2) + pow(vec_BpD0_sf[i]*vec_BpDps_stat[i],2)));
	}

	vector<vector<double>> vec_sf_sem{vec_B0Dp_stat, vec_B0D0_stat, vec_B0Dps_stat, vec_BpDp_stat,
								 vec_BpD0_stat, vec_BpDps_stat, vec_B0sDps_stat, vec_LbLc_stat,
								 vec_B0nonC_stat, vec_BpnonC_stat, vec_B0snonC_stat, vec_LbnonC_stat,
								 vec_B0DpD0_stat,	vec_B0DpDps_stat,	vec_B0D0Dps_stat,	vec_BpDpD0_stat,
								 vec_BpDpDps_stat, vec_BpD0Dps_stat};

	//removing the under- and overflow bin values
	for (int i = 0; i < vec_sf_sem.size(); i++) {
		vec_sf_sem[i].erase(vec_sf_sem[i].begin() + 0);
		vec_sf_sem[i].erase(vec_sf_sem[i].begin() + vec_sf_sem[i].size() - 1);
	}

  //Loop over the TProfile bins
  for (int i = 1; i < hMPFb->GetNbinsX()+1; i++){
		evtW_B0Dp.clear();
		evtW_B0D0.clear();
		evtW_B0Dps.clear();
		evtW_BpDp.clear();
		evtW_BpD0.clear();
		evtW_BpDps.clear();
		evtW_B0sDps.clear();
		evtW_LbLc.clear();
		evtW_B0nonC.clear();
		evtW_BpnonC.clear();
		evtW_B0snonC.clear();
		evtW_LbnonC.clear();

		evtW_B0DpD0.clear();
		evtW_B0DpDps.clear();
		evtW_B0D0Dps.clear();
		evtW_BpDpD0.clear();
		evtW_BpDpDps.clear();
		evtW_BpD0Dps.clear();

		evtWs_B0Dp.clear();
		evtWs_B0D0.clear();
		evtWs_B0Dps.clear();
		evtWs_BpDp.clear();
		evtWs_BpD0.clear();
		evtWs_BpDps.clear();
		evtWs_B0sDps.clear();
		evtWs_LbLc.clear();
		evtWs_B0nonC.clear();
		evtWs_BpnonC.clear();
		evtWs_B0snonC.clear();
		evtWs_LbnonC.clear();

		evtWs_B0DpD0.clear();
		evtWs_B0DpDps.clear();
		evtWs_B0D0Dps.clear();
		evtWs_BpDpD0.clear();
		evtWs_BpDpDps.clear();
		evtWs_BpD0Dps.clear();

		sem_B0Dp.clear();
		sem_B0D0.clear();
		sem_B0Dps.clear();
		sem_BpDp.clear();
		sem_BpD0.clear();
		sem_BpDps.clear();
		sem_B0sDps.clear();
		sem_LbLc.clear();
		sem_B0nonC.clear();
		sem_BpnonC.clear();
		sem_B0snonC.clear();
		sem_LbnonC.clear();

		sem_B0DpD0.clear();
		sem_B0DpDps.clear();
		sem_B0D0Dps.clear();
		sem_BpDpD0.clear();
		sem_BpDpDps.clear();
		sem_BpD0Dps.clear();

		vals_B0Dp.clear();
		vals_B0D0.clear();
		vals_B0Dps.clear();
		vals_BpDp.clear();
		vals_BpD0.clear();
		vals_BpDps.clear();
		vals_B0sDps.clear();
		vals_LbLc.clear();
		vals_B0nonC.clear();
		vals_BpnonC.clear();
		vals_B0snonC.clear();
		vals_LbnonC.clear();

		vals_B0DpD0.clear();
		vals_B0DpDps.clear();
		vals_B0D0Dps.clear();
		vals_BpDpD0.clear();
		vals_BpDpDps.clear();
		vals_BpD0Dps.clear();

		for (int j = 1; j < hMPFb->GetNbinsX()+1; j++) {
			string pr_ind = "_" + to_string(i) + "_" + to_string(j);

			prMPFb_B0Dp = pr2DMPFb_B0Dp->ProfileX(("B0Dp"+pr_ind).c_str(),j,j);
			prMPFb_B0D0 = pr2DMPFb_B0D0->ProfileX(("B0D0"+pr_ind).c_str(),j,j);
			prMPFb_B0Dps = pr2DMPFb_B0Dps->ProfileX(("B0Dps"+pr_ind).c_str(),j,j);
			prMPFb_BpDp = pr2DMPFb_BpDp->ProfileX(("BpDp"+pr_ind).c_str(),j,j);
			prMPFb_BpD0 = pr2DMPFb_BpD0->ProfileX(("BpD0"+pr_ind).c_str(),j,j);
			prMPFb_BpDps = pr2DMPFb_BpDps->ProfileX(("BpDps"+pr_ind).c_str(),j,j);
			prMPFb_B0sDps = pr2DMPFb_B0sDps->ProfileX(("B0sDps"+pr_ind).c_str(),j,j);
			prMPFb_LbLc = pr2DMPFb_LbLc->ProfileX(("LbLc"+pr_ind).c_str(),j,j);
			prMPFb_B0nonC = pr2DMPFb_B0nonC->ProfileX(("B0nonC"+pr_ind).c_str(),j,j);
			prMPFb_BpnonC = pr2DMPFb_BpnonC->ProfileX(("BpnonC"+pr_ind).c_str(),j,j);
			prMPFb_B0snonC = pr2DMPFb_B0snonC->ProfileX(("B0snonC"+pr_ind).c_str(),j,j);
			prMPFb_LbnonC = pr2DMPFb_LbnonC->ProfileX(("LbnonC"+pr_ind).c_str(),j,j);

			prMPFb_B0DpD0 = pr2DMPFb_B0DpD0->ProfileX(("B0DpD0"+pr_ind).c_str(),j,j);
			prMPFb_B0DpDps = pr2DMPFb_B0DpDps->ProfileX(("B0DpDps"+pr_ind).c_str(),j,j);
			prMPFb_B0D0Dps = pr2DMPFb_B0D0Dps->ProfileX(("B0D0Dps"+pr_ind).c_str(),j,j);
			prMPFb_BpDpD0 = pr2DMPFb_BpDpD0->ProfileX(("BpDpD0"+pr_ind).c_str(),j,j);
			prMPFb_BpDpDps = pr2DMPFb_BpDpDps->ProfileX(("BpDpDps"+pr_ind).c_str(),j,j);
			prMPFb_BpD0Dps = pr2DMPFb_BpD0Dps->ProfileX(("BpD0Dps"+pr_ind).c_str(),j,j);

			prMPFb_B0Dp_s = pr2DMPFb_B0Dp_s->ProfileX(("B0Dp_s"+pr_ind).c_str(),j,j);
			prMPFb_B0D0_s = pr2DMPFb_B0D0_s->ProfileX(("B0D0_s"+pr_ind).c_str(),j,j);
			prMPFb_B0Dps_s = pr2DMPFb_B0Dps_s->ProfileX(("B0Dps_s"+pr_ind).c_str(),j,j);
			prMPFb_BpDp_s = pr2DMPFb_BpDp_s->ProfileX(("BpDp_s"+pr_ind).c_str(),j,j);
			prMPFb_BpD0_s = pr2DMPFb_BpD0_s->ProfileX(("BpD0_s"+pr_ind).c_str(),j,j);
			prMPFb_BpDps_s = pr2DMPFb_BpDps_s->ProfileX(("BpDps_s"+pr_ind).c_str(),j,j);
			prMPFb_B0sDps_s = pr2DMPFb_B0sDps_s->ProfileX(("B0sDps_s"+pr_ind).c_str(),j,j);
			prMPFb_LbLc_s = pr2DMPFb_LbLc_s->ProfileX(("LbLc_s"+pr_ind).c_str(),j,j);
			prMPFb_B0nonC_s = pr2DMPFb_B0nonC_s->ProfileX(("B0nonC_s"+pr_ind).c_str(),j,j);
			prMPFb_BpnonC_s = pr2DMPFb_BpnonC_s->ProfileX(("BpnonC_s"+pr_ind).c_str(),j,j);
			prMPFb_B0snonC_s = pr2DMPFb_B0snonC_s->ProfileX(("B0snonC_s"+pr_ind).c_str(),j,j);
			prMPFb_LbnonC_s = pr2DMPFb_LbnonC_s->ProfileX(("LbnonC_s"+pr_ind).c_str(),j,j);

			prMPFb_B0DpD0_s = pr2DMPFb_B0DpD0_s->ProfileX(("B0DpD0_s"+pr_ind).c_str(),j,j);
			prMPFb_B0DpDps_s = pr2DMPFb_B0DpDps_s->ProfileX(("B0DpDps_s"+pr_ind).c_str(),j,j);
			prMPFb_B0D0Dps_s = pr2DMPFb_B0D0Dps_s->ProfileX(("B0D0Dps_s"+pr_ind).c_str(),j,j);
			prMPFb_BpDpD0_s = pr2DMPFb_BpDpD0_s->ProfileX(("BpDpD0_s"+pr_ind).c_str(),j,j);
			prMPFb_BpDpDps_s = pr2DMPFb_BpDpDps_s->ProfileX(("BpDpDps_s"+pr_ind).c_str(),j,j);
			prMPFb_BpD0Dps_s = pr2DMPFb_BpD0Dps_s->ProfileX(("BpD0Dps_s"+pr_ind).c_str(),j,j);

			evtW_B0Dp.push_back(prMPFb_B0Dp->GetBinEntries(i));
			evtW_B0D0.push_back(prMPFb_B0D0->GetBinEntries(i));
			evtW_B0Dps.push_back(prMPFb_B0Dps->GetBinEntries(i));
			evtW_BpDp.push_back(prMPFb_BpDp->GetBinEntries(i));
			evtW_BpD0.push_back(prMPFb_BpD0->GetBinEntries(i));
			evtW_BpDps.push_back(prMPFb_BpDps->GetBinEntries(i));
			evtW_B0sDps.push_back(prMPFb_B0sDps->GetBinEntries(i));
			evtW_LbLc.push_back(prMPFb_LbLc->GetBinEntries(i));
			evtW_B0nonC.push_back(prMPFb_B0nonC->GetBinEntries(i));
			evtW_BpnonC.push_back(prMPFb_BpnonC->GetBinEntries(i));
			evtW_B0snonC.push_back(prMPFb_B0snonC->GetBinEntries(i));
			evtW_LbnonC.push_back(prMPFb_LbnonC->GetBinEntries(i));

			evtW_B0DpD0.push_back(prMPFb_B0DpD0->GetBinEntries(i));
			evtW_B0DpDps.push_back(prMPFb_B0DpDps->GetBinEntries(i));
			evtW_B0D0Dps.push_back(prMPFb_B0D0Dps->GetBinEntries(i));
			evtW_BpDpD0.push_back(prMPFb_BpDpD0->GetBinEntries(i));
			evtW_BpDpDps.push_back(prMPFb_BpDpDps->GetBinEntries(i));
			evtW_BpD0Dps.push_back(prMPFb_BpD0Dps->GetBinEntries(i));

			evtWs_B0Dp.push_back(prMPFb_B0Dp_s->GetBinEntries(i));
			evtWs_B0D0.push_back(prMPFb_B0D0_s->GetBinEntries(i));
			evtWs_B0Dps.push_back(prMPFb_B0Dps_s->GetBinEntries(i));
			evtWs_BpDp.push_back(prMPFb_BpDp_s->GetBinEntries(i));
			evtWs_BpD0.push_back(prMPFb_BpD0_s->GetBinEntries(i));
			evtWs_BpDps.push_back(prMPFb_BpDps_s->GetBinEntries(i));
			evtWs_B0sDps.push_back(prMPFb_B0sDps_s->GetBinEntries(i));
			evtWs_LbLc.push_back(prMPFb_LbLc_s->GetBinEntries(i));
			evtWs_B0nonC.push_back(prMPFb_B0nonC_s->GetBinEntries(i));
			evtWs_BpnonC.push_back(prMPFb_BpnonC_s->GetBinEntries(i));
			evtWs_B0snonC.push_back(prMPFb_B0snonC_s->GetBinEntries(i));
			evtWs_LbnonC.push_back(prMPFb_LbnonC_s->GetBinEntries(i));

			evtWs_B0DpD0.push_back(prMPFb_B0DpD0_s->GetBinEntries(i));
			evtWs_B0DpDps.push_back(prMPFb_B0DpDps_s->GetBinEntries(i));
			evtWs_B0D0Dps.push_back(prMPFb_B0D0Dps_s->GetBinEntries(i));
			evtWs_BpDpD0.push_back(prMPFb_BpDpD0_s->GetBinEntries(i));
			evtWs_BpDpDps.push_back(prMPFb_BpDpDps_s->GetBinEntries(i));
			evtWs_BpD0Dps.push_back(prMPFb_BpD0Dps_s->GetBinEntries(i));

			sem_B0Dp.push_back(prMPFb_B0Dp->GetBinError(i));
			sem_B0D0.push_back(prMPFb_B0D0->GetBinError(i));
			sem_B0Dps.push_back(prMPFb_B0Dps->GetBinError(i));
			sem_BpDp.push_back(prMPFb_BpDp->GetBinError(i));
			sem_BpD0.push_back(prMPFb_BpD0->GetBinError(i));
			sem_BpDps.push_back(prMPFb_BpDps->GetBinError(i));
			sem_B0sDps.push_back(prMPFb_B0sDps->GetBinError(i));
			sem_LbLc.push_back(prMPFb_LbLc->GetBinError(i));
			sem_B0nonC.push_back(prMPFb_B0nonC->GetBinError(i));
			sem_BpnonC.push_back(prMPFb_BpnonC->GetBinError(i));
			sem_B0snonC.push_back(prMPFb_B0snonC->GetBinError(i));
			sem_LbnonC.push_back(prMPFb_LbnonC->GetBinError(i));

			sem_B0DpD0.push_back(prMPFb_B0DpD0->GetBinError(i));
			sem_B0DpDps.push_back(prMPFb_B0DpDps->GetBinError(i));
			sem_B0D0Dps.push_back(prMPFb_B0D0Dps->GetBinError(i));
			sem_BpDpD0.push_back(prMPFb_BpDpD0->GetBinError(i));
			sem_BpDpDps.push_back(prMPFb_BpDpDps->GetBinError(i));
			sem_BpD0Dps.push_back(prMPFb_BpD0Dps->GetBinError(i));

			vals_B0Dp.push_back(prMPFb_B0Dp->GetBinContent(i));
			vals_B0D0.push_back(prMPFb_B0D0->GetBinContent(i));
			vals_B0Dps.push_back(prMPFb_B0Dps->GetBinContent(i));
			vals_BpDp.push_back(prMPFb_BpDp->GetBinContent(i));
			vals_BpD0.push_back(prMPFb_BpD0->GetBinContent(i));
			vals_BpDps.push_back(prMPFb_BpDps->GetBinContent(i));
			vals_B0sDps.push_back(prMPFb_B0sDps->GetBinContent(i));
			vals_LbLc.push_back(prMPFb_LbLc->GetBinContent(i));
			vals_B0nonC.push_back(prMPFb_B0nonC->GetBinContent(i));
			vals_BpnonC.push_back(prMPFb_BpnonC->GetBinContent(i));
			vals_B0snonC.push_back(prMPFb_B0snonC->GetBinContent(i));
			vals_LbnonC.push_back(prMPFb_LbnonC->GetBinContent(i));

			vals_B0DpD0.push_back(prMPFb_B0DpD0->GetBinContent(i));
			vals_B0DpDps.push_back(prMPFb_B0DpDps->GetBinContent(i));
			vals_B0D0Dps.push_back(prMPFb_B0D0Dps->GetBinContent(i));
			vals_BpDpD0.push_back(prMPFb_BpDpD0->GetBinContent(i));
			vals_BpDpDps.push_back(prMPFb_BpDpDps->GetBinContent(i));
			vals_BpD0Dps.push_back(prMPFb_BpD0Dps->GetBinContent(i));

			delete prMPFb_B0Dp;
			delete prMPFb_B0D0;
			delete prMPFb_B0Dps;
			delete prMPFb_BpDp;
			delete prMPFb_BpD0;
			delete prMPFb_BpDps;
			delete prMPFb_B0sDps;
			delete prMPFb_LbLc;
			delete prMPFb_B0nonC;
			delete prMPFb_BpnonC;
			delete prMPFb_B0snonC; 
			delete prMPFb_LbnonC;

			delete prMPFb_B0DpD0;
			delete prMPFb_B0DpDps;
			delete prMPFb_B0D0Dps;
			delete prMPFb_BpDpD0; 
			delete prMPFb_BpDpDps;
			delete prMPFb_BpD0Dps;

			delete prMPFb_B0Dp_s;
			delete prMPFb_B0D0_s;
			delete prMPFb_B0Dps_s;
			delete prMPFb_BpDp_s;
			delete prMPFb_BpD0_s;
			delete prMPFb_BpDps_s;
			delete prMPFb_B0sDps_s;
			delete prMPFb_LbLc_s;
			delete prMPFb_B0nonC_s;
			delete prMPFb_BpnonC_s;
			delete prMPFb_B0snonC_s;
			delete prMPFb_LbnonC_s;

			delete prMPFb_B0DpD0_s;
			delete prMPFb_B0DpDps_s;
			delete prMPFb_B0D0Dps_s;
			delete prMPFb_BpDpD0_s;
			delete prMPFb_BpDpDps_s;
			delete prMPFb_BpD0Dps_s;
		}

		vector<vector<double>> vec_evtW{evtW_B0Dp, evtW_B0D0, evtW_B0Dps, evtW_BpDp,
								 evtW_BpD0, evtW_BpDps, evtW_B0sDps, evtW_LbLc,
								 evtW_B0nonC, evtW_BpnonC, evtW_B0snonC, evtW_LbnonC,
								 evtW_B0DpD0,	evtW_B0DpDps,	evtW_B0D0Dps,	evtW_BpDpD0,
								 evtW_BpDpDps, evtW_BpD0Dps};

		vector<vector<double>> vec_evtWs{evtWs_B0Dp, evtWs_B0D0, evtWs_B0Dps, evtWs_BpDp,
								 evtWs_BpD0, evtWs_BpDps, evtWs_B0sDps, evtWs_LbLc,
								 evtWs_B0nonC, evtWs_BpnonC, evtWs_B0snonC, evtWs_LbnonC,
								 evtWs_B0DpD0,	evtWs_B0DpDps,	evtWs_B0D0Dps,	evtWs_BpDpD0,
								 evtWs_BpDpDps, evtWs_BpD0Dps};

		vector<vector<double>> vec_sem{sem_B0Dp, sem_B0D0, sem_B0Dps, sem_BpDp,
								 sem_BpD0, sem_BpDps, sem_B0sDps, sem_LbLc,
								 sem_B0nonC, sem_BpnonC, sem_B0snonC, sem_LbnonC,
								 sem_B0DpD0,	sem_B0DpDps,	sem_B0D0Dps,	sem_BpDpD0,
								 sem_BpDpDps, sem_BpD0Dps};

		vector<vector<double>> vec_vals{
vals_B0Dp, vals_B0D0, vals_B0Dps, vals_BpDp,
								 vals_BpD0, vals_BpDps, vals_B0sDps, vals_LbLc,
								 vals_B0nonC, vals_BpnonC, vals_B0snonC, vals_LbnonC,
								 vals_B0DpD0,	vals_B0DpDps,	vals_B0D0Dps,	vals_BpDpD0,
								 vals_BpDpDps, vals_BpD0Dps};

		//set small number so that we dont divide by zero 
		evtWtot_B0Dp = 1e-10;
		evtWtot_B0D0 = 1e-10;
		evtWtot_B0Dps = 1e-10;
		evtWtot_BpDp = 1e-10;
		evtWtot_BpD0 = 1e-10;
		evtWtot_BpDps = 1e-10;
		evtWtot_B0sDps = 1e-10;
		evtWtot_LbLc = 1e-10;
		evtWtot_B0nonC = 1e-10;
		evtWtot_BpnonC = 1e-10;
		evtWtot_B0snonC = 1e-10;
		evtWtot_LbnonC = 1e-10;

		evtWtot_B0DpD0 = 1e-10;
		evtWtot_B0DpDps = 1e-10;
		evtWtot_B0D0Dps = 1e-10;
		evtWtot_BpDpD0 = 1e-10;
		evtWtot_BpDpDps = 1e-10;
		evtWtot_BpD0Dps = 1e-10;

		evtWtots_B0Dp = 1e-10;
		evtWtots_B0D0 = 1e-10;
		evtWtots_B0Dps = 1e-10;
		evtWtots_BpDp = 1e-10;
		evtWtots_BpD0 = 1e-10;
		evtWtots_BpDps = 1e-10;
		evtWtots_B0sDps = 1e-10;
		evtWtots_LbLc = 1e-10;
		evtWtots_B0nonC = 1e-10;
		evtWtots_BpnonC = 1e-10;
		evtWtots_B0snonC = 1e-10;
		evtWtots_LbnonC = 1e-10;

		evtWtots_B0DpD0 = 1e-10;
		evtWtots_B0DpDps = 1e-10;
		evtWtots_B0D0Dps = 1e-10;
		evtWtots_BpDpD0 = 1e-10;
		evtWtots_BpDpDps = 1e-10;
		evtWtots_BpD0Dps = 1e-10;

		for (double x : evtW_B0Dp) evtWtot_B0Dp += x;	
		for (double x : evtW_B0D0) evtWtot_B0D0 += x;
		for (double x : evtW_B0Dps) evtWtot_B0Dps += x;
		for (double x : evtW_BpDp) evtWtot_BpDp += x;
		for (double x : evtW_BpD0) evtWtot_BpD0 += x;
		for (double x : evtW_BpDps) evtWtot_BpDps += x;
		for (double x : evtW_B0sDps) evtWtot_B0sDps += x;
		for (double x : evtW_LbLc) evtWtot_LbLc += x;
		for (double x : evtW_B0nonC) evtWtot_B0nonC += x;
		for (double x : evtW_BpnonC) evtWtot_BpnonC += x;
		for (double x : evtW_B0snonC) evtWtot_B0snonC += x;
		for (double x : evtW_LbnonC) evtWtot_LbnonC += x;

		for (double x : evtW_B0DpD0) evtWtot_B0DpD0 += x;
		for (double x : evtW_B0DpDps) evtWtot_B0DpDps += x;
		for (double x : evtW_B0D0Dps) evtWtot_B0D0Dps += x;
		for (double x : evtW_BpDpD0) evtWtot_BpDpD0 += x;
		for (double x : evtW_BpDpDps) evtWtot_BpDpDps += x;
		for (double x : evtW_BpD0Dps) evtWtot_BpD0Dps += x;

		for (double x : evtWs_B0Dp) evtWtots_B0Dp += x;
		for (double x : evtWs_B0D0) evtWtots_B0D0 += x;
		for (double x : evtWs_B0Dps) evtWtots_B0Dps += x;
		for (double x : evtWs_BpDp) evtWtots_BpDp += x;
		for (double x : evtWs_BpD0) evtWtots_BpD0 += x;
		for (double x : evtWs_BpDps) evtWtots_BpDps += x;
		for (double x : evtWs_B0sDps) evtWtots_B0sDps += x;
		for (double x : evtWs_LbLc) evtWtots_LbLc += x;
		for (double x : evtWs_B0nonC) evtWtots_B0nonC += x;
		for (double x : evtWs_BpnonC) evtWtots_BpnonC += x;
		for (double x : evtWs_B0snonC) evtWtots_B0snonC += x;
		for (double x : evtWs_LbnonC) evtWtots_LbnonC += x;

		for (double x : evtWs_B0DpD0) evtWtots_B0DpD0 += x;
		for (double x : evtWs_B0DpDps) evtWtots_B0DpDps += x;
		for (double x : evtWs_B0D0Dps) evtWtots_B0D0Dps += x;
		for (double x : evtWs_BpDpD0) evtWtots_BpDpD0 += x;
		for (double x : evtWs_BpDpDps) evtWtots_BpDpDps += x;
		for (double x : evtWs_BpD0Dps) evtWtots_BpD0Dps += x;
		
		evtWtot = evtWtot_B0Dp + evtWtot_B0D0 + evtWtot_B0Dps + evtWtot_BpDp + evtWtot_BpD0 +
						 	evtWtot_BpDps + evtWtot_B0sDps + evtWtot_LbLc + evtWtot_B0nonC +evtWtot_BpnonC +
							evtWtot_B0snonC + evtWtot_LbnonC + evtWtot_B0DpD0 + evtWtot_B0DpDps +
							evtWtot_B0D0Dps + evtWtot_BpDpD0 + evtWtot_BpDpDps + evtWtot_BpD0Dps;

		evtWtots = evtWtots_B0Dp + evtWtots_B0D0 + evtWtots_B0Dps + evtWtots_BpDp + evtWtots_BpD0 +
						 	 evtWtots_BpDps + evtWtots_B0sDps + evtWtots_LbLc + evtWtots_B0nonC +evtWtots_BpnonC +
							 evtWtots_B0snonC + evtWtots_LbnonC + evtWtots_B0DpD0 + evtWtots_B0DpDps +
							 evtWtots_B0D0Dps + evtWtots_BpDpD0 + evtWtots_BpDpDps + evtWtots_BpD0Dps;

		//loop over diff scale factors 
		errorb = 0;
		for (int j = 0; j < hMPFb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hMPFb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}

		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb->GetBinContent(i) == 0) diffb->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb->SetBinError(i,errorb);
  }

//############### plus variation ####################
//Loop over the TProfile bins
  for (int i = 1; i < hMPFb->GetNbinsX()+1; i++){
		evtW_B0Dp.clear();
		evtW_B0D0.clear();
		evtW_B0Dps.clear();
		evtW_BpDp.clear();
		evtW_BpD0.clear();
		evtW_BpDps.clear();
		evtW_B0sDps.clear();
		evtW_LbLc.clear();
		evtW_B0nonC.clear();
		evtW_BpnonC.clear();
		evtW_B0snonC.clear();
		evtW_LbnonC.clear();

		evtW_B0DpD0.clear();
		evtW_B0DpDps.clear();
		evtW_B0D0Dps.clear();
		evtW_BpDpD0.clear();
		evtW_BpDpDps.clear();
		evtW_BpD0Dps.clear();

		evtWs_B0Dp.clear();
		evtWs_B0D0.clear();
		evtWs_B0Dps.clear();
		evtWs_BpDp.clear();
		evtWs_BpD0.clear();
		evtWs_BpDps.clear();
		evtWs_B0sDps.clear();
		evtWs_LbLc.clear();
		evtWs_B0nonC.clear();
		evtWs_BpnonC.clear();
		evtWs_B0snonC.clear();
		evtWs_LbnonC.clear();

		evtWs_B0DpD0.clear();
		evtWs_B0DpDps.clear();
		evtWs_B0D0Dps.clear();
		evtWs_BpDpD0.clear();
		evtWs_BpDpDps.clear();
		evtWs_BpD0Dps.clear();

		sem_B0Dp.clear();
		sem_B0D0.clear();
		sem_B0Dps.clear();
		sem_BpDp.clear();
		sem_BpD0.clear();
		sem_BpDps.clear();
		sem_B0sDps.clear();
		sem_LbLc.clear();
		sem_B0nonC.clear();
		sem_BpnonC.clear();
		sem_B0snonC.clear();
		sem_LbnonC.clear();

		sem_B0DpD0.clear();
		sem_B0DpDps.clear();
		sem_B0D0Dps.clear();
		sem_BpDpD0.clear();
		sem_BpDpDps.clear();
		sem_BpD0Dps.clear();

		vals_B0Dp.clear();
		vals_B0D0.clear();
		vals_B0Dps.clear();
		vals_BpDp.clear();
		vals_BpD0.clear();
		vals_BpDps.clear();
		vals_B0sDps.clear();
		vals_LbLc.clear();
		vals_B0nonC.clear();
		vals_BpnonC.clear();
		vals_B0snonC.clear();
		vals_LbnonC.clear();

		vals_B0DpD0.clear();
		vals_B0DpDps.clear();
		vals_B0D0Dps.clear();
		vals_BpDpD0.clear();
		vals_BpDpDps.clear();
		vals_BpD0Dps.clear();

		for (int j = 1; j < hMPFb->GetNbinsX()+1; j++) {
			string pr_ind = "_" + to_string(i) + "_" + to_string(j);

			prMPFb_B0Dp = pr2DMPFb_B0Dp->ProfileX(("B0Dp"+pr_ind).c_str(),j,j);
			prMPFb_B0D0 = pr2DMPFb_B0D0->ProfileX(("B0D0"+pr_ind).c_str(),j,j);
			prMPFb_B0Dps = pr2DMPFb_B0Dps->ProfileX(("B0Dps"+pr_ind).c_str(),j,j);
			prMPFb_BpDp = pr2DMPFb_BpDp->ProfileX(("BpDp"+pr_ind).c_str(),j,j);
			prMPFb_BpD0 = pr2DMPFb_BpD0->ProfileX(("BpD0"+pr_ind).c_str(),j,j);
			prMPFb_BpDps = pr2DMPFb_BpDps->ProfileX(("BpDps"+pr_ind).c_str(),j,j);
			prMPFb_B0sDps = pr2DMPFb_B0sDps->ProfileX(("B0sDps"+pr_ind).c_str(),j,j);
			prMPFb_LbLc = pr2DMPFb_LbLc->ProfileX(("LbLc"+pr_ind).c_str(),j,j);
			prMPFb_B0nonC = pr2DMPFb_B0nonC->ProfileX(("B0nonC"+pr_ind).c_str(),j,j);
			prMPFb_BpnonC = pr2DMPFb_BpnonC->ProfileX(("BpnonC"+pr_ind).c_str(),j,j);
			prMPFb_B0snonC = pr2DMPFb_B0snonC->ProfileX(("B0snonC"+pr_ind).c_str(),j,j);
			prMPFb_LbnonC = pr2DMPFb_LbnonC->ProfileX(("LbnonC"+pr_ind).c_str(),j,j);

			prMPFb_B0DpD0 = pr2DMPFb_B0DpD0->ProfileX(("B0DpD0"+pr_ind).c_str(),j,j);
			prMPFb_B0DpDps = pr2DMPFb_B0DpDps->ProfileX(("B0DpDps"+pr_ind).c_str(),j,j);
			prMPFb_B0D0Dps = pr2DMPFb_B0D0Dps->ProfileX(("B0D0Dps"+pr_ind).c_str(),j,j);
			prMPFb_BpDpD0 = pr2DMPFb_BpDpD0->ProfileX(("BpDpD0"+pr_ind).c_str(),j,j);
			prMPFb_BpDpDps = pr2DMPFb_BpDpDps->ProfileX(("BpDpDps"+pr_ind).c_str(),j,j);
			prMPFb_BpD0Dps = pr2DMPFb_BpD0Dps->ProfileX(("BpD0Dps"+pr_ind).c_str(),j,j);

			prMPFb_B0Dp_s_p = pr2DMPFb_B0Dp_s_p->ProfileX(("B0Dp_s"+pr_ind).c_str(),j,j);
			prMPFb_B0D0_s_p = pr2DMPFb_B0D0_s_p->ProfileX(("B0D0_s"+pr_ind).c_str(),j,j);
			prMPFb_B0Dps_s_p = pr2DMPFb_B0Dps_s_p->ProfileX(("B0Dps_s"+pr_ind).c_str(),j,j);
			prMPFb_BpDp_s_p = pr2DMPFb_BpDp_s_p->ProfileX(("BpDp_s"+pr_ind).c_str(),j,j);
			prMPFb_BpD0_s_p = pr2DMPFb_BpD0_s_p->ProfileX(("BpD0_s"+pr_ind).c_str(),j,j);
			prMPFb_BpDps_s_p = pr2DMPFb_BpDps_s_p->ProfileX(("BpDps_s"+pr_ind).c_str(),j,j);
			prMPFb_B0sDps_s_p = pr2DMPFb_B0sDps_s_p->ProfileX(("B0sDps_s"+pr_ind).c_str(),j,j);
			prMPFb_LbLc_s_p = pr2DMPFb_LbLc_s_p->ProfileX(("LbLc_s"+pr_ind).c_str(),j,j);
			prMPFb_B0nonC_s_p = pr2DMPFb_B0nonC_s_p->ProfileX(("B0nonC_s"+pr_ind).c_str(),j,j);
			prMPFb_BpnonC_s_p = pr2DMPFb_BpnonC_s_p->ProfileX(("BpnonC_s"+pr_ind).c_str(),j,j);
			prMPFb_B0snonC_s_p = pr2DMPFb_B0snonC_s_p->ProfileX(("B0snonC_s"+pr_ind).c_str(),j,j);
			prMPFb_LbnonC_s_p = pr2DMPFb_LbnonC_s_p->ProfileX(("LbnonC_s"+pr_ind).c_str(),j,j);

			prMPFb_B0DpD0_s_p = pr2DMPFb_B0DpD0_s_p->ProfileX(("B0DpD0_s"+pr_ind).c_str(),j,j);
			prMPFb_B0DpDps_s_p = pr2DMPFb_B0DpDps_s_p->ProfileX(("B0DpDps_s"+pr_ind).c_str(),j,j);
			prMPFb_B0D0Dps_s_p = pr2DMPFb_B0D0Dps_s_p->ProfileX(("B0D0Dps_s"+pr_ind).c_str(),j,j);
			prMPFb_BpDpD0_s_p = pr2DMPFb_BpDpD0_s_p->ProfileX(("BpDpD0_s"+pr_ind).c_str(),j,j);
			prMPFb_BpDpDps_s_p = pr2DMPFb_BpDpDps_s_p->ProfileX(("BpDpDps_s"+pr_ind).c_str(),j,j);
			prMPFb_BpD0Dps_s_p = pr2DMPFb_BpD0Dps_s_p->ProfileX(("BpD0Dps_s"+pr_ind).c_str(),j,j);

			evtW_B0Dp.push_back(prMPFb_B0Dp->GetBinEntries(i));
			evtW_B0D0.push_back(prMPFb_B0D0->GetBinEntries(i));
			evtW_B0Dps.push_back(prMPFb_B0Dps->GetBinEntries(i));
			evtW_BpDp.push_back(prMPFb_BpDp->GetBinEntries(i));
			evtW_BpD0.push_back(prMPFb_BpD0->GetBinEntries(i));
			evtW_BpDps.push_back(prMPFb_BpDps->GetBinEntries(i));
			evtW_B0sDps.push_back(prMPFb_B0sDps->GetBinEntries(i));
			evtW_LbLc.push_back(prMPFb_LbLc->GetBinEntries(i));
			evtW_B0nonC.push_back(prMPFb_B0nonC->GetBinEntries(i));
			evtW_BpnonC.push_back(prMPFb_BpnonC->GetBinEntries(i));
			evtW_B0snonC.push_back(prMPFb_B0snonC->GetBinEntries(i));
			evtW_LbnonC.push_back(prMPFb_LbnonC->GetBinEntries(i));

			evtW_B0DpD0.push_back(prMPFb_B0DpD0->GetBinEntries(i));
			evtW_B0DpDps.push_back(prMPFb_B0DpDps->GetBinEntries(i));
			evtW_B0D0Dps.push_back(prMPFb_B0D0Dps->GetBinEntries(i));
			evtW_BpDpD0.push_back(prMPFb_BpDpD0->GetBinEntries(i));
			evtW_BpDpDps.push_back(prMPFb_BpDpDps->GetBinEntries(i));
			evtW_BpD0Dps.push_back(prMPFb_BpD0Dps->GetBinEntries(i));

			evtWs_B0Dp.push_back(prMPFb_B0Dp_s_p->GetBinEntries(i));
			evtWs_B0D0.push_back(prMPFb_B0D0_s_p->GetBinEntries(i));
			evtWs_B0Dps.push_back(prMPFb_B0Dps_s_p->GetBinEntries(i));
			evtWs_BpDp.push_back(prMPFb_BpDp_s_p->GetBinEntries(i));
			evtWs_BpD0.push_back(prMPFb_BpD0_s_p->GetBinEntries(i));
			evtWs_BpDps.push_back(prMPFb_BpDps_s_p->GetBinEntries(i));
			evtWs_B0sDps.push_back(prMPFb_B0sDps_s_p->GetBinEntries(i));
			evtWs_LbLc.push_back(prMPFb_LbLc_s_p->GetBinEntries(i));
			evtWs_B0nonC.push_back(prMPFb_B0nonC_s_p->GetBinEntries(i));
			evtWs_BpnonC.push_back(prMPFb_BpnonC_s_p->GetBinEntries(i));
			evtWs_B0snonC.push_back(prMPFb_B0snonC_s_p->GetBinEntries(i));
			evtWs_LbnonC.push_back(prMPFb_LbnonC_s_p->GetBinEntries(i));

			evtWs_B0DpD0.push_back(prMPFb_B0DpD0_s_p->GetBinEntries(i));
			evtWs_B0DpDps.push_back(prMPFb_B0DpDps_s_p->GetBinEntries(i));
			evtWs_B0D0Dps.push_back(prMPFb_B0D0Dps_s_p->GetBinEntries(i));
			evtWs_BpDpD0.push_back(prMPFb_BpDpD0_s_p->GetBinEntries(i));
			evtWs_BpDpDps.push_back(prMPFb_BpDpDps_s_p->GetBinEntries(i));
			evtWs_BpD0Dps.push_back(prMPFb_BpD0Dps_s_p->GetBinEntries(i));

			sem_B0Dp.push_back(prMPFb_B0Dp->GetBinError(i));
			sem_B0D0.push_back(prMPFb_B0D0->GetBinError(i));
			sem_B0Dps.push_back(prMPFb_B0Dps->GetBinError(i));
			sem_BpDp.push_back(prMPFb_BpDp->GetBinError(i));
			sem_BpD0.push_back(prMPFb_BpD0->GetBinError(i));
			sem_BpDps.push_back(prMPFb_BpDps->GetBinError(i));
			sem_B0sDps.push_back(prMPFb_B0sDps->GetBinError(i));
			sem_LbLc.push_back(prMPFb_LbLc->GetBinError(i));
			sem_B0nonC.push_back(prMPFb_B0nonC->GetBinError(i));
			sem_BpnonC.push_back(prMPFb_BpnonC->GetBinError(i));
			sem_B0snonC.push_back(prMPFb_B0snonC->GetBinError(i));
			sem_LbnonC.push_back(prMPFb_LbnonC->GetBinError(i));

			sem_B0DpD0.push_back(prMPFb_B0DpD0->GetBinError(i));
			sem_B0DpDps.push_back(prMPFb_B0DpDps->GetBinError(i));
			sem_B0D0Dps.push_back(prMPFb_B0D0Dps->GetBinError(i));
			sem_BpDpD0.push_back(prMPFb_BpDpD0->GetBinError(i));
			sem_BpDpDps.push_back(prMPFb_BpDpDps->GetBinError(i));
			sem_BpD0Dps.push_back(prMPFb_BpD0Dps->GetBinError(i));

			vals_B0Dp.push_back(prMPFb_B0Dp->GetBinContent(i));
			vals_B0D0.push_back(prMPFb_B0D0->GetBinContent(i));
			vals_B0Dps.push_back(prMPFb_B0Dps->GetBinContent(i));
			vals_BpDp.push_back(prMPFb_BpDp->GetBinContent(i));
			vals_BpD0.push_back(prMPFb_BpD0->GetBinContent(i));
			vals_BpDps.push_back(prMPFb_BpDps->GetBinContent(i));
			vals_B0sDps.push_back(prMPFb_B0sDps->GetBinContent(i));
			vals_LbLc.push_back(prMPFb_LbLc->GetBinContent(i));
			vals_B0nonC.push_back(prMPFb_B0nonC->GetBinContent(i));
			vals_BpnonC.push_back(prMPFb_BpnonC->GetBinContent(i));
			vals_B0snonC.push_back(prMPFb_B0snonC->GetBinContent(i));
			vals_LbnonC.push_back(prMPFb_LbnonC->GetBinContent(i));

			vals_B0DpD0.push_back(prMPFb_B0DpD0->GetBinContent(i));
			vals_B0DpDps.push_back(prMPFb_B0DpDps->GetBinContent(i));
			vals_B0D0Dps.push_back(prMPFb_B0D0Dps->GetBinContent(i));
			vals_BpDpD0.push_back(prMPFb_BpDpD0->GetBinContent(i));
			vals_BpDpDps.push_back(prMPFb_BpDpDps->GetBinContent(i));
			vals_BpD0Dps.push_back(prMPFb_BpD0Dps->GetBinContent(i));

			delete prMPFb_B0Dp;
			delete prMPFb_B0D0;
			delete prMPFb_B0Dps;
			delete prMPFb_BpDp;
			delete prMPFb_BpD0;
			delete prMPFb_BpDps;
			delete prMPFb_B0sDps;
			delete prMPFb_LbLc;
			delete prMPFb_B0nonC;
			delete prMPFb_BpnonC;
			delete prMPFb_B0snonC; 
			delete prMPFb_LbnonC;

			delete prMPFb_B0DpD0;
			delete prMPFb_B0DpDps;
			delete prMPFb_B0D0Dps;
			delete prMPFb_BpDpD0; 
			delete prMPFb_BpDpDps;
			delete prMPFb_BpD0Dps;

			delete prMPFb_B0Dp_s_p;
			delete prMPFb_B0D0_s_p;
			delete prMPFb_B0Dps_s_p;
			delete prMPFb_BpDp_s_p;
			delete prMPFb_BpD0_s_p;
			delete prMPFb_BpDps_s_p;
			delete prMPFb_B0sDps_s_p;
			delete prMPFb_LbLc_s_p;
			delete prMPFb_B0nonC_s_p;
			delete prMPFb_BpnonC_s_p;
			delete prMPFb_B0snonC_s_p;
			delete prMPFb_LbnonC_s_p;

			delete prMPFb_B0DpD0_s_p;
			delete prMPFb_B0DpDps_s_p;
			delete prMPFb_B0D0Dps_s_p;
			delete prMPFb_BpDpD0_s_p;
			delete prMPFb_BpDpDps_s_p;
			delete prMPFb_BpD0Dps_s_p;
		}

		vector<vector<double>> vec_evtW{evtW_B0Dp, evtW_B0D0, evtW_B0Dps, evtW_BpDp,
								 evtW_BpD0, evtW_BpDps, evtW_B0sDps, evtW_LbLc,
								 evtW_B0nonC, evtW_BpnonC, evtW_B0snonC, evtW_LbnonC,
								 evtW_B0DpD0,	evtW_B0DpDps,	evtW_B0D0Dps,	evtW_BpDpD0,
								 evtW_BpDpDps, evtW_BpD0Dps};

		vector<vector<double>> vec_evtWs{evtWs_B0Dp, evtWs_B0D0, evtWs_B0Dps, evtWs_BpDp,
								 evtWs_BpD0, evtWs_BpDps, evtWs_B0sDps, evtWs_LbLc,
								 evtWs_B0nonC, evtWs_BpnonC, evtWs_B0snonC, evtWs_LbnonC,
								 evtWs_B0DpD0,	evtWs_B0DpDps,	evtWs_B0D0Dps,	evtWs_BpDpD0,
								 evtWs_BpDpDps, evtWs_BpD0Dps};

		vector<vector<double>> vec_sem{sem_B0Dp, sem_B0D0, sem_B0Dps, sem_BpDp,
								 sem_BpD0, sem_BpDps, sem_B0sDps, sem_LbLc,
								 sem_B0nonC, sem_BpnonC, sem_B0snonC, sem_LbnonC,
								 sem_B0DpD0,	sem_B0DpDps,	sem_B0D0Dps,	sem_BpDpD0,
								 sem_BpDpDps, sem_BpD0Dps};

		vector<vector<double>> vec_vals{
vals_B0Dp, vals_B0D0, vals_B0Dps, vals_BpDp,
								 vals_BpD0, vals_BpDps, vals_B0sDps, vals_LbLc,
								 vals_B0nonC, vals_BpnonC, vals_B0snonC, vals_LbnonC,
								 vals_B0DpD0,	vals_B0DpDps,	vals_B0D0Dps,	vals_BpDpD0,
								 vals_BpDpDps, vals_BpD0Dps};

		//set small number so that we dont divide by zero 
		evtWtot_B0Dp = 1e-10;
		evtWtot_B0D0 = 1e-10;
		evtWtot_B0Dps = 1e-10;
		evtWtot_BpDp = 1e-10;
		evtWtot_BpD0 = 1e-10;
		evtWtot_BpDps = 1e-10;
		evtWtot_B0sDps = 1e-10;
		evtWtot_LbLc = 1e-10;
		evtWtot_B0nonC = 1e-10;
		evtWtot_BpnonC = 1e-10;
		evtWtot_B0snonC = 1e-10;
		evtWtot_LbnonC = 1e-10;

		evtWtot_B0DpD0 = 1e-10;
		evtWtot_B0DpDps = 1e-10;
		evtWtot_B0D0Dps = 1e-10;
		evtWtot_BpDpD0 = 1e-10;
		evtWtot_BpDpDps = 1e-10;
		evtWtot_BpD0Dps = 1e-10;

		evtWtots_B0Dp = 1e-10;
		evtWtots_B0D0 = 1e-10;
		evtWtots_B0Dps = 1e-10;
		evtWtots_BpDp = 1e-10;
		evtWtots_BpD0 = 1e-10;
		evtWtots_BpDps = 1e-10;
		evtWtots_B0sDps = 1e-10;
		evtWtots_LbLc = 1e-10;
		evtWtots_B0nonC = 1e-10;
		evtWtots_BpnonC = 1e-10;
		evtWtots_B0snonC = 1e-10;
		evtWtots_LbnonC = 1e-10;

		evtWtots_B0DpD0 = 1e-10;
		evtWtots_B0DpDps = 1e-10;
		evtWtots_B0D0Dps = 1e-10;
		evtWtots_BpDpD0 = 1e-10;
		evtWtots_BpDpDps = 1e-10;
		evtWtots_BpD0Dps = 1e-10;

		for (double x : evtW_B0Dp) evtWtot_B0Dp += x;	
		for (double x : evtW_B0D0) evtWtot_B0D0 += x;
		for (double x : evtW_B0Dps) evtWtot_B0Dps += x;
		for (double x : evtW_BpDp) evtWtot_BpDp += x;
		for (double x : evtW_BpD0) evtWtot_BpD0 += x;
		for (double x : evtW_BpDps) evtWtot_BpDps += x;
		for (double x : evtW_B0sDps) evtWtot_B0sDps += x;
		for (double x : evtW_LbLc) evtWtot_LbLc += x;
		for (double x : evtW_B0nonC) evtWtot_B0nonC += x;
		for (double x : evtW_BpnonC) evtWtot_BpnonC += x;
		for (double x : evtW_B0snonC) evtWtot_B0snonC += x;
		for (double x : evtW_LbnonC) evtWtot_LbnonC += x;

		for (double x : evtW_B0DpD0) evtWtot_B0DpD0 += x;
		for (double x : evtW_B0DpDps) evtWtot_B0DpDps += x;
		for (double x : evtW_B0D0Dps) evtWtot_B0D0Dps += x;
		for (double x : evtW_BpDpD0) evtWtot_BpDpD0 += x;
		for (double x : evtW_BpDpDps) evtWtot_BpDpDps += x;
		for (double x : evtW_BpD0Dps) evtWtot_BpD0Dps += x;

		for (double x : evtWs_B0Dp) evtWtots_B0Dp += x;
		for (double x : evtWs_B0D0) evtWtots_B0D0 += x;
		for (double x : evtWs_B0Dps) evtWtots_B0Dps += x;
		for (double x : evtWs_BpDp) evtWtots_BpDp += x;
		for (double x : evtWs_BpD0) evtWtots_BpD0 += x;
		for (double x : evtWs_BpDps) evtWtots_BpDps += x;
		for (double x : evtWs_B0sDps) evtWtots_B0sDps += x;
		for (double x : evtWs_LbLc) evtWtots_LbLc += x;
		for (double x : evtWs_B0nonC) evtWtots_B0nonC += x;
		for (double x : evtWs_BpnonC) evtWtots_BpnonC += x;
		for (double x : evtWs_B0snonC) evtWtots_B0snonC += x;
		for (double x : evtWs_LbnonC) evtWtots_LbnonC += x;

		for (double x : evtWs_B0DpD0) evtWtots_B0DpD0 += x;
		for (double x : evtWs_B0DpDps) evtWtots_B0DpDps += x;
		for (double x : evtWs_B0D0Dps) evtWtots_B0D0Dps += x;
		for (double x : evtWs_BpDpD0) evtWtots_BpDpD0 += x;
		for (double x : evtWs_BpDpDps) evtWtots_BpDpDps += x;
		for (double x : evtWs_BpD0Dps) evtWtots_BpD0Dps += x;
		
		evtWtot = evtWtot_B0Dp + evtWtot_B0D0 + evtWtot_B0Dps + evtWtot_BpDp + evtWtot_BpD0 +
						 	evtWtot_BpDps + evtWtot_B0sDps + evtWtot_LbLc + evtWtot_B0nonC +evtWtot_BpnonC +
							evtWtot_B0snonC + evtWtot_LbnonC + evtWtot_B0DpD0 + evtWtot_B0DpDps +
							evtWtot_B0D0Dps + evtWtot_BpDpD0 + evtWtot_BpDpDps + evtWtot_BpD0Dps;

		evtWtots = evtWtots_B0Dp + evtWtots_B0D0 + evtWtots_B0Dps + evtWtots_BpDp + evtWtots_BpD0 +
						 	 evtWtots_BpDps + evtWtots_B0sDps + evtWtots_LbLc + evtWtots_B0nonC +evtWtots_BpnonC +
							 evtWtots_B0snonC + evtWtots_LbnonC + evtWtots_B0DpD0 + evtWtots_B0DpDps +
							 evtWtots_B0D0Dps + evtWtots_BpDpD0 + evtWtots_BpDpDps + evtWtots_BpD0Dps;

		//loop over diff scale factors 
		errorb = 0;
		for (int j = 0; j < hMPFb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hMPFb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}

		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb_p->GetBinContent(i) == 0) diffb_p->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb_p->SetBinError(i,errorb);
  }

//############### minus variation ####################
//Loop over the TProfile bins
  for (int i = 1; i < hMPFb->GetNbinsX()+1; i++){
		evtW_B0Dp.clear();
		evtW_B0D0.clear();
		evtW_B0Dps.clear();
		evtW_BpDp.clear();
		evtW_BpD0.clear();
		evtW_BpDps.clear();
		evtW_B0sDps.clear();
		evtW_LbLc.clear();
		evtW_B0nonC.clear();
		evtW_BpnonC.clear();
		evtW_B0snonC.clear();
		evtW_LbnonC.clear();

		evtW_B0DpD0.clear();
		evtW_B0DpDps.clear();
		evtW_B0D0Dps.clear();
		evtW_BpDpD0.clear();
		evtW_BpDpDps.clear();
		evtW_BpD0Dps.clear();

		evtWs_B0Dp.clear();
		evtWs_B0D0.clear();
		evtWs_B0Dps.clear();
		evtWs_BpDp.clear();
		evtWs_BpD0.clear();
		evtWs_BpDps.clear();
		evtWs_B0sDps.clear();
		evtWs_LbLc.clear();
		evtWs_B0nonC.clear();
		evtWs_BpnonC.clear();
		evtWs_B0snonC.clear();
		evtWs_LbnonC.clear();

		evtWs_B0DpD0.clear();
		evtWs_B0DpDps.clear();
		evtWs_B0D0Dps.clear();
		evtWs_BpDpD0.clear();
		evtWs_BpDpDps.clear();
		evtWs_BpD0Dps.clear();

		sem_B0Dp.clear();
		sem_B0D0.clear();
		sem_B0Dps.clear();
		sem_BpDp.clear();
		sem_BpD0.clear();
		sem_BpDps.clear();
		sem_B0sDps.clear();
		sem_LbLc.clear();
		sem_B0nonC.clear();
		sem_BpnonC.clear();
		sem_B0snonC.clear();
		sem_LbnonC.clear();

		sem_B0DpD0.clear();
		sem_B0DpDps.clear();
		sem_B0D0Dps.clear();
		sem_BpDpD0.clear();
		sem_BpDpDps.clear();
		sem_BpD0Dps.clear();

		vals_B0Dp.clear();
		vals_B0D0.clear();
		vals_B0Dps.clear();
		vals_BpDp.clear();
		vals_BpD0.clear();
		vals_BpDps.clear();
		vals_B0sDps.clear();
		vals_LbLc.clear();
		vals_B0nonC.clear();
		vals_BpnonC.clear();
		vals_B0snonC.clear();
		vals_LbnonC.clear();

		vals_B0DpD0.clear();
		vals_B0DpDps.clear();
		vals_B0D0Dps.clear();
		vals_BpDpD0.clear();
		vals_BpDpDps.clear();
		vals_BpD0Dps.clear();

		for (int j = 1; j < hMPFb->GetNbinsX()+1; j++) {
			string pr_ind = "_" + to_string(i) + "_" + to_string(j);

			prMPFb_B0Dp = pr2DMPFb_B0Dp->ProfileX(("B0Dp"+pr_ind).c_str(),j,j);
			prMPFb_B0D0 = pr2DMPFb_B0D0->ProfileX(("B0D0"+pr_ind).c_str(),j,j);
			prMPFb_B0Dps = pr2DMPFb_B0Dps->ProfileX(("B0Dps"+pr_ind).c_str(),j,j);
			prMPFb_BpDp = pr2DMPFb_BpDp->ProfileX(("BpDp"+pr_ind).c_str(),j,j);
			prMPFb_BpD0 = pr2DMPFb_BpD0->ProfileX(("BpD0"+pr_ind).c_str(),j,j);
			prMPFb_BpDps = pr2DMPFb_BpDps->ProfileX(("BpDps"+pr_ind).c_str(),j,j);
			prMPFb_B0sDps = pr2DMPFb_B0sDps->ProfileX(("B0sDps"+pr_ind).c_str(),j,j);
			prMPFb_LbLc = pr2DMPFb_LbLc->ProfileX(("LbLc"+pr_ind).c_str(),j,j);
			prMPFb_B0nonC = pr2DMPFb_B0nonC->ProfileX(("B0nonC"+pr_ind).c_str(),j,j);
			prMPFb_BpnonC = pr2DMPFb_BpnonC->ProfileX(("BpnonC"+pr_ind).c_str(),j,j);
			prMPFb_B0snonC = pr2DMPFb_B0snonC->ProfileX(("B0snonC"+pr_ind).c_str(),j,j);
			prMPFb_LbnonC = pr2DMPFb_LbnonC->ProfileX(("LbnonC"+pr_ind).c_str(),j,j);

			prMPFb_B0DpD0 = pr2DMPFb_B0DpD0->ProfileX(("B0DpD0"+pr_ind).c_str(),j,j);
			prMPFb_B0DpDps = pr2DMPFb_B0DpDps->ProfileX(("B0DpDps"+pr_ind).c_str(),j,j);
			prMPFb_B0D0Dps = pr2DMPFb_B0D0Dps->ProfileX(("B0D0Dps"+pr_ind).c_str(),j,j);
			prMPFb_BpDpD0 = pr2DMPFb_BpDpD0->ProfileX(("BpDpD0"+pr_ind).c_str(),j,j);
			prMPFb_BpDpDps = pr2DMPFb_BpDpDps->ProfileX(("BpDpDps"+pr_ind).c_str(),j,j);
			prMPFb_BpD0Dps = pr2DMPFb_BpD0Dps->ProfileX(("BpD0Dps"+pr_ind).c_str(),j,j);

			prMPFb_B0Dp_s_m = pr2DMPFb_B0Dp_s_m->ProfileX(("B0Dp_s"+pr_ind).c_str(),j,j);
			prMPFb_B0D0_s_m = pr2DMPFb_B0D0_s_m->ProfileX(("B0D0_s"+pr_ind).c_str(),j,j);
			prMPFb_B0Dps_s_m = pr2DMPFb_B0Dps_s_m->ProfileX(("B0Dps_s"+pr_ind).c_str(),j,j);
			prMPFb_BpDp_s_m = pr2DMPFb_BpDp_s_m->ProfileX(("BpDp_s"+pr_ind).c_str(),j,j);
			prMPFb_BpD0_s_m = pr2DMPFb_BpD0_s_m->ProfileX(("BpD0_s"+pr_ind).c_str(),j,j);
			prMPFb_BpDps_s_m = pr2DMPFb_BpDps_s_m->ProfileX(("BpDps_s"+pr_ind).c_str(),j,j);
			prMPFb_B0sDps_s_m = pr2DMPFb_B0sDps_s_m->ProfileX(("B0sDps_s"+pr_ind).c_str(),j,j);
			prMPFb_LbLc_s_m = pr2DMPFb_LbLc_s_m->ProfileX(("LbLc_s"+pr_ind).c_str(),j,j);
			prMPFb_B0nonC_s_m = pr2DMPFb_B0nonC_s_m->ProfileX(("B0nonC_s"+pr_ind).c_str(),j,j);
			prMPFb_BpnonC_s_m = pr2DMPFb_BpnonC_s_m->ProfileX(("BpnonC_s"+pr_ind).c_str(),j,j);
			prMPFb_B0snonC_s_m = pr2DMPFb_B0snonC_s_m->ProfileX(("B0snonC_s"+pr_ind).c_str(),j,j);
			prMPFb_LbnonC_s_m = pr2DMPFb_LbnonC_s_m->ProfileX(("LbnonC_s"+pr_ind).c_str(),j,j);

			prMPFb_B0DpD0_s_m = pr2DMPFb_B0DpD0_s_m->ProfileX(("B0DpD0_s"+pr_ind).c_str(),j,j);
			prMPFb_B0DpDps_s_m = pr2DMPFb_B0DpDps_s_m->ProfileX(("B0DpDps_s"+pr_ind).c_str(),j,j);
			prMPFb_B0D0Dps_s_m = pr2DMPFb_B0D0Dps_s_m->ProfileX(("B0D0Dps_s"+pr_ind).c_str(),j,j);
			prMPFb_BpDpD0_s_m = pr2DMPFb_BpDpD0_s_m->ProfileX(("BpDpD0_s"+pr_ind).c_str(),j,j);
			prMPFb_BpDpDps_s_m = pr2DMPFb_BpDpDps_s_m->ProfileX(("BpDpDps_s"+pr_ind).c_str(),j,j);
			prMPFb_BpD0Dps_s_m = pr2DMPFb_BpD0Dps_s_m->ProfileX(("BpD0Dps_s"+pr_ind).c_str(),j,j);

			evtW_B0Dp.push_back(prMPFb_B0Dp->GetBinEntries(i));
			evtW_B0D0.push_back(prMPFb_B0D0->GetBinEntries(i));
			evtW_B0Dps.push_back(prMPFb_B0Dps->GetBinEntries(i));
			evtW_BpDp.push_back(prMPFb_BpDp->GetBinEntries(i));
			evtW_BpD0.push_back(prMPFb_BpD0->GetBinEntries(i));
			evtW_BpDps.push_back(prMPFb_BpDps->GetBinEntries(i));
			evtW_B0sDps.push_back(prMPFb_B0sDps->GetBinEntries(i));
			evtW_LbLc.push_back(prMPFb_LbLc->GetBinEntries(i));
			evtW_B0nonC.push_back(prMPFb_B0nonC->GetBinEntries(i));
			evtW_BpnonC.push_back(prMPFb_BpnonC->GetBinEntries(i));
			evtW_B0snonC.push_back(prMPFb_B0snonC->GetBinEntries(i));
			evtW_LbnonC.push_back(prMPFb_LbnonC->GetBinEntries(i));

			evtW_B0DpD0.push_back(prMPFb_B0DpD0->GetBinEntries(i));
			evtW_B0DpDps.push_back(prMPFb_B0DpDps->GetBinEntries(i));
			evtW_B0D0Dps.push_back(prMPFb_B0D0Dps->GetBinEntries(i));
			evtW_BpDpD0.push_back(prMPFb_BpDpD0->GetBinEntries(i));
			evtW_BpDpDps.push_back(prMPFb_BpDpDps->GetBinEntries(i));
			evtW_BpD0Dps.push_back(prMPFb_BpD0Dps->GetBinEntries(i));

			evtWs_B0Dp.push_back(prMPFb_B0Dp_s_m->GetBinEntries(i));
			evtWs_B0D0.push_back(prMPFb_B0D0_s_m->GetBinEntries(i));
			evtWs_B0Dps.push_back(prMPFb_B0Dps_s_m->GetBinEntries(i));
			evtWs_BpDp.push_back(prMPFb_BpDp_s_m->GetBinEntries(i));
			evtWs_BpD0.push_back(prMPFb_BpD0_s_m->GetBinEntries(i));
			evtWs_BpDps.push_back(prMPFb_BpDps_s_m->GetBinEntries(i));
			evtWs_B0sDps.push_back(prMPFb_B0sDps_s_m->GetBinEntries(i));
			evtWs_LbLc.push_back(prMPFb_LbLc_s_m->GetBinEntries(i));
			evtWs_B0nonC.push_back(prMPFb_B0nonC_s_m->GetBinEntries(i));
			evtWs_BpnonC.push_back(prMPFb_BpnonC_s_m->GetBinEntries(i));
			evtWs_B0snonC.push_back(prMPFb_B0snonC_s_m->GetBinEntries(i));
			evtWs_LbnonC.push_back(prMPFb_LbnonC_s_m->GetBinEntries(i));

			evtWs_B0DpD0.push_back(prMPFb_B0DpD0_s_m->GetBinEntries(i));
			evtWs_B0DpDps.push_back(prMPFb_B0DpDps_s_m->GetBinEntries(i));
			evtWs_B0D0Dps.push_back(prMPFb_B0D0Dps_s_m->GetBinEntries(i));
			evtWs_BpDpD0.push_back(prMPFb_BpDpD0_s_m->GetBinEntries(i));
			evtWs_BpDpDps.push_back(prMPFb_BpDpDps_s_m->GetBinEntries(i));
			evtWs_BpD0Dps.push_back(prMPFb_BpD0Dps_s_m->GetBinEntries(i));

			sem_B0Dp.push_back(prMPFb_B0Dp->GetBinError(i));
			sem_B0D0.push_back(prMPFb_B0D0->GetBinError(i));
			sem_B0Dps.push_back(prMPFb_B0Dps->GetBinError(i));
			sem_BpDp.push_back(prMPFb_BpDp->GetBinError(i));
			sem_BpD0.push_back(prMPFb_BpD0->GetBinError(i));
			sem_BpDps.push_back(prMPFb_BpDps->GetBinError(i));
			sem_B0sDps.push_back(prMPFb_B0sDps->GetBinError(i));
			sem_LbLc.push_back(prMPFb_LbLc->GetBinError(i));
			sem_B0nonC.push_back(prMPFb_B0nonC->GetBinError(i));
			sem_BpnonC.push_back(prMPFb_BpnonC->GetBinError(i));
			sem_B0snonC.push_back(prMPFb_B0snonC->GetBinError(i));
			sem_LbnonC.push_back(prMPFb_LbnonC->GetBinError(i));

			sem_B0DpD0.push_back(prMPFb_B0DpD0->GetBinError(i));
			sem_B0DpDps.push_back(prMPFb_B0DpDps->GetBinError(i));
			sem_B0D0Dps.push_back(prMPFb_B0D0Dps->GetBinError(i));
			sem_BpDpD0.push_back(prMPFb_BpDpD0->GetBinError(i));
			sem_BpDpDps.push_back(prMPFb_BpDpDps->GetBinError(i));
			sem_BpD0Dps.push_back(prMPFb_BpD0Dps->GetBinError(i));

			vals_B0Dp.push_back(prMPFb_B0Dp->GetBinContent(i));
			vals_B0D0.push_back(prMPFb_B0D0->GetBinContent(i));
			vals_B0Dps.push_back(prMPFb_B0Dps->GetBinContent(i));
			vals_BpDp.push_back(prMPFb_BpDp->GetBinContent(i));
			vals_BpD0.push_back(prMPFb_BpD0->GetBinContent(i));
			vals_BpDps.push_back(prMPFb_BpDps->GetBinContent(i));
			vals_B0sDps.push_back(prMPFb_B0sDps->GetBinContent(i));
			vals_LbLc.push_back(prMPFb_LbLc->GetBinContent(i));
			vals_B0nonC.push_back(prMPFb_B0nonC->GetBinContent(i));
			vals_BpnonC.push_back(prMPFb_BpnonC->GetBinContent(i));
			vals_B0snonC.push_back(prMPFb_B0snonC->GetBinContent(i));
			vals_LbnonC.push_back(prMPFb_LbnonC->GetBinContent(i));

			vals_B0DpD0.push_back(prMPFb_B0DpD0->GetBinContent(i));
			vals_B0DpDps.push_back(prMPFb_B0DpDps->GetBinContent(i));
			vals_B0D0Dps.push_back(prMPFb_B0D0Dps->GetBinContent(i));
			vals_BpDpD0.push_back(prMPFb_BpDpD0->GetBinContent(i));
			vals_BpDpDps.push_back(prMPFb_BpDpDps->GetBinContent(i));
			vals_BpD0Dps.push_back(prMPFb_BpD0Dps->GetBinContent(i));

			delete prMPFb_B0Dp;
			delete prMPFb_B0D0;
			delete prMPFb_B0Dps;
			delete prMPFb_BpDp;
			delete prMPFb_BpD0;
			delete prMPFb_BpDps;
			delete prMPFb_B0sDps;
			delete prMPFb_LbLc;
			delete prMPFb_B0nonC;
			delete prMPFb_BpnonC;
			delete prMPFb_B0snonC; 
			delete prMPFb_LbnonC;

			delete prMPFb_B0DpD0;
			delete prMPFb_B0DpDps;
			delete prMPFb_B0D0Dps;
			delete prMPFb_BpDpD0; 
			delete prMPFb_BpDpDps;
			delete prMPFb_BpD0Dps;

			delete prMPFb_B0Dp_s_m;
			delete prMPFb_B0D0_s_m;
			delete prMPFb_B0Dps_s_m;
			delete prMPFb_BpDp_s_m;
			delete prMPFb_BpD0_s_m;
			delete prMPFb_BpDps_s_m;
			delete prMPFb_B0sDps_s_m;
			delete prMPFb_LbLc_s_m;
			delete prMPFb_B0nonC_s_m;
			delete prMPFb_BpnonC_s_m;
			delete prMPFb_B0snonC_s_m;
			delete prMPFb_LbnonC_s_m;

			delete prMPFb_B0DpD0_s_m;
			delete prMPFb_B0DpDps_s_m;
			delete prMPFb_B0D0Dps_s_m;
			delete prMPFb_BpDpD0_s_m;
			delete prMPFb_BpDpDps_s_m;
			delete prMPFb_BpD0Dps_s_m;
		}

		vector<vector<double>> vec_evtW{evtW_B0Dp, evtW_B0D0, evtW_B0Dps, evtW_BpDp,
								 evtW_BpD0, evtW_BpDps, evtW_B0sDps, evtW_LbLc,
								 evtW_B0nonC, evtW_BpnonC, evtW_B0snonC, evtW_LbnonC,
								 evtW_B0DpD0,	evtW_B0DpDps,	evtW_B0D0Dps,	evtW_BpDpD0,
								 evtW_BpDpDps, evtW_BpD0Dps};

		vector<vector<double>> vec_evtWs{evtWs_B0Dp, evtWs_B0D0, evtWs_B0Dps, evtWs_BpDp,
								 evtWs_BpD0, evtWs_BpDps, evtWs_B0sDps, evtWs_LbLc,
								 evtWs_B0nonC, evtWs_BpnonC, evtWs_B0snonC, evtWs_LbnonC,
								 evtWs_B0DpD0,	evtWs_B0DpDps,	evtWs_B0D0Dps,	evtWs_BpDpD0,
								 evtWs_BpDpDps, evtWs_BpD0Dps};

		vector<vector<double>> vec_sem{sem_B0Dp, sem_B0D0, sem_B0Dps, sem_BpDp,
								 sem_BpD0, sem_BpDps, sem_B0sDps, sem_LbLc,
								 sem_B0nonC, sem_BpnonC, sem_B0snonC, sem_LbnonC,
								 sem_B0DpD0,	sem_B0DpDps,	sem_B0D0Dps,	sem_BpDpD0,
								 sem_BpDpDps, sem_BpD0Dps};

		vector<vector<double>> vec_vals{
vals_B0Dp, vals_B0D0, vals_B0Dps, vals_BpDp,
								 vals_BpD0, vals_BpDps, vals_B0sDps, vals_LbLc,
								 vals_B0nonC, vals_BpnonC, vals_B0snonC, vals_LbnonC,
								 vals_B0DpD0,	vals_B0DpDps,	vals_B0D0Dps,	vals_BpDpD0,
								 vals_BpDpDps, vals_BpD0Dps};

		//set small number so that we dont divide by zero 
		evtWtot_B0Dp = 1e-10;
		evtWtot_B0D0 = 1e-10;
		evtWtot_B0Dps = 1e-10;
		evtWtot_BpDp = 1e-10;
		evtWtot_BpD0 = 1e-10;
		evtWtot_BpDps = 1e-10;
		evtWtot_B0sDps = 1e-10;
		evtWtot_LbLc = 1e-10;
		evtWtot_B0nonC = 1e-10;
		evtWtot_BpnonC = 1e-10;
		evtWtot_B0snonC = 1e-10;
		evtWtot_LbnonC = 1e-10;

		evtWtot_B0DpD0 = 1e-10;
		evtWtot_B0DpDps = 1e-10;
		evtWtot_B0D0Dps = 1e-10;
		evtWtot_BpDpD0 = 1e-10;
		evtWtot_BpDpDps = 1e-10;
		evtWtot_BpD0Dps = 1e-10;

		evtWtots_B0Dp = 1e-10;
		evtWtots_B0D0 = 1e-10;
		evtWtots_B0Dps = 1e-10;
		evtWtots_BpDp = 1e-10;
		evtWtots_BpD0 = 1e-10;
		evtWtots_BpDps = 1e-10;
		evtWtots_B0sDps = 1e-10;
		evtWtots_LbLc = 1e-10;
		evtWtots_B0nonC = 1e-10;
		evtWtots_BpnonC = 1e-10;
		evtWtots_B0snonC = 1e-10;
		evtWtots_LbnonC = 1e-10;

		evtWtots_B0DpD0 = 1e-10;
		evtWtots_B0DpDps = 1e-10;
		evtWtots_B0D0Dps = 1e-10;
		evtWtots_BpDpD0 = 1e-10;
		evtWtots_BpDpDps = 1e-10;
		evtWtots_BpD0Dps = 1e-10;

		for (double x : evtW_B0Dp) evtWtot_B0Dp += x;	
		for (double x : evtW_B0D0) evtWtot_B0D0 += x;
		for (double x : evtW_B0Dps) evtWtot_B0Dps += x;
		for (double x : evtW_BpDp) evtWtot_BpDp += x;
		for (double x : evtW_BpD0) evtWtot_BpD0 += x;
		for (double x : evtW_BpDps) evtWtot_BpDps += x;
		for (double x : evtW_B0sDps) evtWtot_B0sDps += x;
		for (double x : evtW_LbLc) evtWtot_LbLc += x;
		for (double x : evtW_B0nonC) evtWtot_B0nonC += x;
		for (double x : evtW_BpnonC) evtWtot_BpnonC += x;
		for (double x : evtW_B0snonC) evtWtot_B0snonC += x;
		for (double x : evtW_LbnonC) evtWtot_LbnonC += x;

		for (double x : evtW_B0DpD0) evtWtot_B0DpD0 += x;
		for (double x : evtW_B0DpDps) evtWtot_B0DpDps += x;
		for (double x : evtW_B0D0Dps) evtWtot_B0D0Dps += x;
		for (double x : evtW_BpDpD0) evtWtot_BpDpD0 += x;
		for (double x : evtW_BpDpDps) evtWtot_BpDpDps += x;
		for (double x : evtW_BpD0Dps) evtWtot_BpD0Dps += x;

		for (double x : evtWs_B0Dp) evtWtots_B0Dp += x;
		for (double x : evtWs_B0D0) evtWtots_B0D0 += x;
		for (double x : evtWs_B0Dps) evtWtots_B0Dps += x;
		for (double x : evtWs_BpDp) evtWtots_BpDp += x;
		for (double x : evtWs_BpD0) evtWtots_BpD0 += x;
		for (double x : evtWs_BpDps) evtWtots_BpDps += x;
		for (double x : evtWs_B0sDps) evtWtots_B0sDps += x;
		for (double x : evtWs_LbLc) evtWtots_LbLc += x;
		for (double x : evtWs_B0nonC) evtWtots_B0nonC += x;
		for (double x : evtWs_BpnonC) evtWtots_BpnonC += x;
		for (double x : evtWs_B0snonC) evtWtots_B0snonC += x;
		for (double x : evtWs_LbnonC) evtWtots_LbnonC += x;

		for (double x : evtWs_B0DpD0) evtWtots_B0DpD0 += x;
		for (double x : evtWs_B0DpDps) evtWtots_B0DpDps += x;
		for (double x : evtWs_B0D0Dps) evtWtots_B0D0Dps += x;
		for (double x : evtWs_BpDpD0) evtWtots_BpDpD0 += x;
		for (double x : evtWs_BpDpDps) evtWtots_BpDpDps += x;
		for (double x : evtWs_BpD0Dps) evtWtots_BpD0Dps += x;
		
		evtWtot = evtWtot_B0Dp + evtWtot_B0D0 + evtWtot_B0Dps + evtWtot_BpDp + evtWtot_BpD0 +
						 	evtWtot_BpDps + evtWtot_B0sDps + evtWtot_LbLc + evtWtot_B0nonC +evtWtot_BpnonC +
							evtWtot_B0snonC + evtWtot_LbnonC + evtWtot_B0DpD0 + evtWtot_B0DpDps +
							evtWtot_B0D0Dps + evtWtot_BpDpD0 + evtWtot_BpDpDps + evtWtot_BpD0Dps;

		evtWtots = evtWtots_B0Dp + evtWtots_B0D0 + evtWtots_B0Dps + evtWtots_BpDp + evtWtots_BpD0 +
						 	 evtWtots_BpDps + evtWtots_B0sDps + evtWtots_LbLc + evtWtots_B0nonC +evtWtots_BpnonC +
							 evtWtots_B0snonC + evtWtots_LbnonC + evtWtots_B0DpD0 + evtWtots_B0DpDps +
							 evtWtots_B0D0Dps + evtWtots_BpDpD0 + evtWtots_BpDpDps + evtWtots_BpD0Dps;

		//loop over diff scale factors 
		errorb = 0;
		for (int j = 0; j < hMPFb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hMPFb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}

		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb_m->GetBinContent(i) == 0) diffb_m->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb_m->SetBinError(i,errorb);
  }  

	//Scale to the percentages
  diffb->Scale(100.0);
  diffb_p->Scale(100.0);
  diffb_m->Scale(100.0);

	MPFb_map["diffb"] = diffb;
	MPFb_map["diffb_p"] = diffb_p;
	MPFb_map["diffb_m"] = diffb_m;

	return MPFb_map;
}

map<string, TH1D*> pTbal_BtoCBr() {
	map <string, TH1D*> pTbalb_map;

  TFile* f_normal = TFile::Open(OpenName.c_str());
  TFile* f_scaled = TFile::Open(OpenName_BtoCBr.c_str());
  TFile* f_scaled_p = TFile::Open(OpenName_BtoCBr_p.c_str());
  TFile* f_scaled_m = TFile::Open(OpenName_BtoCBr_m.c_str());

	TProfile *prpTbalb;
	TProfile *prpTbalb_s;
	TProfile *prpTbalb_s_p;
	TProfile *prpTbalb_s_m;

  //Read the normal sample
  f_normal->GetObject("prpTbalb",   prpTbalb);

  //Read the weighted sample
  f_scaled->GetObject("prpTbalb",   prpTbalb_s);
  f_scaled_p->GetObject("prpTbalb",   prpTbalb_s_p);
  f_scaled_m->GetObject("prpTbalb",   prpTbalb_s_m);

	//2D Tprofiles for the errorb calculations
	TProfile2D* pr2DpTbalb;

	TProfile2D* pr2DpTbalb_B0Dp;
	TProfile2D* pr2DpTbalb_B0D0;
	TProfile2D* pr2DpTbalb_B0Dps;
	TProfile2D* pr2DpTbalb_BpDp;
	TProfile2D* pr2DpTbalb_BpD0;
	TProfile2D* pr2DpTbalb_BpDps;
	TProfile2D* pr2DpTbalb_B0sDps;
	TProfile2D* pr2DpTbalb_LbLc;
	TProfile2D* pr2DpTbalb_B0nonC;
	TProfile2D* pr2DpTbalb_BpnonC;
	TProfile2D* pr2DpTbalb_B0snonC;
	TProfile2D* pr2DpTbalb_LbnonC;

	TProfile2D* pr2DpTbalb_B0DpD0;
	TProfile2D* pr2DpTbalb_B0DpDps;
	TProfile2D* pr2DpTbalb_B0D0Dps;
	TProfile2D* pr2DpTbalb_BpDpD0;
	TProfile2D* pr2DpTbalb_BpDpDps;
	TProfile2D* pr2DpTbalb_BpD0Dps;

  //The sample with the additional weighting (*_s)
	TProfile2D* pr2DpTbalb_s;

	TProfile2D* pr2DpTbalb_B0Dp_s;
	TProfile2D* pr2DpTbalb_B0D0_s;
	TProfile2D* pr2DpTbalb_B0Dps_s;
	TProfile2D* pr2DpTbalb_BpDp_s;
	TProfile2D* pr2DpTbalb_BpD0_s;
	TProfile2D* pr2DpTbalb_BpDps_s;
	TProfile2D* pr2DpTbalb_B0sDps_s;
	TProfile2D* pr2DpTbalb_LbLc_s;
	TProfile2D* pr2DpTbalb_B0nonC_s;
	TProfile2D* pr2DpTbalb_BpnonC_s;
	TProfile2D* pr2DpTbalb_B0snonC_s;
	TProfile2D* pr2DpTbalb_LbnonC_s;

	TProfile2D* pr2DpTbalb_B0DpD0_s;
	TProfile2D* pr2DpTbalb_B0DpDps_s;
	TProfile2D* pr2DpTbalb_B0D0Dps_s;
	TProfile2D* pr2DpTbalb_BpDpD0_s;
	TProfile2D* pr2DpTbalb_BpDpDps_s;
	TProfile2D* pr2DpTbalb_BpD0Dps_s;

	TProfile2D* pr2DpTbalb_s_p;

	TProfile2D* pr2DpTbalb_B0Dp_s_p;
	TProfile2D* pr2DpTbalb_B0D0_s_p;
	TProfile2D* pr2DpTbalb_B0Dps_s_p;
	TProfile2D* pr2DpTbalb_BpDp_s_p;
	TProfile2D* pr2DpTbalb_BpD0_s_p;
	TProfile2D* pr2DpTbalb_BpDps_s_p;
	TProfile2D* pr2DpTbalb_B0sDps_s_p;
	TProfile2D* pr2DpTbalb_LbLc_s_p;
	TProfile2D* pr2DpTbalb_B0nonC_s_p;
	TProfile2D* pr2DpTbalb_BpnonC_s_p;
	TProfile2D* pr2DpTbalb_B0snonC_s_p;
	TProfile2D* pr2DpTbalb_LbnonC_s_p;

	TProfile2D* pr2DpTbalb_B0DpD0_s_p;
	TProfile2D* pr2DpTbalb_B0DpDps_s_p;
	TProfile2D* pr2DpTbalb_B0D0Dps_s_p;
	TProfile2D* pr2DpTbalb_BpDpD0_s_p;
	TProfile2D* pr2DpTbalb_BpDpDps_s_p;
	TProfile2D* pr2DpTbalb_BpD0Dps_s_p;

	TProfile2D* pr2DpTbalb_s_m;

	TProfile2D* pr2DpTbalb_B0Dp_s_m;
	TProfile2D* pr2DpTbalb_B0D0_s_m;
	TProfile2D* pr2DpTbalb_B0Dps_s_m;
	TProfile2D* pr2DpTbalb_BpDp_s_m;
	TProfile2D* pr2DpTbalb_BpD0_s_m;
	TProfile2D* pr2DpTbalb_BpDps_s_m;
	TProfile2D* pr2DpTbalb_B0sDps_s_m;
	TProfile2D* pr2DpTbalb_LbLc_s_m;
	TProfile2D* pr2DpTbalb_B0nonC_s_m;
	TProfile2D* pr2DpTbalb_BpnonC_s_m;
	TProfile2D* pr2DpTbalb_B0snonC_s_m;
	TProfile2D* pr2DpTbalb_LbnonC_s_m;

	TProfile2D* pr2DpTbalb_B0DpD0_s_m;
	TProfile2D* pr2DpTbalb_B0DpDps_s_m;
	TProfile2D* pr2DpTbalb_B0D0Dps_s_m;
	TProfile2D* pr2DpTbalb_BpDpD0_s_m;
	TProfile2D* pr2DpTbalb_BpDpDps_s_m;
	TProfile2D* pr2DpTbalb_BpD0Dps_s_m;

	//Tprofiles for the errorb calculations
	TProfile* prpTbalb_B0Dp;
	TProfile* prpTbalb_B0D0;
	TProfile* prpTbalb_B0Dps;
	TProfile* prpTbalb_BpDp;
	TProfile* prpTbalb_BpD0;
	TProfile* prpTbalb_BpDps;
	TProfile* prpTbalb_B0sDps;
	TProfile* prpTbalb_LbLc;
	TProfile* prpTbalb_B0nonC;
	TProfile* prpTbalb_BpnonC;
	TProfile* prpTbalb_B0snonC;
	TProfile* prpTbalb_LbnonC;

	TProfile* prpTbalb_B0DpD0;
	TProfile* prpTbalb_B0DpDps;
	TProfile* prpTbalb_B0D0Dps;
	TProfile* prpTbalb_BpDpD0;
	TProfile* prpTbalb_BpDpDps;
	TProfile* prpTbalb_BpD0Dps;

  //The sample with the additional weighting (*_s)
	TProfile* prpTbalb_B0Dp_s;
	TProfile* prpTbalb_B0D0_s;
	TProfile* prpTbalb_B0Dps_s;
	TProfile* prpTbalb_BpDp_s;
	TProfile* prpTbalb_BpD0_s;
	TProfile* prpTbalb_BpDps_s;
	TProfile* prpTbalb_B0sDps_s;
	TProfile* prpTbalb_LbLc_s;
	TProfile* prpTbalb_B0nonC_s;
	TProfile* prpTbalb_BpnonC_s;
	TProfile* prpTbalb_B0snonC_s;
	TProfile* prpTbalb_LbnonC_s;

	TProfile* prpTbalb_B0DpD0_s;
	TProfile* prpTbalb_B0DpDps_s;
	TProfile* prpTbalb_B0D0Dps_s;
	TProfile* prpTbalb_BpDpD0_s;
	TProfile* prpTbalb_BpDpDps_s;
	TProfile* prpTbalb_BpD0Dps_s;

	TProfile* prpTbalb_B0Dp_s_p;
	TProfile* prpTbalb_B0D0_s_p;
	TProfile* prpTbalb_B0Dps_s_p;
	TProfile* prpTbalb_BpDp_s_p;
	TProfile* prpTbalb_BpD0_s_p;
	TProfile* prpTbalb_BpDps_s_p;
	TProfile* prpTbalb_B0sDps_s_p;
	TProfile* prpTbalb_LbLc_s_p;
	TProfile* prpTbalb_B0nonC_s_p;
	TProfile* prpTbalb_BpnonC_s_p;
	TProfile* prpTbalb_B0snonC_s_p;
	TProfile* prpTbalb_LbnonC_s_p;

	TProfile* prpTbalb_B0DpD0_s_p;
	TProfile* prpTbalb_B0DpDps_s_p;
	TProfile* prpTbalb_B0D0Dps_s_p;
	TProfile* prpTbalb_BpDpD0_s_p;
	TProfile* prpTbalb_BpDpDps_s_p;
	TProfile* prpTbalb_BpD0Dps_s_p;

	TProfile* prpTbalb_B0Dp_s_m;
	TProfile* prpTbalb_B0D0_s_m;
	TProfile* prpTbalb_B0Dps_s_m;
	TProfile* prpTbalb_BpDp_s_m;
	TProfile* prpTbalb_BpD0_s_m;
	TProfile* prpTbalb_BpDps_s_m;
	TProfile* prpTbalb_B0sDps_s_m;
	TProfile* prpTbalb_LbLc_s_m;
	TProfile* prpTbalb_B0nonC_s_m;
	TProfile* prpTbalb_BpnonC_s_m;
	TProfile* prpTbalb_B0snonC_s_m;
	TProfile* prpTbalb_LbnonC_s_m;

	TProfile* prpTbalb_B0DpD0_s_m;
	TProfile* prpTbalb_B0DpDps_s_m;
	TProfile* prpTbalb_B0D0Dps_s_m;
	TProfile* prpTbalb_BpDpD0_s_m;
	TProfile* prpTbalb_BpDpDps_s_m;
	TProfile* prpTbalb_BpD0Dps_s_m;

  //Read the normal sample
	f_normal->GetObject("pr2DpTbalb",pr2DpTbalb);

	f_normal->GetObject("pr2DpTbalb_B0Dp",pr2DpTbalb_B0Dp);
	f_normal->GetObject("pr2DpTbalb_B0D0",pr2DpTbalb_B0D0);
	f_normal->GetObject("pr2DpTbalb_B0Dps",pr2DpTbalb_B0Dps);
	f_normal->GetObject("pr2DpTbalb_BpDp",pr2DpTbalb_BpDp);
	f_normal->GetObject("pr2DpTbalb_BpD0",pr2DpTbalb_BpD0);
	f_normal->GetObject("pr2DpTbalb_BpDps",pr2DpTbalb_BpDps);
	f_normal->GetObject("pr2DpTbalb_B0sDps",pr2DpTbalb_B0sDps);
	f_normal->GetObject("pr2DpTbalb_LbLc",pr2DpTbalb_LbLc);
	f_normal->GetObject("pr2DpTbalb_B0nonC",pr2DpTbalb_B0nonC);
	f_normal->GetObject("pr2DpTbalb_BpnonC",pr2DpTbalb_BpnonC);
	f_normal->GetObject("pr2DpTbalb_B0snonC",pr2DpTbalb_B0snonC);
	f_normal->GetObject("pr2DpTbalb_LbnonC",pr2DpTbalb_LbnonC);

	f_normal->GetObject("pr2DpTbalb_B0DpD0",pr2DpTbalb_B0DpD0);
	f_normal->GetObject("pr2DpTbalb_B0DpDps",pr2DpTbalb_B0DpDps);
	f_normal->GetObject("pr2DpTbalb_B0D0Dps",pr2DpTbalb_B0D0Dps);
	f_normal->GetObject("pr2DpTbalb_BpDpD0",pr2DpTbalb_BpDpD0);
	f_normal->GetObject("pr2DpTbalb_BpDpDps",pr2DpTbalb_BpDpDps);
	f_normal->GetObject("pr2DpTbalb_BpD0Dps",pr2DpTbalb_BpD0Dps);

  //Read the weighted sample
	f_scaled->GetObject("pr2DpTbalb",pr2DpTbalb_s);

	f_scaled->GetObject("pr2DpTbalb_B0Dp",pr2DpTbalb_B0Dp_s);
	f_scaled->GetObject("pr2DpTbalb_B0D0",pr2DpTbalb_B0D0_s);
	f_scaled->GetObject("pr2DpTbalb_B0Dps",pr2DpTbalb_B0Dps_s);
	f_scaled->GetObject("pr2DpTbalb_BpDp",pr2DpTbalb_BpDp_s);
	f_scaled->GetObject("pr2DpTbalb_BpD0",pr2DpTbalb_BpD0_s);
	f_scaled->GetObject("pr2DpTbalb_BpDps",pr2DpTbalb_BpDps_s);
	f_scaled->GetObject("pr2DpTbalb_B0sDps",pr2DpTbalb_B0sDps_s);
	f_scaled->GetObject("pr2DpTbalb_LbLc",pr2DpTbalb_LbLc_s);
	f_scaled->GetObject("pr2DpTbalb_B0nonC",pr2DpTbalb_B0nonC_s);
	f_scaled->GetObject("pr2DpTbalb_BpnonC",pr2DpTbalb_BpnonC_s);
	f_scaled->GetObject("pr2DpTbalb_B0snonC",pr2DpTbalb_B0snonC_s);
	f_scaled->GetObject("pr2DpTbalb_LbnonC",pr2DpTbalb_LbnonC_s);

	f_scaled->GetObject("pr2DpTbalb_B0DpD0",pr2DpTbalb_B0DpD0_s);
	f_scaled->GetObject("pr2DpTbalb_B0DpDps",pr2DpTbalb_B0DpDps_s);
	f_scaled->GetObject("pr2DpTbalb_B0D0Dps",pr2DpTbalb_B0D0Dps_s);
	f_scaled->GetObject("pr2DpTbalb_BpDpD0",pr2DpTbalb_BpDpD0_s);
	f_scaled->GetObject("pr2DpTbalb_BpDpDps",pr2DpTbalb_BpDpDps_s);
	f_scaled->GetObject("pr2DpTbalb_BpD0Dps",pr2DpTbalb_BpD0Dps_s);

	f_scaled_p->GetObject("pr2DpTbalb",pr2DpTbalb_s_p);

	f_scaled_p->GetObject("pr2DpTbalb_B0Dp",pr2DpTbalb_B0Dp_s_p);
	f_scaled_p->GetObject("pr2DpTbalb_B0D0",pr2DpTbalb_B0D0_s_p);
	f_scaled_p->GetObject("pr2DpTbalb_B0Dps",pr2DpTbalb_B0Dps_s_p);
	f_scaled_p->GetObject("pr2DpTbalb_BpDp",pr2DpTbalb_BpDp_s_p);
	f_scaled_p->GetObject("pr2DpTbalb_BpD0",pr2DpTbalb_BpD0_s_p);
	f_scaled_p->GetObject("pr2DpTbalb_BpDps",pr2DpTbalb_BpDps_s_p);
	f_scaled_p->GetObject("pr2DpTbalb_B0sDps",pr2DpTbalb_B0sDps_s_p);
	f_scaled_p->GetObject("pr2DpTbalb_LbLc",pr2DpTbalb_LbLc_s_p);
	f_scaled_p->GetObject("pr2DpTbalb_B0nonC",pr2DpTbalb_B0nonC_s_p);
	f_scaled_p->GetObject("pr2DpTbalb_BpnonC",pr2DpTbalb_BpnonC_s_p);
	f_scaled_p->GetObject("pr2DpTbalb_B0snonC",pr2DpTbalb_B0snonC_s_p);
	f_scaled_p->GetObject("pr2DpTbalb_LbnonC",pr2DpTbalb_LbnonC_s_p);

	f_scaled_p->GetObject("pr2DpTbalb_B0DpD0",pr2DpTbalb_B0DpD0_s_p);
	f_scaled_p->GetObject("pr2DpTbalb_B0DpDps",pr2DpTbalb_B0DpDps_s_p);
	f_scaled_p->GetObject("pr2DpTbalb_B0D0Dps",pr2DpTbalb_B0D0Dps_s_p);
	f_scaled_p->GetObject("pr2DpTbalb_BpDpD0",pr2DpTbalb_BpDpD0_s_p);
	f_scaled_p->GetObject("pr2DpTbalb_BpDpDps",pr2DpTbalb_BpDpDps_s_p);
	f_scaled_p->GetObject("pr2DpTbalb_BpD0Dps",pr2DpTbalb_BpD0Dps_s_p);

	f_scaled_m->GetObject("pr2DpTbalb",pr2DpTbalb_s_m);

	f_scaled_m->GetObject("pr2DpTbalb_B0Dp",pr2DpTbalb_B0Dp_s_m);
	f_scaled_m->GetObject("pr2DpTbalb_B0D0",pr2DpTbalb_B0D0_s_m);
	f_scaled_m->GetObject("pr2DpTbalb_B0Dps",pr2DpTbalb_B0Dps_s_m);
	f_scaled_m->GetObject("pr2DpTbalb_BpDp",pr2DpTbalb_BpDp_s_m);
	f_scaled_m->GetObject("pr2DpTbalb_BpD0",pr2DpTbalb_BpD0_s_m);
	f_scaled_m->GetObject("pr2DpTbalb_BpDps",pr2DpTbalb_BpDps_s_m);
	f_scaled_m->GetObject("pr2DpTbalb_B0sDps",pr2DpTbalb_B0sDps_s_m);
	f_scaled_m->GetObject("pr2DpTbalb_LbLc",pr2DpTbalb_LbLc_s_m);
	f_scaled_m->GetObject("pr2DpTbalb_B0nonC",pr2DpTbalb_B0nonC_s_m);
	f_scaled_m->GetObject("pr2DpTbalb_BpnonC",pr2DpTbalb_BpnonC_s_m);
	f_scaled_m->GetObject("pr2DpTbalb_B0snonC",pr2DpTbalb_B0snonC_s_m);
	f_scaled_m->GetObject("pr2DpTbalb_LbnonC",pr2DpTbalb_LbnonC_s_m);

	f_scaled_m->GetObject("pr2DpTbalb_B0DpD0",pr2DpTbalb_B0DpD0_s_m);
	f_scaled_m->GetObject("pr2DpTbalb_B0DpDps",pr2DpTbalb_B0DpDps_s_m);
	f_scaled_m->GetObject("pr2DpTbalb_B0D0Dps",pr2DpTbalb_B0D0Dps_s_m);
	f_scaled_m->GetObject("pr2DpTbalb_BpDpD0",pr2DpTbalb_BpDpD0_s_m);
	f_scaled_m->GetObject("pr2DpTbalb_BpDpDps",pr2DpTbalb_BpDpDps_s_m);
	f_scaled_m->GetObject("pr2DpTbalb_BpD0Dps",pr2DpTbalb_BpD0Dps_s_m);

	//calculate the difference in response between normal and the rescaled sample 
	//the second ProjectionX has new name as argument, otherwise would have same name as the previous
	//histo and causes memory leak 
	TH1D* hpTbalb     = prpTbalb   ->ProjectionX();
	TH1D* hpTbalb_s   = prpTbalb_s ->ProjectionX("pr2DpTbalb_s");
	TH1D* hpTbalb_s_p = prpTbalb_s_p ->ProjectionX("pr2DpTbalb_s_p");
	TH1D* hpTbalb_s_m = prpTbalb_s_m ->ProjectionX("pr2DpTbalb_s_m");

	TH1D* diffb 		= (TH1D*) hpTbalb_s->Clone();
	TH1D* diffb_p 	= (TH1D*) hpTbalb_s_p->Clone();
	TH1D* diffb_m 	= (TH1D*) hpTbalb_s_m->Clone();

	diffb->Add(hpTbalb,-1);
	diffb_p->Add(hpTbalb,-1);
	diffb_m->Add(hpTbalb,-1);

	//evtW_* = event weight, sem_* = standard errorb of the mean,   
	double errorb, evtWtot, evtWtots;

	double evtWtot_B0Dp;
	double evtWtot_B0D0;
	double evtWtot_B0Dps;
	double evtWtot_BpDp;
	double evtWtot_BpD0;
	double evtWtot_BpDps;
	double evtWtot_B0sDps;
	double evtWtot_LbLc;
	double evtWtot_B0nonC;
	double evtWtot_BpnonC;
	double evtWtot_B0snonC;
	double evtWtot_LbnonC;

	double evtWtot_B0DpD0;
	double evtWtot_B0DpDps;
	double evtWtot_B0D0Dps;
	double evtWtot_BpDpD0;
	double evtWtot_BpDpDps;
	double evtWtot_BpD0Dps;

	double evtWtots_B0Dp;
	double evtWtots_B0D0;
	double evtWtots_B0Dps;
	double evtWtots_BpDp;
	double evtWtots_BpD0;
	double evtWtots_BpDps;
	double evtWtots_B0sDps;
	double evtWtots_LbLc;
	double evtWtots_B0nonC;
	double evtWtots_BpnonC;
	double evtWtots_B0snonC;
	double evtWtots_LbnonC;

	double evtWtots_B0DpD0;
	double evtWtots_B0DpDps;
	double evtWtots_B0D0Dps;
	double evtWtots_BpDpD0;
	double evtWtots_BpDpDps;
	double evtWtots_BpD0Dps;

	vector<double> evtW_B0Dp;
	vector<double> evtW_B0D0;
	vector<double> evtW_B0Dps;
	vector<double> evtW_BpDp;
	vector<double> evtW_BpD0;
	vector<double> evtW_BpDps;
	vector<double> evtW_B0sDps;
	vector<double> evtW_LbLc;
	vector<double> evtW_B0nonC;
	vector<double> evtW_BpnonC;
	vector<double> evtW_B0snonC;
	vector<double> evtW_LbnonC;

	vector<double> evtW_B0DpD0;
	vector<double> evtW_B0DpDps;
	vector<double> evtW_B0D0Dps;
	vector<double> evtW_BpDpD0;
	vector<double> evtW_BpDpDps;
	vector<double> evtW_BpD0Dps;

	vector<double> evtWs_B0Dp;
	vector<double> evtWs_B0D0;
	vector<double> evtWs_B0Dps;
	vector<double> evtWs_BpDp;
	vector<double> evtWs_BpD0;
	vector<double> evtWs_BpDps;
	vector<double> evtWs_B0sDps;
	vector<double> evtWs_LbLc;
	vector<double> evtWs_B0nonC;
	vector<double> evtWs_BpnonC;
	vector<double> evtWs_B0snonC;
	vector<double> evtWs_LbnonC;

	vector<double> evtWs_B0DpD0;
	vector<double> evtWs_B0DpDps;
	vector<double> evtWs_B0D0Dps;
	vector<double> evtWs_BpDpD0;
	vector<double> evtWs_BpDpDps;
	vector<double> evtWs_BpD0Dps;

	vector<double> sem_B0Dp;
	vector<double> sem_B0D0;
	vector<double> sem_B0Dps;
	vector<double> sem_BpDp;
	vector<double> sem_BpD0;
	vector<double> sem_BpDps;
	vector<double> sem_B0sDps;
	vector<double> sem_LbLc;
	vector<double> sem_B0nonC;
	vector<double> sem_BpnonC;
	vector<double> sem_B0snonC;
	vector<double> sem_LbnonC;

	vector<double> sem_B0DpD0;
	vector<double> sem_B0DpDps;
	vector<double> sem_B0D0Dps;
	vector<double> sem_BpDpD0;
	vector<double> sem_BpDpDps;
	vector<double> sem_BpD0Dps;

	vector<double> vals_B0Dp;
	vector<double> vals_B0D0;
	vector<double> vals_B0Dps;
	vector<double> vals_BpDp;
	vector<double> vals_BpD0;
	vector<double> vals_BpDps;
	vector<double> vals_B0sDps;
	vector<double> vals_LbLc;
	vector<double> vals_B0nonC;
	vector<double> vals_BpnonC;
	vector<double> vals_B0snonC;
	vector<double> vals_LbnonC;

	vector<double> vals_B0DpD0;
	vector<double> vals_B0DpDps;
	vector<double> vals_B0D0Dps;
	vector<double> vals_BpDpD0;
	vector<double> vals_BpDpDps;
	vector<double> vals_BpD0Dps;

	vector<double> vec_B0Dp_stat, vec_B0D0_stat, vec_B0Dps_stat, vec_BpDp_stat,
								 vec_BpD0_stat, vec_BpDps_stat, vec_B0sDps_stat, vec_LbLc_stat,
								 vec_B0nonC_stat, vec_BpnonC_stat, vec_B0snonC_stat, vec_LbnonC_stat,
								 vec_B0DpD0_stat,	vec_B0DpDps_stat,	vec_B0D0Dps_stat,	vec_BpDpD0_stat,
								 vec_BpDpDps_stat, vec_BpD0Dps_stat;

	ReadFromFile(vec_B0Dp_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/stat_err/B0Dp_sf_stat.txt");
	ReadFromFile(vec_B0D0_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/stat_err/B0D0_sf_stat.txt");
	ReadFromFile(vec_B0Dps_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/stat_err/B0Dps_sf_stat.txt");
	ReadFromFile(vec_BpDp_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/stat_err/BpDp_sf_stat.txt");
	ReadFromFile(vec_BpD0_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/stat_err/BpD0_sf_stat.txt");
	ReadFromFile(vec_BpDps_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/stat_err/BpDps_sf_stat.txt");
	ReadFromFile(vec_B0sDps_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/stat_err/B0sDps_sf_stat.txt");
	ReadFromFile(vec_LbLc_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/stat_err/LbLc_sf_stat.txt");
	ReadFromFile(vec_B0nonC_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/stat_err/B0nonC_sf_stat.txt");
	ReadFromFile(vec_BpnonC_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/stat_err/BpnonC_sf_stat.txt");
	ReadFromFile(vec_B0snonC_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/stat_err/B0snonC_sf_stat.txt");
	ReadFromFile(vec_LbnonC_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/stat_err/LbnonC_sf_stat.txt");

	//calculating the stat error of the categories with two scaling factors 
	vector<double> vec_B0Dp_sf, vec_B0D0_sf, vec_B0Dps_sf, vec_BpDp_sf,
								 vec_BpD0_sf, vec_BpDps_sf, vec_B0sDps_sf, vec_LbLc_sf,
								 vec_B0nonC_sf, vec_BpnonC_sf, vec_B0snonC_sf, vec_LbnonC_sf;

	ReadFromFile(vec_B0Dp_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/normal/B0Dp_sf.txt");
	ReadFromFile(vec_B0D0_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/normal/B0D0_sf.txt");
	ReadFromFile(vec_B0Dps_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/normal/B0Dps_sf.txt");
	ReadFromFile(vec_BpDp_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/normal/BpDp_sf.txt");
	ReadFromFile(vec_BpD0_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/normal/BpD0_sf.txt");
	ReadFromFile(vec_BpDps_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/normal/BpDps_sf.txt");
	ReadFromFile(vec_B0sDps_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/normal/B0sDps_sf.txt");
	ReadFromFile(vec_LbLc_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/normal/LbLc_sf.txt");
	ReadFromFile(vec_B0nonC_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/normal/B0nonC_sf.txt");
	ReadFromFile(vec_BpnonC_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/normal/BpnonC_sf.txt");
	ReadFromFile(vec_B0snonC_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/normal/B0snonC_sf.txt");
	ReadFromFile(vec_LbnonC_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/normal/LbnonC_sf.txt");

	for (int i = 0; i < vec_B0Dp_sf.size(); i++) {
		vec_B0DpD0_stat.push_back(sqrt(pow(vec_B0D0_sf[i]*vec_B0Dp_stat[i],2) + pow(vec_B0Dp_sf[i]*vec_B0D0_stat[i],2)));
		vec_B0DpDps_stat.push_back(sqrt(pow(vec_B0Dps_sf[i]*vec_B0Dp_stat[i],2) + pow(vec_B0Dp_sf[i]*vec_B0Dps_stat[i],2)));
		vec_B0D0Dps_stat.push_back(sqrt(pow(vec_B0Dps_sf[i]*vec_B0D0_stat[i],2) + pow(vec_B0D0_sf[i]*vec_B0Dps_stat[i],2)));

		vec_BpDpD0_stat.push_back(sqrt(pow(vec_BpD0_sf[i]*vec_BpDp_stat[i],2) + pow(vec_BpDp_sf[i]*vec_BpD0_stat[i],2)));
		vec_BpDpDps_stat.push_back(sqrt(pow(vec_BpDps_sf[i]*vec_BpDp_stat[i],2) + pow(vec_BpDp_sf[i]*vec_BpDps_stat[i],2)));
		vec_BpD0Dps_stat.push_back(sqrt(pow(vec_BpDps_sf[i]*vec_BpD0_stat[i],2) + pow(vec_BpD0_sf[i]*vec_BpDps_stat[i],2)));
	}

	vector<vector<double>> vec_sf_sem{vec_B0Dp_stat, vec_B0D0_stat, vec_B0Dps_stat, vec_BpDp_stat,
								 vec_BpD0_stat, vec_BpDps_stat, vec_B0sDps_stat, vec_LbLc_stat,
								 vec_B0nonC_stat, vec_BpnonC_stat, vec_B0snonC_stat, vec_LbnonC_stat,
								 vec_B0DpD0_stat,	vec_B0DpDps_stat,	vec_B0D0Dps_stat,	vec_BpDpD0_stat,
								 vec_BpDpDps_stat, vec_BpD0Dps_stat};

	//removing the under- and overflow bin values
	for (int i = 0; i < vec_sf_sem.size(); i++) {
		vec_sf_sem[i].erase(vec_sf_sem[i].begin() + 0);
		vec_sf_sem[i].erase(vec_sf_sem[i].begin() + vec_sf_sem[i].size() - 1);
	}

  //Loop over the TProfile bins
  for (int i = 1; i < hpTbalb->GetNbinsX()+1; i++){
		evtW_B0Dp.clear();
		evtW_B0D0.clear();
		evtW_B0Dps.clear();
		evtW_BpDp.clear();
		evtW_BpD0.clear();
		evtW_BpDps.clear();
		evtW_B0sDps.clear();
		evtW_LbLc.clear();
		evtW_B0nonC.clear();
		evtW_BpnonC.clear();
		evtW_B0snonC.clear();
		evtW_LbnonC.clear();

		evtW_B0DpD0.clear();
		evtW_B0DpDps.clear();
		evtW_B0D0Dps.clear();
		evtW_BpDpD0.clear();
		evtW_BpDpDps.clear();
		evtW_BpD0Dps.clear();

		evtWs_B0Dp.clear();
		evtWs_B0D0.clear();
		evtWs_B0Dps.clear();
		evtWs_BpDp.clear();
		evtWs_BpD0.clear();
		evtWs_BpDps.clear();
		evtWs_B0sDps.clear();
		evtWs_LbLc.clear();
		evtWs_B0nonC.clear();
		evtWs_BpnonC.clear();
		evtWs_B0snonC.clear();
		evtWs_LbnonC.clear();

		evtWs_B0DpD0.clear();
		evtWs_B0DpDps.clear();
		evtWs_B0D0Dps.clear();
		evtWs_BpDpD0.clear();
		evtWs_BpDpDps.clear();
		evtWs_BpD0Dps.clear();

		sem_B0Dp.clear();
		sem_B0D0.clear();
		sem_B0Dps.clear();
		sem_BpDp.clear();
		sem_BpD0.clear();
		sem_BpDps.clear();
		sem_B0sDps.clear();
		sem_LbLc.clear();
		sem_B0nonC.clear();
		sem_BpnonC.clear();
		sem_B0snonC.clear();
		sem_LbnonC.clear();

		sem_B0DpD0.clear();
		sem_B0DpDps.clear();
		sem_B0D0Dps.clear();
		sem_BpDpD0.clear();
		sem_BpDpDps.clear();
		sem_BpD0Dps.clear();

		vals_B0Dp.clear();
		vals_B0D0.clear();
		vals_B0Dps.clear();
		vals_BpDp.clear();
		vals_BpD0.clear();
		vals_BpDps.clear();
		vals_B0sDps.clear();
		vals_LbLc.clear();
		vals_B0nonC.clear();
		vals_BpnonC.clear();
		vals_B0snonC.clear();
		vals_LbnonC.clear();

		vals_B0DpD0.clear();
		vals_B0DpDps.clear();
		vals_B0D0Dps.clear();
		vals_BpDpD0.clear();
		vals_BpDpDps.clear();
		vals_BpD0Dps.clear();

		for (int j = 1; j < hpTbalb->GetNbinsX()+1; j++) {
			string pr_ind = "_" + to_string(i) + "_" + to_string(j);

			prpTbalb_B0Dp = pr2DpTbalb_B0Dp->ProfileX(("B0Dp"+pr_ind).c_str(),j,j);
			prpTbalb_B0D0 = pr2DpTbalb_B0D0->ProfileX(("B0D0"+pr_ind).c_str(),j,j);
			prpTbalb_B0Dps = pr2DpTbalb_B0Dps->ProfileX(("B0Dps"+pr_ind).c_str(),j,j);
			prpTbalb_BpDp = pr2DpTbalb_BpDp->ProfileX(("BpDp"+pr_ind).c_str(),j,j);
			prpTbalb_BpD0 = pr2DpTbalb_BpD0->ProfileX(("BpD0"+pr_ind).c_str(),j,j);
			prpTbalb_BpDps = pr2DpTbalb_BpDps->ProfileX(("BpDps"+pr_ind).c_str(),j,j);
			prpTbalb_B0sDps = pr2DpTbalb_B0sDps->ProfileX(("B0sDps"+pr_ind).c_str(),j,j);
			prpTbalb_LbLc = pr2DpTbalb_LbLc->ProfileX(("LbLc"+pr_ind).c_str(),j,j);
			prpTbalb_B0nonC = pr2DpTbalb_B0nonC->ProfileX(("B0nonC"+pr_ind).c_str(),j,j);
			prpTbalb_BpnonC = pr2DpTbalb_BpnonC->ProfileX(("BpnonC"+pr_ind).c_str(),j,j);
			prpTbalb_B0snonC = pr2DpTbalb_B0snonC->ProfileX(("B0snonC"+pr_ind).c_str(),j,j);
			prpTbalb_LbnonC = pr2DpTbalb_LbnonC->ProfileX(("LbnonC"+pr_ind).c_str(),j,j);

			prpTbalb_B0DpD0 = pr2DpTbalb_B0DpD0->ProfileX(("B0DpD0"+pr_ind).c_str(),j,j);
			prpTbalb_B0DpDps = pr2DpTbalb_B0DpDps->ProfileX(("B0DpDps"+pr_ind).c_str(),j,j);
			prpTbalb_B0D0Dps = pr2DpTbalb_B0D0Dps->ProfileX(("B0D0Dps"+pr_ind).c_str(),j,j);
			prpTbalb_BpDpD0 = pr2DpTbalb_BpDpD0->ProfileX(("BpDpD0"+pr_ind).c_str(),j,j);
			prpTbalb_BpDpDps = pr2DpTbalb_BpDpDps->ProfileX(("BpDpDps"+pr_ind).c_str(),j,j);
			prpTbalb_BpD0Dps = pr2DpTbalb_BpD0Dps->ProfileX(("BpD0Dps"+pr_ind).c_str(),j,j);

			prpTbalb_B0Dp_s = pr2DpTbalb_B0Dp_s->ProfileX(("B0Dp_s"+pr_ind).c_str(),j,j);
			prpTbalb_B0D0_s = pr2DpTbalb_B0D0_s->ProfileX(("B0D0_s"+pr_ind).c_str(),j,j);
			prpTbalb_B0Dps_s = pr2DpTbalb_B0Dps_s->ProfileX(("B0Dps_s"+pr_ind).c_str(),j,j);
			prpTbalb_BpDp_s = pr2DpTbalb_BpDp_s->ProfileX(("BpDp_s"+pr_ind).c_str(),j,j);
			prpTbalb_BpD0_s = pr2DpTbalb_BpD0_s->ProfileX(("BpD0_s"+pr_ind).c_str(),j,j);
			prpTbalb_BpDps_s = pr2DpTbalb_BpDps_s->ProfileX(("BpDps_s"+pr_ind).c_str(),j,j);
			prpTbalb_B0sDps_s = pr2DpTbalb_B0sDps_s->ProfileX(("B0sDps_s"+pr_ind).c_str(),j,j);
			prpTbalb_LbLc_s = pr2DpTbalb_LbLc_s->ProfileX(("LbLc_s"+pr_ind).c_str(),j,j);
			prpTbalb_B0nonC_s = pr2DpTbalb_B0nonC_s->ProfileX(("B0nonC_s"+pr_ind).c_str(),j,j);
			prpTbalb_BpnonC_s = pr2DpTbalb_BpnonC_s->ProfileX(("BpnonC_s"+pr_ind).c_str(),j,j);
			prpTbalb_B0snonC_s = pr2DpTbalb_B0snonC_s->ProfileX(("B0snonC_s"+pr_ind).c_str(),j,j);
			prpTbalb_LbnonC_s = pr2DpTbalb_LbnonC_s->ProfileX(("LbnonC_s"+pr_ind).c_str(),j,j);

			prpTbalb_B0DpD0_s = pr2DpTbalb_B0DpD0_s->ProfileX(("B0DpD0_s"+pr_ind).c_str(),j,j);
			prpTbalb_B0DpDps_s = pr2DpTbalb_B0DpDps_s->ProfileX(("B0DpDps_s"+pr_ind).c_str(),j,j);
			prpTbalb_B0D0Dps_s = pr2DpTbalb_B0D0Dps_s->ProfileX(("B0D0Dps_s"+pr_ind).c_str(),j,j);
			prpTbalb_BpDpD0_s = pr2DpTbalb_BpDpD0_s->ProfileX(("BpDpD0_s"+pr_ind).c_str(),j,j);
			prpTbalb_BpDpDps_s = pr2DpTbalb_BpDpDps_s->ProfileX(("BpDpDps_s"+pr_ind).c_str(),j,j);
			prpTbalb_BpD0Dps_s = pr2DpTbalb_BpD0Dps_s->ProfileX(("BpD0Dps_s"+pr_ind).c_str(),j,j);

			evtW_B0Dp.push_back(prpTbalb_B0Dp->GetBinEntries(i));
			evtW_B0D0.push_back(prpTbalb_B0D0->GetBinEntries(i));
			evtW_B0Dps.push_back(prpTbalb_B0Dps->GetBinEntries(i));
			evtW_BpDp.push_back(prpTbalb_BpDp->GetBinEntries(i));
			evtW_BpD0.push_back(prpTbalb_BpD0->GetBinEntries(i));
			evtW_BpDps.push_back(prpTbalb_BpDps->GetBinEntries(i));
			evtW_B0sDps.push_back(prpTbalb_B0sDps->GetBinEntries(i));
			evtW_LbLc.push_back(prpTbalb_LbLc->GetBinEntries(i));
			evtW_B0nonC.push_back(prpTbalb_B0nonC->GetBinEntries(i));
			evtW_BpnonC.push_back(prpTbalb_BpnonC->GetBinEntries(i));
			evtW_B0snonC.push_back(prpTbalb_B0snonC->GetBinEntries(i));
			evtW_LbnonC.push_back(prpTbalb_LbnonC->GetBinEntries(i));

			evtW_B0DpD0.push_back(prpTbalb_B0DpD0->GetBinEntries(i));
			evtW_B0DpDps.push_back(prpTbalb_B0DpDps->GetBinEntries(i));
			evtW_B0D0Dps.push_back(prpTbalb_B0D0Dps->GetBinEntries(i));
			evtW_BpDpD0.push_back(prpTbalb_BpDpD0->GetBinEntries(i));
			evtW_BpDpDps.push_back(prpTbalb_BpDpDps->GetBinEntries(i));
			evtW_BpD0Dps.push_back(prpTbalb_BpD0Dps->GetBinEntries(i));

			evtWs_B0Dp.push_back(prpTbalb_B0Dp_s->GetBinEntries(i));
			evtWs_B0D0.push_back(prpTbalb_B0D0_s->GetBinEntries(i));
			evtWs_B0Dps.push_back(prpTbalb_B0Dps_s->GetBinEntries(i));
			evtWs_BpDp.push_back(prpTbalb_BpDp_s->GetBinEntries(i));
			evtWs_BpD0.push_back(prpTbalb_BpD0_s->GetBinEntries(i));
			evtWs_BpDps.push_back(prpTbalb_BpDps_s->GetBinEntries(i));
			evtWs_B0sDps.push_back(prpTbalb_B0sDps_s->GetBinEntries(i));
			evtWs_LbLc.push_back(prpTbalb_LbLc_s->GetBinEntries(i));
			evtWs_B0nonC.push_back(prpTbalb_B0nonC_s->GetBinEntries(i));
			evtWs_BpnonC.push_back(prpTbalb_BpnonC_s->GetBinEntries(i));
			evtWs_B0snonC.push_back(prpTbalb_B0snonC_s->GetBinEntries(i));
			evtWs_LbnonC.push_back(prpTbalb_LbnonC_s->GetBinEntries(i));

			evtWs_B0DpD0.push_back(prpTbalb_B0DpD0_s->GetBinEntries(i));
			evtWs_B0DpDps.push_back(prpTbalb_B0DpDps_s->GetBinEntries(i));
			evtWs_B0D0Dps.push_back(prpTbalb_B0D0Dps_s->GetBinEntries(i));
			evtWs_BpDpD0.push_back(prpTbalb_BpDpD0_s->GetBinEntries(i));
			evtWs_BpDpDps.push_back(prpTbalb_BpDpDps_s->GetBinEntries(i));
			evtWs_BpD0Dps.push_back(prpTbalb_BpD0Dps_s->GetBinEntries(i));

			sem_B0Dp.push_back(prpTbalb_B0Dp->GetBinError(i));
			sem_B0D0.push_back(prpTbalb_B0D0->GetBinError(i));
			sem_B0Dps.push_back(prpTbalb_B0Dps->GetBinError(i));
			sem_BpDp.push_back(prpTbalb_BpDp->GetBinError(i));
			sem_BpD0.push_back(prpTbalb_BpD0->GetBinError(i));
			sem_BpDps.push_back(prpTbalb_BpDps->GetBinError(i));
			sem_B0sDps.push_back(prpTbalb_B0sDps->GetBinError(i));
			sem_LbLc.push_back(prpTbalb_LbLc->GetBinError(i));
			sem_B0nonC.push_back(prpTbalb_B0nonC->GetBinError(i));
			sem_BpnonC.push_back(prpTbalb_BpnonC->GetBinError(i));
			sem_B0snonC.push_back(prpTbalb_B0snonC->GetBinError(i));
			sem_LbnonC.push_back(prpTbalb_LbnonC->GetBinError(i));

			sem_B0DpD0.push_back(prpTbalb_B0DpD0->GetBinError(i));
			sem_B0DpDps.push_back(prpTbalb_B0DpDps->GetBinError(i));
			sem_B0D0Dps.push_back(prpTbalb_B0D0Dps->GetBinError(i));
			sem_BpDpD0.push_back(prpTbalb_BpDpD0->GetBinError(i));
			sem_BpDpDps.push_back(prpTbalb_BpDpDps->GetBinError(i));
			sem_BpD0Dps.push_back(prpTbalb_BpD0Dps->GetBinError(i));

			vals_B0Dp.push_back(prpTbalb_B0Dp->GetBinContent(i));
			vals_B0D0.push_back(prpTbalb_B0D0->GetBinContent(i));
			vals_B0Dps.push_back(prpTbalb_B0Dps->GetBinContent(i));
			vals_BpDp.push_back(prpTbalb_BpDp->GetBinContent(i));
			vals_BpD0.push_back(prpTbalb_BpD0->GetBinContent(i));
			vals_BpDps.push_back(prpTbalb_BpDps->GetBinContent(i));
			vals_B0sDps.push_back(prpTbalb_B0sDps->GetBinContent(i));
			vals_LbLc.push_back(prpTbalb_LbLc->GetBinContent(i));
			vals_B0nonC.push_back(prpTbalb_B0nonC->GetBinContent(i));
			vals_BpnonC.push_back(prpTbalb_BpnonC->GetBinContent(i));
			vals_B0snonC.push_back(prpTbalb_B0snonC->GetBinContent(i));
			vals_LbnonC.push_back(prpTbalb_LbnonC->GetBinContent(i));

			vals_B0DpD0.push_back(prpTbalb_B0DpD0->GetBinContent(i));
			vals_B0DpDps.push_back(prpTbalb_B0DpDps->GetBinContent(i));
			vals_B0D0Dps.push_back(prpTbalb_B0D0Dps->GetBinContent(i));
			vals_BpDpD0.push_back(prpTbalb_BpDpD0->GetBinContent(i));
			vals_BpDpDps.push_back(prpTbalb_BpDpDps->GetBinContent(i));
			vals_BpD0Dps.push_back(prpTbalb_BpD0Dps->GetBinContent(i));

			delete prpTbalb_B0Dp;
			delete prpTbalb_B0D0;
			delete prpTbalb_B0Dps;
			delete prpTbalb_BpDp;
			delete prpTbalb_BpD0;
			delete prpTbalb_BpDps;
			delete prpTbalb_B0sDps;
			delete prpTbalb_LbLc;
			delete prpTbalb_B0nonC;
			delete prpTbalb_BpnonC;
			delete prpTbalb_B0snonC; 
			delete prpTbalb_LbnonC;

			delete prpTbalb_B0DpD0;
			delete prpTbalb_B0DpDps;
			delete prpTbalb_B0D0Dps;
			delete prpTbalb_BpDpD0; 
			delete prpTbalb_BpDpDps;
			delete prpTbalb_BpD0Dps;

			delete prpTbalb_B0Dp_s;
			delete prpTbalb_B0D0_s;
			delete prpTbalb_B0Dps_s;
			delete prpTbalb_BpDp_s;
			delete prpTbalb_BpD0_s;
			delete prpTbalb_BpDps_s;
			delete prpTbalb_B0sDps_s;
			delete prpTbalb_LbLc_s;
			delete prpTbalb_B0nonC_s;
			delete prpTbalb_BpnonC_s;
			delete prpTbalb_B0snonC_s;
			delete prpTbalb_LbnonC_s;

			delete prpTbalb_B0DpD0_s;
			delete prpTbalb_B0DpDps_s;
			delete prpTbalb_B0D0Dps_s;
			delete prpTbalb_BpDpD0_s;
			delete prpTbalb_BpDpDps_s;
			delete prpTbalb_BpD0Dps_s;
		}

		vector<vector<double>> vec_evtW{evtW_B0Dp, evtW_B0D0, evtW_B0Dps, evtW_BpDp,
								 evtW_BpD0, evtW_BpDps, evtW_B0sDps, evtW_LbLc,
								 evtW_B0nonC, evtW_BpnonC, evtW_B0snonC, evtW_LbnonC,
								 evtW_B0DpD0,	evtW_B0DpDps,	evtW_B0D0Dps,	evtW_BpDpD0,
								 evtW_BpDpDps, evtW_BpD0Dps};

		vector<vector<double>> vec_evtWs{evtWs_B0Dp, evtWs_B0D0, evtWs_B0Dps, evtWs_BpDp,
								 evtWs_BpD0, evtWs_BpDps, evtWs_B0sDps, evtWs_LbLc,
								 evtWs_B0nonC, evtWs_BpnonC, evtWs_B0snonC, evtWs_LbnonC,
								 evtWs_B0DpD0,	evtWs_B0DpDps,	evtWs_B0D0Dps,	evtWs_BpDpD0,
								 evtWs_BpDpDps, evtWs_BpD0Dps};

		vector<vector<double>> vec_sem{sem_B0Dp, sem_B0D0, sem_B0Dps, sem_BpDp,
								 sem_BpD0, sem_BpDps, sem_B0sDps, sem_LbLc,
								 sem_B0nonC, sem_BpnonC, sem_B0snonC, sem_LbnonC,
								 sem_B0DpD0,	sem_B0DpDps,	sem_B0D0Dps,	sem_BpDpD0,
								 sem_BpDpDps, sem_BpD0Dps};

		vector<vector<double>> vec_vals{
vals_B0Dp, vals_B0D0, vals_B0Dps, vals_BpDp,
								 vals_BpD0, vals_BpDps, vals_B0sDps, vals_LbLc,
								 vals_B0nonC, vals_BpnonC, vals_B0snonC, vals_LbnonC,
								 vals_B0DpD0,	vals_B0DpDps,	vals_B0D0Dps,	vals_BpDpD0,
								 vals_BpDpDps, vals_BpD0Dps};

		//set small number so that we dont divide by zero 
		evtWtot_B0Dp = 1e-10;
		evtWtot_B0D0 = 1e-10;
		evtWtot_B0Dps = 1e-10;
		evtWtot_BpDp = 1e-10;
		evtWtot_BpD0 = 1e-10;
		evtWtot_BpDps = 1e-10;
		evtWtot_B0sDps = 1e-10;
		evtWtot_LbLc = 1e-10;
		evtWtot_B0nonC = 1e-10;
		evtWtot_BpnonC = 1e-10;
		evtWtot_B0snonC = 1e-10;
		evtWtot_LbnonC = 1e-10;

		evtWtot_B0DpD0 = 1e-10;
		evtWtot_B0DpDps = 1e-10;
		evtWtot_B0D0Dps = 1e-10;
		evtWtot_BpDpD0 = 1e-10;
		evtWtot_BpDpDps = 1e-10;
		evtWtot_BpD0Dps = 1e-10;

		evtWtots_B0Dp = 1e-10;
		evtWtots_B0D0 = 1e-10;
		evtWtots_B0Dps = 1e-10;
		evtWtots_BpDp = 1e-10;
		evtWtots_BpD0 = 1e-10;
		evtWtots_BpDps = 1e-10;
		evtWtots_B0sDps = 1e-10;
		evtWtots_LbLc = 1e-10;
		evtWtots_B0nonC = 1e-10;
		evtWtots_BpnonC = 1e-10;
		evtWtots_B0snonC = 1e-10;
		evtWtots_LbnonC = 1e-10;

		evtWtots_B0DpD0 = 1e-10;
		evtWtots_B0DpDps = 1e-10;
		evtWtots_B0D0Dps = 1e-10;
		evtWtots_BpDpD0 = 1e-10;
		evtWtots_BpDpDps = 1e-10;
		evtWtots_BpD0Dps = 1e-10;

		for (double x : evtW_B0Dp) evtWtot_B0Dp += x;	
		for (double x : evtW_B0D0) evtWtot_B0D0 += x;
		for (double x : evtW_B0Dps) evtWtot_B0Dps += x;
		for (double x : evtW_BpDp) evtWtot_BpDp += x;
		for (double x : evtW_BpD0) evtWtot_BpD0 += x;
		for (double x : evtW_BpDps) evtWtot_BpDps += x;
		for (double x : evtW_B0sDps) evtWtot_B0sDps += x;
		for (double x : evtW_LbLc) evtWtot_LbLc += x;
		for (double x : evtW_B0nonC) evtWtot_B0nonC += x;
		for (double x : evtW_BpnonC) evtWtot_BpnonC += x;
		for (double x : evtW_B0snonC) evtWtot_B0snonC += x;
		for (double x : evtW_LbnonC) evtWtot_LbnonC += x;

		for (double x : evtW_B0DpD0) evtWtot_B0DpD0 += x;
		for (double x : evtW_B0DpDps) evtWtot_B0DpDps += x;
		for (double x : evtW_B0D0Dps) evtWtot_B0D0Dps += x;
		for (double x : evtW_BpDpD0) evtWtot_BpDpD0 += x;
		for (double x : evtW_BpDpDps) evtWtot_BpDpDps += x;
		for (double x : evtW_BpD0Dps) evtWtot_BpD0Dps += x;

		for (double x : evtWs_B0Dp) evtWtots_B0Dp += x;
		for (double x : evtWs_B0D0) evtWtots_B0D0 += x;
		for (double x : evtWs_B0Dps) evtWtots_B0Dps += x;
		for (double x : evtWs_BpDp) evtWtots_BpDp += x;
		for (double x : evtWs_BpD0) evtWtots_BpD0 += x;
		for (double x : evtWs_BpDps) evtWtots_BpDps += x;
		for (double x : evtWs_B0sDps) evtWtots_B0sDps += x;
		for (double x : evtWs_LbLc) evtWtots_LbLc += x;
		for (double x : evtWs_B0nonC) evtWtots_B0nonC += x;
		for (double x : evtWs_BpnonC) evtWtots_BpnonC += x;
		for (double x : evtWs_B0snonC) evtWtots_B0snonC += x;
		for (double x : evtWs_LbnonC) evtWtots_LbnonC += x;

		for (double x : evtWs_B0DpD0) evtWtots_B0DpD0 += x;
		for (double x : evtWs_B0DpDps) evtWtots_B0DpDps += x;
		for (double x : evtWs_B0D0Dps) evtWtots_B0D0Dps += x;
		for (double x : evtWs_BpDpD0) evtWtots_BpDpD0 += x;
		for (double x : evtWs_BpDpDps) evtWtots_BpDpDps += x;
		for (double x : evtWs_BpD0Dps) evtWtots_BpD0Dps += x;
		
		evtWtot = evtWtot_B0Dp + evtWtot_B0D0 + evtWtot_B0Dps + evtWtot_BpDp + evtWtot_BpD0 +
						 	evtWtot_BpDps + evtWtot_B0sDps + evtWtot_LbLc + evtWtot_B0nonC +evtWtot_BpnonC +
							evtWtot_B0snonC + evtWtot_LbnonC + evtWtot_B0DpD0 + evtWtot_B0DpDps +
							evtWtot_B0D0Dps + evtWtot_BpDpD0 + evtWtot_BpDpDps + evtWtot_BpD0Dps;

		evtWtots = evtWtots_B0Dp + evtWtots_B0D0 + evtWtots_B0Dps + evtWtots_BpDp + evtWtots_BpD0 +
						 	 evtWtots_BpDps + evtWtots_B0sDps + evtWtots_LbLc + evtWtots_B0nonC +evtWtots_BpnonC +
							 evtWtots_B0snonC + evtWtots_LbnonC + evtWtots_B0DpD0 + evtWtots_B0DpDps +
							 evtWtots_B0D0Dps + evtWtots_BpDpD0 + evtWtots_BpDpDps + evtWtots_BpD0Dps;

		//loop over diff scale factors 
		errorb = 0;
		for (int j = 0; j < hpTbalb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hpTbalb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}

		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb->GetBinContent(i) == 0) diffb->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb->SetBinError(i,errorb);
  }

//############### plus variation ####################
//Loop over the TProfile bins
  for (int i = 1; i < hpTbalb->GetNbinsX()+1; i++){
		evtW_B0Dp.clear();
		evtW_B0D0.clear();
		evtW_B0Dps.clear();
		evtW_BpDp.clear();
		evtW_BpD0.clear();
		evtW_BpDps.clear();
		evtW_B0sDps.clear();
		evtW_LbLc.clear();
		evtW_B0nonC.clear();
		evtW_BpnonC.clear();
		evtW_B0snonC.clear();
		evtW_LbnonC.clear();

		evtW_B0DpD0.clear();
		evtW_B0DpDps.clear();
		evtW_B0D0Dps.clear();
		evtW_BpDpD0.clear();
		evtW_BpDpDps.clear();
		evtW_BpD0Dps.clear();

		evtWs_B0Dp.clear();
		evtWs_B0D0.clear();
		evtWs_B0Dps.clear();
		evtWs_BpDp.clear();
		evtWs_BpD0.clear();
		evtWs_BpDps.clear();
		evtWs_B0sDps.clear();
		evtWs_LbLc.clear();
		evtWs_B0nonC.clear();
		evtWs_BpnonC.clear();
		evtWs_B0snonC.clear();
		evtWs_LbnonC.clear();

		evtWs_B0DpD0.clear();
		evtWs_B0DpDps.clear();
		evtWs_B0D0Dps.clear();
		evtWs_BpDpD0.clear();
		evtWs_BpDpDps.clear();
		evtWs_BpD0Dps.clear();

		sem_B0Dp.clear();
		sem_B0D0.clear();
		sem_B0Dps.clear();
		sem_BpDp.clear();
		sem_BpD0.clear();
		sem_BpDps.clear();
		sem_B0sDps.clear();
		sem_LbLc.clear();
		sem_B0nonC.clear();
		sem_BpnonC.clear();
		sem_B0snonC.clear();
		sem_LbnonC.clear();

		sem_B0DpD0.clear();
		sem_B0DpDps.clear();
		sem_B0D0Dps.clear();
		sem_BpDpD0.clear();
		sem_BpDpDps.clear();
		sem_BpD0Dps.clear();

		vals_B0Dp.clear();
		vals_B0D0.clear();
		vals_B0Dps.clear();
		vals_BpDp.clear();
		vals_BpD0.clear();
		vals_BpDps.clear();
		vals_B0sDps.clear();
		vals_LbLc.clear();
		vals_B0nonC.clear();
		vals_BpnonC.clear();
		vals_B0snonC.clear();
		vals_LbnonC.clear();

		vals_B0DpD0.clear();
		vals_B0DpDps.clear();
		vals_B0D0Dps.clear();
		vals_BpDpD0.clear();
		vals_BpDpDps.clear();
		vals_BpD0Dps.clear();

		for (int j = 1; j < hpTbalb->GetNbinsX()+1; j++) {
			string pr_ind = "_" + to_string(i) + "_" + to_string(j);

			prpTbalb_B0Dp = pr2DpTbalb_B0Dp->ProfileX(("B0Dp"+pr_ind).c_str(),j,j);
			prpTbalb_B0D0 = pr2DpTbalb_B0D0->ProfileX(("B0D0"+pr_ind).c_str(),j,j);
			prpTbalb_B0Dps = pr2DpTbalb_B0Dps->ProfileX(("B0Dps"+pr_ind).c_str(),j,j);
			prpTbalb_BpDp = pr2DpTbalb_BpDp->ProfileX(("BpDp"+pr_ind).c_str(),j,j);
			prpTbalb_BpD0 = pr2DpTbalb_BpD0->ProfileX(("BpD0"+pr_ind).c_str(),j,j);
			prpTbalb_BpDps = pr2DpTbalb_BpDps->ProfileX(("BpDps"+pr_ind).c_str(),j,j);
			prpTbalb_B0sDps = pr2DpTbalb_B0sDps->ProfileX(("B0sDps"+pr_ind).c_str(),j,j);
			prpTbalb_LbLc = pr2DpTbalb_LbLc->ProfileX(("LbLc"+pr_ind).c_str(),j,j);
			prpTbalb_B0nonC = pr2DpTbalb_B0nonC->ProfileX(("B0nonC"+pr_ind).c_str(),j,j);
			prpTbalb_BpnonC = pr2DpTbalb_BpnonC->ProfileX(("BpnonC"+pr_ind).c_str(),j,j);
			prpTbalb_B0snonC = pr2DpTbalb_B0snonC->ProfileX(("B0snonC"+pr_ind).c_str(),j,j);
			prpTbalb_LbnonC = pr2DpTbalb_LbnonC->ProfileX(("LbnonC"+pr_ind).c_str(),j,j);

			prpTbalb_B0DpD0 = pr2DpTbalb_B0DpD0->ProfileX(("B0DpD0"+pr_ind).c_str(),j,j);
			prpTbalb_B0DpDps = pr2DpTbalb_B0DpDps->ProfileX(("B0DpDps"+pr_ind).c_str(),j,j);
			prpTbalb_B0D0Dps = pr2DpTbalb_B0D0Dps->ProfileX(("B0D0Dps"+pr_ind).c_str(),j,j);
			prpTbalb_BpDpD0 = pr2DpTbalb_BpDpD0->ProfileX(("BpDpD0"+pr_ind).c_str(),j,j);
			prpTbalb_BpDpDps = pr2DpTbalb_BpDpDps->ProfileX(("BpDpDps"+pr_ind).c_str(),j,j);
			prpTbalb_BpD0Dps = pr2DpTbalb_BpD0Dps->ProfileX(("BpD0Dps"+pr_ind).c_str(),j,j);

			prpTbalb_B0Dp_s_p = pr2DpTbalb_B0Dp_s_p->ProfileX(("B0Dp_s"+pr_ind).c_str(),j,j);
			prpTbalb_B0D0_s_p = pr2DpTbalb_B0D0_s_p->ProfileX(("B0D0_s"+pr_ind).c_str(),j,j);
			prpTbalb_B0Dps_s_p = pr2DpTbalb_B0Dps_s_p->ProfileX(("B0Dps_s"+pr_ind).c_str(),j,j);
			prpTbalb_BpDp_s_p = pr2DpTbalb_BpDp_s_p->ProfileX(("BpDp_s"+pr_ind).c_str(),j,j);
			prpTbalb_BpD0_s_p = pr2DpTbalb_BpD0_s_p->ProfileX(("BpD0_s"+pr_ind).c_str(),j,j);
			prpTbalb_BpDps_s_p = pr2DpTbalb_BpDps_s_p->ProfileX(("BpDps_s"+pr_ind).c_str(),j,j);
			prpTbalb_B0sDps_s_p = pr2DpTbalb_B0sDps_s_p->ProfileX(("B0sDps_s"+pr_ind).c_str(),j,j);
			prpTbalb_LbLc_s_p = pr2DpTbalb_LbLc_s_p->ProfileX(("LbLc_s"+pr_ind).c_str(),j,j);
			prpTbalb_B0nonC_s_p = pr2DpTbalb_B0nonC_s_p->ProfileX(("B0nonC_s"+pr_ind).c_str(),j,j);
			prpTbalb_BpnonC_s_p = pr2DpTbalb_BpnonC_s_p->ProfileX(("BpnonC_s"+pr_ind).c_str(),j,j);
			prpTbalb_B0snonC_s_p = pr2DpTbalb_B0snonC_s_p->ProfileX(("B0snonC_s"+pr_ind).c_str(),j,j);
			prpTbalb_LbnonC_s_p = pr2DpTbalb_LbnonC_s_p->ProfileX(("LbnonC_s"+pr_ind).c_str(),j,j);

			prpTbalb_B0DpD0_s_p = pr2DpTbalb_B0DpD0_s_p->ProfileX(("B0DpD0_s"+pr_ind).c_str(),j,j);
			prpTbalb_B0DpDps_s_p = pr2DpTbalb_B0DpDps_s_p->ProfileX(("B0DpDps_s"+pr_ind).c_str(),j,j);
			prpTbalb_B0D0Dps_s_p = pr2DpTbalb_B0D0Dps_s_p->ProfileX(("B0D0Dps_s"+pr_ind).c_str(),j,j);
			prpTbalb_BpDpD0_s_p = pr2DpTbalb_BpDpD0_s_p->ProfileX(("BpDpD0_s"+pr_ind).c_str(),j,j);
			prpTbalb_BpDpDps_s_p = pr2DpTbalb_BpDpDps_s_p->ProfileX(("BpDpDps_s"+pr_ind).c_str(),j,j);
			prpTbalb_BpD0Dps_s_p = pr2DpTbalb_BpD0Dps_s_p->ProfileX(("BpD0Dps_s"+pr_ind).c_str(),j,j);

			evtW_B0Dp.push_back(prpTbalb_B0Dp->GetBinEntries(i));
			evtW_B0D0.push_back(prpTbalb_B0D0->GetBinEntries(i));
			evtW_B0Dps.push_back(prpTbalb_B0Dps->GetBinEntries(i));
			evtW_BpDp.push_back(prpTbalb_BpDp->GetBinEntries(i));
			evtW_BpD0.push_back(prpTbalb_BpD0->GetBinEntries(i));
			evtW_BpDps.push_back(prpTbalb_BpDps->GetBinEntries(i));
			evtW_B0sDps.push_back(prpTbalb_B0sDps->GetBinEntries(i));
			evtW_LbLc.push_back(prpTbalb_LbLc->GetBinEntries(i));
			evtW_B0nonC.push_back(prpTbalb_B0nonC->GetBinEntries(i));
			evtW_BpnonC.push_back(prpTbalb_BpnonC->GetBinEntries(i));
			evtW_B0snonC.push_back(prpTbalb_B0snonC->GetBinEntries(i));
			evtW_LbnonC.push_back(prpTbalb_LbnonC->GetBinEntries(i));

			evtW_B0DpD0.push_back(prpTbalb_B0DpD0->GetBinEntries(i));
			evtW_B0DpDps.push_back(prpTbalb_B0DpDps->GetBinEntries(i));
			evtW_B0D0Dps.push_back(prpTbalb_B0D0Dps->GetBinEntries(i));
			evtW_BpDpD0.push_back(prpTbalb_BpDpD0->GetBinEntries(i));
			evtW_BpDpDps.push_back(prpTbalb_BpDpDps->GetBinEntries(i));
			evtW_BpD0Dps.push_back(prpTbalb_BpD0Dps->GetBinEntries(i));

			evtWs_B0Dp.push_back(prpTbalb_B0Dp_s_p->GetBinEntries(i));
			evtWs_B0D0.push_back(prpTbalb_B0D0_s_p->GetBinEntries(i));
			evtWs_B0Dps.push_back(prpTbalb_B0Dps_s_p->GetBinEntries(i));
			evtWs_BpDp.push_back(prpTbalb_BpDp_s_p->GetBinEntries(i));
			evtWs_BpD0.push_back(prpTbalb_BpD0_s_p->GetBinEntries(i));
			evtWs_BpDps.push_back(prpTbalb_BpDps_s_p->GetBinEntries(i));
			evtWs_B0sDps.push_back(prpTbalb_B0sDps_s_p->GetBinEntries(i));
			evtWs_LbLc.push_back(prpTbalb_LbLc_s_p->GetBinEntries(i));
			evtWs_B0nonC.push_back(prpTbalb_B0nonC_s_p->GetBinEntries(i));
			evtWs_BpnonC.push_back(prpTbalb_BpnonC_s_p->GetBinEntries(i));
			evtWs_B0snonC.push_back(prpTbalb_B0snonC_s_p->GetBinEntries(i));
			evtWs_LbnonC.push_back(prpTbalb_LbnonC_s_p->GetBinEntries(i));

			evtWs_B0DpD0.push_back(prpTbalb_B0DpD0_s_p->GetBinEntries(i));
			evtWs_B0DpDps.push_back(prpTbalb_B0DpDps_s_p->GetBinEntries(i));
			evtWs_B0D0Dps.push_back(prpTbalb_B0D0Dps_s_p->GetBinEntries(i));
			evtWs_BpDpD0.push_back(prpTbalb_BpDpD0_s_p->GetBinEntries(i));
			evtWs_BpDpDps.push_back(prpTbalb_BpDpDps_s_p->GetBinEntries(i));
			evtWs_BpD0Dps.push_back(prpTbalb_BpD0Dps_s_p->GetBinEntries(i));

			sem_B0Dp.push_back(prpTbalb_B0Dp->GetBinError(i));
			sem_B0D0.push_back(prpTbalb_B0D0->GetBinError(i));
			sem_B0Dps.push_back(prpTbalb_B0Dps->GetBinError(i));
			sem_BpDp.push_back(prpTbalb_BpDp->GetBinError(i));
			sem_BpD0.push_back(prpTbalb_BpD0->GetBinError(i));
			sem_BpDps.push_back(prpTbalb_BpDps->GetBinError(i));
			sem_B0sDps.push_back(prpTbalb_B0sDps->GetBinError(i));
			sem_LbLc.push_back(prpTbalb_LbLc->GetBinError(i));
			sem_B0nonC.push_back(prpTbalb_B0nonC->GetBinError(i));
			sem_BpnonC.push_back(prpTbalb_BpnonC->GetBinError(i));
			sem_B0snonC.push_back(prpTbalb_B0snonC->GetBinError(i));
			sem_LbnonC.push_back(prpTbalb_LbnonC->GetBinError(i));

			sem_B0DpD0.push_back(prpTbalb_B0DpD0->GetBinError(i));
			sem_B0DpDps.push_back(prpTbalb_B0DpDps->GetBinError(i));
			sem_B0D0Dps.push_back(prpTbalb_B0D0Dps->GetBinError(i));
			sem_BpDpD0.push_back(prpTbalb_BpDpD0->GetBinError(i));
			sem_BpDpDps.push_back(prpTbalb_BpDpDps->GetBinError(i));
			sem_BpD0Dps.push_back(prpTbalb_BpD0Dps->GetBinError(i));

			vals_B0Dp.push_back(prpTbalb_B0Dp->GetBinContent(i));
			vals_B0D0.push_back(prpTbalb_B0D0->GetBinContent(i));
			vals_B0Dps.push_back(prpTbalb_B0Dps->GetBinContent(i));
			vals_BpDp.push_back(prpTbalb_BpDp->GetBinContent(i));
			vals_BpD0.push_back(prpTbalb_BpD0->GetBinContent(i));
			vals_BpDps.push_back(prpTbalb_BpDps->GetBinContent(i));
			vals_B0sDps.push_back(prpTbalb_B0sDps->GetBinContent(i));
			vals_LbLc.push_back(prpTbalb_LbLc->GetBinContent(i));
			vals_B0nonC.push_back(prpTbalb_B0nonC->GetBinContent(i));
			vals_BpnonC.push_back(prpTbalb_BpnonC->GetBinContent(i));
			vals_B0snonC.push_back(prpTbalb_B0snonC->GetBinContent(i));
			vals_LbnonC.push_back(prpTbalb_LbnonC->GetBinContent(i));

			vals_B0DpD0.push_back(prpTbalb_B0DpD0->GetBinContent(i));
			vals_B0DpDps.push_back(prpTbalb_B0DpDps->GetBinContent(i));
			vals_B0D0Dps.push_back(prpTbalb_B0D0Dps->GetBinContent(i));
			vals_BpDpD0.push_back(prpTbalb_BpDpD0->GetBinContent(i));
			vals_BpDpDps.push_back(prpTbalb_BpDpDps->GetBinContent(i));
			vals_BpD0Dps.push_back(prpTbalb_BpD0Dps->GetBinContent(i));

			delete prpTbalb_B0Dp;
			delete prpTbalb_B0D0;
			delete prpTbalb_B0Dps;
			delete prpTbalb_BpDp;
			delete prpTbalb_BpD0;
			delete prpTbalb_BpDps;
			delete prpTbalb_B0sDps;
			delete prpTbalb_LbLc;
			delete prpTbalb_B0nonC;
			delete prpTbalb_BpnonC;
			delete prpTbalb_B0snonC; 
			delete prpTbalb_LbnonC;

			delete prpTbalb_B0DpD0;
			delete prpTbalb_B0DpDps;
			delete prpTbalb_B0D0Dps;
			delete prpTbalb_BpDpD0; 
			delete prpTbalb_BpDpDps;
			delete prpTbalb_BpD0Dps;

			delete prpTbalb_B0Dp_s_p;
			delete prpTbalb_B0D0_s_p;
			delete prpTbalb_B0Dps_s_p;
			delete prpTbalb_BpDp_s_p;
			delete prpTbalb_BpD0_s_p;
			delete prpTbalb_BpDps_s_p;
			delete prpTbalb_B0sDps_s_p;
			delete prpTbalb_LbLc_s_p;
			delete prpTbalb_B0nonC_s_p;
			delete prpTbalb_BpnonC_s_p;
			delete prpTbalb_B0snonC_s_p;
			delete prpTbalb_LbnonC_s_p;

			delete prpTbalb_B0DpD0_s_p;
			delete prpTbalb_B0DpDps_s_p;
			delete prpTbalb_B0D0Dps_s_p;
			delete prpTbalb_BpDpD0_s_p;
			delete prpTbalb_BpDpDps_s_p;
			delete prpTbalb_BpD0Dps_s_p;
		}

		vector<vector<double>> vec_evtW{evtW_B0Dp, evtW_B0D0, evtW_B0Dps, evtW_BpDp,
								 evtW_BpD0, evtW_BpDps, evtW_B0sDps, evtW_LbLc,
								 evtW_B0nonC, evtW_BpnonC, evtW_B0snonC, evtW_LbnonC,
								 evtW_B0DpD0,	evtW_B0DpDps,	evtW_B0D0Dps,	evtW_BpDpD0,
								 evtW_BpDpDps, evtW_BpD0Dps};

		vector<vector<double>> vec_evtWs{evtWs_B0Dp, evtWs_B0D0, evtWs_B0Dps, evtWs_BpDp,
								 evtWs_BpD0, evtWs_BpDps, evtWs_B0sDps, evtWs_LbLc,
								 evtWs_B0nonC, evtWs_BpnonC, evtWs_B0snonC, evtWs_LbnonC,
								 evtWs_B0DpD0,	evtWs_B0DpDps,	evtWs_B0D0Dps,	evtWs_BpDpD0,
								 evtWs_BpDpDps, evtWs_BpD0Dps};

		vector<vector<double>> vec_sem{sem_B0Dp, sem_B0D0, sem_B0Dps, sem_BpDp,
								 sem_BpD0, sem_BpDps, sem_B0sDps, sem_LbLc,
								 sem_B0nonC, sem_BpnonC, sem_B0snonC, sem_LbnonC,
								 sem_B0DpD0,	sem_B0DpDps,	sem_B0D0Dps,	sem_BpDpD0,
								 sem_BpDpDps, sem_BpD0Dps};

		vector<vector<double>> vec_vals{
vals_B0Dp, vals_B0D0, vals_B0Dps, vals_BpDp,
								 vals_BpD0, vals_BpDps, vals_B0sDps, vals_LbLc,
								 vals_B0nonC, vals_BpnonC, vals_B0snonC, vals_LbnonC,
								 vals_B0DpD0,	vals_B0DpDps,	vals_B0D0Dps,	vals_BpDpD0,
								 vals_BpDpDps, vals_BpD0Dps};

		//set small number so that we dont divide by zero 
		evtWtot_B0Dp = 1e-10;
		evtWtot_B0D0 = 1e-10;
		evtWtot_B0Dps = 1e-10;
		evtWtot_BpDp = 1e-10;
		evtWtot_BpD0 = 1e-10;
		evtWtot_BpDps = 1e-10;
		evtWtot_B0sDps = 1e-10;
		evtWtot_LbLc = 1e-10;
		evtWtot_B0nonC = 1e-10;
		evtWtot_BpnonC = 1e-10;
		evtWtot_B0snonC = 1e-10;
		evtWtot_LbnonC = 1e-10;

		evtWtot_B0DpD0 = 1e-10;
		evtWtot_B0DpDps = 1e-10;
		evtWtot_B0D0Dps = 1e-10;
		evtWtot_BpDpD0 = 1e-10;
		evtWtot_BpDpDps = 1e-10;
		evtWtot_BpD0Dps = 1e-10;

		evtWtots_B0Dp = 1e-10;
		evtWtots_B0D0 = 1e-10;
		evtWtots_B0Dps = 1e-10;
		evtWtots_BpDp = 1e-10;
		evtWtots_BpD0 = 1e-10;
		evtWtots_BpDps = 1e-10;
		evtWtots_B0sDps = 1e-10;
		evtWtots_LbLc = 1e-10;
		evtWtots_B0nonC = 1e-10;
		evtWtots_BpnonC = 1e-10;
		evtWtots_B0snonC = 1e-10;
		evtWtots_LbnonC = 1e-10;

		evtWtots_B0DpD0 = 1e-10;
		evtWtots_B0DpDps = 1e-10;
		evtWtots_B0D0Dps = 1e-10;
		evtWtots_BpDpD0 = 1e-10;
		evtWtots_BpDpDps = 1e-10;
		evtWtots_BpD0Dps = 1e-10;

		for (double x : evtW_B0Dp) evtWtot_B0Dp += x;	
		for (double x : evtW_B0D0) evtWtot_B0D0 += x;
		for (double x : evtW_B0Dps) evtWtot_B0Dps += x;
		for (double x : evtW_BpDp) evtWtot_BpDp += x;
		for (double x : evtW_BpD0) evtWtot_BpD0 += x;
		for (double x : evtW_BpDps) evtWtot_BpDps += x;
		for (double x : evtW_B0sDps) evtWtot_B0sDps += x;
		for (double x : evtW_LbLc) evtWtot_LbLc += x;
		for (double x : evtW_B0nonC) evtWtot_B0nonC += x;
		for (double x : evtW_BpnonC) evtWtot_BpnonC += x;
		for (double x : evtW_B0snonC) evtWtot_B0snonC += x;
		for (double x : evtW_LbnonC) evtWtot_LbnonC += x;

		for (double x : evtW_B0DpD0) evtWtot_B0DpD0 += x;
		for (double x : evtW_B0DpDps) evtWtot_B0DpDps += x;
		for (double x : evtW_B0D0Dps) evtWtot_B0D0Dps += x;
		for (double x : evtW_BpDpD0) evtWtot_BpDpD0 += x;
		for (double x : evtW_BpDpDps) evtWtot_BpDpDps += x;
		for (double x : evtW_BpD0Dps) evtWtot_BpD0Dps += x;

		for (double x : evtWs_B0Dp) evtWtots_B0Dp += x;
		for (double x : evtWs_B0D0) evtWtots_B0D0 += x;
		for (double x : evtWs_B0Dps) evtWtots_B0Dps += x;
		for (double x : evtWs_BpDp) evtWtots_BpDp += x;
		for (double x : evtWs_BpD0) evtWtots_BpD0 += x;
		for (double x : evtWs_BpDps) evtWtots_BpDps += x;
		for (double x : evtWs_B0sDps) evtWtots_B0sDps += x;
		for (double x : evtWs_LbLc) evtWtots_LbLc += x;
		for (double x : evtWs_B0nonC) evtWtots_B0nonC += x;
		for (double x : evtWs_BpnonC) evtWtots_BpnonC += x;
		for (double x : evtWs_B0snonC) evtWtots_B0snonC += x;
		for (double x : evtWs_LbnonC) evtWtots_LbnonC += x;

		for (double x : evtWs_B0DpD0) evtWtots_B0DpD0 += x;
		for (double x : evtWs_B0DpDps) evtWtots_B0DpDps += x;
		for (double x : evtWs_B0D0Dps) evtWtots_B0D0Dps += x;
		for (double x : evtWs_BpDpD0) evtWtots_BpDpD0 += x;
		for (double x : evtWs_BpDpDps) evtWtots_BpDpDps += x;
		for (double x : evtWs_BpD0Dps) evtWtots_BpD0Dps += x;
		
		evtWtot = evtWtot_B0Dp + evtWtot_B0D0 + evtWtot_B0Dps + evtWtot_BpDp + evtWtot_BpD0 +
						 	evtWtot_BpDps + evtWtot_B0sDps + evtWtot_LbLc + evtWtot_B0nonC +evtWtot_BpnonC +
							evtWtot_B0snonC + evtWtot_LbnonC + evtWtot_B0DpD0 + evtWtot_B0DpDps +
							evtWtot_B0D0Dps + evtWtot_BpDpD0 + evtWtot_BpDpDps + evtWtot_BpD0Dps;

		evtWtots = evtWtots_B0Dp + evtWtots_B0D0 + evtWtots_B0Dps + evtWtots_BpDp + evtWtots_BpD0 +
						 	 evtWtots_BpDps + evtWtots_B0sDps + evtWtots_LbLc + evtWtots_B0nonC +evtWtots_BpnonC +
							 evtWtots_B0snonC + evtWtots_LbnonC + evtWtots_B0DpD0 + evtWtots_B0DpDps +
							 evtWtots_B0D0Dps + evtWtots_BpDpD0 + evtWtots_BpDpDps + evtWtots_BpD0Dps;

		//loop over diff scale factors 
		errorb = 0;
		for (int j = 0; j < hpTbalb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hpTbalb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}

		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb_p->GetBinContent(i) == 0) diffb_p->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb_p->SetBinError(i,errorb);
  }

//############### minus variation ####################
//Loop over the TProfile bins
  for (int i = 1; i < hpTbalb->GetNbinsX()+1; i++){
		evtW_B0Dp.clear();
		evtW_B0D0.clear();
		evtW_B0Dps.clear();
		evtW_BpDp.clear();
		evtW_BpD0.clear();
		evtW_BpDps.clear();
		evtW_B0sDps.clear();
		evtW_LbLc.clear();
		evtW_B0nonC.clear();
		evtW_BpnonC.clear();
		evtW_B0snonC.clear();
		evtW_LbnonC.clear();

		evtW_B0DpD0.clear();
		evtW_B0DpDps.clear();
		evtW_B0D0Dps.clear();
		evtW_BpDpD0.clear();
		evtW_BpDpDps.clear();
		evtW_BpD0Dps.clear();

		evtWs_B0Dp.clear();
		evtWs_B0D0.clear();
		evtWs_B0Dps.clear();
		evtWs_BpDp.clear();
		evtWs_BpD0.clear();
		evtWs_BpDps.clear();
		evtWs_B0sDps.clear();
		evtWs_LbLc.clear();
		evtWs_B0nonC.clear();
		evtWs_BpnonC.clear();
		evtWs_B0snonC.clear();
		evtWs_LbnonC.clear();

		evtWs_B0DpD0.clear();
		evtWs_B0DpDps.clear();
		evtWs_B0D0Dps.clear();
		evtWs_BpDpD0.clear();
		evtWs_BpDpDps.clear();
		evtWs_BpD0Dps.clear();

		sem_B0Dp.clear();
		sem_B0D0.clear();
		sem_B0Dps.clear();
		sem_BpDp.clear();
		sem_BpD0.clear();
		sem_BpDps.clear();
		sem_B0sDps.clear();
		sem_LbLc.clear();
		sem_B0nonC.clear();
		sem_BpnonC.clear();
		sem_B0snonC.clear();
		sem_LbnonC.clear();

		sem_B0DpD0.clear();
		sem_B0DpDps.clear();
		sem_B0D0Dps.clear();
		sem_BpDpD0.clear();
		sem_BpDpDps.clear();
		sem_BpD0Dps.clear();

		vals_B0Dp.clear();
		vals_B0D0.clear();
		vals_B0Dps.clear();
		vals_BpDp.clear();
		vals_BpD0.clear();
		vals_BpDps.clear();
		vals_B0sDps.clear();
		vals_LbLc.clear();
		vals_B0nonC.clear();
		vals_BpnonC.clear();
		vals_B0snonC.clear();
		vals_LbnonC.clear();

		vals_B0DpD0.clear();
		vals_B0DpDps.clear();
		vals_B0D0Dps.clear();
		vals_BpDpD0.clear();
		vals_BpDpDps.clear();
		vals_BpD0Dps.clear();

		for (int j = 1; j < hpTbalb->GetNbinsX()+1; j++) {
			string pr_ind = "_" + to_string(i) + "_" + to_string(j);

			prpTbalb_B0Dp = pr2DpTbalb_B0Dp->ProfileX(("B0Dp"+pr_ind).c_str(),j,j);
			prpTbalb_B0D0 = pr2DpTbalb_B0D0->ProfileX(("B0D0"+pr_ind).c_str(),j,j);
			prpTbalb_B0Dps = pr2DpTbalb_B0Dps->ProfileX(("B0Dps"+pr_ind).c_str(),j,j);
			prpTbalb_BpDp = pr2DpTbalb_BpDp->ProfileX(("BpDp"+pr_ind).c_str(),j,j);
			prpTbalb_BpD0 = pr2DpTbalb_BpD0->ProfileX(("BpD0"+pr_ind).c_str(),j,j);
			prpTbalb_BpDps = pr2DpTbalb_BpDps->ProfileX(("BpDps"+pr_ind).c_str(),j,j);
			prpTbalb_B0sDps = pr2DpTbalb_B0sDps->ProfileX(("B0sDps"+pr_ind).c_str(),j,j);
			prpTbalb_LbLc = pr2DpTbalb_LbLc->ProfileX(("LbLc"+pr_ind).c_str(),j,j);
			prpTbalb_B0nonC = pr2DpTbalb_B0nonC->ProfileX(("B0nonC"+pr_ind).c_str(),j,j);
			prpTbalb_BpnonC = pr2DpTbalb_BpnonC->ProfileX(("BpnonC"+pr_ind).c_str(),j,j);
			prpTbalb_B0snonC = pr2DpTbalb_B0snonC->ProfileX(("B0snonC"+pr_ind).c_str(),j,j);
			prpTbalb_LbnonC = pr2DpTbalb_LbnonC->ProfileX(("LbnonC"+pr_ind).c_str(),j,j);

			prpTbalb_B0DpD0 = pr2DpTbalb_B0DpD0->ProfileX(("B0DpD0"+pr_ind).c_str(),j,j);
			prpTbalb_B0DpDps = pr2DpTbalb_B0DpDps->ProfileX(("B0DpDps"+pr_ind).c_str(),j,j);
			prpTbalb_B0D0Dps = pr2DpTbalb_B0D0Dps->ProfileX(("B0D0Dps"+pr_ind).c_str(),j,j);
			prpTbalb_BpDpD0 = pr2DpTbalb_BpDpD0->ProfileX(("BpDpD0"+pr_ind).c_str(),j,j);
			prpTbalb_BpDpDps = pr2DpTbalb_BpDpDps->ProfileX(("BpDpDps"+pr_ind).c_str(),j,j);
			prpTbalb_BpD0Dps = pr2DpTbalb_BpD0Dps->ProfileX(("BpD0Dps"+pr_ind).c_str(),j,j);

			prpTbalb_B0Dp_s_m = pr2DpTbalb_B0Dp_s_m->ProfileX(("B0Dp_s"+pr_ind).c_str(),j,j);
			prpTbalb_B0D0_s_m = pr2DpTbalb_B0D0_s_m->ProfileX(("B0D0_s"+pr_ind).c_str(),j,j);
			prpTbalb_B0Dps_s_m = pr2DpTbalb_B0Dps_s_m->ProfileX(("B0Dps_s"+pr_ind).c_str(),j,j);
			prpTbalb_BpDp_s_m = pr2DpTbalb_BpDp_s_m->ProfileX(("BpDp_s"+pr_ind).c_str(),j,j);
			prpTbalb_BpD0_s_m = pr2DpTbalb_BpD0_s_m->ProfileX(("BpD0_s"+pr_ind).c_str(),j,j);
			prpTbalb_BpDps_s_m = pr2DpTbalb_BpDps_s_m->ProfileX(("BpDps_s"+pr_ind).c_str(),j,j);
			prpTbalb_B0sDps_s_m = pr2DpTbalb_B0sDps_s_m->ProfileX(("B0sDps_s"+pr_ind).c_str(),j,j);
			prpTbalb_LbLc_s_m = pr2DpTbalb_LbLc_s_m->ProfileX(("LbLc_s"+pr_ind).c_str(),j,j);
			prpTbalb_B0nonC_s_m = pr2DpTbalb_B0nonC_s_m->ProfileX(("B0nonC_s"+pr_ind).c_str(),j,j);
			prpTbalb_BpnonC_s_m = pr2DpTbalb_BpnonC_s_m->ProfileX(("BpnonC_s"+pr_ind).c_str(),j,j);
			prpTbalb_B0snonC_s_m = pr2DpTbalb_B0snonC_s_m->ProfileX(("B0snonC_s"+pr_ind).c_str(),j,j);
			prpTbalb_LbnonC_s_m = pr2DpTbalb_LbnonC_s_m->ProfileX(("LbnonC_s"+pr_ind).c_str(),j,j);

			prpTbalb_B0DpD0_s_m = pr2DpTbalb_B0DpD0_s_m->ProfileX(("B0DpD0_s"+pr_ind).c_str(),j,j);
			prpTbalb_B0DpDps_s_m = pr2DpTbalb_B0DpDps_s_m->ProfileX(("B0DpDps_s"+pr_ind).c_str(),j,j);
			prpTbalb_B0D0Dps_s_m = pr2DpTbalb_B0D0Dps_s_m->ProfileX(("B0D0Dps_s"+pr_ind).c_str(),j,j);
			prpTbalb_BpDpD0_s_m = pr2DpTbalb_BpDpD0_s_m->ProfileX(("BpDpD0_s"+pr_ind).c_str(),j,j);
			prpTbalb_BpDpDps_s_m = pr2DpTbalb_BpDpDps_s_m->ProfileX(("BpDpDps_s"+pr_ind).c_str(),j,j);
			prpTbalb_BpD0Dps_s_m = pr2DpTbalb_BpD0Dps_s_m->ProfileX(("BpD0Dps_s"+pr_ind).c_str(),j,j);

			evtW_B0Dp.push_back(prpTbalb_B0Dp->GetBinEntries(i));
			evtW_B0D0.push_back(prpTbalb_B0D0->GetBinEntries(i));
			evtW_B0Dps.push_back(prpTbalb_B0Dps->GetBinEntries(i));
			evtW_BpDp.push_back(prpTbalb_BpDp->GetBinEntries(i));
			evtW_BpD0.push_back(prpTbalb_BpD0->GetBinEntries(i));
			evtW_BpDps.push_back(prpTbalb_BpDps->GetBinEntries(i));
			evtW_B0sDps.push_back(prpTbalb_B0sDps->GetBinEntries(i));
			evtW_LbLc.push_back(prpTbalb_LbLc->GetBinEntries(i));
			evtW_B0nonC.push_back(prpTbalb_B0nonC->GetBinEntries(i));
			evtW_BpnonC.push_back(prpTbalb_BpnonC->GetBinEntries(i));
			evtW_B0snonC.push_back(prpTbalb_B0snonC->GetBinEntries(i));
			evtW_LbnonC.push_back(prpTbalb_LbnonC->GetBinEntries(i));

			evtW_B0DpD0.push_back(prpTbalb_B0DpD0->GetBinEntries(i));
			evtW_B0DpDps.push_back(prpTbalb_B0DpDps->GetBinEntries(i));
			evtW_B0D0Dps.push_back(prpTbalb_B0D0Dps->GetBinEntries(i));
			evtW_BpDpD0.push_back(prpTbalb_BpDpD0->GetBinEntries(i));
			evtW_BpDpDps.push_back(prpTbalb_BpDpDps->GetBinEntries(i));
			evtW_BpD0Dps.push_back(prpTbalb_BpD0Dps->GetBinEntries(i));

			evtWs_B0Dp.push_back(prpTbalb_B0Dp_s_m->GetBinEntries(i));
			evtWs_B0D0.push_back(prpTbalb_B0D0_s_m->GetBinEntries(i));
			evtWs_B0Dps.push_back(prpTbalb_B0Dps_s_m->GetBinEntries(i));
			evtWs_BpDp.push_back(prpTbalb_BpDp_s_m->GetBinEntries(i));
			evtWs_BpD0.push_back(prpTbalb_BpD0_s_m->GetBinEntries(i));
			evtWs_BpDps.push_back(prpTbalb_BpDps_s_m->GetBinEntries(i));
			evtWs_B0sDps.push_back(prpTbalb_B0sDps_s_m->GetBinEntries(i));
			evtWs_LbLc.push_back(prpTbalb_LbLc_s_m->GetBinEntries(i));
			evtWs_B0nonC.push_back(prpTbalb_B0nonC_s_m->GetBinEntries(i));
			evtWs_BpnonC.push_back(prpTbalb_BpnonC_s_m->GetBinEntries(i));
			evtWs_B0snonC.push_back(prpTbalb_B0snonC_s_m->GetBinEntries(i));
			evtWs_LbnonC.push_back(prpTbalb_LbnonC_s_m->GetBinEntries(i));

			evtWs_B0DpD0.push_back(prpTbalb_B0DpD0_s_m->GetBinEntries(i));
			evtWs_B0DpDps.push_back(prpTbalb_B0DpDps_s_m->GetBinEntries(i));
			evtWs_B0D0Dps.push_back(prpTbalb_B0D0Dps_s_m->GetBinEntries(i));
			evtWs_BpDpD0.push_back(prpTbalb_BpDpD0_s_m->GetBinEntries(i));
			evtWs_BpDpDps.push_back(prpTbalb_BpDpDps_s_m->GetBinEntries(i));
			evtWs_BpD0Dps.push_back(prpTbalb_BpD0Dps_s_m->GetBinEntries(i));

			sem_B0Dp.push_back(prpTbalb_B0Dp->GetBinError(i));
			sem_B0D0.push_back(prpTbalb_B0D0->GetBinError(i));
			sem_B0Dps.push_back(prpTbalb_B0Dps->GetBinError(i));
			sem_BpDp.push_back(prpTbalb_BpDp->GetBinError(i));
			sem_BpD0.push_back(prpTbalb_BpD0->GetBinError(i));
			sem_BpDps.push_back(prpTbalb_BpDps->GetBinError(i));
			sem_B0sDps.push_back(prpTbalb_B0sDps->GetBinError(i));
			sem_LbLc.push_back(prpTbalb_LbLc->GetBinError(i));
			sem_B0nonC.push_back(prpTbalb_B0nonC->GetBinError(i));
			sem_BpnonC.push_back(prpTbalb_BpnonC->GetBinError(i));
			sem_B0snonC.push_back(prpTbalb_B0snonC->GetBinError(i));
			sem_LbnonC.push_back(prpTbalb_LbnonC->GetBinError(i));

			sem_B0DpD0.push_back(prpTbalb_B0DpD0->GetBinError(i));
			sem_B0DpDps.push_back(prpTbalb_B0DpDps->GetBinError(i));
			sem_B0D0Dps.push_back(prpTbalb_B0D0Dps->GetBinError(i));
			sem_BpDpD0.push_back(prpTbalb_BpDpD0->GetBinError(i));
			sem_BpDpDps.push_back(prpTbalb_BpDpDps->GetBinError(i));
			sem_BpD0Dps.push_back(prpTbalb_BpD0Dps->GetBinError(i));

			vals_B0Dp.push_back(prpTbalb_B0Dp->GetBinContent(i));
			vals_B0D0.push_back(prpTbalb_B0D0->GetBinContent(i));
			vals_B0Dps.push_back(prpTbalb_B0Dps->GetBinContent(i));
			vals_BpDp.push_back(prpTbalb_BpDp->GetBinContent(i));
			vals_BpD0.push_back(prpTbalb_BpD0->GetBinContent(i));
			vals_BpDps.push_back(prpTbalb_BpDps->GetBinContent(i));
			vals_B0sDps.push_back(prpTbalb_B0sDps->GetBinContent(i));
			vals_LbLc.push_back(prpTbalb_LbLc->GetBinContent(i));
			vals_B0nonC.push_back(prpTbalb_B0nonC->GetBinContent(i));
			vals_BpnonC.push_back(prpTbalb_BpnonC->GetBinContent(i));
			vals_B0snonC.push_back(prpTbalb_B0snonC->GetBinContent(i));
			vals_LbnonC.push_back(prpTbalb_LbnonC->GetBinContent(i));

			vals_B0DpD0.push_back(prpTbalb_B0DpD0->GetBinContent(i));
			vals_B0DpDps.push_back(prpTbalb_B0DpDps->GetBinContent(i));
			vals_B0D0Dps.push_back(prpTbalb_B0D0Dps->GetBinContent(i));
			vals_BpDpD0.push_back(prpTbalb_BpDpD0->GetBinContent(i));
			vals_BpDpDps.push_back(prpTbalb_BpDpDps->GetBinContent(i));
			vals_BpD0Dps.push_back(prpTbalb_BpD0Dps->GetBinContent(i));

			delete prpTbalb_B0Dp;
			delete prpTbalb_B0D0;
			delete prpTbalb_B0Dps;
			delete prpTbalb_BpDp;
			delete prpTbalb_BpD0;
			delete prpTbalb_BpDps;
			delete prpTbalb_B0sDps;
			delete prpTbalb_LbLc;
			delete prpTbalb_B0nonC;
			delete prpTbalb_BpnonC;
			delete prpTbalb_B0snonC; 
			delete prpTbalb_LbnonC;

			delete prpTbalb_B0DpD0;
			delete prpTbalb_B0DpDps;
			delete prpTbalb_B0D0Dps;
			delete prpTbalb_BpDpD0; 
			delete prpTbalb_BpDpDps;
			delete prpTbalb_BpD0Dps;

			delete prpTbalb_B0Dp_s_m;
			delete prpTbalb_B0D0_s_m;
			delete prpTbalb_B0Dps_s_m;
			delete prpTbalb_BpDp_s_m;
			delete prpTbalb_BpD0_s_m;
			delete prpTbalb_BpDps_s_m;
			delete prpTbalb_B0sDps_s_m;
			delete prpTbalb_LbLc_s_m;
			delete prpTbalb_B0nonC_s_m;
			delete prpTbalb_BpnonC_s_m;
			delete prpTbalb_B0snonC_s_m;
			delete prpTbalb_LbnonC_s_m;

			delete prpTbalb_B0DpD0_s_m;
			delete prpTbalb_B0DpDps_s_m;
			delete prpTbalb_B0D0Dps_s_m;
			delete prpTbalb_BpDpD0_s_m;
			delete prpTbalb_BpDpDps_s_m;
			delete prpTbalb_BpD0Dps_s_m;
		}

		vector<vector<double>> vec_evtW{evtW_B0Dp, evtW_B0D0, evtW_B0Dps, evtW_BpDp,
								 evtW_BpD0, evtW_BpDps, evtW_B0sDps, evtW_LbLc,
								 evtW_B0nonC, evtW_BpnonC, evtW_B0snonC, evtW_LbnonC,
								 evtW_B0DpD0,	evtW_B0DpDps,	evtW_B0D0Dps,	evtW_BpDpD0,
								 evtW_BpDpDps, evtW_BpD0Dps};

		vector<vector<double>> vec_evtWs{evtWs_B0Dp, evtWs_B0D0, evtWs_B0Dps, evtWs_BpDp,
								 evtWs_BpD0, evtWs_BpDps, evtWs_B0sDps, evtWs_LbLc,
								 evtWs_B0nonC, evtWs_BpnonC, evtWs_B0snonC, evtWs_LbnonC,
								 evtWs_B0DpD0,	evtWs_B0DpDps,	evtWs_B0D0Dps,	evtWs_BpDpD0,
								 evtWs_BpDpDps, evtWs_BpD0Dps};

		vector<vector<double>> vec_sem{sem_B0Dp, sem_B0D0, sem_B0Dps, sem_BpDp,
								 sem_BpD0, sem_BpDps, sem_B0sDps, sem_LbLc,
								 sem_B0nonC, sem_BpnonC, sem_B0snonC, sem_LbnonC,
								 sem_B0DpD0,	sem_B0DpDps,	sem_B0D0Dps,	sem_BpDpD0,
								 sem_BpDpDps, sem_BpD0Dps};

		vector<vector<double>> vec_vals{
vals_B0Dp, vals_B0D0, vals_B0Dps, vals_BpDp,
								 vals_BpD0, vals_BpDps, vals_B0sDps, vals_LbLc,
								 vals_B0nonC, vals_BpnonC, vals_B0snonC, vals_LbnonC,
								 vals_B0DpD0,	vals_B0DpDps,	vals_B0D0Dps,	vals_BpDpD0,
								 vals_BpDpDps, vals_BpD0Dps};

		//set small number so that we dont divide by zero 
		evtWtot_B0Dp = 1e-10;
		evtWtot_B0D0 = 1e-10;
		evtWtot_B0Dps = 1e-10;
		evtWtot_BpDp = 1e-10;
		evtWtot_BpD0 = 1e-10;
		evtWtot_BpDps = 1e-10;
		evtWtot_B0sDps = 1e-10;
		evtWtot_LbLc = 1e-10;
		evtWtot_B0nonC = 1e-10;
		evtWtot_BpnonC = 1e-10;
		evtWtot_B0snonC = 1e-10;
		evtWtot_LbnonC = 1e-10;

		evtWtot_B0DpD0 = 1e-10;
		evtWtot_B0DpDps = 1e-10;
		evtWtot_B0D0Dps = 1e-10;
		evtWtot_BpDpD0 = 1e-10;
		evtWtot_BpDpDps = 1e-10;
		evtWtot_BpD0Dps = 1e-10;

		evtWtots_B0Dp = 1e-10;
		evtWtots_B0D0 = 1e-10;
		evtWtots_B0Dps = 1e-10;
		evtWtots_BpDp = 1e-10;
		evtWtots_BpD0 = 1e-10;
		evtWtots_BpDps = 1e-10;
		evtWtots_B0sDps = 1e-10;
		evtWtots_LbLc = 1e-10;
		evtWtots_B0nonC = 1e-10;
		evtWtots_BpnonC = 1e-10;
		evtWtots_B0snonC = 1e-10;
		evtWtots_LbnonC = 1e-10;

		evtWtots_B0DpD0 = 1e-10;
		evtWtots_B0DpDps = 1e-10;
		evtWtots_B0D0Dps = 1e-10;
		evtWtots_BpDpD0 = 1e-10;
		evtWtots_BpDpDps = 1e-10;
		evtWtots_BpD0Dps = 1e-10;

		for (double x : evtW_B0Dp) evtWtot_B0Dp += x;	
		for (double x : evtW_B0D0) evtWtot_B0D0 += x;
		for (double x : evtW_B0Dps) evtWtot_B0Dps += x;
		for (double x : evtW_BpDp) evtWtot_BpDp += x;
		for (double x : evtW_BpD0) evtWtot_BpD0 += x;
		for (double x : evtW_BpDps) evtWtot_BpDps += x;
		for (double x : evtW_B0sDps) evtWtot_B0sDps += x;
		for (double x : evtW_LbLc) evtWtot_LbLc += x;
		for (double x : evtW_B0nonC) evtWtot_B0nonC += x;
		for (double x : evtW_BpnonC) evtWtot_BpnonC += x;
		for (double x : evtW_B0snonC) evtWtot_B0snonC += x;
		for (double x : evtW_LbnonC) evtWtot_LbnonC += x;

		for (double x : evtW_B0DpD0) evtWtot_B0DpD0 += x;
		for (double x : evtW_B0DpDps) evtWtot_B0DpDps += x;
		for (double x : evtW_B0D0Dps) evtWtot_B0D0Dps += x;
		for (double x : evtW_BpDpD0) evtWtot_BpDpD0 += x;
		for (double x : evtW_BpDpDps) evtWtot_BpDpDps += x;
		for (double x : evtW_BpD0Dps) evtWtot_BpD0Dps += x;

		for (double x : evtWs_B0Dp) evtWtots_B0Dp += x;
		for (double x : evtWs_B0D0) evtWtots_B0D0 += x;
		for (double x : evtWs_B0Dps) evtWtots_B0Dps += x;
		for (double x : evtWs_BpDp) evtWtots_BpDp += x;
		for (double x : evtWs_BpD0) evtWtots_BpD0 += x;
		for (double x : evtWs_BpDps) evtWtots_BpDps += x;
		for (double x : evtWs_B0sDps) evtWtots_B0sDps += x;
		for (double x : evtWs_LbLc) evtWtots_LbLc += x;
		for (double x : evtWs_B0nonC) evtWtots_B0nonC += x;
		for (double x : evtWs_BpnonC) evtWtots_BpnonC += x;
		for (double x : evtWs_B0snonC) evtWtots_B0snonC += x;
		for (double x : evtWs_LbnonC) evtWtots_LbnonC += x;

		for (double x : evtWs_B0DpD0) evtWtots_B0DpD0 += x;
		for (double x : evtWs_B0DpDps) evtWtots_B0DpDps += x;
		for (double x : evtWs_B0D0Dps) evtWtots_B0D0Dps += x;
		for (double x : evtWs_BpDpD0) evtWtots_BpDpD0 += x;
		for (double x : evtWs_BpDpDps) evtWtots_BpDpDps += x;
		for (double x : evtWs_BpD0Dps) evtWtots_BpD0Dps += x;
		
		evtWtot = evtWtot_B0Dp + evtWtot_B0D0 + evtWtot_B0Dps + evtWtot_BpDp + evtWtot_BpD0 +
						 	evtWtot_BpDps + evtWtot_B0sDps + evtWtot_LbLc + evtWtot_B0nonC +evtWtot_BpnonC +
							evtWtot_B0snonC + evtWtot_LbnonC + evtWtot_B0DpD0 + evtWtot_B0DpDps +
							evtWtot_B0D0Dps + evtWtot_BpDpD0 + evtWtot_BpDpDps + evtWtot_BpD0Dps;

		evtWtots = evtWtots_B0Dp + evtWtots_B0D0 + evtWtots_B0Dps + evtWtots_BpDp + evtWtots_BpD0 +
						 	 evtWtots_BpDps + evtWtots_B0sDps + evtWtots_LbLc + evtWtots_B0nonC +evtWtots_BpnonC +
							 evtWtots_B0snonC + evtWtots_LbnonC + evtWtots_B0DpD0 + evtWtots_B0DpDps +
							 evtWtots_B0D0Dps + evtWtots_BpDpD0 + evtWtots_BpDpDps + evtWtots_BpD0Dps;

		//loop over diff scale factors 
		errorb = 0;
		for (int j = 0; j < hpTbalb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hpTbalb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}

		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb_m->GetBinContent(i) == 0) diffb_m->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb_m->SetBinError(i,errorb);
  }  

	//Scale to the percentages
  diffb->Scale(100.0);
  diffb_p->Scale(100.0);
  diffb_m->Scale(100.0);

	pTbalb_map["diffb"] = diffb;
	pTbalb_map["diffb_p"] = diffb_p;
	pTbalb_map["diffb_m"] = diffb_m;

	return pTbalb_map;
}

map<string, TH1D*> Rjet_BtoCBr() {
	map <string, TH1D*> Rjetb_map;

  TFile* f_normal = TFile::Open(OpenName.c_str());
  TFile* f_scaled = TFile::Open(OpenName_BtoCBr.c_str());
  TFile* f_scaled_p = TFile::Open(OpenName_BtoCBr_p.c_str());
  TFile* f_scaled_m = TFile::Open(OpenName_BtoCBr_m.c_str());

	TProfile *prRjetb;
	TProfile *prRjetb_s;
	TProfile *prRjetb_s_p;
	TProfile *prRjetb_s_m;

  //Read the normal sample
  f_normal->GetObject("prRjetb",   prRjetb);

  //Read the weighted sample
  f_scaled->GetObject("prRjetb",   prRjetb_s);
  f_scaled_p->GetObject("prRjetb",   prRjetb_s_p);
  f_scaled_m->GetObject("prRjetb",   prRjetb_s_m);

	//2D Tprofiles for the errorb calculations
	TProfile2D* pr2DRjetb;

	TProfile2D* pr2DRjetb_B0Dp;
	TProfile2D* pr2DRjetb_B0D0;
	TProfile2D* pr2DRjetb_B0Dps;
	TProfile2D* pr2DRjetb_BpDp;
	TProfile2D* pr2DRjetb_BpD0;
	TProfile2D* pr2DRjetb_BpDps;
	TProfile2D* pr2DRjetb_B0sDps;
	TProfile2D* pr2DRjetb_LbLc;
	TProfile2D* pr2DRjetb_B0nonC;
	TProfile2D* pr2DRjetb_BpnonC;
	TProfile2D* pr2DRjetb_B0snonC;
	TProfile2D* pr2DRjetb_LbnonC;

	TProfile2D* pr2DRjetb_B0DpD0;
	TProfile2D* pr2DRjetb_B0DpDps;
	TProfile2D* pr2DRjetb_B0D0Dps;
	TProfile2D* pr2DRjetb_BpDpD0;
	TProfile2D* pr2DRjetb_BpDpDps;
	TProfile2D* pr2DRjetb_BpD0Dps;

  //The sample with the additional weighting (*_s)
	TProfile2D* pr2DRjetb_s;

	TProfile2D* pr2DRjetb_B0Dp_s;
	TProfile2D* pr2DRjetb_B0D0_s;
	TProfile2D* pr2DRjetb_B0Dps_s;
	TProfile2D* pr2DRjetb_BpDp_s;
	TProfile2D* pr2DRjetb_BpD0_s;
	TProfile2D* pr2DRjetb_BpDps_s;
	TProfile2D* pr2DRjetb_B0sDps_s;
	TProfile2D* pr2DRjetb_LbLc_s;
	TProfile2D* pr2DRjetb_B0nonC_s;
	TProfile2D* pr2DRjetb_BpnonC_s;
	TProfile2D* pr2DRjetb_B0snonC_s;
	TProfile2D* pr2DRjetb_LbnonC_s;

	TProfile2D* pr2DRjetb_B0DpD0_s;
	TProfile2D* pr2DRjetb_B0DpDps_s;
	TProfile2D* pr2DRjetb_B0D0Dps_s;
	TProfile2D* pr2DRjetb_BpDpD0_s;
	TProfile2D* pr2DRjetb_BpDpDps_s;
	TProfile2D* pr2DRjetb_BpD0Dps_s;

	TProfile2D* pr2DRjetb_s_p;

	TProfile2D* pr2DRjetb_B0Dp_s_p;
	TProfile2D* pr2DRjetb_B0D0_s_p;
	TProfile2D* pr2DRjetb_B0Dps_s_p;
	TProfile2D* pr2DRjetb_BpDp_s_p;
	TProfile2D* pr2DRjetb_BpD0_s_p;
	TProfile2D* pr2DRjetb_BpDps_s_p;
	TProfile2D* pr2DRjetb_B0sDps_s_p;
	TProfile2D* pr2DRjetb_LbLc_s_p;
	TProfile2D* pr2DRjetb_B0nonC_s_p;
	TProfile2D* pr2DRjetb_BpnonC_s_p;
	TProfile2D* pr2DRjetb_B0snonC_s_p;
	TProfile2D* pr2DRjetb_LbnonC_s_p;

	TProfile2D* pr2DRjetb_B0DpD0_s_p;
	TProfile2D* pr2DRjetb_B0DpDps_s_p;
	TProfile2D* pr2DRjetb_B0D0Dps_s_p;
	TProfile2D* pr2DRjetb_BpDpD0_s_p;
	TProfile2D* pr2DRjetb_BpDpDps_s_p;
	TProfile2D* pr2DRjetb_BpD0Dps_s_p;

	TProfile2D* pr2DRjetb_s_m;

	TProfile2D* pr2DRjetb_B0Dp_s_m;
	TProfile2D* pr2DRjetb_B0D0_s_m;
	TProfile2D* pr2DRjetb_B0Dps_s_m;
	TProfile2D* pr2DRjetb_BpDp_s_m;
	TProfile2D* pr2DRjetb_BpD0_s_m;
	TProfile2D* pr2DRjetb_BpDps_s_m;
	TProfile2D* pr2DRjetb_B0sDps_s_m;
	TProfile2D* pr2DRjetb_LbLc_s_m;
	TProfile2D* pr2DRjetb_B0nonC_s_m;
	TProfile2D* pr2DRjetb_BpnonC_s_m;
	TProfile2D* pr2DRjetb_B0snonC_s_m;
	TProfile2D* pr2DRjetb_LbnonC_s_m;

	TProfile2D* pr2DRjetb_B0DpD0_s_m;
	TProfile2D* pr2DRjetb_B0DpDps_s_m;
	TProfile2D* pr2DRjetb_B0D0Dps_s_m;
	TProfile2D* pr2DRjetb_BpDpD0_s_m;
	TProfile2D* pr2DRjetb_BpDpDps_s_m;
	TProfile2D* pr2DRjetb_BpD0Dps_s_m;

	//Tprofiles for the errorb calculations
	TProfile* prRjetb_B0Dp;
	TProfile* prRjetb_B0D0;
	TProfile* prRjetb_B0Dps;
	TProfile* prRjetb_BpDp;
	TProfile* prRjetb_BpD0;
	TProfile* prRjetb_BpDps;
	TProfile* prRjetb_B0sDps;
	TProfile* prRjetb_LbLc;
	TProfile* prRjetb_B0nonC;
	TProfile* prRjetb_BpnonC;
	TProfile* prRjetb_B0snonC;
	TProfile* prRjetb_LbnonC;

	TProfile* prRjetb_B0DpD0;
	TProfile* prRjetb_B0DpDps;
	TProfile* prRjetb_B0D0Dps;
	TProfile* prRjetb_BpDpD0;
	TProfile* prRjetb_BpDpDps;
	TProfile* prRjetb_BpD0Dps;

  //The sample with the additional weighting (*_s)
	TProfile* prRjetb_B0Dp_s;
	TProfile* prRjetb_B0D0_s;
	TProfile* prRjetb_B0Dps_s;
	TProfile* prRjetb_BpDp_s;
	TProfile* prRjetb_BpD0_s;
	TProfile* prRjetb_BpDps_s;
	TProfile* prRjetb_B0sDps_s;
	TProfile* prRjetb_LbLc_s;
	TProfile* prRjetb_B0nonC_s;
	TProfile* prRjetb_BpnonC_s;
	TProfile* prRjetb_B0snonC_s;
	TProfile* prRjetb_LbnonC_s;

	TProfile* prRjetb_B0DpD0_s;
	TProfile* prRjetb_B0DpDps_s;
	TProfile* prRjetb_B0D0Dps_s;
	TProfile* prRjetb_BpDpD0_s;
	TProfile* prRjetb_BpDpDps_s;
	TProfile* prRjetb_BpD0Dps_s;

	TProfile* prRjetb_B0Dp_s_p;
	TProfile* prRjetb_B0D0_s_p;
	TProfile* prRjetb_B0Dps_s_p;
	TProfile* prRjetb_BpDp_s_p;
	TProfile* prRjetb_BpD0_s_p;
	TProfile* prRjetb_BpDps_s_p;
	TProfile* prRjetb_B0sDps_s_p;
	TProfile* prRjetb_LbLc_s_p;
	TProfile* prRjetb_B0nonC_s_p;
	TProfile* prRjetb_BpnonC_s_p;
	TProfile* prRjetb_B0snonC_s_p;
	TProfile* prRjetb_LbnonC_s_p;

	TProfile* prRjetb_B0DpD0_s_p;
	TProfile* prRjetb_B0DpDps_s_p;
	TProfile* prRjetb_B0D0Dps_s_p;
	TProfile* prRjetb_BpDpD0_s_p;
	TProfile* prRjetb_BpDpDps_s_p;
	TProfile* prRjetb_BpD0Dps_s_p;

	TProfile* prRjetb_B0Dp_s_m;
	TProfile* prRjetb_B0D0_s_m;
	TProfile* prRjetb_B0Dps_s_m;
	TProfile* prRjetb_BpDp_s_m;
	TProfile* prRjetb_BpD0_s_m;
	TProfile* prRjetb_BpDps_s_m;
	TProfile* prRjetb_B0sDps_s_m;
	TProfile* prRjetb_LbLc_s_m;
	TProfile* prRjetb_B0nonC_s_m;
	TProfile* prRjetb_BpnonC_s_m;
	TProfile* prRjetb_B0snonC_s_m;
	TProfile* prRjetb_LbnonC_s_m;

	TProfile* prRjetb_B0DpD0_s_m;
	TProfile* prRjetb_B0DpDps_s_m;
	TProfile* prRjetb_B0D0Dps_s_m;
	TProfile* prRjetb_BpDpD0_s_m;
	TProfile* prRjetb_BpDpDps_s_m;
	TProfile* prRjetb_BpD0Dps_s_m;

  //Read the normal sample
	f_normal->GetObject("pr2DRjetb",pr2DRjetb);

	f_normal->GetObject("pr2DRjetb_B0Dp",pr2DRjetb_B0Dp);
	f_normal->GetObject("pr2DRjetb_B0D0",pr2DRjetb_B0D0);
	f_normal->GetObject("pr2DRjetb_B0Dps",pr2DRjetb_B0Dps);
	f_normal->GetObject("pr2DRjetb_BpDp",pr2DRjetb_BpDp);
	f_normal->GetObject("pr2DRjetb_BpD0",pr2DRjetb_BpD0);
	f_normal->GetObject("pr2DRjetb_BpDps",pr2DRjetb_BpDps);
	f_normal->GetObject("pr2DRjetb_B0sDps",pr2DRjetb_B0sDps);
	f_normal->GetObject("pr2DRjetb_LbLc",pr2DRjetb_LbLc);
	f_normal->GetObject("pr2DRjetb_B0nonC",pr2DRjetb_B0nonC);
	f_normal->GetObject("pr2DRjetb_BpnonC",pr2DRjetb_BpnonC);
	f_normal->GetObject("pr2DRjetb_B0snonC",pr2DRjetb_B0snonC);
	f_normal->GetObject("pr2DRjetb_LbnonC",pr2DRjetb_LbnonC);

	f_normal->GetObject("pr2DRjetb_B0DpD0",pr2DRjetb_B0DpD0);
	f_normal->GetObject("pr2DRjetb_B0DpDps",pr2DRjetb_B0DpDps);
	f_normal->GetObject("pr2DRjetb_B0D0Dps",pr2DRjetb_B0D0Dps);
	f_normal->GetObject("pr2DRjetb_BpDpD0",pr2DRjetb_BpDpD0);
	f_normal->GetObject("pr2DRjetb_BpDpDps",pr2DRjetb_BpDpDps);
	f_normal->GetObject("pr2DRjetb_BpD0Dps",pr2DRjetb_BpD0Dps);

  //Read the weighted sample
	f_scaled->GetObject("pr2DRjetb",pr2DRjetb_s);

	f_scaled->GetObject("pr2DRjetb_B0Dp",pr2DRjetb_B0Dp_s);
	f_scaled->GetObject("pr2DRjetb_B0D0",pr2DRjetb_B0D0_s);
	f_scaled->GetObject("pr2DRjetb_B0Dps",pr2DRjetb_B0Dps_s);
	f_scaled->GetObject("pr2DRjetb_BpDp",pr2DRjetb_BpDp_s);
	f_scaled->GetObject("pr2DRjetb_BpD0",pr2DRjetb_BpD0_s);
	f_scaled->GetObject("pr2DRjetb_BpDps",pr2DRjetb_BpDps_s);
	f_scaled->GetObject("pr2DRjetb_B0sDps",pr2DRjetb_B0sDps_s);
	f_scaled->GetObject("pr2DRjetb_LbLc",pr2DRjetb_LbLc_s);
	f_scaled->GetObject("pr2DRjetb_B0nonC",pr2DRjetb_B0nonC_s);
	f_scaled->GetObject("pr2DRjetb_BpnonC",pr2DRjetb_BpnonC_s);
	f_scaled->GetObject("pr2DRjetb_B0snonC",pr2DRjetb_B0snonC_s);
	f_scaled->GetObject("pr2DRjetb_LbnonC",pr2DRjetb_LbnonC_s);

	f_scaled->GetObject("pr2DRjetb_B0DpD0",pr2DRjetb_B0DpD0_s);
	f_scaled->GetObject("pr2DRjetb_B0DpDps",pr2DRjetb_B0DpDps_s);
	f_scaled->GetObject("pr2DRjetb_B0D0Dps",pr2DRjetb_B0D0Dps_s);
	f_scaled->GetObject("pr2DRjetb_BpDpD0",pr2DRjetb_BpDpD0_s);
	f_scaled->GetObject("pr2DRjetb_BpDpDps",pr2DRjetb_BpDpDps_s);
	f_scaled->GetObject("pr2DRjetb_BpD0Dps",pr2DRjetb_BpD0Dps_s);

	f_scaled_p->GetObject("pr2DRjetb",pr2DRjetb_s_p);

	f_scaled_p->GetObject("pr2DRjetb_B0Dp",pr2DRjetb_B0Dp_s_p);
	f_scaled_p->GetObject("pr2DRjetb_B0D0",pr2DRjetb_B0D0_s_p);
	f_scaled_p->GetObject("pr2DRjetb_B0Dps",pr2DRjetb_B0Dps_s_p);
	f_scaled_p->GetObject("pr2DRjetb_BpDp",pr2DRjetb_BpDp_s_p);
	f_scaled_p->GetObject("pr2DRjetb_BpD0",pr2DRjetb_BpD0_s_p);
	f_scaled_p->GetObject("pr2DRjetb_BpDps",pr2DRjetb_BpDps_s_p);
	f_scaled_p->GetObject("pr2DRjetb_B0sDps",pr2DRjetb_B0sDps_s_p);
	f_scaled_p->GetObject("pr2DRjetb_LbLc",pr2DRjetb_LbLc_s_p);
	f_scaled_p->GetObject("pr2DRjetb_B0nonC",pr2DRjetb_B0nonC_s_p);
	f_scaled_p->GetObject("pr2DRjetb_BpnonC",pr2DRjetb_BpnonC_s_p);
	f_scaled_p->GetObject("pr2DRjetb_B0snonC",pr2DRjetb_B0snonC_s_p);
	f_scaled_p->GetObject("pr2DRjetb_LbnonC",pr2DRjetb_LbnonC_s_p);

	f_scaled_p->GetObject("pr2DRjetb_B0DpD0",pr2DRjetb_B0DpD0_s_p);
	f_scaled_p->GetObject("pr2DRjetb_B0DpDps",pr2DRjetb_B0DpDps_s_p);
	f_scaled_p->GetObject("pr2DRjetb_B0D0Dps",pr2DRjetb_B0D0Dps_s_p);
	f_scaled_p->GetObject("pr2DRjetb_BpDpD0",pr2DRjetb_BpDpD0_s_p);
	f_scaled_p->GetObject("pr2DRjetb_BpDpDps",pr2DRjetb_BpDpDps_s_p);
	f_scaled_p->GetObject("pr2DRjetb_BpD0Dps",pr2DRjetb_BpD0Dps_s_p);

	f_scaled_m->GetObject("pr2DRjetb",pr2DRjetb_s_m);

	f_scaled_m->GetObject("pr2DRjetb_B0Dp",pr2DRjetb_B0Dp_s_m);
	f_scaled_m->GetObject("pr2DRjetb_B0D0",pr2DRjetb_B0D0_s_m);
	f_scaled_m->GetObject("pr2DRjetb_B0Dps",pr2DRjetb_B0Dps_s_m);
	f_scaled_m->GetObject("pr2DRjetb_BpDp",pr2DRjetb_BpDp_s_m);
	f_scaled_m->GetObject("pr2DRjetb_BpD0",pr2DRjetb_BpD0_s_m);
	f_scaled_m->GetObject("pr2DRjetb_BpDps",pr2DRjetb_BpDps_s_m);
	f_scaled_m->GetObject("pr2DRjetb_B0sDps",pr2DRjetb_B0sDps_s_m);
	f_scaled_m->GetObject("pr2DRjetb_LbLc",pr2DRjetb_LbLc_s_m);
	f_scaled_m->GetObject("pr2DRjetb_B0nonC",pr2DRjetb_B0nonC_s_m);
	f_scaled_m->GetObject("pr2DRjetb_BpnonC",pr2DRjetb_BpnonC_s_m);
	f_scaled_m->GetObject("pr2DRjetb_B0snonC",pr2DRjetb_B0snonC_s_m);
	f_scaled_m->GetObject("pr2DRjetb_LbnonC",pr2DRjetb_LbnonC_s_m);

	f_scaled_m->GetObject("pr2DRjetb_B0DpD0",pr2DRjetb_B0DpD0_s_m);
	f_scaled_m->GetObject("pr2DRjetb_B0DpDps",pr2DRjetb_B0DpDps_s_m);
	f_scaled_m->GetObject("pr2DRjetb_B0D0Dps",pr2DRjetb_B0D0Dps_s_m);
	f_scaled_m->GetObject("pr2DRjetb_BpDpD0",pr2DRjetb_BpDpD0_s_m);
	f_scaled_m->GetObject("pr2DRjetb_BpDpDps",pr2DRjetb_BpDpDps_s_m);
	f_scaled_m->GetObject("pr2DRjetb_BpD0Dps",pr2DRjetb_BpD0Dps_s_m);

	//calculate the difference in response between normal and the rescaled sample 
	//the second ProjectionX has new name as argument, otherwise would have same name as the previous
	//histo and causes memory leak 
	TH1D* hRjetb     = prRjetb   ->ProjectionX();
	TH1D* hRjetb_s   = prRjetb_s ->ProjectionX("pr2DRjetb_s");
	TH1D* hRjetb_s_p = prRjetb_s_p ->ProjectionX("pr2DRjetb_s_p");
	TH1D* hRjetb_s_m = prRjetb_s_m ->ProjectionX("pr2DRjetb_s_m");

	TH1D* diffb 		= (TH1D*) hRjetb_s->Clone();
	TH1D* diffb_p 	= (TH1D*) hRjetb_s_p->Clone();
	TH1D* diffb_m 	= (TH1D*) hRjetb_s_m->Clone();

	diffb->Add(hRjetb,-1);
	diffb_p->Add(hRjetb,-1);
	diffb_m->Add(hRjetb,-1);

	//evtW_* = event weight, sem_* = standard errorb of the mean,   
	double errorb, evtWtot, evtWtots;

	double evtWtot_B0Dp;
	double evtWtot_B0D0;
	double evtWtot_B0Dps;
	double evtWtot_BpDp;
	double evtWtot_BpD0;
	double evtWtot_BpDps;
	double evtWtot_B0sDps;
	double evtWtot_LbLc;
	double evtWtot_B0nonC;
	double evtWtot_BpnonC;
	double evtWtot_B0snonC;
	double evtWtot_LbnonC;

	double evtWtot_B0DpD0;
	double evtWtot_B0DpDps;
	double evtWtot_B0D0Dps;
	double evtWtot_BpDpD0;
	double evtWtot_BpDpDps;
	double evtWtot_BpD0Dps;

	double evtWtots_B0Dp;
	double evtWtots_B0D0;
	double evtWtots_B0Dps;
	double evtWtots_BpDp;
	double evtWtots_BpD0;
	double evtWtots_BpDps;
	double evtWtots_B0sDps;
	double evtWtots_LbLc;
	double evtWtots_B0nonC;
	double evtWtots_BpnonC;
	double evtWtots_B0snonC;
	double evtWtots_LbnonC;

	double evtWtots_B0DpD0;
	double evtWtots_B0DpDps;
	double evtWtots_B0D0Dps;
	double evtWtots_BpDpD0;
	double evtWtots_BpDpDps;
	double evtWtots_BpD0Dps;

	vector<double> evtW_B0Dp;
	vector<double> evtW_B0D0;
	vector<double> evtW_B0Dps;
	vector<double> evtW_BpDp;
	vector<double> evtW_BpD0;
	vector<double> evtW_BpDps;
	vector<double> evtW_B0sDps;
	vector<double> evtW_LbLc;
	vector<double> evtW_B0nonC;
	vector<double> evtW_BpnonC;
	vector<double> evtW_B0snonC;
	vector<double> evtW_LbnonC;

	vector<double> evtW_B0DpD0;
	vector<double> evtW_B0DpDps;
	vector<double> evtW_B0D0Dps;
	vector<double> evtW_BpDpD0;
	vector<double> evtW_BpDpDps;
	vector<double> evtW_BpD0Dps;

	vector<double> evtWs_B0Dp;
	vector<double> evtWs_B0D0;
	vector<double> evtWs_B0Dps;
	vector<double> evtWs_BpDp;
	vector<double> evtWs_BpD0;
	vector<double> evtWs_BpDps;
	vector<double> evtWs_B0sDps;
	vector<double> evtWs_LbLc;
	vector<double> evtWs_B0nonC;
	vector<double> evtWs_BpnonC;
	vector<double> evtWs_B0snonC;
	vector<double> evtWs_LbnonC;

	vector<double> evtWs_B0DpD0;
	vector<double> evtWs_B0DpDps;
	vector<double> evtWs_B0D0Dps;
	vector<double> evtWs_BpDpD0;
	vector<double> evtWs_BpDpDps;
	vector<double> evtWs_BpD0Dps;

	vector<double> sem_B0Dp;
	vector<double> sem_B0D0;
	vector<double> sem_B0Dps;
	vector<double> sem_BpDp;
	vector<double> sem_BpD0;
	vector<double> sem_BpDps;
	vector<double> sem_B0sDps;
	vector<double> sem_LbLc;
	vector<double> sem_B0nonC;
	vector<double> sem_BpnonC;
	vector<double> sem_B0snonC;
	vector<double> sem_LbnonC;

	vector<double> sem_B0DpD0;
	vector<double> sem_B0DpDps;
	vector<double> sem_B0D0Dps;
	vector<double> sem_BpDpD0;
	vector<double> sem_BpDpDps;
	vector<double> sem_BpD0Dps;

	vector<double> vals_B0Dp;
	vector<double> vals_B0D0;
	vector<double> vals_B0Dps;
	vector<double> vals_BpDp;
	vector<double> vals_BpD0;
	vector<double> vals_BpDps;
	vector<double> vals_B0sDps;
	vector<double> vals_LbLc;
	vector<double> vals_B0nonC;
	vector<double> vals_BpnonC;
	vector<double> vals_B0snonC;
	vector<double> vals_LbnonC;

	vector<double> vals_B0DpD0;
	vector<double> vals_B0DpDps;
	vector<double> vals_B0D0Dps;
	vector<double> vals_BpDpD0;
	vector<double> vals_BpDpDps;
	vector<double> vals_BpD0Dps;

	vector<double> vec_B0Dp_stat, vec_B0D0_stat, vec_B0Dps_stat, vec_BpDp_stat,
								 vec_BpD0_stat, vec_BpDps_stat, vec_B0sDps_stat, vec_LbLc_stat,
								 vec_B0nonC_stat, vec_BpnonC_stat, vec_B0snonC_stat, vec_LbnonC_stat,
								 vec_B0DpD0_stat,	vec_B0DpDps_stat,	vec_B0D0Dps_stat,	vec_BpDpD0_stat,
								 vec_BpDpDps_stat, vec_BpD0Dps_stat;

	ReadFromFile(vec_B0Dp_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/stat_err/B0Dp_sf_stat.txt");
	ReadFromFile(vec_B0D0_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/stat_err/B0D0_sf_stat.txt");
	ReadFromFile(vec_B0Dps_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/stat_err/B0Dps_sf_stat.txt");
	ReadFromFile(vec_BpDp_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/stat_err/BpDp_sf_stat.txt");
	ReadFromFile(vec_BpD0_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/stat_err/BpD0_sf_stat.txt");
	ReadFromFile(vec_BpDps_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/stat_err/BpDps_sf_stat.txt");
	ReadFromFile(vec_B0sDps_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/stat_err/B0sDps_sf_stat.txt");
	ReadFromFile(vec_LbLc_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/stat_err/LbLc_sf_stat.txt");
	ReadFromFile(vec_B0nonC_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/stat_err/B0nonC_sf_stat.txt");
	ReadFromFile(vec_BpnonC_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/stat_err/BpnonC_sf_stat.txt");
	ReadFromFile(vec_B0snonC_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/stat_err/B0snonC_sf_stat.txt");
	ReadFromFile(vec_LbnonC_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/stat_err/LbnonC_sf_stat.txt");

	//calculating the stat error of the categories with two scaling factors 
	vector<double> vec_B0Dp_sf, vec_B0D0_sf, vec_B0Dps_sf, vec_BpDp_sf,
								 vec_BpD0_sf, vec_BpDps_sf, vec_B0sDps_sf, vec_LbLc_sf,
								 vec_B0nonC_sf, vec_BpnonC_sf, vec_B0snonC_sf, vec_LbnonC_sf;

	ReadFromFile(vec_B0Dp_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/normal/B0Dp_sf.txt");
	ReadFromFile(vec_B0D0_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/normal/B0D0_sf.txt");
	ReadFromFile(vec_B0Dps_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/normal/B0Dps_sf.txt");
	ReadFromFile(vec_BpDp_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/normal/BpDp_sf.txt");
	ReadFromFile(vec_BpD0_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/normal/BpD0_sf.txt");
	ReadFromFile(vec_BpDps_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/normal/BpDps_sf.txt");
	ReadFromFile(vec_B0sDps_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/normal/B0sDps_sf.txt");
	ReadFromFile(vec_LbLc_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/normal/LbLc_sf.txt");
	ReadFromFile(vec_B0nonC_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/normal/B0nonC_sf.txt");
	ReadFromFile(vec_BpnonC_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/normal/BpnonC_sf.txt");
	ReadFromFile(vec_B0snonC_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/normal/B0snonC_sf.txt");
	ReadFromFile(vec_LbnonC_sf,"/home/anpirtti/ultimate_generator/final_rescaling/BtoCBr/calc_scale_factors/scale_factors/BtoCBr/normal/LbnonC_sf.txt");

	for (int i = 0; i < vec_B0Dp_sf.size(); i++) {
		vec_B0DpD0_stat.push_back(sqrt(pow(vec_B0D0_sf[i]*vec_B0Dp_stat[i],2) + pow(vec_B0Dp_sf[i]*vec_B0D0_stat[i],2)));
		vec_B0DpDps_stat.push_back(sqrt(pow(vec_B0Dps_sf[i]*vec_B0Dp_stat[i],2) + pow(vec_B0Dp_sf[i]*vec_B0Dps_stat[i],2)));
		vec_B0D0Dps_stat.push_back(sqrt(pow(vec_B0Dps_sf[i]*vec_B0D0_stat[i],2) + pow(vec_B0D0_sf[i]*vec_B0Dps_stat[i],2)));

		vec_BpDpD0_stat.push_back(sqrt(pow(vec_BpD0_sf[i]*vec_BpDp_stat[i],2) + pow(vec_BpDp_sf[i]*vec_BpD0_stat[i],2)));
		vec_BpDpDps_stat.push_back(sqrt(pow(vec_BpDps_sf[i]*vec_BpDp_stat[i],2) + pow(vec_BpDp_sf[i]*vec_BpDps_stat[i],2)));
		vec_BpD0Dps_stat.push_back(sqrt(pow(vec_BpDps_sf[i]*vec_BpD0_stat[i],2) + pow(vec_BpD0_sf[i]*vec_BpDps_stat[i],2)));
	}

	vector<vector<double>> vec_sf_sem{vec_B0Dp_stat, vec_B0D0_stat, vec_B0Dps_stat, vec_BpDp_stat,
								 vec_BpD0_stat, vec_BpDps_stat, vec_B0sDps_stat, vec_LbLc_stat,
								 vec_B0nonC_stat, vec_BpnonC_stat, vec_B0snonC_stat, vec_LbnonC_stat,
								 vec_B0DpD0_stat,	vec_B0DpDps_stat,	vec_B0D0Dps_stat,	vec_BpDpD0_stat,
								 vec_BpDpDps_stat, vec_BpD0Dps_stat};

	//removing the under- and overflow bin values
	for (int i = 0; i < vec_sf_sem.size(); i++) {
		vec_sf_sem[i].erase(vec_sf_sem[i].begin() + 0);
		vec_sf_sem[i].erase(vec_sf_sem[i].begin() + vec_sf_sem[i].size() - 1);
	}

  //Loop over the TProfile bins
  for (int i = 1; i < hRjetb->GetNbinsX()+1; i++){
		evtW_B0Dp.clear();
		evtW_B0D0.clear();
		evtW_B0Dps.clear();
		evtW_BpDp.clear();
		evtW_BpD0.clear();
		evtW_BpDps.clear();
		evtW_B0sDps.clear();
		evtW_LbLc.clear();
		evtW_B0nonC.clear();
		evtW_BpnonC.clear();
		evtW_B0snonC.clear();
		evtW_LbnonC.clear();

		evtW_B0DpD0.clear();
		evtW_B0DpDps.clear();
		evtW_B0D0Dps.clear();
		evtW_BpDpD0.clear();
		evtW_BpDpDps.clear();
		evtW_BpD0Dps.clear();

		evtWs_B0Dp.clear();
		evtWs_B0D0.clear();
		evtWs_B0Dps.clear();
		evtWs_BpDp.clear();
		evtWs_BpD0.clear();
		evtWs_BpDps.clear();
		evtWs_B0sDps.clear();
		evtWs_LbLc.clear();
		evtWs_B0nonC.clear();
		evtWs_BpnonC.clear();
		evtWs_B0snonC.clear();
		evtWs_LbnonC.clear();

		evtWs_B0DpD0.clear();
		evtWs_B0DpDps.clear();
		evtWs_B0D0Dps.clear();
		evtWs_BpDpD0.clear();
		evtWs_BpDpDps.clear();
		evtWs_BpD0Dps.clear();

		sem_B0Dp.clear();
		sem_B0D0.clear();
		sem_B0Dps.clear();
		sem_BpDp.clear();
		sem_BpD0.clear();
		sem_BpDps.clear();
		sem_B0sDps.clear();
		sem_LbLc.clear();
		sem_B0nonC.clear();
		sem_BpnonC.clear();
		sem_B0snonC.clear();
		sem_LbnonC.clear();

		sem_B0DpD0.clear();
		sem_B0DpDps.clear();
		sem_B0D0Dps.clear();
		sem_BpDpD0.clear();
		sem_BpDpDps.clear();
		sem_BpD0Dps.clear();

		vals_B0Dp.clear();
		vals_B0D0.clear();
		vals_B0Dps.clear();
		vals_BpDp.clear();
		vals_BpD0.clear();
		vals_BpDps.clear();
		vals_B0sDps.clear();
		vals_LbLc.clear();
		vals_B0nonC.clear();
		vals_BpnonC.clear();
		vals_B0snonC.clear();
		vals_LbnonC.clear();

		vals_B0DpD0.clear();
		vals_B0DpDps.clear();
		vals_B0D0Dps.clear();
		vals_BpDpD0.clear();
		vals_BpDpDps.clear();
		vals_BpD0Dps.clear();

		for (int j = 1; j < hRjetb->GetNbinsX()+1; j++) {
			string pr_ind = "_" + to_string(i) + "_" + to_string(j);

			prRjetb_B0Dp = pr2DRjetb_B0Dp->ProfileX(("B0Dp"+pr_ind).c_str(),j,j);
			prRjetb_B0D0 = pr2DRjetb_B0D0->ProfileX(("B0D0"+pr_ind).c_str(),j,j);
			prRjetb_B0Dps = pr2DRjetb_B0Dps->ProfileX(("B0Dps"+pr_ind).c_str(),j,j);
			prRjetb_BpDp = pr2DRjetb_BpDp->ProfileX(("BpDp"+pr_ind).c_str(),j,j);
			prRjetb_BpD0 = pr2DRjetb_BpD0->ProfileX(("BpD0"+pr_ind).c_str(),j,j);
			prRjetb_BpDps = pr2DRjetb_BpDps->ProfileX(("BpDps"+pr_ind).c_str(),j,j);
			prRjetb_B0sDps = pr2DRjetb_B0sDps->ProfileX(("B0sDps"+pr_ind).c_str(),j,j);
			prRjetb_LbLc = pr2DRjetb_LbLc->ProfileX(("LbLc"+pr_ind).c_str(),j,j);
			prRjetb_B0nonC = pr2DRjetb_B0nonC->ProfileX(("B0nonC"+pr_ind).c_str(),j,j);
			prRjetb_BpnonC = pr2DRjetb_BpnonC->ProfileX(("BpnonC"+pr_ind).c_str(),j,j);
			prRjetb_B0snonC = pr2DRjetb_B0snonC->ProfileX(("B0snonC"+pr_ind).c_str(),j,j);
			prRjetb_LbnonC = pr2DRjetb_LbnonC->ProfileX(("LbnonC"+pr_ind).c_str(),j,j);

			prRjetb_B0DpD0 = pr2DRjetb_B0DpD0->ProfileX(("B0DpD0"+pr_ind).c_str(),j,j);
			prRjetb_B0DpDps = pr2DRjetb_B0DpDps->ProfileX(("B0DpDps"+pr_ind).c_str(),j,j);
			prRjetb_B0D0Dps = pr2DRjetb_B0D0Dps->ProfileX(("B0D0Dps"+pr_ind).c_str(),j,j);
			prRjetb_BpDpD0 = pr2DRjetb_BpDpD0->ProfileX(("BpDpD0"+pr_ind).c_str(),j,j);
			prRjetb_BpDpDps = pr2DRjetb_BpDpDps->ProfileX(("BpDpDps"+pr_ind).c_str(),j,j);
			prRjetb_BpD0Dps = pr2DRjetb_BpD0Dps->ProfileX(("BpD0Dps"+pr_ind).c_str(),j,j);

			prRjetb_B0Dp_s = pr2DRjetb_B0Dp_s->ProfileX(("B0Dp_s"+pr_ind).c_str(),j,j);
			prRjetb_B0D0_s = pr2DRjetb_B0D0_s->ProfileX(("B0D0_s"+pr_ind).c_str(),j,j);
			prRjetb_B0Dps_s = pr2DRjetb_B0Dps_s->ProfileX(("B0Dps_s"+pr_ind).c_str(),j,j);
			prRjetb_BpDp_s = pr2DRjetb_BpDp_s->ProfileX(("BpDp_s"+pr_ind).c_str(),j,j);
			prRjetb_BpD0_s = pr2DRjetb_BpD0_s->ProfileX(("BpD0_s"+pr_ind).c_str(),j,j);
			prRjetb_BpDps_s = pr2DRjetb_BpDps_s->ProfileX(("BpDps_s"+pr_ind).c_str(),j,j);
			prRjetb_B0sDps_s = pr2DRjetb_B0sDps_s->ProfileX(("B0sDps_s"+pr_ind).c_str(),j,j);
			prRjetb_LbLc_s = pr2DRjetb_LbLc_s->ProfileX(("LbLc_s"+pr_ind).c_str(),j,j);
			prRjetb_B0nonC_s = pr2DRjetb_B0nonC_s->ProfileX(("B0nonC_s"+pr_ind).c_str(),j,j);
			prRjetb_BpnonC_s = pr2DRjetb_BpnonC_s->ProfileX(("BpnonC_s"+pr_ind).c_str(),j,j);
			prRjetb_B0snonC_s = pr2DRjetb_B0snonC_s->ProfileX(("B0snonC_s"+pr_ind).c_str(),j,j);
			prRjetb_LbnonC_s = pr2DRjetb_LbnonC_s->ProfileX(("LbnonC_s"+pr_ind).c_str(),j,j);

			prRjetb_B0DpD0_s = pr2DRjetb_B0DpD0_s->ProfileX(("B0DpD0_s"+pr_ind).c_str(),j,j);
			prRjetb_B0DpDps_s = pr2DRjetb_B0DpDps_s->ProfileX(("B0DpDps_s"+pr_ind).c_str(),j,j);
			prRjetb_B0D0Dps_s = pr2DRjetb_B0D0Dps_s->ProfileX(("B0D0Dps_s"+pr_ind).c_str(),j,j);
			prRjetb_BpDpD0_s = pr2DRjetb_BpDpD0_s->ProfileX(("BpDpD0_s"+pr_ind).c_str(),j,j);
			prRjetb_BpDpDps_s = pr2DRjetb_BpDpDps_s->ProfileX(("BpDpDps_s"+pr_ind).c_str(),j,j);
			prRjetb_BpD0Dps_s = pr2DRjetb_BpD0Dps_s->ProfileX(("BpD0Dps_s"+pr_ind).c_str(),j,j);

			evtW_B0Dp.push_back(prRjetb_B0Dp->GetBinEntries(i));
			evtW_B0D0.push_back(prRjetb_B0D0->GetBinEntries(i));
			evtW_B0Dps.push_back(prRjetb_B0Dps->GetBinEntries(i));
			evtW_BpDp.push_back(prRjetb_BpDp->GetBinEntries(i));
			evtW_BpD0.push_back(prRjetb_BpD0->GetBinEntries(i));
			evtW_BpDps.push_back(prRjetb_BpDps->GetBinEntries(i));
			evtW_B0sDps.push_back(prRjetb_B0sDps->GetBinEntries(i));
			evtW_LbLc.push_back(prRjetb_LbLc->GetBinEntries(i));
			evtW_B0nonC.push_back(prRjetb_B0nonC->GetBinEntries(i));
			evtW_BpnonC.push_back(prRjetb_BpnonC->GetBinEntries(i));
			evtW_B0snonC.push_back(prRjetb_B0snonC->GetBinEntries(i));
			evtW_LbnonC.push_back(prRjetb_LbnonC->GetBinEntries(i));

			evtW_B0DpD0.push_back(prRjetb_B0DpD0->GetBinEntries(i));
			evtW_B0DpDps.push_back(prRjetb_B0DpDps->GetBinEntries(i));
			evtW_B0D0Dps.push_back(prRjetb_B0D0Dps->GetBinEntries(i));
			evtW_BpDpD0.push_back(prRjetb_BpDpD0->GetBinEntries(i));
			evtW_BpDpDps.push_back(prRjetb_BpDpDps->GetBinEntries(i));
			evtW_BpD0Dps.push_back(prRjetb_BpD0Dps->GetBinEntries(i));

			evtWs_B0Dp.push_back(prRjetb_B0Dp_s->GetBinEntries(i));
			evtWs_B0D0.push_back(prRjetb_B0D0_s->GetBinEntries(i));
			evtWs_B0Dps.push_back(prRjetb_B0Dps_s->GetBinEntries(i));
			evtWs_BpDp.push_back(prRjetb_BpDp_s->GetBinEntries(i));
			evtWs_BpD0.push_back(prRjetb_BpD0_s->GetBinEntries(i));
			evtWs_BpDps.push_back(prRjetb_BpDps_s->GetBinEntries(i));
			evtWs_B0sDps.push_back(prRjetb_B0sDps_s->GetBinEntries(i));
			evtWs_LbLc.push_back(prRjetb_LbLc_s->GetBinEntries(i));
			evtWs_B0nonC.push_back(prRjetb_B0nonC_s->GetBinEntries(i));
			evtWs_BpnonC.push_back(prRjetb_BpnonC_s->GetBinEntries(i));
			evtWs_B0snonC.push_back(prRjetb_B0snonC_s->GetBinEntries(i));
			evtWs_LbnonC.push_back(prRjetb_LbnonC_s->GetBinEntries(i));

			evtWs_B0DpD0.push_back(prRjetb_B0DpD0_s->GetBinEntries(i));
			evtWs_B0DpDps.push_back(prRjetb_B0DpDps_s->GetBinEntries(i));
			evtWs_B0D0Dps.push_back(prRjetb_B0D0Dps_s->GetBinEntries(i));
			evtWs_BpDpD0.push_back(prRjetb_BpDpD0_s->GetBinEntries(i));
			evtWs_BpDpDps.push_back(prRjetb_BpDpDps_s->GetBinEntries(i));
			evtWs_BpD0Dps.push_back(prRjetb_BpD0Dps_s->GetBinEntries(i));

			sem_B0Dp.push_back(prRjetb_B0Dp->GetBinError(i));
			sem_B0D0.push_back(prRjetb_B0D0->GetBinError(i));
			sem_B0Dps.push_back(prRjetb_B0Dps->GetBinError(i));
			sem_BpDp.push_back(prRjetb_BpDp->GetBinError(i));
			sem_BpD0.push_back(prRjetb_BpD0->GetBinError(i));
			sem_BpDps.push_back(prRjetb_BpDps->GetBinError(i));
			sem_B0sDps.push_back(prRjetb_B0sDps->GetBinError(i));
			sem_LbLc.push_back(prRjetb_LbLc->GetBinError(i));
			sem_B0nonC.push_back(prRjetb_B0nonC->GetBinError(i));
			sem_BpnonC.push_back(prRjetb_BpnonC->GetBinError(i));
			sem_B0snonC.push_back(prRjetb_B0snonC->GetBinError(i));
			sem_LbnonC.push_back(prRjetb_LbnonC->GetBinError(i));

			sem_B0DpD0.push_back(prRjetb_B0DpD0->GetBinError(i));
			sem_B0DpDps.push_back(prRjetb_B0DpDps->GetBinError(i));
			sem_B0D0Dps.push_back(prRjetb_B0D0Dps->GetBinError(i));
			sem_BpDpD0.push_back(prRjetb_BpDpD0->GetBinError(i));
			sem_BpDpDps.push_back(prRjetb_BpDpDps->GetBinError(i));
			sem_BpD0Dps.push_back(prRjetb_BpD0Dps->GetBinError(i));

			vals_B0Dp.push_back(prRjetb_B0Dp->GetBinContent(i));
			vals_B0D0.push_back(prRjetb_B0D0->GetBinContent(i));
			vals_B0Dps.push_back(prRjetb_B0Dps->GetBinContent(i));
			vals_BpDp.push_back(prRjetb_BpDp->GetBinContent(i));
			vals_BpD0.push_back(prRjetb_BpD0->GetBinContent(i));
			vals_BpDps.push_back(prRjetb_BpDps->GetBinContent(i));
			vals_B0sDps.push_back(prRjetb_B0sDps->GetBinContent(i));
			vals_LbLc.push_back(prRjetb_LbLc->GetBinContent(i));
			vals_B0nonC.push_back(prRjetb_B0nonC->GetBinContent(i));
			vals_BpnonC.push_back(prRjetb_BpnonC->GetBinContent(i));
			vals_B0snonC.push_back(prRjetb_B0snonC->GetBinContent(i));
			vals_LbnonC.push_back(prRjetb_LbnonC->GetBinContent(i));

			vals_B0DpD0.push_back(prRjetb_B0DpD0->GetBinContent(i));
			vals_B0DpDps.push_back(prRjetb_B0DpDps->GetBinContent(i));
			vals_B0D0Dps.push_back(prRjetb_B0D0Dps->GetBinContent(i));
			vals_BpDpD0.push_back(prRjetb_BpDpD0->GetBinContent(i));
			vals_BpDpDps.push_back(prRjetb_BpDpDps->GetBinContent(i));
			vals_BpD0Dps.push_back(prRjetb_BpD0Dps->GetBinContent(i));

			delete prRjetb_B0Dp;
			delete prRjetb_B0D0;
			delete prRjetb_B0Dps;
			delete prRjetb_BpDp;
			delete prRjetb_BpD0;
			delete prRjetb_BpDps;
			delete prRjetb_B0sDps;
			delete prRjetb_LbLc;
			delete prRjetb_B0nonC;
			delete prRjetb_BpnonC;
			delete prRjetb_B0snonC; 
			delete prRjetb_LbnonC;

			delete prRjetb_B0DpD0;
			delete prRjetb_B0DpDps;
			delete prRjetb_B0D0Dps;
			delete prRjetb_BpDpD0; 
			delete prRjetb_BpDpDps;
			delete prRjetb_BpD0Dps;

			delete prRjetb_B0Dp_s;
			delete prRjetb_B0D0_s;
			delete prRjetb_B0Dps_s;
			delete prRjetb_BpDp_s;
			delete prRjetb_BpD0_s;
			delete prRjetb_BpDps_s;
			delete prRjetb_B0sDps_s;
			delete prRjetb_LbLc_s;
			delete prRjetb_B0nonC_s;
			delete prRjetb_BpnonC_s;
			delete prRjetb_B0snonC_s;
			delete prRjetb_LbnonC_s;

			delete prRjetb_B0DpD0_s;
			delete prRjetb_B0DpDps_s;
			delete prRjetb_B0D0Dps_s;
			delete prRjetb_BpDpD0_s;
			delete prRjetb_BpDpDps_s;
			delete prRjetb_BpD0Dps_s;
		}

		vector<vector<double>> vec_evtW{evtW_B0Dp, evtW_B0D0, evtW_B0Dps, evtW_BpDp,
								 evtW_BpD0, evtW_BpDps, evtW_B0sDps, evtW_LbLc,
								 evtW_B0nonC, evtW_BpnonC, evtW_B0snonC, evtW_LbnonC,
								 evtW_B0DpD0,	evtW_B0DpDps,	evtW_B0D0Dps,	evtW_BpDpD0,
								 evtW_BpDpDps, evtW_BpD0Dps};

		vector<vector<double>> vec_evtWs{evtWs_B0Dp, evtWs_B0D0, evtWs_B0Dps, evtWs_BpDp,
								 evtWs_BpD0, evtWs_BpDps, evtWs_B0sDps, evtWs_LbLc,
								 evtWs_B0nonC, evtWs_BpnonC, evtWs_B0snonC, evtWs_LbnonC,
								 evtWs_B0DpD0,	evtWs_B0DpDps,	evtWs_B0D0Dps,	evtWs_BpDpD0,
								 evtWs_BpDpDps, evtWs_BpD0Dps};

		vector<vector<double>> vec_sem{sem_B0Dp, sem_B0D0, sem_B0Dps, sem_BpDp,
								 sem_BpD0, sem_BpDps, sem_B0sDps, sem_LbLc,
								 sem_B0nonC, sem_BpnonC, sem_B0snonC, sem_LbnonC,
								 sem_B0DpD0,	sem_B0DpDps,	sem_B0D0Dps,	sem_BpDpD0,
								 sem_BpDpDps, sem_BpD0Dps};

		vector<vector<double>> vec_vals{
vals_B0Dp, vals_B0D0, vals_B0Dps, vals_BpDp,
								 vals_BpD0, vals_BpDps, vals_B0sDps, vals_LbLc,
								 vals_B0nonC, vals_BpnonC, vals_B0snonC, vals_LbnonC,
								 vals_B0DpD0,	vals_B0DpDps,	vals_B0D0Dps,	vals_BpDpD0,
								 vals_BpDpDps, vals_BpD0Dps};

		//set small number so that we dont divide by zero 
		evtWtot_B0Dp = 1e-10;
		evtWtot_B0D0 = 1e-10;
		evtWtot_B0Dps = 1e-10;
		evtWtot_BpDp = 1e-10;
		evtWtot_BpD0 = 1e-10;
		evtWtot_BpDps = 1e-10;
		evtWtot_B0sDps = 1e-10;
		evtWtot_LbLc = 1e-10;
		evtWtot_B0nonC = 1e-10;
		evtWtot_BpnonC = 1e-10;
		evtWtot_B0snonC = 1e-10;
		evtWtot_LbnonC = 1e-10;

		evtWtot_B0DpD0 = 1e-10;
		evtWtot_B0DpDps = 1e-10;
		evtWtot_B0D0Dps = 1e-10;
		evtWtot_BpDpD0 = 1e-10;
		evtWtot_BpDpDps = 1e-10;
		evtWtot_BpD0Dps = 1e-10;

		evtWtots_B0Dp = 1e-10;
		evtWtots_B0D0 = 1e-10;
		evtWtots_B0Dps = 1e-10;
		evtWtots_BpDp = 1e-10;
		evtWtots_BpD0 = 1e-10;
		evtWtots_BpDps = 1e-10;
		evtWtots_B0sDps = 1e-10;
		evtWtots_LbLc = 1e-10;
		evtWtots_B0nonC = 1e-10;
		evtWtots_BpnonC = 1e-10;
		evtWtots_B0snonC = 1e-10;
		evtWtots_LbnonC = 1e-10;

		evtWtots_B0DpD0 = 1e-10;
		evtWtots_B0DpDps = 1e-10;
		evtWtots_B0D0Dps = 1e-10;
		evtWtots_BpDpD0 = 1e-10;
		evtWtots_BpDpDps = 1e-10;
		evtWtots_BpD0Dps = 1e-10;

		for (double x : evtW_B0Dp) evtWtot_B0Dp += x;	
		for (double x : evtW_B0D0) evtWtot_B0D0 += x;
		for (double x : evtW_B0Dps) evtWtot_B0Dps += x;
		for (double x : evtW_BpDp) evtWtot_BpDp += x;
		for (double x : evtW_BpD0) evtWtot_BpD0 += x;
		for (double x : evtW_BpDps) evtWtot_BpDps += x;
		for (double x : evtW_B0sDps) evtWtot_B0sDps += x;
		for (double x : evtW_LbLc) evtWtot_LbLc += x;
		for (double x : evtW_B0nonC) evtWtot_B0nonC += x;
		for (double x : evtW_BpnonC) evtWtot_BpnonC += x;
		for (double x : evtW_B0snonC) evtWtot_B0snonC += x;
		for (double x : evtW_LbnonC) evtWtot_LbnonC += x;

		for (double x : evtW_B0DpD0) evtWtot_B0DpD0 += x;
		for (double x : evtW_B0DpDps) evtWtot_B0DpDps += x;
		for (double x : evtW_B0D0Dps) evtWtot_B0D0Dps += x;
		for (double x : evtW_BpDpD0) evtWtot_BpDpD0 += x;
		for (double x : evtW_BpDpDps) evtWtot_BpDpDps += x;
		for (double x : evtW_BpD0Dps) evtWtot_BpD0Dps += x;

		for (double x : evtWs_B0Dp) evtWtots_B0Dp += x;
		for (double x : evtWs_B0D0) evtWtots_B0D0 += x;
		for (double x : evtWs_B0Dps) evtWtots_B0Dps += x;
		for (double x : evtWs_BpDp) evtWtots_BpDp += x;
		for (double x : evtWs_BpD0) evtWtots_BpD0 += x;
		for (double x : evtWs_BpDps) evtWtots_BpDps += x;
		for (double x : evtWs_B0sDps) evtWtots_B0sDps += x;
		for (double x : evtWs_LbLc) evtWtots_LbLc += x;
		for (double x : evtWs_B0nonC) evtWtots_B0nonC += x;
		for (double x : evtWs_BpnonC) evtWtots_BpnonC += x;
		for (double x : evtWs_B0snonC) evtWtots_B0snonC += x;
		for (double x : evtWs_LbnonC) evtWtots_LbnonC += x;

		for (double x : evtWs_B0DpD0) evtWtots_B0DpD0 += x;
		for (double x : evtWs_B0DpDps) evtWtots_B0DpDps += x;
		for (double x : evtWs_B0D0Dps) evtWtots_B0D0Dps += x;
		for (double x : evtWs_BpDpD0) evtWtots_BpDpD0 += x;
		for (double x : evtWs_BpDpDps) evtWtots_BpDpDps += x;
		for (double x : evtWs_BpD0Dps) evtWtots_BpD0Dps += x;
		
		evtWtot = evtWtot_B0Dp + evtWtot_B0D0 + evtWtot_B0Dps + evtWtot_BpDp + evtWtot_BpD0 +
						 	evtWtot_BpDps + evtWtot_B0sDps + evtWtot_LbLc + evtWtot_B0nonC +evtWtot_BpnonC +
							evtWtot_B0snonC + evtWtot_LbnonC + evtWtot_B0DpD0 + evtWtot_B0DpDps +
							evtWtot_B0D0Dps + evtWtot_BpDpD0 + evtWtot_BpDpDps + evtWtot_BpD0Dps;

		evtWtots = evtWtots_B0Dp + evtWtots_B0D0 + evtWtots_B0Dps + evtWtots_BpDp + evtWtots_BpD0 +
						 	 evtWtots_BpDps + evtWtots_B0sDps + evtWtots_LbLc + evtWtots_B0nonC +evtWtots_BpnonC +
							 evtWtots_B0snonC + evtWtots_LbnonC + evtWtots_B0DpD0 + evtWtots_B0DpDps +
							 evtWtots_B0D0Dps + evtWtots_BpDpD0 + evtWtots_BpDpDps + evtWtots_BpD0Dps;

		//loop over diff scale factors 
		errorb = 0;
		for (int j = 0; j < hRjetb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hRjetb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}

		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb->GetBinContent(i) == 0) diffb->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb->SetBinError(i,errorb);
  }

//############### plus variation ####################
//Loop over the TProfile bins
  for (int i = 1; i < hRjetb->GetNbinsX()+1; i++){
		evtW_B0Dp.clear();
		evtW_B0D0.clear();
		evtW_B0Dps.clear();
		evtW_BpDp.clear();
		evtW_BpD0.clear();
		evtW_BpDps.clear();
		evtW_B0sDps.clear();
		evtW_LbLc.clear();
		evtW_B0nonC.clear();
		evtW_BpnonC.clear();
		evtW_B0snonC.clear();
		evtW_LbnonC.clear();

		evtW_B0DpD0.clear();
		evtW_B0DpDps.clear();
		evtW_B0D0Dps.clear();
		evtW_BpDpD0.clear();
		evtW_BpDpDps.clear();
		evtW_BpD0Dps.clear();

		evtWs_B0Dp.clear();
		evtWs_B0D0.clear();
		evtWs_B0Dps.clear();
		evtWs_BpDp.clear();
		evtWs_BpD0.clear();
		evtWs_BpDps.clear();
		evtWs_B0sDps.clear();
		evtWs_LbLc.clear();
		evtWs_B0nonC.clear();
		evtWs_BpnonC.clear();
		evtWs_B0snonC.clear();
		evtWs_LbnonC.clear();

		evtWs_B0DpD0.clear();
		evtWs_B0DpDps.clear();
		evtWs_B0D0Dps.clear();
		evtWs_BpDpD0.clear();
		evtWs_BpDpDps.clear();
		evtWs_BpD0Dps.clear();

		sem_B0Dp.clear();
		sem_B0D0.clear();
		sem_B0Dps.clear();
		sem_BpDp.clear();
		sem_BpD0.clear();
		sem_BpDps.clear();
		sem_B0sDps.clear();
		sem_LbLc.clear();
		sem_B0nonC.clear();
		sem_BpnonC.clear();
		sem_B0snonC.clear();
		sem_LbnonC.clear();

		sem_B0DpD0.clear();
		sem_B0DpDps.clear();
		sem_B0D0Dps.clear();
		sem_BpDpD0.clear();
		sem_BpDpDps.clear();
		sem_BpD0Dps.clear();

		vals_B0Dp.clear();
		vals_B0D0.clear();
		vals_B0Dps.clear();
		vals_BpDp.clear();
		vals_BpD0.clear();
		vals_BpDps.clear();
		vals_B0sDps.clear();
		vals_LbLc.clear();
		vals_B0nonC.clear();
		vals_BpnonC.clear();
		vals_B0snonC.clear();
		vals_LbnonC.clear();

		vals_B0DpD0.clear();
		vals_B0DpDps.clear();
		vals_B0D0Dps.clear();
		vals_BpDpD0.clear();
		vals_BpDpDps.clear();
		vals_BpD0Dps.clear();

		for (int j = 1; j < hRjetb->GetNbinsX()+1; j++) {
			string pr_ind = "_" + to_string(i) + "_" + to_string(j);

			prRjetb_B0Dp = pr2DRjetb_B0Dp->ProfileX(("B0Dp"+pr_ind).c_str(),j,j);
			prRjetb_B0D0 = pr2DRjetb_B0D0->ProfileX(("B0D0"+pr_ind).c_str(),j,j);
			prRjetb_B0Dps = pr2DRjetb_B0Dps->ProfileX(("B0Dps"+pr_ind).c_str(),j,j);
			prRjetb_BpDp = pr2DRjetb_BpDp->ProfileX(("BpDp"+pr_ind).c_str(),j,j);
			prRjetb_BpD0 = pr2DRjetb_BpD0->ProfileX(("BpD0"+pr_ind).c_str(),j,j);
			prRjetb_BpDps = pr2DRjetb_BpDps->ProfileX(("BpDps"+pr_ind).c_str(),j,j);
			prRjetb_B0sDps = pr2DRjetb_B0sDps->ProfileX(("B0sDps"+pr_ind).c_str(),j,j);
			prRjetb_LbLc = pr2DRjetb_LbLc->ProfileX(("LbLc"+pr_ind).c_str(),j,j);
			prRjetb_B0nonC = pr2DRjetb_B0nonC->ProfileX(("B0nonC"+pr_ind).c_str(),j,j);
			prRjetb_BpnonC = pr2DRjetb_BpnonC->ProfileX(("BpnonC"+pr_ind).c_str(),j,j);
			prRjetb_B0snonC = pr2DRjetb_B0snonC->ProfileX(("B0snonC"+pr_ind).c_str(),j,j);
			prRjetb_LbnonC = pr2DRjetb_LbnonC->ProfileX(("LbnonC"+pr_ind).c_str(),j,j);

			prRjetb_B0DpD0 = pr2DRjetb_B0DpD0->ProfileX(("B0DpD0"+pr_ind).c_str(),j,j);
			prRjetb_B0DpDps = pr2DRjetb_B0DpDps->ProfileX(("B0DpDps"+pr_ind).c_str(),j,j);
			prRjetb_B0D0Dps = pr2DRjetb_B0D0Dps->ProfileX(("B0D0Dps"+pr_ind).c_str(),j,j);
			prRjetb_BpDpD0 = pr2DRjetb_BpDpD0->ProfileX(("BpDpD0"+pr_ind).c_str(),j,j);
			prRjetb_BpDpDps = pr2DRjetb_BpDpDps->ProfileX(("BpDpDps"+pr_ind).c_str(),j,j);
			prRjetb_BpD0Dps = pr2DRjetb_BpD0Dps->ProfileX(("BpD0Dps"+pr_ind).c_str(),j,j);

			prRjetb_B0Dp_s_p = pr2DRjetb_B0Dp_s_p->ProfileX(("B0Dp_s"+pr_ind).c_str(),j,j);
			prRjetb_B0D0_s_p = pr2DRjetb_B0D0_s_p->ProfileX(("B0D0_s"+pr_ind).c_str(),j,j);
			prRjetb_B0Dps_s_p = pr2DRjetb_B0Dps_s_p->ProfileX(("B0Dps_s"+pr_ind).c_str(),j,j);
			prRjetb_BpDp_s_p = pr2DRjetb_BpDp_s_p->ProfileX(("BpDp_s"+pr_ind).c_str(),j,j);
			prRjetb_BpD0_s_p = pr2DRjetb_BpD0_s_p->ProfileX(("BpD0_s"+pr_ind).c_str(),j,j);
			prRjetb_BpDps_s_p = pr2DRjetb_BpDps_s_p->ProfileX(("BpDps_s"+pr_ind).c_str(),j,j);
			prRjetb_B0sDps_s_p = pr2DRjetb_B0sDps_s_p->ProfileX(("B0sDps_s"+pr_ind).c_str(),j,j);
			prRjetb_LbLc_s_p = pr2DRjetb_LbLc_s_p->ProfileX(("LbLc_s"+pr_ind).c_str(),j,j);
			prRjetb_B0nonC_s_p = pr2DRjetb_B0nonC_s_p->ProfileX(("B0nonC_s"+pr_ind).c_str(),j,j);
			prRjetb_BpnonC_s_p = pr2DRjetb_BpnonC_s_p->ProfileX(("BpnonC_s"+pr_ind).c_str(),j,j);
			prRjetb_B0snonC_s_p = pr2DRjetb_B0snonC_s_p->ProfileX(("B0snonC_s"+pr_ind).c_str(),j,j);
			prRjetb_LbnonC_s_p = pr2DRjetb_LbnonC_s_p->ProfileX(("LbnonC_s"+pr_ind).c_str(),j,j);

			prRjetb_B0DpD0_s_p = pr2DRjetb_B0DpD0_s_p->ProfileX(("B0DpD0_s"+pr_ind).c_str(),j,j);
			prRjetb_B0DpDps_s_p = pr2DRjetb_B0DpDps_s_p->ProfileX(("B0DpDps_s"+pr_ind).c_str(),j,j);
			prRjetb_B0D0Dps_s_p = pr2DRjetb_B0D0Dps_s_p->ProfileX(("B0D0Dps_s"+pr_ind).c_str(),j,j);
			prRjetb_BpDpD0_s_p = pr2DRjetb_BpDpD0_s_p->ProfileX(("BpDpD0_s"+pr_ind).c_str(),j,j);
			prRjetb_BpDpDps_s_p = pr2DRjetb_BpDpDps_s_p->ProfileX(("BpDpDps_s"+pr_ind).c_str(),j,j);
			prRjetb_BpD0Dps_s_p = pr2DRjetb_BpD0Dps_s_p->ProfileX(("BpD0Dps_s"+pr_ind).c_str(),j,j);

			evtW_B0Dp.push_back(prRjetb_B0Dp->GetBinEntries(i));
			evtW_B0D0.push_back(prRjetb_B0D0->GetBinEntries(i));
			evtW_B0Dps.push_back(prRjetb_B0Dps->GetBinEntries(i));
			evtW_BpDp.push_back(prRjetb_BpDp->GetBinEntries(i));
			evtW_BpD0.push_back(prRjetb_BpD0->GetBinEntries(i));
			evtW_BpDps.push_back(prRjetb_BpDps->GetBinEntries(i));
			evtW_B0sDps.push_back(prRjetb_B0sDps->GetBinEntries(i));
			evtW_LbLc.push_back(prRjetb_LbLc->GetBinEntries(i));
			evtW_B0nonC.push_back(prRjetb_B0nonC->GetBinEntries(i));
			evtW_BpnonC.push_back(prRjetb_BpnonC->GetBinEntries(i));
			evtW_B0snonC.push_back(prRjetb_B0snonC->GetBinEntries(i));
			evtW_LbnonC.push_back(prRjetb_LbnonC->GetBinEntries(i));

			evtW_B0DpD0.push_back(prRjetb_B0DpD0->GetBinEntries(i));
			evtW_B0DpDps.push_back(prRjetb_B0DpDps->GetBinEntries(i));
			evtW_B0D0Dps.push_back(prRjetb_B0D0Dps->GetBinEntries(i));
			evtW_BpDpD0.push_back(prRjetb_BpDpD0->GetBinEntries(i));
			evtW_BpDpDps.push_back(prRjetb_BpDpDps->GetBinEntries(i));
			evtW_BpD0Dps.push_back(prRjetb_BpD0Dps->GetBinEntries(i));

			evtWs_B0Dp.push_back(prRjetb_B0Dp_s_p->GetBinEntries(i));
			evtWs_B0D0.push_back(prRjetb_B0D0_s_p->GetBinEntries(i));
			evtWs_B0Dps.push_back(prRjetb_B0Dps_s_p->GetBinEntries(i));
			evtWs_BpDp.push_back(prRjetb_BpDp_s_p->GetBinEntries(i));
			evtWs_BpD0.push_back(prRjetb_BpD0_s_p->GetBinEntries(i));
			evtWs_BpDps.push_back(prRjetb_BpDps_s_p->GetBinEntries(i));
			evtWs_B0sDps.push_back(prRjetb_B0sDps_s_p->GetBinEntries(i));
			evtWs_LbLc.push_back(prRjetb_LbLc_s_p->GetBinEntries(i));
			evtWs_B0nonC.push_back(prRjetb_B0nonC_s_p->GetBinEntries(i));
			evtWs_BpnonC.push_back(prRjetb_BpnonC_s_p->GetBinEntries(i));
			evtWs_B0snonC.push_back(prRjetb_B0snonC_s_p->GetBinEntries(i));
			evtWs_LbnonC.push_back(prRjetb_LbnonC_s_p->GetBinEntries(i));

			evtWs_B0DpD0.push_back(prRjetb_B0DpD0_s_p->GetBinEntries(i));
			evtWs_B0DpDps.push_back(prRjetb_B0DpDps_s_p->GetBinEntries(i));
			evtWs_B0D0Dps.push_back(prRjetb_B0D0Dps_s_p->GetBinEntries(i));
			evtWs_BpDpD0.push_back(prRjetb_BpDpD0_s_p->GetBinEntries(i));
			evtWs_BpDpDps.push_back(prRjetb_BpDpDps_s_p->GetBinEntries(i));
			evtWs_BpD0Dps.push_back(prRjetb_BpD0Dps_s_p->GetBinEntries(i));

			sem_B0Dp.push_back(prRjetb_B0Dp->GetBinError(i));
			sem_B0D0.push_back(prRjetb_B0D0->GetBinError(i));
			sem_B0Dps.push_back(prRjetb_B0Dps->GetBinError(i));
			sem_BpDp.push_back(prRjetb_BpDp->GetBinError(i));
			sem_BpD0.push_back(prRjetb_BpD0->GetBinError(i));
			sem_BpDps.push_back(prRjetb_BpDps->GetBinError(i));
			sem_B0sDps.push_back(prRjetb_B0sDps->GetBinError(i));
			sem_LbLc.push_back(prRjetb_LbLc->GetBinError(i));
			sem_B0nonC.push_back(prRjetb_B0nonC->GetBinError(i));
			sem_BpnonC.push_back(prRjetb_BpnonC->GetBinError(i));
			sem_B0snonC.push_back(prRjetb_B0snonC->GetBinError(i));
			sem_LbnonC.push_back(prRjetb_LbnonC->GetBinError(i));

			sem_B0DpD0.push_back(prRjetb_B0DpD0->GetBinError(i));
			sem_B0DpDps.push_back(prRjetb_B0DpDps->GetBinError(i));
			sem_B0D0Dps.push_back(prRjetb_B0D0Dps->GetBinError(i));
			sem_BpDpD0.push_back(prRjetb_BpDpD0->GetBinError(i));
			sem_BpDpDps.push_back(prRjetb_BpDpDps->GetBinError(i));
			sem_BpD0Dps.push_back(prRjetb_BpD0Dps->GetBinError(i));

			vals_B0Dp.push_back(prRjetb_B0Dp->GetBinContent(i));
			vals_B0D0.push_back(prRjetb_B0D0->GetBinContent(i));
			vals_B0Dps.push_back(prRjetb_B0Dps->GetBinContent(i));
			vals_BpDp.push_back(prRjetb_BpDp->GetBinContent(i));
			vals_BpD0.push_back(prRjetb_BpD0->GetBinContent(i));
			vals_BpDps.push_back(prRjetb_BpDps->GetBinContent(i));
			vals_B0sDps.push_back(prRjetb_B0sDps->GetBinContent(i));
			vals_LbLc.push_back(prRjetb_LbLc->GetBinContent(i));
			vals_B0nonC.push_back(prRjetb_B0nonC->GetBinContent(i));
			vals_BpnonC.push_back(prRjetb_BpnonC->GetBinContent(i));
			vals_B0snonC.push_back(prRjetb_B0snonC->GetBinContent(i));
			vals_LbnonC.push_back(prRjetb_LbnonC->GetBinContent(i));

			vals_B0DpD0.push_back(prRjetb_B0DpD0->GetBinContent(i));
			vals_B0DpDps.push_back(prRjetb_B0DpDps->GetBinContent(i));
			vals_B0D0Dps.push_back(prRjetb_B0D0Dps->GetBinContent(i));
			vals_BpDpD0.push_back(prRjetb_BpDpD0->GetBinContent(i));
			vals_BpDpDps.push_back(prRjetb_BpDpDps->GetBinContent(i));
			vals_BpD0Dps.push_back(prRjetb_BpD0Dps->GetBinContent(i));

			delete prRjetb_B0Dp;
			delete prRjetb_B0D0;
			delete prRjetb_B0Dps;
			delete prRjetb_BpDp;
			delete prRjetb_BpD0;
			delete prRjetb_BpDps;
			delete prRjetb_B0sDps;
			delete prRjetb_LbLc;
			delete prRjetb_B0nonC;
			delete prRjetb_BpnonC;
			delete prRjetb_B0snonC; 
			delete prRjetb_LbnonC;

			delete prRjetb_B0DpD0;
			delete prRjetb_B0DpDps;
			delete prRjetb_B0D0Dps;
			delete prRjetb_BpDpD0; 
			delete prRjetb_BpDpDps;
			delete prRjetb_BpD0Dps;

			delete prRjetb_B0Dp_s_p;
			delete prRjetb_B0D0_s_p;
			delete prRjetb_B0Dps_s_p;
			delete prRjetb_BpDp_s_p;
			delete prRjetb_BpD0_s_p;
			delete prRjetb_BpDps_s_p;
			delete prRjetb_B0sDps_s_p;
			delete prRjetb_LbLc_s_p;
			delete prRjetb_B0nonC_s_p;
			delete prRjetb_BpnonC_s_p;
			delete prRjetb_B0snonC_s_p;
			delete prRjetb_LbnonC_s_p;

			delete prRjetb_B0DpD0_s_p;
			delete prRjetb_B0DpDps_s_p;
			delete prRjetb_B0D0Dps_s_p;
			delete prRjetb_BpDpD0_s_p;
			delete prRjetb_BpDpDps_s_p;
			delete prRjetb_BpD0Dps_s_p;
		}

		vector<vector<double>> vec_evtW{evtW_B0Dp, evtW_B0D0, evtW_B0Dps, evtW_BpDp,
								 evtW_BpD0, evtW_BpDps, evtW_B0sDps, evtW_LbLc,
								 evtW_B0nonC, evtW_BpnonC, evtW_B0snonC, evtW_LbnonC,
								 evtW_B0DpD0,	evtW_B0DpDps,	evtW_B0D0Dps,	evtW_BpDpD0,
								 evtW_BpDpDps, evtW_BpD0Dps};

		vector<vector<double>> vec_evtWs{evtWs_B0Dp, evtWs_B0D0, evtWs_B0Dps, evtWs_BpDp,
								 evtWs_BpD0, evtWs_BpDps, evtWs_B0sDps, evtWs_LbLc,
								 evtWs_B0nonC, evtWs_BpnonC, evtWs_B0snonC, evtWs_LbnonC,
								 evtWs_B0DpD0,	evtWs_B0DpDps,	evtWs_B0D0Dps,	evtWs_BpDpD0,
								 evtWs_BpDpDps, evtWs_BpD0Dps};

		vector<vector<double>> vec_sem{sem_B0Dp, sem_B0D0, sem_B0Dps, sem_BpDp,
								 sem_BpD0, sem_BpDps, sem_B0sDps, sem_LbLc,
								 sem_B0nonC, sem_BpnonC, sem_B0snonC, sem_LbnonC,
								 sem_B0DpD0,	sem_B0DpDps,	sem_B0D0Dps,	sem_BpDpD0,
								 sem_BpDpDps, sem_BpD0Dps};

		vector<vector<double>> vec_vals{
vals_B0Dp, vals_B0D0, vals_B0Dps, vals_BpDp,
								 vals_BpD0, vals_BpDps, vals_B0sDps, vals_LbLc,
								 vals_B0nonC, vals_BpnonC, vals_B0snonC, vals_LbnonC,
								 vals_B0DpD0,	vals_B0DpDps,	vals_B0D0Dps,	vals_BpDpD0,
								 vals_BpDpDps, vals_BpD0Dps};

		//set small number so that we dont divide by zero 
		evtWtot_B0Dp = 1e-10;
		evtWtot_B0D0 = 1e-10;
		evtWtot_B0Dps = 1e-10;
		evtWtot_BpDp = 1e-10;
		evtWtot_BpD0 = 1e-10;
		evtWtot_BpDps = 1e-10;
		evtWtot_B0sDps = 1e-10;
		evtWtot_LbLc = 1e-10;
		evtWtot_B0nonC = 1e-10;
		evtWtot_BpnonC = 1e-10;
		evtWtot_B0snonC = 1e-10;
		evtWtot_LbnonC = 1e-10;

		evtWtot_B0DpD0 = 1e-10;
		evtWtot_B0DpDps = 1e-10;
		evtWtot_B0D0Dps = 1e-10;
		evtWtot_BpDpD0 = 1e-10;
		evtWtot_BpDpDps = 1e-10;
		evtWtot_BpD0Dps = 1e-10;

		evtWtots_B0Dp = 1e-10;
		evtWtots_B0D0 = 1e-10;
		evtWtots_B0Dps = 1e-10;
		evtWtots_BpDp = 1e-10;
		evtWtots_BpD0 = 1e-10;
		evtWtots_BpDps = 1e-10;
		evtWtots_B0sDps = 1e-10;
		evtWtots_LbLc = 1e-10;
		evtWtots_B0nonC = 1e-10;
		evtWtots_BpnonC = 1e-10;
		evtWtots_B0snonC = 1e-10;
		evtWtots_LbnonC = 1e-10;

		evtWtots_B0DpD0 = 1e-10;
		evtWtots_B0DpDps = 1e-10;
		evtWtots_B0D0Dps = 1e-10;
		evtWtots_BpDpD0 = 1e-10;
		evtWtots_BpDpDps = 1e-10;
		evtWtots_BpD0Dps = 1e-10;

		for (double x : evtW_B0Dp) evtWtot_B0Dp += x;	
		for (double x : evtW_B0D0) evtWtot_B0D0 += x;
		for (double x : evtW_B0Dps) evtWtot_B0Dps += x;
		for (double x : evtW_BpDp) evtWtot_BpDp += x;
		for (double x : evtW_BpD0) evtWtot_BpD0 += x;
		for (double x : evtW_BpDps) evtWtot_BpDps += x;
		for (double x : evtW_B0sDps) evtWtot_B0sDps += x;
		for (double x : evtW_LbLc) evtWtot_LbLc += x;
		for (double x : evtW_B0nonC) evtWtot_B0nonC += x;
		for (double x : evtW_BpnonC) evtWtot_BpnonC += x;
		for (double x : evtW_B0snonC) evtWtot_B0snonC += x;
		for (double x : evtW_LbnonC) evtWtot_LbnonC += x;

		for (double x : evtW_B0DpD0) evtWtot_B0DpD0 += x;
		for (double x : evtW_B0DpDps) evtWtot_B0DpDps += x;
		for (double x : evtW_B0D0Dps) evtWtot_B0D0Dps += x;
		for (double x : evtW_BpDpD0) evtWtot_BpDpD0 += x;
		for (double x : evtW_BpDpDps) evtWtot_BpDpDps += x;
		for (double x : evtW_BpD0Dps) evtWtot_BpD0Dps += x;

		for (double x : evtWs_B0Dp) evtWtots_B0Dp += x;
		for (double x : evtWs_B0D0) evtWtots_B0D0 += x;
		for (double x : evtWs_B0Dps) evtWtots_B0Dps += x;
		for (double x : evtWs_BpDp) evtWtots_BpDp += x;
		for (double x : evtWs_BpD0) evtWtots_BpD0 += x;
		for (double x : evtWs_BpDps) evtWtots_BpDps += x;
		for (double x : evtWs_B0sDps) evtWtots_B0sDps += x;
		for (double x : evtWs_LbLc) evtWtots_LbLc += x;
		for (double x : evtWs_B0nonC) evtWtots_B0nonC += x;
		for (double x : evtWs_BpnonC) evtWtots_BpnonC += x;
		for (double x : evtWs_B0snonC) evtWtots_B0snonC += x;
		for (double x : evtWs_LbnonC) evtWtots_LbnonC += x;

		for (double x : evtWs_B0DpD0) evtWtots_B0DpD0 += x;
		for (double x : evtWs_B0DpDps) evtWtots_B0DpDps += x;
		for (double x : evtWs_B0D0Dps) evtWtots_B0D0Dps += x;
		for (double x : evtWs_BpDpD0) evtWtots_BpDpD0 += x;
		for (double x : evtWs_BpDpDps) evtWtots_BpDpDps += x;
		for (double x : evtWs_BpD0Dps) evtWtots_BpD0Dps += x;
		
		evtWtot = evtWtot_B0Dp + evtWtot_B0D0 + evtWtot_B0Dps + evtWtot_BpDp + evtWtot_BpD0 +
						 	evtWtot_BpDps + evtWtot_B0sDps + evtWtot_LbLc + evtWtot_B0nonC +evtWtot_BpnonC +
							evtWtot_B0snonC + evtWtot_LbnonC + evtWtot_B0DpD0 + evtWtot_B0DpDps +
							evtWtot_B0D0Dps + evtWtot_BpDpD0 + evtWtot_BpDpDps + evtWtot_BpD0Dps;

		evtWtots = evtWtots_B0Dp + evtWtots_B0D0 + evtWtots_B0Dps + evtWtots_BpDp + evtWtots_BpD0 +
						 	 evtWtots_BpDps + evtWtots_B0sDps + evtWtots_LbLc + evtWtots_B0nonC +evtWtots_BpnonC +
							 evtWtots_B0snonC + evtWtots_LbnonC + evtWtots_B0DpD0 + evtWtots_B0DpDps +
							 evtWtots_B0D0Dps + evtWtots_BpDpD0 + evtWtots_BpDpDps + evtWtots_BpD0Dps;

		//loop over diff scale factors 
		errorb = 0;
		for (int j = 0; j < hRjetb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hRjetb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}

		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb_p->GetBinContent(i) == 0) diffb_p->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb_p->SetBinError(i,errorb);
  }

//############### minus variation ####################
//Loop over the TProfile bins
  for (int i = 1; i < hRjetb->GetNbinsX()+1; i++){
		evtW_B0Dp.clear();
		evtW_B0D0.clear();
		evtW_B0Dps.clear();
		evtW_BpDp.clear();
		evtW_BpD0.clear();
		evtW_BpDps.clear();
		evtW_B0sDps.clear();
		evtW_LbLc.clear();
		evtW_B0nonC.clear();
		evtW_BpnonC.clear();
		evtW_B0snonC.clear();
		evtW_LbnonC.clear();

		evtW_B0DpD0.clear();
		evtW_B0DpDps.clear();
		evtW_B0D0Dps.clear();
		evtW_BpDpD0.clear();
		evtW_BpDpDps.clear();
		evtW_BpD0Dps.clear();

		evtWs_B0Dp.clear();
		evtWs_B0D0.clear();
		evtWs_B0Dps.clear();
		evtWs_BpDp.clear();
		evtWs_BpD0.clear();
		evtWs_BpDps.clear();
		evtWs_B0sDps.clear();
		evtWs_LbLc.clear();
		evtWs_B0nonC.clear();
		evtWs_BpnonC.clear();
		evtWs_B0snonC.clear();
		evtWs_LbnonC.clear();

		evtWs_B0DpD0.clear();
		evtWs_B0DpDps.clear();
		evtWs_B0D0Dps.clear();
		evtWs_BpDpD0.clear();
		evtWs_BpDpDps.clear();
		evtWs_BpD0Dps.clear();

		sem_B0Dp.clear();
		sem_B0D0.clear();
		sem_B0Dps.clear();
		sem_BpDp.clear();
		sem_BpD0.clear();
		sem_BpDps.clear();
		sem_B0sDps.clear();
		sem_LbLc.clear();
		sem_B0nonC.clear();
		sem_BpnonC.clear();
		sem_B0snonC.clear();
		sem_LbnonC.clear();

		sem_B0DpD0.clear();
		sem_B0DpDps.clear();
		sem_B0D0Dps.clear();
		sem_BpDpD0.clear();
		sem_BpDpDps.clear();
		sem_BpD0Dps.clear();

		vals_B0Dp.clear();
		vals_B0D0.clear();
		vals_B0Dps.clear();
		vals_BpDp.clear();
		vals_BpD0.clear();
		vals_BpDps.clear();
		vals_B0sDps.clear();
		vals_LbLc.clear();
		vals_B0nonC.clear();
		vals_BpnonC.clear();
		vals_B0snonC.clear();
		vals_LbnonC.clear();

		vals_B0DpD0.clear();
		vals_B0DpDps.clear();
		vals_B0D0Dps.clear();
		vals_BpDpD0.clear();
		vals_BpDpDps.clear();
		vals_BpD0Dps.clear();

		for (int j = 1; j < hRjetb->GetNbinsX()+1; j++) {
			string pr_ind = "_" + to_string(i) + "_" + to_string(j);

			prRjetb_B0Dp = pr2DRjetb_B0Dp->ProfileX(("B0Dp"+pr_ind).c_str(),j,j);
			prRjetb_B0D0 = pr2DRjetb_B0D0->ProfileX(("B0D0"+pr_ind).c_str(),j,j);
			prRjetb_B0Dps = pr2DRjetb_B0Dps->ProfileX(("B0Dps"+pr_ind).c_str(),j,j);
			prRjetb_BpDp = pr2DRjetb_BpDp->ProfileX(("BpDp"+pr_ind).c_str(),j,j);
			prRjetb_BpD0 = pr2DRjetb_BpD0->ProfileX(("BpD0"+pr_ind).c_str(),j,j);
			prRjetb_BpDps = pr2DRjetb_BpDps->ProfileX(("BpDps"+pr_ind).c_str(),j,j);
			prRjetb_B0sDps = pr2DRjetb_B0sDps->ProfileX(("B0sDps"+pr_ind).c_str(),j,j);
			prRjetb_LbLc = pr2DRjetb_LbLc->ProfileX(("LbLc"+pr_ind).c_str(),j,j);
			prRjetb_B0nonC = pr2DRjetb_B0nonC->ProfileX(("B0nonC"+pr_ind).c_str(),j,j);
			prRjetb_BpnonC = pr2DRjetb_BpnonC->ProfileX(("BpnonC"+pr_ind).c_str(),j,j);
			prRjetb_B0snonC = pr2DRjetb_B0snonC->ProfileX(("B0snonC"+pr_ind).c_str(),j,j);
			prRjetb_LbnonC = pr2DRjetb_LbnonC->ProfileX(("LbnonC"+pr_ind).c_str(),j,j);

			prRjetb_B0DpD0 = pr2DRjetb_B0DpD0->ProfileX(("B0DpD0"+pr_ind).c_str(),j,j);
			prRjetb_B0DpDps = pr2DRjetb_B0DpDps->ProfileX(("B0DpDps"+pr_ind).c_str(),j,j);
			prRjetb_B0D0Dps = pr2DRjetb_B0D0Dps->ProfileX(("B0D0Dps"+pr_ind).c_str(),j,j);
			prRjetb_BpDpD0 = pr2DRjetb_BpDpD0->ProfileX(("BpDpD0"+pr_ind).c_str(),j,j);
			prRjetb_BpDpDps = pr2DRjetb_BpDpDps->ProfileX(("BpDpDps"+pr_ind).c_str(),j,j);
			prRjetb_BpD0Dps = pr2DRjetb_BpD0Dps->ProfileX(("BpD0Dps"+pr_ind).c_str(),j,j);

			prRjetb_B0Dp_s_m = pr2DRjetb_B0Dp_s_m->ProfileX(("B0Dp_s"+pr_ind).c_str(),j,j);
			prRjetb_B0D0_s_m = pr2DRjetb_B0D0_s_m->ProfileX(("B0D0_s"+pr_ind).c_str(),j,j);
			prRjetb_B0Dps_s_m = pr2DRjetb_B0Dps_s_m->ProfileX(("B0Dps_s"+pr_ind).c_str(),j,j);
			prRjetb_BpDp_s_m = pr2DRjetb_BpDp_s_m->ProfileX(("BpDp_s"+pr_ind).c_str(),j,j);
			prRjetb_BpD0_s_m = pr2DRjetb_BpD0_s_m->ProfileX(("BpD0_s"+pr_ind).c_str(),j,j);
			prRjetb_BpDps_s_m = pr2DRjetb_BpDps_s_m->ProfileX(("BpDps_s"+pr_ind).c_str(),j,j);
			prRjetb_B0sDps_s_m = pr2DRjetb_B0sDps_s_m->ProfileX(("B0sDps_s"+pr_ind).c_str(),j,j);
			prRjetb_LbLc_s_m = pr2DRjetb_LbLc_s_m->ProfileX(("LbLc_s"+pr_ind).c_str(),j,j);
			prRjetb_B0nonC_s_m = pr2DRjetb_B0nonC_s_m->ProfileX(("B0nonC_s"+pr_ind).c_str(),j,j);
			prRjetb_BpnonC_s_m = pr2DRjetb_BpnonC_s_m->ProfileX(("BpnonC_s"+pr_ind).c_str(),j,j);
			prRjetb_B0snonC_s_m = pr2DRjetb_B0snonC_s_m->ProfileX(("B0snonC_s"+pr_ind).c_str(),j,j);
			prRjetb_LbnonC_s_m = pr2DRjetb_LbnonC_s_m->ProfileX(("LbnonC_s"+pr_ind).c_str(),j,j);

			prRjetb_B0DpD0_s_m = pr2DRjetb_B0DpD0_s_m->ProfileX(("B0DpD0_s"+pr_ind).c_str(),j,j);
			prRjetb_B0DpDps_s_m = pr2DRjetb_B0DpDps_s_m->ProfileX(("B0DpDps_s"+pr_ind).c_str(),j,j);
			prRjetb_B0D0Dps_s_m = pr2DRjetb_B0D0Dps_s_m->ProfileX(("B0D0Dps_s"+pr_ind).c_str(),j,j);
			prRjetb_BpDpD0_s_m = pr2DRjetb_BpDpD0_s_m->ProfileX(("BpDpD0_s"+pr_ind).c_str(),j,j);
			prRjetb_BpDpDps_s_m = pr2DRjetb_BpDpDps_s_m->ProfileX(("BpDpDps_s"+pr_ind).c_str(),j,j);
			prRjetb_BpD0Dps_s_m = pr2DRjetb_BpD0Dps_s_m->ProfileX(("BpD0Dps_s"+pr_ind).c_str(),j,j);

			evtW_B0Dp.push_back(prRjetb_B0Dp->GetBinEntries(i));
			evtW_B0D0.push_back(prRjetb_B0D0->GetBinEntries(i));
			evtW_B0Dps.push_back(prRjetb_B0Dps->GetBinEntries(i));
			evtW_BpDp.push_back(prRjetb_BpDp->GetBinEntries(i));
			evtW_BpD0.push_back(prRjetb_BpD0->GetBinEntries(i));
			evtW_BpDps.push_back(prRjetb_BpDps->GetBinEntries(i));
			evtW_B0sDps.push_back(prRjetb_B0sDps->GetBinEntries(i));
			evtW_LbLc.push_back(prRjetb_LbLc->GetBinEntries(i));
			evtW_B0nonC.push_back(prRjetb_B0nonC->GetBinEntries(i));
			evtW_BpnonC.push_back(prRjetb_BpnonC->GetBinEntries(i));
			evtW_B0snonC.push_back(prRjetb_B0snonC->GetBinEntries(i));
			evtW_LbnonC.push_back(prRjetb_LbnonC->GetBinEntries(i));

			evtW_B0DpD0.push_back(prRjetb_B0DpD0->GetBinEntries(i));
			evtW_B0DpDps.push_back(prRjetb_B0DpDps->GetBinEntries(i));
			evtW_B0D0Dps.push_back(prRjetb_B0D0Dps->GetBinEntries(i));
			evtW_BpDpD0.push_back(prRjetb_BpDpD0->GetBinEntries(i));
			evtW_BpDpDps.push_back(prRjetb_BpDpDps->GetBinEntries(i));
			evtW_BpD0Dps.push_back(prRjetb_BpD0Dps->GetBinEntries(i));

			evtWs_B0Dp.push_back(prRjetb_B0Dp_s_m->GetBinEntries(i));
			evtWs_B0D0.push_back(prRjetb_B0D0_s_m->GetBinEntries(i));
			evtWs_B0Dps.push_back(prRjetb_B0Dps_s_m->GetBinEntries(i));
			evtWs_BpDp.push_back(prRjetb_BpDp_s_m->GetBinEntries(i));
			evtWs_BpD0.push_back(prRjetb_BpD0_s_m->GetBinEntries(i));
			evtWs_BpDps.push_back(prRjetb_BpDps_s_m->GetBinEntries(i));
			evtWs_B0sDps.push_back(prRjetb_B0sDps_s_m->GetBinEntries(i));
			evtWs_LbLc.push_back(prRjetb_LbLc_s_m->GetBinEntries(i));
			evtWs_B0nonC.push_back(prRjetb_B0nonC_s_m->GetBinEntries(i));
			evtWs_BpnonC.push_back(prRjetb_BpnonC_s_m->GetBinEntries(i));
			evtWs_B0snonC.push_back(prRjetb_B0snonC_s_m->GetBinEntries(i));
			evtWs_LbnonC.push_back(prRjetb_LbnonC_s_m->GetBinEntries(i));

			evtWs_B0DpD0.push_back(prRjetb_B0DpD0_s_m->GetBinEntries(i));
			evtWs_B0DpDps.push_back(prRjetb_B0DpDps_s_m->GetBinEntries(i));
			evtWs_B0D0Dps.push_back(prRjetb_B0D0Dps_s_m->GetBinEntries(i));
			evtWs_BpDpD0.push_back(prRjetb_BpDpD0_s_m->GetBinEntries(i));
			evtWs_BpDpDps.push_back(prRjetb_BpDpDps_s_m->GetBinEntries(i));
			evtWs_BpD0Dps.push_back(prRjetb_BpD0Dps_s_m->GetBinEntries(i));

			sem_B0Dp.push_back(prRjetb_B0Dp->GetBinError(i));
			sem_B0D0.push_back(prRjetb_B0D0->GetBinError(i));
			sem_B0Dps.push_back(prRjetb_B0Dps->GetBinError(i));
			sem_BpDp.push_back(prRjetb_BpDp->GetBinError(i));
			sem_BpD0.push_back(prRjetb_BpD0->GetBinError(i));
			sem_BpDps.push_back(prRjetb_BpDps->GetBinError(i));
			sem_B0sDps.push_back(prRjetb_B0sDps->GetBinError(i));
			sem_LbLc.push_back(prRjetb_LbLc->GetBinError(i));
			sem_B0nonC.push_back(prRjetb_B0nonC->GetBinError(i));
			sem_BpnonC.push_back(prRjetb_BpnonC->GetBinError(i));
			sem_B0snonC.push_back(prRjetb_B0snonC->GetBinError(i));
			sem_LbnonC.push_back(prRjetb_LbnonC->GetBinError(i));

			sem_B0DpD0.push_back(prRjetb_B0DpD0->GetBinError(i));
			sem_B0DpDps.push_back(prRjetb_B0DpDps->GetBinError(i));
			sem_B0D0Dps.push_back(prRjetb_B0D0Dps->GetBinError(i));
			sem_BpDpD0.push_back(prRjetb_BpDpD0->GetBinError(i));
			sem_BpDpDps.push_back(prRjetb_BpDpDps->GetBinError(i));
			sem_BpD0Dps.push_back(prRjetb_BpD0Dps->GetBinError(i));

			vals_B0Dp.push_back(prRjetb_B0Dp->GetBinContent(i));
			vals_B0D0.push_back(prRjetb_B0D0->GetBinContent(i));
			vals_B0Dps.push_back(prRjetb_B0Dps->GetBinContent(i));
			vals_BpDp.push_back(prRjetb_BpDp->GetBinContent(i));
			vals_BpD0.push_back(prRjetb_BpD0->GetBinContent(i));
			vals_BpDps.push_back(prRjetb_BpDps->GetBinContent(i));
			vals_B0sDps.push_back(prRjetb_B0sDps->GetBinContent(i));
			vals_LbLc.push_back(prRjetb_LbLc->GetBinContent(i));
			vals_B0nonC.push_back(prRjetb_B0nonC->GetBinContent(i));
			vals_BpnonC.push_back(prRjetb_BpnonC->GetBinContent(i));
			vals_B0snonC.push_back(prRjetb_B0snonC->GetBinContent(i));
			vals_LbnonC.push_back(prRjetb_LbnonC->GetBinContent(i));

			vals_B0DpD0.push_back(prRjetb_B0DpD0->GetBinContent(i));
			vals_B0DpDps.push_back(prRjetb_B0DpDps->GetBinContent(i));
			vals_B0D0Dps.push_back(prRjetb_B0D0Dps->GetBinContent(i));
			vals_BpDpD0.push_back(prRjetb_BpDpD0->GetBinContent(i));
			vals_BpDpDps.push_back(prRjetb_BpDpDps->GetBinContent(i));
			vals_BpD0Dps.push_back(prRjetb_BpD0Dps->GetBinContent(i));

			delete prRjetb_B0Dp;
			delete prRjetb_B0D0;
			delete prRjetb_B0Dps;
			delete prRjetb_BpDp;
			delete prRjetb_BpD0;
			delete prRjetb_BpDps;
			delete prRjetb_B0sDps;
			delete prRjetb_LbLc;
			delete prRjetb_B0nonC;
			delete prRjetb_BpnonC;
			delete prRjetb_B0snonC; 
			delete prRjetb_LbnonC;

			delete prRjetb_B0DpD0;
			delete prRjetb_B0DpDps;
			delete prRjetb_B0D0Dps;
			delete prRjetb_BpDpD0; 
			delete prRjetb_BpDpDps;
			delete prRjetb_BpD0Dps;

			delete prRjetb_B0Dp_s_m;
			delete prRjetb_B0D0_s_m;
			delete prRjetb_B0Dps_s_m;
			delete prRjetb_BpDp_s_m;
			delete prRjetb_BpD0_s_m;
			delete prRjetb_BpDps_s_m;
			delete prRjetb_B0sDps_s_m;
			delete prRjetb_LbLc_s_m;
			delete prRjetb_B0nonC_s_m;
			delete prRjetb_BpnonC_s_m;
			delete prRjetb_B0snonC_s_m;
			delete prRjetb_LbnonC_s_m;

			delete prRjetb_B0DpD0_s_m;
			delete prRjetb_B0DpDps_s_m;
			delete prRjetb_B0D0Dps_s_m;
			delete prRjetb_BpDpD0_s_m;
			delete prRjetb_BpDpDps_s_m;
			delete prRjetb_BpD0Dps_s_m;
		}

		vector<vector<double>> vec_evtW{evtW_B0Dp, evtW_B0D0, evtW_B0Dps, evtW_BpDp,
								 evtW_BpD0, evtW_BpDps, evtW_B0sDps, evtW_LbLc,
								 evtW_B0nonC, evtW_BpnonC, evtW_B0snonC, evtW_LbnonC,
								 evtW_B0DpD0,	evtW_B0DpDps,	evtW_B0D0Dps,	evtW_BpDpD0,
								 evtW_BpDpDps, evtW_BpD0Dps};

		vector<vector<double>> vec_evtWs{evtWs_B0Dp, evtWs_B0D0, evtWs_B0Dps, evtWs_BpDp,
								 evtWs_BpD0, evtWs_BpDps, evtWs_B0sDps, evtWs_LbLc,
								 evtWs_B0nonC, evtWs_BpnonC, evtWs_B0snonC, evtWs_LbnonC,
								 evtWs_B0DpD0,	evtWs_B0DpDps,	evtWs_B0D0Dps,	evtWs_BpDpD0,
								 evtWs_BpDpDps, evtWs_BpD0Dps};

		vector<vector<double>> vec_sem{sem_B0Dp, sem_B0D0, sem_B0Dps, sem_BpDp,
								 sem_BpD0, sem_BpDps, sem_B0sDps, sem_LbLc,
								 sem_B0nonC, sem_BpnonC, sem_B0snonC, sem_LbnonC,
								 sem_B0DpD0,	sem_B0DpDps,	sem_B0D0Dps,	sem_BpDpD0,
								 sem_BpDpDps, sem_BpD0Dps};

		vector<vector<double>> vec_vals{
vals_B0Dp, vals_B0D0, vals_B0Dps, vals_BpDp,
								 vals_BpD0, vals_BpDps, vals_B0sDps, vals_LbLc,
								 vals_B0nonC, vals_BpnonC, vals_B0snonC, vals_LbnonC,
								 vals_B0DpD0,	vals_B0DpDps,	vals_B0D0Dps,	vals_BpDpD0,
								 vals_BpDpDps, vals_BpD0Dps};

		//set small number so that we dont divide by zero 
		evtWtot_B0Dp = 1e-10;
		evtWtot_B0D0 = 1e-10;
		evtWtot_B0Dps = 1e-10;
		evtWtot_BpDp = 1e-10;
		evtWtot_BpD0 = 1e-10;
		evtWtot_BpDps = 1e-10;
		evtWtot_B0sDps = 1e-10;
		evtWtot_LbLc = 1e-10;
		evtWtot_B0nonC = 1e-10;
		evtWtot_BpnonC = 1e-10;
		evtWtot_B0snonC = 1e-10;
		evtWtot_LbnonC = 1e-10;

		evtWtot_B0DpD0 = 1e-10;
		evtWtot_B0DpDps = 1e-10;
		evtWtot_B0D0Dps = 1e-10;
		evtWtot_BpDpD0 = 1e-10;
		evtWtot_BpDpDps = 1e-10;
		evtWtot_BpD0Dps = 1e-10;

		evtWtots_B0Dp = 1e-10;
		evtWtots_B0D0 = 1e-10;
		evtWtots_B0Dps = 1e-10;
		evtWtots_BpDp = 1e-10;
		evtWtots_BpD0 = 1e-10;
		evtWtots_BpDps = 1e-10;
		evtWtots_B0sDps = 1e-10;
		evtWtots_LbLc = 1e-10;
		evtWtots_B0nonC = 1e-10;
		evtWtots_BpnonC = 1e-10;
		evtWtots_B0snonC = 1e-10;
		evtWtots_LbnonC = 1e-10;

		evtWtots_B0DpD0 = 1e-10;
		evtWtots_B0DpDps = 1e-10;
		evtWtots_B0D0Dps = 1e-10;
		evtWtots_BpDpD0 = 1e-10;
		evtWtots_BpDpDps = 1e-10;
		evtWtots_BpD0Dps = 1e-10;

		for (double x : evtW_B0Dp) evtWtot_B0Dp += x;	
		for (double x : evtW_B0D0) evtWtot_B0D0 += x;
		for (double x : evtW_B0Dps) evtWtot_B0Dps += x;
		for (double x : evtW_BpDp) evtWtot_BpDp += x;
		for (double x : evtW_BpD0) evtWtot_BpD0 += x;
		for (double x : evtW_BpDps) evtWtot_BpDps += x;
		for (double x : evtW_B0sDps) evtWtot_B0sDps += x;
		for (double x : evtW_LbLc) evtWtot_LbLc += x;
		for (double x : evtW_B0nonC) evtWtot_B0nonC += x;
		for (double x : evtW_BpnonC) evtWtot_BpnonC += x;
		for (double x : evtW_B0snonC) evtWtot_B0snonC += x;
		for (double x : evtW_LbnonC) evtWtot_LbnonC += x;

		for (double x : evtW_B0DpD0) evtWtot_B0DpD0 += x;
		for (double x : evtW_B0DpDps) evtWtot_B0DpDps += x;
		for (double x : evtW_B0D0Dps) evtWtot_B0D0Dps += x;
		for (double x : evtW_BpDpD0) evtWtot_BpDpD0 += x;
		for (double x : evtW_BpDpDps) evtWtot_BpDpDps += x;
		for (double x : evtW_BpD0Dps) evtWtot_BpD0Dps += x;

		for (double x : evtWs_B0Dp) evtWtots_B0Dp += x;
		for (double x : evtWs_B0D0) evtWtots_B0D0 += x;
		for (double x : evtWs_B0Dps) evtWtots_B0Dps += x;
		for (double x : evtWs_BpDp) evtWtots_BpDp += x;
		for (double x : evtWs_BpD0) evtWtots_BpD0 += x;
		for (double x : evtWs_BpDps) evtWtots_BpDps += x;
		for (double x : evtWs_B0sDps) evtWtots_B0sDps += x;
		for (double x : evtWs_LbLc) evtWtots_LbLc += x;
		for (double x : evtWs_B0nonC) evtWtots_B0nonC += x;
		for (double x : evtWs_BpnonC) evtWtots_BpnonC += x;
		for (double x : evtWs_B0snonC) evtWtots_B0snonC += x;
		for (double x : evtWs_LbnonC) evtWtots_LbnonC += x;

		for (double x : evtWs_B0DpD0) evtWtots_B0DpD0 += x;
		for (double x : evtWs_B0DpDps) evtWtots_B0DpDps += x;
		for (double x : evtWs_B0D0Dps) evtWtots_B0D0Dps += x;
		for (double x : evtWs_BpDpD0) evtWtots_BpDpD0 += x;
		for (double x : evtWs_BpDpDps) evtWtots_BpDpDps += x;
		for (double x : evtWs_BpD0Dps) evtWtots_BpD0Dps += x;
		
		evtWtot = evtWtot_B0Dp + evtWtot_B0D0 + evtWtot_B0Dps + evtWtot_BpDp + evtWtot_BpD0 +
						 	evtWtot_BpDps + evtWtot_B0sDps + evtWtot_LbLc + evtWtot_B0nonC +evtWtot_BpnonC +
							evtWtot_B0snonC + evtWtot_LbnonC + evtWtot_B0DpD0 + evtWtot_B0DpDps +
							evtWtot_B0D0Dps + evtWtot_BpDpD0 + evtWtot_BpDpDps + evtWtot_BpD0Dps;

		evtWtots = evtWtots_B0Dp + evtWtots_B0D0 + evtWtots_B0Dps + evtWtots_BpDp + evtWtots_BpD0 +
						 	 evtWtots_BpDps + evtWtots_B0sDps + evtWtots_LbLc + evtWtots_B0nonC +evtWtots_BpnonC +
							 evtWtots_B0snonC + evtWtots_LbnonC + evtWtots_B0DpD0 + evtWtots_B0DpDps +
							 evtWtots_B0D0Dps + evtWtots_BpDpD0 + evtWtots_BpDpDps + evtWtots_BpD0Dps;

		//loop over diff scale factors 
		errorb = 0;
		for (int j = 0; j < hRjetb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hRjetb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}

		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb_m->GetBinContent(i) == 0) diffb_m->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb_m->SetBinError(i,errorb);
  }  

	//Scale to the percentages
  diffb->Scale(100.0);
  diffb_p->Scale(100.0);
  diffb_m->Scale(100.0);

	Rjetb_map["diffb"] = diffb;
	Rjetb_map["diffb_p"] = diffb_p;
	Rjetb_map["diffb_m"] = diffb_m;

	return Rjetb_map;
}

map<string, TH1D*> mpf_BprodFrac() {
	map <string, TH1D*> MPFb_map;

  TFile* f_normal = TFile::Open(OpenName.c_str());
  TFile* f_scaled = TFile::Open(OpenName_BprodFrac.c_str());
  TFile* f_scaled_p = TFile::Open(OpenName_BprodFrac_p.c_str());
  TFile* f_scaled_m = TFile::Open(OpenName_BprodFrac_m.c_str());

	TProfile *prMPFb;
	TProfile *prMPFb_s;
	TProfile *prMPFb_s_p;
	TProfile *prMPFb_s_m;

  //Read the normal sample
  f_normal->GetObject("prMPFb",   prMPFb);

  //Read the weighted sample
  f_scaled->GetObject("prMPFb",   prMPFb_s);
  f_scaled_p->GetObject("prMPFb",   prMPFb_s_p);
  f_scaled_m->GetObject("prMPFb",   prMPFb_s_m);

	//2D Tprofiles for the errorb calculations
	TProfile2D* pr2DMPFb;

	TProfile2D* pr2DMPFb_B0Bp;
	TProfile2D* pr2DMPFb_B0sBm;
	TProfile2D* pr2DMPFb_LbBb;

  //The sample with the additional weighting (*_s)
	TProfile2D* pr2DMPFb_s;

	TProfile2D* pr2DMPFb_B0Bp_s;
	TProfile2D* pr2DMPFb_B0sBm_s;
	TProfile2D* pr2DMPFb_LbBb_s;

	TProfile2D* pr2DMPFb_s_p;

	TProfile2D* pr2DMPFb_B0Bp_s_p;
	TProfile2D* pr2DMPFb_B0sBm_s_p;
	TProfile2D* pr2DMPFb_LbBb_s_p;

	TProfile2D* pr2DMPFb_s_m;

	TProfile2D* pr2DMPFb_B0Bp_s_m;
	TProfile2D* pr2DMPFb_B0sBm_s_m;
	TProfile2D* pr2DMPFb_LbBb_s_m;

	//Tprofiles for the errorb calculations
	TProfile* prMPFb_B0Bp;
	TProfile* prMPFb_B0sBm;
	TProfile* prMPFb_LbBb;

  //The sample with the additional weighting (*_s)
	TProfile* prMPFb_B0Bp_s;
	TProfile* prMPFb_B0sBm_s;
	TProfile* prMPFb_LbBb_s;

	TProfile* prMPFb_B0Bp_s_p;
	TProfile* prMPFb_B0sBm_s_p;
	TProfile* prMPFb_LbBb_s_p;

	TProfile* prMPFb_B0Bp_s_m;
	TProfile* prMPFb_B0sBm_s_m;
	TProfile* prMPFb_LbBb_s_m;

  //Read the normal sample
	f_normal->GetObject("pr2DMPFb",pr2DMPFb);

	f_normal->GetObject("pr2DMPFb_B0Bp",pr2DMPFb_B0Bp);
	f_normal->GetObject("pr2DMPFb_B0sBm",pr2DMPFb_B0sBm);
	f_normal->GetObject("pr2DMPFb_LbBb",pr2DMPFb_LbBb);

  //Read the weighted sample
	f_scaled->GetObject("pr2DMPFb",pr2DMPFb_s);

	f_scaled->GetObject("pr2DMPFb_B0Bp",pr2DMPFb_B0Bp_s);
	f_scaled->GetObject("pr2DMPFb_B0sBm",pr2DMPFb_B0sBm_s);
	f_scaled->GetObject("pr2DMPFb_LbBb",pr2DMPFb_LbBb_s);

	f_scaled_p->GetObject("pr2DMPFb",pr2DMPFb_s_p);

	f_scaled_p->GetObject("pr2DMPFb_B0Bp",pr2DMPFb_B0Bp_s_p);
	f_scaled_p->GetObject("pr2DMPFb_B0sBm",pr2DMPFb_B0sBm_s_p);
	f_scaled_p->GetObject("pr2DMPFb_LbBb",pr2DMPFb_LbBb_s_p);

	f_scaled_m->GetObject("pr2DMPFb",pr2DMPFb_s_m);

	f_scaled_m->GetObject("pr2DMPFb_B0Bp",pr2DMPFb_B0Bp_s_m);
	f_scaled_m->GetObject("pr2DMPFb_B0sBm",pr2DMPFb_B0sBm_s_m);
	f_scaled_m->GetObject("pr2DMPFb_LbBb",pr2DMPFb_LbBb_s_m);

	//calculate the difference in response between normal and the rescaled sample 
	//the second ProjectionX has new name as argument, otherwise would have same name as the previous
	//histo and causes memory leak 
	TH1D* hMPFb     = prMPFb   ->ProjectionX();
	TH1D* hMPFb_s   = prMPFb_s ->ProjectionX("pr2DMPFb_s");
	TH1D* hMPFb_s_p = prMPFb_s_p ->ProjectionX("pr2DMPFb_s_p");
	TH1D* hMPFb_s_m = prMPFb_s_m ->ProjectionX("pr2DMPFb_s_m");

	TH1D* diffb 		= (TH1D*) hMPFb_s->Clone();
	TH1D* diffb_p 	= (TH1D*) hMPFb_s_p->Clone();
	TH1D* diffb_m 	= (TH1D*) hMPFb_s_m->Clone();

	diffb->Add(hMPFb,-1);
	diffb_p->Add(hMPFb,-1);
	diffb_m->Add(hMPFb,-1);

	//evtW_* = event weight, sem_* = standard errorb of the mean,   
	double errorb, evtWtot, evtWtots;

	double evtWtot_B0Bp;
	double evtWtot_B0sBm;
	double evtWtot_LbBb;

	double evtWtots_B0Bp;
	double evtWtots_B0sBm;
	double evtWtots_LbBb;

	vector<double> evtW_B0Bp;
	vector<double> evtW_B0sBm;
	vector<double> evtW_LbBb;

	vector<double> evtWs_B0Bp;
	vector<double> evtWs_B0sBm;
	vector<double> evtWs_LbBb;

	vector<double> sem_B0Bp;
	vector<double> sem_B0sBm;
	vector<double> sem_LbBb;

	vector<double> vals_B0Bp;
	vector<double> vals_B0sBm;
	vector<double> vals_LbBb;

	vector<double> vec_B0Bp_stat, vec_B0sBm_stat, vec_LbBb_stat;

	ReadFromFile(vec_B0Bp_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BprodFrac/calc_scale_factors/scale_factors/BprodFrac/stat_err/B0Bp_sf_stat.txt");
	ReadFromFile(vec_B0sBm_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BprodFrac/calc_scale_factors/scale_factors/BprodFrac/stat_err/B0sBm_sf_stat.txt");
	ReadFromFile(vec_LbBb_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BprodFrac/calc_scale_factors/scale_factors/BprodFrac/stat_err/LbBb_sf_stat.txt");

	vector<vector<double>> vec_sf_sem{vec_B0Bp_stat, vec_B0sBm_stat, vec_LbBb_stat}; 

	//removing the under- and overflow bin values
	for (int i = 0; i < vec_sf_sem.size(); i++) {
		vec_sf_sem[i].erase(vec_sf_sem[i].begin() + 0);
		vec_sf_sem[i].erase(vec_sf_sem[i].begin() + vec_sf_sem[i].size() - 1);
	}

  //Loop over the TProfile bins
  for (int i = 1; i < hMPFb->GetNbinsX()+1; i++){
		evtW_B0Bp.clear();
		evtW_B0sBm.clear();
		evtW_LbBb.clear();

		evtWs_B0Bp.clear();
		evtWs_B0sBm.clear();
		evtWs_LbBb.clear();

		sem_B0Bp.clear();
		sem_B0sBm.clear();
		sem_LbBb.clear();

		for (int j = 1; j < hMPFb->GetNbinsX()+1; j++) {
			string pr_ind = "_" + to_string(i) + "_" + to_string(j);

			prMPFb_B0Bp = pr2DMPFb_B0Bp->ProfileX(("B0Bp"+pr_ind).c_str(),j,j);
			prMPFb_B0sBm = pr2DMPFb_B0sBm->ProfileX(("B0sBm"+pr_ind).c_str(),j,j);
			prMPFb_LbBb = pr2DMPFb_LbBb->ProfileX(("LbBb"+pr_ind).c_str(),j,j);

			prMPFb_B0Bp_s = pr2DMPFb_B0Bp_s->ProfileX(("B0Bp_s"+pr_ind).c_str(),j,j);
			prMPFb_B0sBm_s = pr2DMPFb_B0sBm_s->ProfileX(("B0sBm_s"+pr_ind).c_str(),j,j);
			prMPFb_LbBb_s = pr2DMPFb_LbBb_s->ProfileX(("LbBb_s"+pr_ind).c_str(),j,j);

			evtW_B0Bp.push_back(prMPFb_B0Bp->GetBinEntries(i));
			evtW_B0sBm.push_back(prMPFb_B0sBm->GetBinEntries(i));
			evtW_LbBb.push_back(prMPFb_LbBb->GetBinEntries(i));

			evtWs_B0Bp.push_back(prMPFb_B0Bp_s->GetBinEntries(i));
			evtWs_B0sBm.push_back(prMPFb_B0sBm_s->GetBinEntries(i));
			evtWs_LbBb.push_back(prMPFb_LbBb_s->GetBinEntries(i));

			sem_B0Bp.push_back(prMPFb_B0Bp->GetBinError(i));
			sem_B0sBm.push_back(prMPFb_B0sBm->GetBinError(i));
			sem_LbBb.push_back(prMPFb_LbBb->GetBinError(i));

			vals_B0Bp.push_back(prMPFb_B0Bp->GetBinContent(i));
			vals_B0sBm.push_back(prMPFb_B0sBm->GetBinContent(i));
			vals_LbBb.push_back(prMPFb_LbBb->GetBinContent(i));

			delete prMPFb_B0Bp;
			delete prMPFb_B0sBm;
			delete prMPFb_LbBb;

			delete prMPFb_B0Bp_s;
			delete prMPFb_B0sBm_s;
			delete prMPFb_LbBb_s;
		}

		vector<vector<double>> vec_evtW{evtW_B0Bp, evtW_B0sBm, evtW_LbBb};

		vector<vector<double>> vec_evtWs{evtWs_B0Bp, evtWs_B0sBm, evtWs_LbBb};

		vector<vector<double>> vec_sem{sem_B0Bp, sem_B0sBm, sem_LbBb};

		vector<vector<double>> vec_vals{vals_B0Bp, vals_B0sBm, vals_LbBb};

		//set small number so that we dont divide by zero 
		evtWtot_B0Bp = 1e-10;
		evtWtot_B0sBm = 1e-10;
		evtWtot_LbBb = 1e-10;

		evtWtots_B0Bp = 1e-10;
		evtWtots_B0sBm = 1e-10;
		evtWtots_LbBb = 1e-10;

		for (double x : evtW_B0Bp) evtWtot_B0Bp += x;	
		for (double x : evtW_B0sBm) evtWtot_B0sBm += x;
		for (double x : evtW_LbBb) evtWtot_LbBb += x;

		for (double x : evtWs_B0Bp) evtWtots_B0Bp += x;
		for (double x : evtWs_B0sBm) evtWtots_B0sBm += x;
		for (double x : evtWs_LbBb) evtWtots_LbBb += x;

		evtWtot = evtWtot_B0Bp + evtWtot_B0sBm + evtWtot_LbBb;

		evtWtots = evtWtots_B0Bp + evtWtots_B0sBm + evtWtots_LbBb;

		//loop over different scale factors
		errorb = 0;
		for (int j = 0; j < hMPFb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hMPFb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}

		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb->GetBinContent(i) == 0) diffb->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb->SetBinError(i,errorb);
  }

	//############### plus variation ####################
  //Loop over the TProfile bins
  for (int i = 1; i < hMPFb->GetNbinsX()+1; i++){
		evtW_B0Bp.clear();
		evtW_B0sBm.clear();
		evtW_LbBb.clear();

		evtWs_B0Bp.clear();
		evtWs_B0sBm.clear();
		evtWs_LbBb.clear();

		sem_B0Bp.clear();
		sem_B0sBm.clear();
		sem_LbBb.clear();

		vals_B0Bp.clear();
		vals_B0sBm.clear();
		vals_LbBb.clear();

		for (int j = 1; j < hMPFb->GetNbinsX()+1; j++) {
			string pr_ind = "p_" + to_string(i) + "_" + to_string(j);

			prMPFb_B0Bp = pr2DMPFb_B0Bp->ProfileX(("B0Bp"+pr_ind).c_str(),j,j);
			prMPFb_B0sBm = pr2DMPFb_B0sBm->ProfileX(("B0sBm"+pr_ind).c_str(),j,j);
			prMPFb_LbBb = pr2DMPFb_LbBb->ProfileX(("LbBb"+pr_ind).c_str(),j,j);

			prMPFb_B0Bp_s_p = pr2DMPFb_B0Bp_s_p->ProfileX(("B0Bp_s"+pr_ind).c_str(),j,j);
			prMPFb_B0sBm_s_p = pr2DMPFb_B0sBm_s_p->ProfileX(("B0sBm_s"+pr_ind).c_str(),j,j);
			prMPFb_LbBb_s_p = pr2DMPFb_LbBb_s_p->ProfileX(("LbBb_s"+pr_ind).c_str(),j,j);

			evtW_B0Bp.push_back(prMPFb_B0Bp->GetBinEntries(i));
			evtW_B0sBm.push_back(prMPFb_B0sBm->GetBinEntries(i));
			evtW_LbBb.push_back(prMPFb_LbBb->GetBinEntries(i));

			evtWs_B0Bp.push_back(prMPFb_B0Bp_s_p->GetBinEntries(i));
			evtWs_B0sBm.push_back(prMPFb_B0sBm_s_p->GetBinEntries(i));
			evtWs_LbBb.push_back(prMPFb_LbBb_s_p->GetBinEntries(i));

			sem_B0Bp.push_back(prMPFb_B0Bp->GetBinError(i));
			sem_B0sBm.push_back(prMPFb_B0sBm->GetBinError(i));
			sem_LbBb.push_back(prMPFb_LbBb->GetBinError(i));

			vals_B0Bp.push_back(prMPFb_B0Bp->GetBinContent(i));
			vals_B0sBm.push_back(prMPFb_B0sBm->GetBinContent(i));
			vals_LbBb.push_back(prMPFb_LbBb->GetBinContent(i));

			delete prMPFb_B0Bp;
			delete prMPFb_B0sBm;
			delete prMPFb_LbBb;

			delete prMPFb_B0Bp_s_p;
			delete prMPFb_B0sBm_s_p;
			delete prMPFb_LbBb_s_p;
		}

		vector<vector<double>> vec_evtW{evtW_B0Bp, evtW_B0sBm, evtW_LbBb};

		vector<vector<double>> vec_evtWs{evtWs_B0Bp, evtWs_B0sBm, evtWs_LbBb};

		vector<vector<double>> vec_sem{sem_B0Bp, sem_B0sBm, sem_LbBb};

		vector<vector<double>> vec_vals{vals_B0Bp, vals_B0sBm, vals_LbBb};

		//set small number so that we dont divide by zero 
		evtWtot_B0Bp = 1e-10;
		evtWtot_B0sBm = 1e-10;
		evtWtot_LbBb = 1e-10;

		evtWtots_B0Bp = 1e-10;
		evtWtots_B0sBm = 1e-10;
		evtWtots_LbBb = 1e-10;

		for (double x : evtW_B0Bp) evtWtot_B0Bp += x;	
		for (double x : evtW_B0sBm) evtWtot_B0sBm += x;
		for (double x : evtW_LbBb) evtWtot_LbBb += x;

		for (double x : evtWs_B0Bp) evtWtots_B0Bp += x;
		for (double x : evtWs_B0sBm) evtWtots_B0sBm += x;
		for (double x : evtWs_LbBb) evtWtots_LbBb += x;

		evtWtot = evtWtot_B0Bp + evtWtot_B0sBm + evtWtot_LbBb;

		evtWtots = evtWtots_B0Bp + evtWtots_B0sBm + evtWtots_LbBb;

		//loop over different scale factors
		errorb = 0;
		for (int j = 0; j < hMPFb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hMPFb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}

		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb_p->GetBinContent(i) == 0) diffb_p->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb_p->SetBinError(i,errorb);
  }

	//############### minus variation ####################
  //Loop over the TProfile bins
  for (int i = 1; i < hMPFb->GetNbinsX()+1; i++){
		evtW_B0Bp.clear();
		evtW_B0sBm.clear();
		evtW_LbBb.clear();

		evtWs_B0Bp.clear();
		evtWs_B0sBm.clear();
		evtWs_LbBb.clear();

		sem_B0Bp.clear();
		sem_B0sBm.clear();
		sem_LbBb.clear();

		vals_B0Bp.clear();
		vals_B0sBm.clear();
		vals_LbBb.clear();

		for (int j = 1; j < hMPFb->GetNbinsX()+1; j++) {
			string pr_ind = "m_" + to_string(i) + "_" + to_string(j);

			prMPFb_B0Bp = pr2DMPFb_B0Bp->ProfileX(("B0Bp"+pr_ind).c_str(),j,j);
			prMPFb_B0sBm = pr2DMPFb_B0sBm->ProfileX(("B0sBm"+pr_ind).c_str(),j,j);
			prMPFb_LbBb = pr2DMPFb_LbBb->ProfileX(("LbBb"+pr_ind).c_str(),j,j);

			prMPFb_B0Bp_s_m = pr2DMPFb_B0Bp_s_m->ProfileX(("B0Bp_s"+pr_ind).c_str(),j,j);
			prMPFb_B0sBm_s_m = pr2DMPFb_B0sBm_s_m->ProfileX(("B0sBm_s"+pr_ind).c_str(),j,j);
			prMPFb_LbBb_s_m = pr2DMPFb_LbBb_s_m->ProfileX(("LbBb_s"+pr_ind).c_str(),j,j);

			evtW_B0Bp.push_back(prMPFb_B0Bp->GetBinEntries(i));
			evtW_B0sBm.push_back(prMPFb_B0sBm->GetBinEntries(i));
			evtW_LbBb.push_back(prMPFb_LbBb->GetBinEntries(i));

			evtWs_B0Bp.push_back(prMPFb_B0Bp_s_m->GetBinEntries(i));
			evtWs_B0sBm.push_back(prMPFb_B0sBm_s_m->GetBinEntries(i));
			evtWs_LbBb.push_back(prMPFb_LbBb_s_m->GetBinEntries(i));

			sem_B0Bp.push_back(prMPFb_B0Bp->GetBinError(i));
			sem_B0sBm.push_back(prMPFb_B0sBm->GetBinError(i));
			sem_LbBb.push_back(prMPFb_LbBb->GetBinError(i));

			vals_B0Bp.push_back(prMPFb_B0Bp->GetBinContent(i));
			vals_B0sBm.push_back(prMPFb_B0sBm->GetBinContent(i));
			vals_LbBb.push_back(prMPFb_LbBb->GetBinContent(i));

			delete prMPFb_B0Bp;
			delete prMPFb_B0sBm;
			delete prMPFb_LbBb;

			delete prMPFb_B0Bp_s_m;
			delete prMPFb_B0sBm_s_m;
			delete prMPFb_LbBb_s_m;
		}

		vector<vector<double>> vec_evtW{evtW_B0Bp, evtW_B0sBm, evtW_LbBb};

		vector<vector<double>> vec_evtWs{evtWs_B0Bp, evtWs_B0sBm, evtWs_LbBb};

		vector<vector<double>> vec_sem{sem_B0Bp, sem_B0sBm, sem_LbBb};

		vector<vector<double>> vec_vals{vals_B0Bp, vals_B0sBm, vals_LbBb};

		//set small number so that we dont divide by zero 
		evtWtot_B0Bp = 1e-10;
		evtWtot_B0sBm = 1e-10;
		evtWtot_LbBb = 1e-10;

		evtWtots_B0Bp = 1e-10;
		evtWtots_B0sBm = 1e-10;
		evtWtots_LbBb = 1e-10;

		for (double x : evtW_B0Bp) evtWtot_B0Bp += x;	
		for (double x : evtW_B0sBm) evtWtot_B0sBm += x;
		for (double x : evtW_LbBb) evtWtot_LbBb += x;

		for (double x : evtWs_B0Bp) evtWtots_B0Bp += x;
		for (double x : evtWs_B0sBm) evtWtots_B0sBm += x;
		for (double x : evtWs_LbBb) evtWtots_LbBb += x;

		evtWtot = evtWtot_B0Bp + evtWtot_B0sBm + evtWtot_LbBb;

		evtWtots = evtWtots_B0Bp + evtWtots_B0sBm + evtWtots_LbBb;

		//loop over different scale factors
		errorb = 0;
		for (int j = 0; j < hMPFb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hMPFb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}

		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb_m->GetBinContent(i) == 0) diffb_m->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb_m->SetBinError(i,errorb);
  }  

	//Scale to the percentages
  diffb->Scale(100.0);
  diffb_p->Scale(100.0);
  diffb_m->Scale(100.0);

	MPFb_map["diffb"] = diffb;
	MPFb_map["diffb_p"] = diffb_p;
	MPFb_map["diffb_m"] = diffb_m;

	return MPFb_map;
}

map<string, TH1D*> pTbal_BprodFrac() {
	map <string, TH1D*> pTbalb_map;

  TFile* f_normal = TFile::Open(OpenName.c_str());
  TFile* f_scaled = TFile::Open(OpenName_BprodFrac.c_str());
  TFile* f_scaled_p = TFile::Open(OpenName_BprodFrac_p.c_str());
  TFile* f_scaled_m = TFile::Open(OpenName_BprodFrac_m.c_str());

	TProfile *prpTbalb;
	TProfile *prpTbalb_s;
	TProfile *prpTbalb_s_p;
	TProfile *prpTbalb_s_m;

  //Read the normal sample
  f_normal->GetObject("prpTbalb",   prpTbalb);

  //Read the weighted sample
  f_scaled->GetObject("prpTbalb",   prpTbalb_s);
  f_scaled_p->GetObject("prpTbalb",   prpTbalb_s_p);
  f_scaled_m->GetObject("prpTbalb",   prpTbalb_s_m);

	//2D Tprofiles for the errorb calculations
	TProfile2D* pr2DpTbalb;

	TProfile2D* pr2DpTbalb_B0Bp;
	TProfile2D* pr2DpTbalb_B0sBm;
	TProfile2D* pr2DpTbalb_LbBb;

  //The sample with the additional weighting (*_s)
	TProfile2D* pr2DpTbalb_s;

	TProfile2D* pr2DpTbalb_B0Bp_s;
	TProfile2D* pr2DpTbalb_B0sBm_s;
	TProfile2D* pr2DpTbalb_LbBb_s;

	TProfile2D* pr2DpTbalb_s_p;

	TProfile2D* pr2DpTbalb_B0Bp_s_p;
	TProfile2D* pr2DpTbalb_B0sBm_s_p;
	TProfile2D* pr2DpTbalb_LbBb_s_p;

	TProfile2D* pr2DpTbalb_s_m;

	TProfile2D* pr2DpTbalb_B0Bp_s_m;
	TProfile2D* pr2DpTbalb_B0sBm_s_m;
	TProfile2D* pr2DpTbalb_LbBb_s_m;

	//Tprofiles for the errorb calculations
	TProfile* prpTbalb_B0Bp;
	TProfile* prpTbalb_B0sBm;
	TProfile* prpTbalb_LbBb;

  //The sample with the additional weighting (*_s)
	TProfile* prpTbalb_B0Bp_s;
	TProfile* prpTbalb_B0sBm_s;
	TProfile* prpTbalb_LbBb_s;

	TProfile* prpTbalb_B0Bp_s_p;
	TProfile* prpTbalb_B0sBm_s_p;
	TProfile* prpTbalb_LbBb_s_p;

	TProfile* prpTbalb_B0Bp_s_m;
	TProfile* prpTbalb_B0sBm_s_m;
	TProfile* prpTbalb_LbBb_s_m;

  //Read the normal sample
	f_normal->GetObject("pr2DpTbalb",pr2DpTbalb);

	f_normal->GetObject("pr2DpTbalb_B0Bp",pr2DpTbalb_B0Bp);
	f_normal->GetObject("pr2DpTbalb_B0sBm",pr2DpTbalb_B0sBm);
	f_normal->GetObject("pr2DpTbalb_LbBb",pr2DpTbalb_LbBb);

  //Read the weighted sample
	f_scaled->GetObject("pr2DpTbalb",pr2DpTbalb_s);

	f_scaled->GetObject("pr2DpTbalb_B0Bp",pr2DpTbalb_B0Bp_s);
	f_scaled->GetObject("pr2DpTbalb_B0sBm",pr2DpTbalb_B0sBm_s);
	f_scaled->GetObject("pr2DpTbalb_LbBb",pr2DpTbalb_LbBb_s);

	f_scaled_p->GetObject("pr2DpTbalb",pr2DpTbalb_s_p);

	f_scaled_p->GetObject("pr2DpTbalb_B0Bp",pr2DpTbalb_B0Bp_s_p);
	f_scaled_p->GetObject("pr2DpTbalb_B0sBm",pr2DpTbalb_B0sBm_s_p);
	f_scaled_p->GetObject("pr2DpTbalb_LbBb",pr2DpTbalb_LbBb_s_p);

	f_scaled_m->GetObject("pr2DpTbalb",pr2DpTbalb_s_m);

	f_scaled_m->GetObject("pr2DpTbalb_B0Bp",pr2DpTbalb_B0Bp_s_m);
	f_scaled_m->GetObject("pr2DpTbalb_B0sBm",pr2DpTbalb_B0sBm_s_m);
	f_scaled_m->GetObject("pr2DpTbalb_LbBb",pr2DpTbalb_LbBb_s_m);

	//calculate the difference in response between normal and the rescaled sample 
	//the second ProjectionX has new name as argument, otherwise would have same name as the previous
	//histo and causes memory leak 
	TH1D* hpTbalb     = prpTbalb   ->ProjectionX();
	TH1D* hpTbalb_s   = prpTbalb_s ->ProjectionX("pr2DpTbalb_s");
	TH1D* hpTbalb_s_p = prpTbalb_s_p ->ProjectionX("pr2DpTbalb_s_p");
	TH1D* hpTbalb_s_m = prpTbalb_s_m ->ProjectionX("pr2DpTbalb_s_m");

	TH1D* diffb 		= (TH1D*) hpTbalb_s->Clone();
	TH1D* diffb_p 	= (TH1D*) hpTbalb_s_p->Clone();
	TH1D* diffb_m 	= (TH1D*) hpTbalb_s_m->Clone();

	diffb->Add(hpTbalb,-1);
	diffb_p->Add(hpTbalb,-1);
	diffb_m->Add(hpTbalb,-1);

	//evtW_* = event weight, sem_* = standard errorb of the mean,   
	double errorb, evtWtot, evtWtots;

	double evtWtot_B0Bp;
	double evtWtot_B0sBm;
	double evtWtot_LbBb;

	double evtWtots_B0Bp;
	double evtWtots_B0sBm;
	double evtWtots_LbBb;

	vector<double> evtW_B0Bp;
	vector<double> evtW_B0sBm;
	vector<double> evtW_LbBb;

	vector<double> evtWs_B0Bp;
	vector<double> evtWs_B0sBm;
	vector<double> evtWs_LbBb;

	vector<double> sem_B0Bp;
	vector<double> sem_B0sBm;
	vector<double> sem_LbBb;

	vector<double> vals_B0Bp;
	vector<double> vals_B0sBm;
	vector<double> vals_LbBb;

	vector<double> vec_B0Bp_stat, vec_B0sBm_stat, vec_LbBb_stat;

	ReadFromFile(vec_B0Bp_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BprodFrac/calc_scale_factors/scale_factors/BprodFrac/stat_err/B0Bp_sf_stat.txt");
	ReadFromFile(vec_B0sBm_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BprodFrac/calc_scale_factors/scale_factors/BprodFrac/stat_err/B0sBm_sf_stat.txt");
	ReadFromFile(vec_LbBb_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BprodFrac/calc_scale_factors/scale_factors/BprodFrac/stat_err/LbBb_sf_stat.txt");

	vector<vector<double>> vec_sf_sem{vec_B0Bp_stat, vec_B0sBm_stat, vec_LbBb_stat}; 

	//removing the under- and overflow bin values
	for (int i = 0; i < vec_sf_sem.size(); i++) {
		vec_sf_sem[i].erase(vec_sf_sem[i].begin() + 0);
		vec_sf_sem[i].erase(vec_sf_sem[i].begin() + vec_sf_sem[i].size() - 1);
	}

  //Loop over the TProfile bins
  for (int i = 1; i < hpTbalb->GetNbinsX()+1; i++){
		evtW_B0Bp.clear();
		evtW_B0sBm.clear();
		evtW_LbBb.clear();

		evtWs_B0Bp.clear();
		evtWs_B0sBm.clear();
		evtWs_LbBb.clear();

		sem_B0Bp.clear();
		sem_B0sBm.clear();
		sem_LbBb.clear();

		vals_B0Bp.clear();
		vals_B0sBm.clear();
		vals_LbBb.clear();

		for (int j = 1; j < hpTbalb->GetNbinsX()+1; j++) {
			string pr_ind = "_" + to_string(i) + "_" + to_string(j);

			prpTbalb_B0Bp = pr2DpTbalb_B0Bp->ProfileX(("B0Bp"+pr_ind).c_str(),j,j);
			prpTbalb_B0sBm = pr2DpTbalb_B0sBm->ProfileX(("B0sBm"+pr_ind).c_str(),j,j);
			prpTbalb_LbBb = pr2DpTbalb_LbBb->ProfileX(("LbBb"+pr_ind).c_str(),j,j);

			prpTbalb_B0Bp_s = pr2DpTbalb_B0Bp_s->ProfileX(("B0Bp_s"+pr_ind).c_str(),j,j);
			prpTbalb_B0sBm_s = pr2DpTbalb_B0sBm_s->ProfileX(("B0sBm_s"+pr_ind).c_str(),j,j);
			prpTbalb_LbBb_s = pr2DpTbalb_LbBb_s->ProfileX(("LbBb_s"+pr_ind).c_str(),j,j);

			evtW_B0Bp.push_back(prpTbalb_B0Bp->GetBinEntries(i));
			evtW_B0sBm.push_back(prpTbalb_B0sBm->GetBinEntries(i));
			evtW_LbBb.push_back(prpTbalb_LbBb->GetBinEntries(i));

			evtWs_B0Bp.push_back(prpTbalb_B0Bp_s->GetBinEntries(i));
			evtWs_B0sBm.push_back(prpTbalb_B0sBm_s->GetBinEntries(i));
			evtWs_LbBb.push_back(prpTbalb_LbBb_s->GetBinEntries(i));

			sem_B0Bp.push_back(prpTbalb_B0Bp->GetBinError(i));
			sem_B0sBm.push_back(prpTbalb_B0sBm->GetBinError(i));
			sem_LbBb.push_back(prpTbalb_LbBb->GetBinError(i));

			vals_B0Bp.push_back(prpTbalb_B0Bp->GetBinContent(i));
			vals_B0sBm.push_back(prpTbalb_B0sBm->GetBinContent(i));
			vals_LbBb.push_back(prpTbalb_LbBb->GetBinContent(i));

			delete prpTbalb_B0Bp;
			delete prpTbalb_B0sBm;
			delete prpTbalb_LbBb;

			delete prpTbalb_B0Bp_s;
			delete prpTbalb_B0sBm_s;
			delete prpTbalb_LbBb_s;
		}

		vector<vector<double>> vec_evtW{evtW_B0Bp, evtW_B0sBm, evtW_LbBb};

		vector<vector<double>> vec_evtWs{evtWs_B0Bp, evtWs_B0sBm, evtWs_LbBb};

		vector<vector<double>> vec_sem{sem_B0Bp, sem_B0sBm, sem_LbBb};

		vector<vector<double>> vec_vals{vals_B0Bp, vals_B0sBm, vals_LbBb};

		//set small number so that we dont divide by zero 
		evtWtot_B0Bp = 1e-10;
		evtWtot_B0sBm = 1e-10;
		evtWtot_LbBb = 1e-10;

		evtWtots_B0Bp = 1e-10;
		evtWtots_B0sBm = 1e-10;
		evtWtots_LbBb = 1e-10;

		for (double x : evtW_B0Bp) evtWtot_B0Bp += x;	
		for (double x : evtW_B0sBm) evtWtot_B0sBm += x;
		for (double x : evtW_LbBb) evtWtot_LbBb += x;

		for (double x : evtWs_B0Bp) evtWtots_B0Bp += x;
		for (double x : evtWs_B0sBm) evtWtots_B0sBm += x;
		for (double x : evtWs_LbBb) evtWtots_LbBb += x;

		evtWtot = evtWtot_B0Bp + evtWtot_B0sBm + evtWtot_LbBb;

		evtWtots = evtWtots_B0Bp + evtWtots_B0sBm + evtWtots_LbBb;

		//loop over different scale factors
		errorb = 0;
		for (int j = 0; j < hpTbalb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hpTbalb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}

		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb->GetBinContent(i) == 0) diffb->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb->SetBinError(i,errorb);
  }

	//############### plus variation ####################
  //Loop over the TProfile bins
  for (int i = 1; i < hpTbalb->GetNbinsX()+1; i++){
		evtW_B0Bp.clear();
		evtW_B0sBm.clear();
		evtW_LbBb.clear();

		evtWs_B0Bp.clear();
		evtWs_B0sBm.clear();
		evtWs_LbBb.clear();

		sem_B0Bp.clear();
		sem_B0sBm.clear();
		sem_LbBb.clear();

		vals_B0Bp.clear();
		vals_B0sBm.clear();
		vals_LbBb.clear();

		for (int j = 1; j < hpTbalb->GetNbinsX()+1; j++) {
			string pr_ind = "p_" + to_string(i) + "_" + to_string(j);

			prpTbalb_B0Bp = pr2DpTbalb_B0Bp->ProfileX(("B0Bp"+pr_ind).c_str(),j,j);
			prpTbalb_B0sBm = pr2DpTbalb_B0sBm->ProfileX(("B0sBm"+pr_ind).c_str(),j,j);
			prpTbalb_LbBb = pr2DpTbalb_LbBb->ProfileX(("LbBb"+pr_ind).c_str(),j,j);

			prpTbalb_B0Bp_s_p = pr2DpTbalb_B0Bp_s_p->ProfileX(("B0Bp_s"+pr_ind).c_str(),j,j);
			prpTbalb_B0sBm_s_p = pr2DpTbalb_B0sBm_s_p->ProfileX(("B0sBm_s"+pr_ind).c_str(),j,j);
			prpTbalb_LbBb_s_p = pr2DpTbalb_LbBb_s_p->ProfileX(("LbBb_s"+pr_ind).c_str(),j,j);

			evtW_B0Bp.push_back(prpTbalb_B0Bp->GetBinEntries(i));
			evtW_B0sBm.push_back(prpTbalb_B0sBm->GetBinEntries(i));
			evtW_LbBb.push_back(prpTbalb_LbBb->GetBinEntries(i));

			evtWs_B0Bp.push_back(prpTbalb_B0Bp_s_p->GetBinEntries(i));
			evtWs_B0sBm.push_back(prpTbalb_B0sBm_s_p->GetBinEntries(i));
			evtWs_LbBb.push_back(prpTbalb_LbBb_s_p->GetBinEntries(i));

			sem_B0Bp.push_back(prpTbalb_B0Bp->GetBinError(i));
			sem_B0sBm.push_back(prpTbalb_B0sBm->GetBinError(i));
			sem_LbBb.push_back(prpTbalb_LbBb->GetBinError(i));

			vals_B0Bp.push_back(prpTbalb_B0Bp->GetBinContent(i));
			vals_B0sBm.push_back(prpTbalb_B0sBm->GetBinContent(i));
			vals_LbBb.push_back(prpTbalb_LbBb->GetBinContent(i));

			delete prpTbalb_B0Bp;
			delete prpTbalb_B0sBm;
			delete prpTbalb_LbBb;

			delete prpTbalb_B0Bp_s_p;
			delete prpTbalb_B0sBm_s_p;
			delete prpTbalb_LbBb_s_p;
		}

		vector<vector<double>> vec_evtW{evtW_B0Bp, evtW_B0sBm, evtW_LbBb};

		vector<vector<double>> vec_evtWs{evtWs_B0Bp, evtWs_B0sBm, evtWs_LbBb};

		vector<vector<double>> vec_sem{sem_B0Bp, sem_B0sBm, sem_LbBb};

		vector<vector<double>> vec_vals{vals_B0Bp, vals_B0sBm, vals_LbBb};

		//set small number so that we dont divide by zero 
		evtWtot_B0Bp = 1e-10;
		evtWtot_B0sBm = 1e-10;
		evtWtot_LbBb = 1e-10;

		evtWtots_B0Bp = 1e-10;
		evtWtots_B0sBm = 1e-10;
		evtWtots_LbBb = 1e-10;

		for (double x : evtW_B0Bp) evtWtot_B0Bp += x;	
		for (double x : evtW_B0sBm) evtWtot_B0sBm += x;
		for (double x : evtW_LbBb) evtWtot_LbBb += x;

		for (double x : evtWs_B0Bp) evtWtots_B0Bp += x;
		for (double x : evtWs_B0sBm) evtWtots_B0sBm += x;
		for (double x : evtWs_LbBb) evtWtots_LbBb += x;

		evtWtot = evtWtot_B0Bp + evtWtot_B0sBm + evtWtot_LbBb;

		evtWtots = evtWtots_B0Bp + evtWtots_B0sBm + evtWtots_LbBb;

		//loop over different scale factors
		errorb = 0;
		for (int j = 0; j < hpTbalb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hpTbalb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}

		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb_p->GetBinContent(i) == 0) diffb_p->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb_p->SetBinError(i,errorb);
  }

	//############### minus variation ####################
  //Loop over the TProfile bins
  for (int i = 1; i < hpTbalb->GetNbinsX()+1; i++){
		evtW_B0Bp.clear();
		evtW_B0sBm.clear();
		evtW_LbBb.clear();

		evtWs_B0Bp.clear();
		evtWs_B0sBm.clear();
		evtWs_LbBb.clear();

		sem_B0Bp.clear();
		sem_B0sBm.clear();
		sem_LbBb.clear();

		vals_B0Bp.clear();
		vals_B0sBm.clear();
		vals_LbBb.clear();

		for (int j = 1; j < hpTbalb->GetNbinsX()+1; j++) {
			string pr_ind = "m_" + to_string(i) + "_" + to_string(j);

			prpTbalb_B0Bp = pr2DpTbalb_B0Bp->ProfileX(("B0Bp"+pr_ind).c_str(),j,j);
			prpTbalb_B0sBm = pr2DpTbalb_B0sBm->ProfileX(("B0sBm"+pr_ind).c_str(),j,j);
			prpTbalb_LbBb = pr2DpTbalb_LbBb->ProfileX(("LbBb"+pr_ind).c_str(),j,j);

			prpTbalb_B0Bp_s_m = pr2DpTbalb_B0Bp_s_m->ProfileX(("B0Bp_s"+pr_ind).c_str(),j,j);
			prpTbalb_B0sBm_s_m = pr2DpTbalb_B0sBm_s_m->ProfileX(("B0sBm_s"+pr_ind).c_str(),j,j);
			prpTbalb_LbBb_s_m = pr2DpTbalb_LbBb_s_m->ProfileX(("LbBb_s"+pr_ind).c_str(),j,j);

			evtW_B0Bp.push_back(prpTbalb_B0Bp->GetBinEntries(i));
			evtW_B0sBm.push_back(prpTbalb_B0sBm->GetBinEntries(i));
			evtW_LbBb.push_back(prpTbalb_LbBb->GetBinEntries(i));

			evtWs_B0Bp.push_back(prpTbalb_B0Bp_s_m->GetBinEntries(i));
			evtWs_B0sBm.push_back(prpTbalb_B0sBm_s_m->GetBinEntries(i));
			evtWs_LbBb.push_back(prpTbalb_LbBb_s_m->GetBinEntries(i));

			sem_B0Bp.push_back(prpTbalb_B0Bp->GetBinError(i));
			sem_B0sBm.push_back(prpTbalb_B0sBm->GetBinError(i));
			sem_LbBb.push_back(prpTbalb_LbBb->GetBinError(i));

			vals_B0Bp.push_back(prpTbalb_B0Bp->GetBinContent(i));
			vals_B0sBm.push_back(prpTbalb_B0sBm->GetBinContent(i));
			vals_LbBb.push_back(prpTbalb_LbBb->GetBinContent(i));

			delete prpTbalb_B0Bp;
			delete prpTbalb_B0sBm;
			delete prpTbalb_LbBb;

			delete prpTbalb_B0Bp_s_m;
			delete prpTbalb_B0sBm_s_m;
			delete prpTbalb_LbBb_s_m;
		}

		vector<vector<double>> vec_evtW{evtW_B0Bp, evtW_B0sBm, evtW_LbBb};

		vector<vector<double>> vec_evtWs{evtWs_B0Bp, evtWs_B0sBm, evtWs_LbBb};

		vector<vector<double>> vec_sem{sem_B0Bp, sem_B0sBm, sem_LbBb};

		vector<vector<double>> vec_vals{vals_B0Bp, vals_B0sBm, vals_LbBb};

		//set small number so that we dont divide by zero 
		evtWtot_B0Bp = 1e-10;
		evtWtot_B0sBm = 1e-10;
		evtWtot_LbBb = 1e-10;

		evtWtots_B0Bp = 1e-10;
		evtWtots_B0sBm = 1e-10;
		evtWtots_LbBb = 1e-10;

		for (double x : evtW_B0Bp) evtWtot_B0Bp += x;	
		for (double x : evtW_B0sBm) evtWtot_B0sBm += x;
		for (double x : evtW_LbBb) evtWtot_LbBb += x;

		for (double x : evtWs_B0Bp) evtWtots_B0Bp += x;
		for (double x : evtWs_B0sBm) evtWtots_B0sBm += x;
		for (double x : evtWs_LbBb) evtWtots_LbBb += x;

		evtWtot = evtWtot_B0Bp + evtWtot_B0sBm + evtWtot_LbBb;

		evtWtots = evtWtots_B0Bp + evtWtots_B0sBm + evtWtots_LbBb;

		//loop over different scale factors
		errorb = 0;
		for (int j = 0; j < hpTbalb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hpTbalb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}

		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb_m->GetBinContent(i) == 0) diffb_m->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb_m->SetBinError(i,errorb);
  }  

	//Scale to the percentages
  diffb->Scale(100.0);
  diffb_p->Scale(100.0);
  diffb_m->Scale(100.0);

	pTbalb_map["diffb"] = diffb;
	pTbalb_map["diffb_p"] = diffb_p;
	pTbalb_map["diffb_m"] = diffb_m;

	return pTbalb_map;
}

map<string, TH1D*> Rjet_BprodFrac() {
	map <string, TH1D*> Rjetb_map;

  TFile* f_normal = TFile::Open(OpenName.c_str());
  TFile* f_scaled = TFile::Open(OpenName_BprodFrac.c_str());
  TFile* f_scaled_p = TFile::Open(OpenName_BprodFrac_p.c_str());
  TFile* f_scaled_m = TFile::Open(OpenName_BprodFrac_m.c_str());

	TProfile *prRjetb;
	TProfile *prRjetb_s;
	TProfile *prRjetb_s_p;
	TProfile *prRjetb_s_m;

  //Read the normal sample
  f_normal->GetObject("prRjetb",   prRjetb);

  //Read the weighted sample
  f_scaled->GetObject("prRjetb",   prRjetb_s);
  f_scaled_p->GetObject("prRjetb",   prRjetb_s_p);
  f_scaled_m->GetObject("prRjetb",   prRjetb_s_m);

	//2D Tprofiles for the errorb calculations
	TProfile2D* pr2DRjetb;

	TProfile2D* pr2DRjetb_B0Bp;
	TProfile2D* pr2DRjetb_B0sBm;
	TProfile2D* pr2DRjetb_LbBb;

  //The sample with the additional weighting (*_s)
	TProfile2D* pr2DRjetb_s;

	TProfile2D* pr2DRjetb_B0Bp_s;
	TProfile2D* pr2DRjetb_B0sBm_s;
	TProfile2D* pr2DRjetb_LbBb_s;

	TProfile2D* pr2DRjetb_s_p;

	TProfile2D* pr2DRjetb_B0Bp_s_p;
	TProfile2D* pr2DRjetb_B0sBm_s_p;
	TProfile2D* pr2DRjetb_LbBb_s_p;

	TProfile2D* pr2DRjetb_s_m;

	TProfile2D* pr2DRjetb_B0Bp_s_m;
	TProfile2D* pr2DRjetb_B0sBm_s_m;
	TProfile2D* pr2DRjetb_LbBb_s_m;

	//Tprofiles for the errorb calculations
	TProfile* prRjetb_B0Bp;
	TProfile* prRjetb_B0sBm;
	TProfile* prRjetb_LbBb;

  //The sample with the additional weighting (*_s)
	TProfile* prRjetb_B0Bp_s;
	TProfile* prRjetb_B0sBm_s;
	TProfile* prRjetb_LbBb_s;

	TProfile* prRjetb_B0Bp_s_p;
	TProfile* prRjetb_B0sBm_s_p;
	TProfile* prRjetb_LbBb_s_p;

	TProfile* prRjetb_B0Bp_s_m;
	TProfile* prRjetb_B0sBm_s_m;
	TProfile* prRjetb_LbBb_s_m;

  //Read the normal sample
	f_normal->GetObject("pr2DRjetb",pr2DRjetb);

	f_normal->GetObject("pr2DRjetb_B0Bp",pr2DRjetb_B0Bp);
	f_normal->GetObject("pr2DRjetb_B0sBm",pr2DRjetb_B0sBm);
	f_normal->GetObject("pr2DRjetb_LbBb",pr2DRjetb_LbBb);

  //Read the weighted sample
	f_scaled->GetObject("pr2DRjetb",pr2DRjetb_s);

	f_scaled->GetObject("pr2DRjetb_B0Bp",pr2DRjetb_B0Bp_s);
	f_scaled->GetObject("pr2DRjetb_B0sBm",pr2DRjetb_B0sBm_s);
	f_scaled->GetObject("pr2DRjetb_LbBb",pr2DRjetb_LbBb_s);

	f_scaled_p->GetObject("pr2DRjetb",pr2DRjetb_s_p);

	f_scaled_p->GetObject("pr2DRjetb_B0Bp",pr2DRjetb_B0Bp_s_p);
	f_scaled_p->GetObject("pr2DRjetb_B0sBm",pr2DRjetb_B0sBm_s_p);
	f_scaled_p->GetObject("pr2DRjetb_LbBb",pr2DRjetb_LbBb_s_p);

	f_scaled_m->GetObject("pr2DRjetb",pr2DRjetb_s_m);

	f_scaled_m->GetObject("pr2DRjetb_B0Bp",pr2DRjetb_B0Bp_s_m);
	f_scaled_m->GetObject("pr2DRjetb_B0sBm",pr2DRjetb_B0sBm_s_m);
	f_scaled_m->GetObject("pr2DRjetb_LbBb",pr2DRjetb_LbBb_s_m);

	//calculate the difference in response between normal and the rescaled sample 
	//the second ProjectionX has new name as argument, otherwise would have same name as the previous
	//histo and causes memory leak 
	TH1D* hRjetb     = prRjetb   ->ProjectionX();
	TH1D* hRjetb_s   = prRjetb_s ->ProjectionX("pr2DRjetb_s");
	TH1D* hRjetb_s_p = prRjetb_s_p ->ProjectionX("pr2DRjetb_s_p");
	TH1D* hRjetb_s_m = prRjetb_s_m ->ProjectionX("pr2DRjetb_s_m");

	TH1D* diffb 		= (TH1D*) hRjetb_s->Clone();
	TH1D* diffb_p 	= (TH1D*) hRjetb_s_p->Clone();
	TH1D* diffb_m 	= (TH1D*) hRjetb_s_m->Clone();

	diffb->Add(hRjetb,-1);
	diffb_p->Add(hRjetb,-1);
	diffb_m->Add(hRjetb,-1);

	//evtW_* = event weight, sem_* = standard errorb of the mean,   
	double errorb, evtWtot, evtWtots;

	double evtWtot_B0Bp;
	double evtWtot_B0sBm;
	double evtWtot_LbBb;

	double evtWtots_B0Bp;
	double evtWtots_B0sBm;
	double evtWtots_LbBb;

	vector<double> evtW_B0Bp;
	vector<double> evtW_B0sBm;
	vector<double> evtW_LbBb;

	vector<double> evtWs_B0Bp;
	vector<double> evtWs_B0sBm;
	vector<double> evtWs_LbBb;

	vector<double> sem_B0Bp;
	vector<double> sem_B0sBm;
	vector<double> sem_LbBb;

	vector<double> vals_B0Bp;
	vector<double> vals_B0sBm;
	vector<double> vals_LbBb;

	vector<double> vec_B0Bp_stat, vec_B0sBm_stat, vec_LbBb_stat;

	ReadFromFile(vec_B0Bp_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BprodFrac/calc_scale_factors/scale_factors/BprodFrac/stat_err/B0Bp_sf_stat.txt");
	ReadFromFile(vec_B0sBm_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BprodFrac/calc_scale_factors/scale_factors/BprodFrac/stat_err/B0sBm_sf_stat.txt");
	ReadFromFile(vec_LbBb_stat,"/home/anpirtti/ultimate_generator/final_rescaling/BprodFrac/calc_scale_factors/scale_factors/BprodFrac/stat_err/LbBb_sf_stat.txt");

	vector<vector<double>> vec_sf_sem{vec_B0Bp_stat, vec_B0sBm_stat, vec_LbBb_stat}; 

	//removing the under- and overflow bin values
	for (int i = 0; i < vec_sf_sem.size(); i++) {
		vec_sf_sem[i].erase(vec_sf_sem[i].begin() + 0);
		vec_sf_sem[i].erase(vec_sf_sem[i].begin() + vec_sf_sem[i].size() - 1);
	}

  //Loop over the TProfile bins
  for (int i = 1; i < hRjetb->GetNbinsX()+1; i++){
		evtW_B0Bp.clear();
		evtW_B0sBm.clear();
		evtW_LbBb.clear();

		evtWs_B0Bp.clear();
		evtWs_B0sBm.clear();
		evtWs_LbBb.clear();

		sem_B0Bp.clear();
		sem_B0sBm.clear();
		sem_LbBb.clear();

		vals_B0Bp.clear();
		vals_B0sBm.clear();
		vals_LbBb.clear();

		for (int j = 1; j < hRjetb->GetNbinsX()+1; j++) {
			string pr_ind = "_" + to_string(i) + "_" + to_string(j);

			prRjetb_B0Bp = pr2DRjetb_B0Bp->ProfileX(("B0Bp"+pr_ind).c_str(),j,j);
			prRjetb_B0sBm = pr2DRjetb_B0sBm->ProfileX(("B0sBm"+pr_ind).c_str(),j,j);
			prRjetb_LbBb = pr2DRjetb_LbBb->ProfileX(("LbBb"+pr_ind).c_str(),j,j);

			prRjetb_B0Bp_s = pr2DRjetb_B0Bp_s->ProfileX(("B0Bp_s"+pr_ind).c_str(),j,j);
			prRjetb_B0sBm_s = pr2DRjetb_B0sBm_s->ProfileX(("B0sBm_s"+pr_ind).c_str(),j,j);
			prRjetb_LbBb_s = pr2DRjetb_LbBb_s->ProfileX(("LbBb_s"+pr_ind).c_str(),j,j);

			evtW_B0Bp.push_back(prRjetb_B0Bp->GetBinEntries(i));
			evtW_B0sBm.push_back(prRjetb_B0sBm->GetBinEntries(i));
			evtW_LbBb.push_back(prRjetb_LbBb->GetBinEntries(i));

			evtWs_B0Bp.push_back(prRjetb_B0Bp_s->GetBinEntries(i));
			evtWs_B0sBm.push_back(prRjetb_B0sBm_s->GetBinEntries(i));
			evtWs_LbBb.push_back(prRjetb_LbBb_s->GetBinEntries(i));

			sem_B0Bp.push_back(prRjetb_B0Bp->GetBinError(i));
			sem_B0sBm.push_back(prRjetb_B0sBm->GetBinError(i));
			sem_LbBb.push_back(prRjetb_LbBb->GetBinError(i));

			vals_B0Bp.push_back(prRjetb_B0Bp->GetBinContent(i));
			vals_B0sBm.push_back(prRjetb_B0sBm->GetBinContent(i));
			vals_LbBb.push_back(prRjetb_LbBb->GetBinContent(i));

			delete prRjetb_B0Bp;
			delete prRjetb_B0sBm;
			delete prRjetb_LbBb;

			delete prRjetb_B0Bp_s;
			delete prRjetb_B0sBm_s;
			delete prRjetb_LbBb_s;
		}

		vector<vector<double>> vec_evtW{evtW_B0Bp, evtW_B0sBm, evtW_LbBb};

		vector<vector<double>> vec_evtWs{evtWs_B0Bp, evtWs_B0sBm, evtWs_LbBb};

		vector<vector<double>> vec_sem{sem_B0Bp, sem_B0sBm, sem_LbBb};

		vector<vector<double>> vec_vals{vals_B0Bp, vals_B0sBm, vals_LbBb};

		//set small number so that we dont divide by zero 
		evtWtot_B0Bp = 1e-10;
		evtWtot_B0sBm = 1e-10;
		evtWtot_LbBb = 1e-10;

		evtWtots_B0Bp = 1e-10;
		evtWtots_B0sBm = 1e-10;
		evtWtots_LbBb = 1e-10;

		for (double x : evtW_B0Bp) evtWtot_B0Bp += x;	
		for (double x : evtW_B0sBm) evtWtot_B0sBm += x;
		for (double x : evtW_LbBb) evtWtot_LbBb += x;

		for (double x : evtWs_B0Bp) evtWtots_B0Bp += x;
		for (double x : evtWs_B0sBm) evtWtots_B0sBm += x;
		for (double x : evtWs_LbBb) evtWtots_LbBb += x;

		evtWtot = evtWtot_B0Bp + evtWtot_B0sBm + evtWtot_LbBb;

		evtWtots = evtWtots_B0Bp + evtWtots_B0sBm + evtWtots_LbBb;

		//loop over different scale factors
		errorb = 0;
		for (int j = 0; j < hRjetb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hRjetb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}

		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb->GetBinContent(i) == 0) diffb->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb->SetBinError(i,errorb);
  }

	//############### plus variation ####################
  //Loop over the TProfile bins
  for (int i = 1; i < hRjetb->GetNbinsX()+1; i++){
		evtW_B0Bp.clear();
		evtW_B0sBm.clear();
		evtW_LbBb.clear();

		evtWs_B0Bp.clear();
		evtWs_B0sBm.clear();
		evtWs_LbBb.clear();

		sem_B0Bp.clear();
		sem_B0sBm.clear();
		sem_LbBb.clear();

		vals_B0Bp.clear();
		vals_B0sBm.clear();
		vals_LbBb.clear();

		for (int j = 1; j < hRjetb->GetNbinsX()+1; j++) {
			string pr_ind = "p_" + to_string(i) + "_" + to_string(j);

			prRjetb_B0Bp = pr2DRjetb_B0Bp->ProfileX(("B0Bp"+pr_ind).c_str(),j,j);
			prRjetb_B0sBm = pr2DRjetb_B0sBm->ProfileX(("B0sBm"+pr_ind).c_str(),j,j);
			prRjetb_LbBb = pr2DRjetb_LbBb->ProfileX(("LbBb"+pr_ind).c_str(),j,j);

			prRjetb_B0Bp_s_p = pr2DRjetb_B0Bp_s_p->ProfileX(("B0Bp_s"+pr_ind).c_str(),j,j);
			prRjetb_B0sBm_s_p = pr2DRjetb_B0sBm_s_p->ProfileX(("B0sBm_s"+pr_ind).c_str(),j,j);
			prRjetb_LbBb_s_p = pr2DRjetb_LbBb_s_p->ProfileX(("LbBb_s"+pr_ind).c_str(),j,j);

			evtW_B0Bp.push_back(prRjetb_B0Bp->GetBinEntries(i));
			evtW_B0sBm.push_back(prRjetb_B0sBm->GetBinEntries(i));
			evtW_LbBb.push_back(prRjetb_LbBb->GetBinEntries(i));

			evtWs_B0Bp.push_back(prRjetb_B0Bp_s_p->GetBinEntries(i));
			evtWs_B0sBm.push_back(prRjetb_B0sBm_s_p->GetBinEntries(i));
			evtWs_LbBb.push_back(prRjetb_LbBb_s_p->GetBinEntries(i));

			sem_B0Bp.push_back(prRjetb_B0Bp->GetBinError(i));
			sem_B0sBm.push_back(prRjetb_B0sBm->GetBinError(i));
			sem_LbBb.push_back(prRjetb_LbBb->GetBinError(i));

			vals_B0Bp.push_back(prRjetb_B0Bp->GetBinContent(i));
			vals_B0sBm.push_back(prRjetb_B0sBm->GetBinContent(i));
			vals_LbBb.push_back(prRjetb_LbBb->GetBinContent(i));

			delete prRjetb_B0Bp;
			delete prRjetb_B0sBm;
			delete prRjetb_LbBb;

			delete prRjetb_B0Bp_s_p;
			delete prRjetb_B0sBm_s_p;
			delete prRjetb_LbBb_s_p;
		}

		vector<vector<double>> vec_evtW{evtW_B0Bp, evtW_B0sBm, evtW_LbBb};

		vector<vector<double>> vec_evtWs{evtWs_B0Bp, evtWs_B0sBm, evtWs_LbBb};

		vector<vector<double>> vec_sem{sem_B0Bp, sem_B0sBm, sem_LbBb};

		vector<vector<double>> vec_vals{vals_B0Bp, vals_B0sBm, vals_LbBb};

		//set small number so that we dont divide by zero 
		evtWtot_B0Bp = 1e-10;
		evtWtot_B0sBm = 1e-10;
		evtWtot_LbBb = 1e-10;

		evtWtots_B0Bp = 1e-10;
		evtWtots_B0sBm = 1e-10;
		evtWtots_LbBb = 1e-10;

		for (double x : evtW_B0Bp) evtWtot_B0Bp += x;	
		for (double x : evtW_B0sBm) evtWtot_B0sBm += x;
		for (double x : evtW_LbBb) evtWtot_LbBb += x;

		for (double x : evtWs_B0Bp) evtWtots_B0Bp += x;
		for (double x : evtWs_B0sBm) evtWtots_B0sBm += x;
		for (double x : evtWs_LbBb) evtWtots_LbBb += x;

		evtWtot = evtWtot_B0Bp + evtWtot_B0sBm + evtWtot_LbBb;

		evtWtots = evtWtots_B0Bp + evtWtots_B0sBm + evtWtots_LbBb;

		//loop over different scale factors
		errorb = 0;
		for (int j = 0; j < hRjetb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hRjetb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}

		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb_p->GetBinContent(i) == 0) diffb_p->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb_p->SetBinError(i,errorb);
  }

	//############### minus variation ####################
  //Loop over the TProfile bins
  for (int i = 1; i < hRjetb->GetNbinsX()+1; i++){
		evtW_B0Bp.clear();
		evtW_B0sBm.clear();
		evtW_LbBb.clear();

		evtWs_B0Bp.clear();
		evtWs_B0sBm.clear();
		evtWs_LbBb.clear();

		sem_B0Bp.clear();
		sem_B0sBm.clear();
		sem_LbBb.clear();

		vals_B0Bp.clear();
		vals_B0sBm.clear();
		vals_LbBb.clear();

		for (int j = 1; j < hRjetb->GetNbinsX()+1; j++) {
			string pr_ind = "m_" + to_string(i) + "_" + to_string(j);

			prRjetb_B0Bp = pr2DRjetb_B0Bp->ProfileX(("B0Bp"+pr_ind).c_str(),j,j);
			prRjetb_B0sBm = pr2DRjetb_B0sBm->ProfileX(("B0sBm"+pr_ind).c_str(),j,j);
			prRjetb_LbBb = pr2DRjetb_LbBb->ProfileX(("LbBb"+pr_ind).c_str(),j,j);

			prRjetb_B0Bp_s_m = pr2DRjetb_B0Bp_s_m->ProfileX(("B0Bp_s"+pr_ind).c_str(),j,j);
			prRjetb_B0sBm_s_m = pr2DRjetb_B0sBm_s_m->ProfileX(("B0sBm_s"+pr_ind).c_str(),j,j);
			prRjetb_LbBb_s_m = pr2DRjetb_LbBb_s_m->ProfileX(("LbBb_s"+pr_ind).c_str(),j,j);

			evtW_B0Bp.push_back(prRjetb_B0Bp->GetBinEntries(i));
			evtW_B0sBm.push_back(prRjetb_B0sBm->GetBinEntries(i));
			evtW_LbBb.push_back(prRjetb_LbBb->GetBinEntries(i));

			evtWs_B0Bp.push_back(prRjetb_B0Bp_s_m->GetBinEntries(i));
			evtWs_B0sBm.push_back(prRjetb_B0sBm_s_m->GetBinEntries(i));
			evtWs_LbBb.push_back(prRjetb_LbBb_s_m->GetBinEntries(i));

			sem_B0Bp.push_back(prRjetb_B0Bp->GetBinError(i));
			sem_B0sBm.push_back(prRjetb_B0sBm->GetBinError(i));
			sem_LbBb.push_back(prRjetb_LbBb->GetBinError(i));

			vals_B0Bp.push_back(prRjetb_B0Bp->GetBinContent(i));
			vals_B0sBm.push_back(prRjetb_B0sBm->GetBinContent(i));
			vals_LbBb.push_back(prRjetb_LbBb->GetBinContent(i));

			delete prRjetb_B0Bp;
			delete prRjetb_B0sBm;
			delete prRjetb_LbBb;

			delete prRjetb_B0Bp_s_m;
			delete prRjetb_B0sBm_s_m;
			delete prRjetb_LbBb_s_m;
		}

		vector<vector<double>> vec_evtW{evtW_B0Bp, evtW_B0sBm, evtW_LbBb};

		vector<vector<double>> vec_evtWs{evtWs_B0Bp, evtWs_B0sBm, evtWs_LbBb};

		vector<vector<double>> vec_sem{sem_B0Bp, sem_B0sBm, sem_LbBb};

		vector<vector<double>> vec_vals{vals_B0Bp, vals_B0sBm, vals_LbBb};

		//set small number so that we dont divide by zero 
		evtWtot_B0Bp = 1e-10;
		evtWtot_B0sBm = 1e-10;
		evtWtot_LbBb = 1e-10;

		evtWtots_B0Bp = 1e-10;
		evtWtots_B0sBm = 1e-10;
		evtWtots_LbBb = 1e-10;

		for (double x : evtW_B0Bp) evtWtot_B0Bp += x;	
		for (double x : evtW_B0sBm) evtWtot_B0sBm += x;
		for (double x : evtW_LbBb) evtWtot_LbBb += x;

		for (double x : evtWs_B0Bp) evtWtots_B0Bp += x;
		for (double x : evtWs_B0sBm) evtWtots_B0sBm += x;
		for (double x : evtWs_LbBb) evtWtots_LbBb += x;

		evtWtot = evtWtot_B0Bp + evtWtot_B0sBm + evtWtot_LbBb;

		evtWtots = evtWtots_B0Bp + evtWtots_B0sBm + evtWtots_LbBb;

		//loop over different scale factors
		errorb = 0;
		for (int j = 0; j < hRjetb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hRjetb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}

		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb_m->GetBinContent(i) == 0) diffb_m->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb_m->SetBinError(i,errorb);
  }  

	//Scale to the percentages
  diffb->Scale(100.0);
  diffb_p->Scale(100.0);
  diffb_m->Scale(100.0);

	Rjetb_map["diffb"] = diffb;
	Rjetb_map["diffb_p"] = diffb_p;
	Rjetb_map["diffb_m"] = diffb_m;

	return Rjetb_map;
}

map<string, TH1D*> mpf_lundBowler() {
	map <string, TH1D*> MPFb_map;

  TFile* f_normal = TFile::Open(OpenName.c_str());
  TFile* f_scaled = TFile::Open(OpenName_lundBowler.c_str());
  TFile* f_scaled_p = TFile::Open(OpenName_lundBowler_p.c_str());
  TFile* f_scaled_m = TFile::Open(OpenName_lundBowler_m.c_str());

	TProfile *prMPFb;
	TProfile *prMPFb_s;
	TProfile *prMPFb_s_p;
	TProfile *prMPFb_s_m;

  //Read the normal sample
  f_normal->GetObject("prMPFb",   prMPFb);

  //Read the weighted sample
  f_scaled->GetObject("prMPFb",   prMPFb_s);
  f_scaled_p->GetObject("prMPFb",   prMPFb_s_p);
  f_scaled_m->GetObject("prMPFb",   prMPFb_s_m);

	//2D Tprofiles for the errorb calculations
	TProfile2D* pr2DMPFb;

	TProfile2D* pr2DMPFb_xB;

  //The sample with the additional weighting (*_s)
	TProfile2D* pr2DMPFb_s;
	TProfile2D* pr2DMPFb_s_p;
	TProfile2D* pr2DMPFb_s_m;

	TProfile2D* pr2DMPFb_xB_s;
	TProfile2D* pr2DMPFb_xB_s_p;
	TProfile2D* pr2DMPFb_xB_s_m;

	//Tprofiles for the errorb calculations
	TProfile* prMPFb_xB;

  //The sample with the additional weighting (*_s)
	TProfile* prMPFb_xB_s;
	TProfile* prMPFb_xB_s_p;
	TProfile* prMPFb_xB_s_m;

  //Read the normal sample
	f_normal->GetObject("pr2DMPFb",pr2DMPFb);

	f_normal->GetObject("pr2DMPFb_xB",pr2DMPFb_xB);

  //Read the weighted sample
	f_scaled->GetObject("pr2DMPFb",pr2DMPFb_s);
	f_scaled_p->GetObject("pr2DMPFb",pr2DMPFb_s_p);
	f_scaled_m->GetObject("pr2DMPFb",pr2DMPFb_s_m);

	f_scaled->GetObject("pr2DMPFb_xB",pr2DMPFb_xB_s);
	f_scaled_p->GetObject("pr2DMPFb_xB",pr2DMPFb_xB_s_p);
	f_scaled_m->GetObject("pr2DMPFb_xB",pr2DMPFb_xB_s_m);

	//calculate the difference in response between normal and the rescaled sample 
	//the second ProjectionX has new name as argument, otherwise would have same name as the previous
	//histo and causes memory leak 
	TH1D* hMPFb     = prMPFb   ->ProjectionX();
	TH1D* hMPFb_s   = prMPFb_s ->ProjectionX("pr2DMPFb_s");
	TH1D* hMPFb_s_p   = prMPFb_s_p ->ProjectionX("pr2DMPFb_s_p");
	TH1D* hMPFb_s_m   = prMPFb_s_m ->ProjectionX("pr2DMPFb_s_m");

	TH1D* diffb 		= (TH1D*) hMPFb_s->Clone();
	TH1D* diffb_p 		= (TH1D*) hMPFb_s_p->Clone();
	TH1D* diffb_m 		= (TH1D*) hMPFb_s_m->Clone();

	diffb->Add(hMPFb,-1);
	diffb_p->Add(hMPFb,-1);
	diffb_m->Add(hMPFb,-1);

	//evtW_* = event weight, sem_* = standard errorb of the mean,   
	double errorb, evtWtot, evtWtots;
	double evtWtot_xB;
	double evtWtots_xB;

	vector<double> evtW_xB;
	vector<double> evtWs_xB;
	vector<double> sem_xB;
	vector<double> vals_xB;

	vector<double> vec_xB_stat;

	ReadFromFile(vec_xB_stat,"/home/anpirtti/ultimate_generator/final_rescaling/lundBowler/calc_scale_factors/scale_factors/lundBowler/stat_err/lund_bowler_sf_stat.txt");

	vector<vector<double>> vec_sf_sem{vec_xB_stat};

	//removing the under- and overflow bin values
	for (int i = 0; i < vec_sf_sem.size(); i++) {
		vec_sf_sem[i].erase(vec_sf_sem[i].begin() + 0);
		vec_sf_sem[i].erase(vec_sf_sem[i].begin() + vec_sf_sem[i].size() - 1);
	}

  //Loop over the TProfile bins
  for (int i = 1; i < hMPFb->GetNbinsX()+1; i++){
		evtW_xB.clear();
		evtWs_xB.clear();
		sem_xB.clear();
		vals_xB.clear();

		for (int j = 1; j < pr2DMPFb_xB->GetNbinsY()+1; j++) {
			string pr_ind = "_" + to_string(i) + "_" + to_string(j);

			prMPFb_xB = pr2DMPFb_xB->ProfileX(("xB"+pr_ind).c_str(),j,j);
			prMPFb_xB_s = pr2DMPFb_xB_s->ProfileX(("xB_s"+pr_ind).c_str(),j,j);

			evtW_xB.push_back(prMPFb_xB->GetBinEntries(i));
			evtWs_xB.push_back(prMPFb_xB_s->GetBinEntries(i));
			sem_xB.push_back(prMPFb_xB->GetBinError(i));
			vals_xB.push_back(prMPFb_xB->GetBinContent(i));

			delete prMPFb_xB;
			delete prMPFb_xB_s;
		}

		vector<vector<double>> vec_evtW{evtW_xB};

		vector<vector<double>> vec_evtWs{evtWs_xB};

		vector<vector<double>> vec_sem{sem_xB};

		vector<vector<double>> vec_vals{vals_xB};

		//set small number so that we dont divide by zero 
		evtWtot_xB = 1e-10;
		evtWtots_xB = 1e-10;

		for (double x : evtW_xB) evtWtot_xB += x;	
		for (double x : evtWs_xB) evtWtots_xB += x;

		evtWtot = evtWtot_xB;
		evtWtots = evtWtots_xB;

		//loop over different scale factors
		errorb = 0;
		for (int j = 0; j < hMPFb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hMPFb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}

		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb->GetBinContent(i) == 0) diffb->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb->SetBinError(i,errorb);
  }

	//############### plus variation ####################
  //Loop over the TProfile bins
  for (int i = 1; i < hMPFb->GetNbinsX()+1; i++){
		evtW_xB.clear();
		evtWs_xB.clear();
		sem_xB.clear();
		vals_xB.clear();

		for (int j = 1; j < pr2DMPFb_xB->GetNbinsY()+1; j++) {
			string pr_ind = "_" + to_string(i) + "_" + to_string(j);

			prMPFb_xB = pr2DMPFb_xB->ProfileX(("xB"+pr_ind).c_str(),j,j);
			prMPFb_xB_s_p = pr2DMPFb_xB_s_p->ProfileX(("xB_s"+pr_ind).c_str(),j,j);

			evtW_xB.push_back(prMPFb_xB->GetBinEntries(i));
			evtWs_xB.push_back(prMPFb_xB_s_p->GetBinEntries(i));
			sem_xB.push_back(prMPFb_xB->GetBinError(i));
			vals_xB.push_back(prMPFb_xB->GetBinContent(i));

			delete prMPFb_xB;
			delete prMPFb_xB_s_p;
		}

		vector<vector<double>> vec_evtW{evtW_xB};

		vector<vector<double>> vec_evtWs{evtWs_xB};

		vector<vector<double>> vec_sem{sem_xB};

		vector<vector<double>> vec_vals{vals_xB};

		//set small number so that we dont divide by zero 
		evtWtot_xB = 1e-10;
		evtWtots_xB = 1e-10;

		for (double x : evtW_xB) evtWtot_xB += x;	
		for (double x : evtWs_xB) evtWtots_xB += x;

		evtWtot = evtWtot_xB;
		evtWtots = evtWtots_xB;

		//loop over different scale factors
		errorb = 0;
		for (int j = 0; j < hMPFb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hMPFb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}

		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb->GetBinContent(i) == 0) diffb->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb_p->SetBinError(i,errorb);
  }

	//############### minus variation ####################
  //Loop over the TProfile bins
  for (int i = 1; i < hMPFb->GetNbinsX()+1; i++){
		evtW_xB.clear();
		evtWs_xB.clear();
		sem_xB.clear();
		vals_xB.clear();

		for (int j = 1; j < pr2DMPFb_xB->GetNbinsY()+1; j++) {
			string pr_ind = "_" + to_string(i) + "_" + to_string(j);

			prMPFb_xB = pr2DMPFb_xB->ProfileX(("xB"+pr_ind).c_str(),j,j);
			prMPFb_xB_s_m = pr2DMPFb_xB_s_m->ProfileX(("xB_s"+pr_ind).c_str(),j,j);

			evtW_xB.push_back(prMPFb_xB->GetBinEntries(i));
			evtWs_xB.push_back(prMPFb_xB_s_m->GetBinEntries(i));
			sem_xB.push_back(prMPFb_xB->GetBinError(i));
			vals_xB.push_back(prMPFb_xB->GetBinContent(i));

			delete prMPFb_xB;
			delete prMPFb_xB_s_m;
		}

		vector<vector<double>> vec_evtW{evtW_xB};

		vector<vector<double>> vec_evtWs{evtWs_xB};

		vector<vector<double>> vec_sem{sem_xB};

		vector<vector<double>> vec_vals{vals_xB};

		//set small number so that we dont divide by zero 
		evtWtot_xB = 1e-10;
		evtWtots_xB = 1e-10;

		for (double x : evtW_xB) evtWtot_xB += x;	
		for (double x : evtWs_xB) evtWtots_xB += x;

		evtWtot = evtWtot_xB;
		evtWtots = evtWtots_xB;

		//loop over different scale factors
		errorb = 0;
		for (int j = 0; j < hMPFb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hMPFb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}

		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb->GetBinContent(i) == 0) diffb->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb_m->SetBinError(i,errorb);
  }

	//Scale to the percentages
  diffb->Scale(100.0);
  diffb_p->Scale(100.0);
  diffb_m->Scale(100.0);

	MPFb_map["diffb"] = diffb;
	MPFb_map["diffb_p"] = diffb_p;
	MPFb_map["diffb_m"] = diffb_m;

	return MPFb_map;
}


map<string, TH1D*> pTbal_lundBowler() {
	map <string, TH1D*> pTbalb_map;

  TFile* f_normal = TFile::Open(OpenName.c_str());
  TFile* f_scaled = TFile::Open(OpenName_lundBowler.c_str());
  TFile* f_scaled_p = TFile::Open(OpenName_lundBowler_p.c_str());
  TFile* f_scaled_m = TFile::Open(OpenName_lundBowler_m.c_str());

	TProfile *prpTbalb;
	TProfile *prpTbalb_s;
	TProfile *prpTbalb_s_p;
	TProfile *prpTbalb_s_m;

  //Read the normal sample
  f_normal->GetObject("prpTbalb",   prpTbalb);

  //Read the weighted sample
  f_scaled->GetObject("prpTbalb",   prpTbalb_s);
  f_scaled_p->GetObject("prpTbalb",   prpTbalb_s_p);
  f_scaled_m->GetObject("prpTbalb",   prpTbalb_s_m);

	//2D Tprofiles for the errorb calculations
	TProfile2D* pr2DpTbalb;

	TProfile2D* pr2DpTbalb_xB;

  //The sample with the additional weighting (*_s)
	TProfile2D* pr2DpTbalb_s;
	TProfile2D* pr2DpTbalb_s_p;
	TProfile2D* pr2DpTbalb_s_m;

	TProfile2D* pr2DpTbalb_xB_s;
	TProfile2D* pr2DpTbalb_xB_s_p;
	TProfile2D* pr2DpTbalb_xB_s_m;

	//Tprofiles for the errorb calculations
	TProfile* prpTbalb_xB;

  //The sample with the additional weighting (*_s)
	TProfile* prpTbalb_xB_s;
	TProfile* prpTbalb_xB_s_p;
	TProfile* prpTbalb_xB_s_m;

  //Read the normal sample
	f_normal->GetObject("pr2DpTbalb",pr2DpTbalb);

	f_normal->GetObject("pr2DpTbalb_xB",pr2DpTbalb_xB);

  //Read the weighted sample
	f_scaled->GetObject("pr2DpTbalb",pr2DpTbalb_s);
	f_scaled_p->GetObject("pr2DpTbalb",pr2DpTbalb_s_p);
	f_scaled_m->GetObject("pr2DpTbalb",pr2DpTbalb_s_m);

	f_scaled->GetObject("pr2DpTbalb_xB",pr2DpTbalb_xB_s);
	f_scaled_p->GetObject("pr2DpTbalb_xB",pr2DpTbalb_xB_s_p);
	f_scaled_m->GetObject("pr2DpTbalb_xB",pr2DpTbalb_xB_s_m);

	//calculate the difference in response between normal and the rescaled sample 
	//the second ProjectionX has new name as argument, otherwise would have same name as the previous
	//histo and causes memory leak 
	TH1D* hpTbalb     = prpTbalb   ->ProjectionX();
	TH1D* hpTbalb_s   = prpTbalb_s ->ProjectionX("pr2DpTbalb_s");
	TH1D* hpTbalb_s_p   = prpTbalb_s_p ->ProjectionX("pr2DpTbalb_s_p");
	TH1D* hpTbalb_s_m   = prpTbalb_s_m ->ProjectionX("pr2DpTbalb_s_m");

	TH1D* diffb 		= (TH1D*) hpTbalb_s->Clone();
	TH1D* diffb_p 		= (TH1D*) hpTbalb_s_p->Clone();
	TH1D* diffb_m 		= (TH1D*) hpTbalb_s_m->Clone();

	diffb->Add(hpTbalb,-1);
	diffb_p->Add(hpTbalb,-1);
	diffb_m->Add(hpTbalb,-1);

	//evtW_* = event weight, sem_* = standard errorb of the mean,   
	double errorb, evtWtot, evtWtots;
	double evtWtot_xB;
	double evtWtots_xB;

	vector<double> evtW_xB;
	vector<double> evtWs_xB;
	vector<double> sem_xB;
	vector<double> vals_xB;

	vector<double> vec_xB_stat;

	ReadFromFile(vec_xB_stat,"/home/anpirtti/ultimate_generator/final_rescaling/lundBowler/calc_scale_factors/scale_factors/lundBowler/stat_err/lund_bowler_sf_stat.txt");

	vector<vector<double>> vec_sf_sem{vec_xB_stat};

	//removing the under- and overflow bin values
	for (int i = 0; i < vec_sf_sem.size(); i++) {
		vec_sf_sem[i].erase(vec_sf_sem[i].begin() + 0);
		vec_sf_sem[i].erase(vec_sf_sem[i].begin() + vec_sf_sem[i].size() - 1);
	}

  //Loop over the TProfile bins
  for (int i = 1; i < hpTbalb->GetNbinsX()+1; i++){
		evtW_xB.clear();
		evtWs_xB.clear();
		sem_xB.clear();
		vals_xB.clear();

		for (int j = 1; j < pr2DpTbalb_xB->GetNbinsY()+1; j++) {
			string pr_ind = "_" + to_string(i) + "_" + to_string(j);

			prpTbalb_xB = pr2DpTbalb_xB->ProfileX(("xB"+pr_ind).c_str(),j,j);
			prpTbalb_xB_s = pr2DpTbalb_xB_s->ProfileX(("xB_s"+pr_ind).c_str(),j,j);

			evtW_xB.push_back(prpTbalb_xB->GetBinEntries(i));
			evtWs_xB.push_back(prpTbalb_xB_s->GetBinEntries(i));
			sem_xB.push_back(prpTbalb_xB->GetBinError(i));
			vals_xB.push_back(prpTbalb_xB->GetBinContent(i));

			delete prpTbalb_xB;
			delete prpTbalb_xB_s;
		}

		vector<vector<double>> vec_evtW{evtW_xB};

		vector<vector<double>> vec_evtWs{evtWs_xB};

		vector<vector<double>> vec_sem{sem_xB};

		vector<vector<double>> vec_vals{vals_xB};

		//set small number so that we dont divide by zero 
		evtWtot_xB = 1e-10;
		evtWtots_xB = 1e-10;

		for (double x : evtW_xB) evtWtot_xB += x;	
		for (double x : evtWs_xB) evtWtots_xB += x;

		evtWtot = evtWtot_xB;
		evtWtots = evtWtots_xB;

		//loop over different scale factors
		errorb = 0;
		for (int j = 0; j < hpTbalb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hpTbalb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}

		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb->GetBinContent(i) == 0) diffb->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb->SetBinError(i,errorb);
  }

	//############### plus variation ####################
  //Loop over the TProfile bins
  for (int i = 1; i < hpTbalb->GetNbinsX()+1; i++){
		evtW_xB.clear();
		evtWs_xB.clear();
		sem_xB.clear();
		vals_xB.clear();

		for (int j = 1; j < pr2DpTbalb_xB->GetNbinsY()+1; j++) {
			string pr_ind = "_" + to_string(i) + "_" + to_string(j);

			prpTbalb_xB = pr2DpTbalb_xB->ProfileX(("xB"+pr_ind).c_str(),j,j);
			prpTbalb_xB_s_p = pr2DpTbalb_xB_s_p->ProfileX(("xB_s"+pr_ind).c_str(),j,j);

			evtW_xB.push_back(prpTbalb_xB->GetBinEntries(i));
			evtWs_xB.push_back(prpTbalb_xB_s_p->GetBinEntries(i));
			sem_xB.push_back(prpTbalb_xB->GetBinError(i));
			vals_xB.push_back(prpTbalb_xB->GetBinContent(i));

			delete prpTbalb_xB;
			delete prpTbalb_xB_s_p;
		}

		vector<vector<double>> vec_evtW{evtW_xB};

		vector<vector<double>> vec_evtWs{evtWs_xB};

		vector<vector<double>> vec_sem{sem_xB};

		vector<vector<double>> vec_vals{vals_xB};

		//set small number so that we dont divide by zero 
		evtWtot_xB = 1e-10;
		evtWtots_xB = 1e-10;

		for (double x : evtW_xB) evtWtot_xB += x;	
		for (double x : evtWs_xB) evtWtots_xB += x;

		evtWtot = evtWtot_xB;
		evtWtots = evtWtots_xB;

		//loop over different scale factors
		errorb = 0;
		for (int j = 0; j < hpTbalb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hpTbalb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}

		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb->GetBinContent(i) == 0) diffb->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb_p->SetBinError(i,errorb);
  }

	//############### minus variation ####################
  //Loop over the TProfile bins
  for (int i = 1; i < hpTbalb->GetNbinsX()+1; i++){
		evtW_xB.clear();
		evtWs_xB.clear();
		sem_xB.clear();
		vals_xB.clear();

		for (int j = 1; j < pr2DpTbalb_xB->GetNbinsY()+1; j++) {
			string pr_ind = "_" + to_string(i) + "_" + to_string(j);

			prpTbalb_xB = pr2DpTbalb_xB->ProfileX(("xB"+pr_ind).c_str(),j,j);
			prpTbalb_xB_s_m = pr2DpTbalb_xB_s_m->ProfileX(("xB_s"+pr_ind).c_str(),j,j);

			evtW_xB.push_back(prpTbalb_xB->GetBinEntries(i));
			evtWs_xB.push_back(prpTbalb_xB_s_m->GetBinEntries(i));
			sem_xB.push_back(prpTbalb_xB->GetBinError(i));
			vals_xB.push_back(prpTbalb_xB->GetBinContent(i));

			delete prpTbalb_xB;
			delete prpTbalb_xB_s_m;
		}

		vector<vector<double>> vec_evtW{evtW_xB};

		vector<vector<double>> vec_evtWs{evtWs_xB};

		vector<vector<double>> vec_sem{sem_xB};

		vector<vector<double>> vec_vals{vals_xB};

		//set small number so that we dont divide by zero 
		evtWtot_xB = 1e-10;
		evtWtots_xB = 1e-10;

		for (double x : evtW_xB) evtWtot_xB += x;	
		for (double x : evtWs_xB) evtWtots_xB += x;

		evtWtot = evtWtot_xB;
		evtWtots = evtWtots_xB;

		//loop over different scale factors
		errorb = 0;
		for (int j = 0; j < hpTbalb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hpTbalb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}

		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb->GetBinContent(i) == 0) diffb->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb_m->SetBinError(i,errorb);
  }

	//Scale to the percentages
  diffb->Scale(100.0);
  diffb_p->Scale(100.0);
  diffb_m->Scale(100.0);

	pTbalb_map["diffb"] = diffb;
	pTbalb_map["diffb_p"] = diffb_p;
	pTbalb_map["diffb_m"] = diffb_m;

	return pTbalb_map;
}

map<string, TH1D*> Rjet_lundBowler() {
	map <string, TH1D*> Rjetb_map;

  TFile* f_normal = TFile::Open(OpenName.c_str());
  TFile* f_scaled = TFile::Open(OpenName_lundBowler.c_str());
  TFile* f_scaled_p = TFile::Open(OpenName_lundBowler_p.c_str());
  TFile* f_scaled_m = TFile::Open(OpenName_lundBowler_m.c_str());

	TProfile *prRjetb;
	TProfile *prRjetb_s;
	TProfile *prRjetb_s_p;
	TProfile *prRjetb_s_m;

  //Read the normal sample
  f_normal->GetObject("prRjetb",   prRjetb);

  //Read the weighted sample
  f_scaled->GetObject("prRjetb",   prRjetb_s);
  f_scaled_p->GetObject("prRjetb",   prRjetb_s_p);
  f_scaled_m->GetObject("prRjetb",   prRjetb_s_m);

	//2D Tprofiles for the errorb calculations
	TProfile2D* pr2DRjetb;

	TProfile2D* pr2DRjetb_xB;

  //The sample with the additional weighting (*_s)
	TProfile2D* pr2DRjetb_s;
	TProfile2D* pr2DRjetb_s_p;
	TProfile2D* pr2DRjetb_s_m;

	TProfile2D* pr2DRjetb_xB_s;
	TProfile2D* pr2DRjetb_xB_s_p;
	TProfile2D* pr2DRjetb_xB_s_m;

	//Tprofiles for the errorb calculations
	TProfile* prRjetb_xB;

  //The sample with the additional weighting (*_s)
	TProfile* prRjetb_xB_s;
	TProfile* prRjetb_xB_s_p;
	TProfile* prRjetb_xB_s_m;

  //Read the normal sample
	f_normal->GetObject("pr2DRjetb",pr2DRjetb);

	f_normal->GetObject("pr2DRjetb_xB",pr2DRjetb_xB);

  //Read the weighted sample
	f_scaled->GetObject("pr2DRjetb",pr2DRjetb_s);
	f_scaled_p->GetObject("pr2DRjetb",pr2DRjetb_s_p);
	f_scaled_m->GetObject("pr2DRjetb",pr2DRjetb_s_m);

	f_scaled->GetObject("pr2DRjetb_xB",pr2DRjetb_xB_s);
	f_scaled_p->GetObject("pr2DRjetb_xB",pr2DRjetb_xB_s_p);
	f_scaled_m->GetObject("pr2DRjetb_xB",pr2DRjetb_xB_s_m);

	//calculate the difference in response between normal and the rescaled sample 
	//the second ProjectionX has new name as argument, otherwise would have same name as the previous
	//histo and causes memory leak 
	TH1D* hRjetb     = prRjetb   ->ProjectionX();
	TH1D* hRjetb_s   = prRjetb_s ->ProjectionX("pr2DRjetb_s");
	TH1D* hRjetb_s_p   = prRjetb_s_p ->ProjectionX("pr2DRjetb_s_p");
	TH1D* hRjetb_s_m   = prRjetb_s_m ->ProjectionX("pr2DRjetb_s_m");

	TH1D* diffb 		= (TH1D*) hRjetb_s->Clone();
	TH1D* diffb_p 		= (TH1D*) hRjetb_s_p->Clone();
	TH1D* diffb_m 		= (TH1D*) hRjetb_s_m->Clone();

	diffb->Add(hRjetb,-1);
	diffb_p->Add(hRjetb,-1);
	diffb_m->Add(hRjetb,-1);

	//evtW_* = event weight, sem_* = standard errorb of the mean,   
	double errorb, evtWtot, evtWtots;
	double evtWtot_xB;
	double evtWtots_xB;

	vector<double> evtW_xB;
	vector<double> evtWs_xB;
	vector<double> sem_xB;
	vector<double> vals_xB;

	vector<double> vec_xB_stat;

	ReadFromFile(vec_xB_stat,"/home/anpirtti/ultimate_generator/final_rescaling/lundBowler/calc_scale_factors/scale_factors/lundBowler/stat_err/lund_bowler_sf_stat.txt");

	vector<vector<double>> vec_sf_sem{vec_xB_stat};

	//removing the under- and overflow bin values
	for (int i = 0; i < vec_sf_sem.size(); i++) {
		vec_sf_sem[i].erase(vec_sf_sem[i].begin() + 0);
		vec_sf_sem[i].erase(vec_sf_sem[i].begin() + vec_sf_sem[i].size() - 1);
	}

  //Loop over the TProfile bins
  for (int i = 1; i < hRjetb->GetNbinsX()+1; i++){
		evtW_xB.clear();
		evtWs_xB.clear();
		sem_xB.clear();
		vals_xB.clear();

		for (int j = 1; j < pr2DRjetb_xB->GetNbinsY()+1; j++) {
			string pr_ind = "_" + to_string(i) + "_" + to_string(j);

			prRjetb_xB = pr2DRjetb_xB->ProfileX(("xB"+pr_ind).c_str(),j,j);
			prRjetb_xB_s = pr2DRjetb_xB_s->ProfileX(("xB_s"+pr_ind).c_str(),j,j);

			evtW_xB.push_back(prRjetb_xB->GetBinEntries(i));
			evtWs_xB.push_back(prRjetb_xB_s->GetBinEntries(i));
			sem_xB.push_back(prRjetb_xB->GetBinError(i));
			vals_xB.push_back(prRjetb_xB->GetBinContent(i));

			delete prRjetb_xB;
			delete prRjetb_xB_s;
		}

		vector<vector<double>> vec_evtW{evtW_xB};

		vector<vector<double>> vec_evtWs{evtWs_xB};

		vector<vector<double>> vec_sem{sem_xB};

		vector<vector<double>> vec_vals{vals_xB};

		//set small number so that we dont divide by zero 
		evtWtot_xB = 1e-10;
		evtWtots_xB = 1e-10;

		for (double x : evtW_xB) evtWtot_xB += x;	
		for (double x : evtWs_xB) evtWtots_xB += x;

		evtWtot = evtWtot_xB;
		evtWtots = evtWtots_xB;

		//loop over different scale factors
		errorb = 0;
		for (int j = 0; j < hRjetb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hRjetb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}

		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb->GetBinContent(i) == 0) diffb->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb->SetBinError(i,errorb);
  }

	//############### plus variation ####################
  //Loop over the TProfile bins
  for (int i = 1; i < hRjetb->GetNbinsX()+1; i++){
		evtW_xB.clear();
		evtWs_xB.clear();
		sem_xB.clear();
		vals_xB.clear();

		for (int j = 1; j < pr2DRjetb_xB->GetNbinsY()+1; j++) {
			string pr_ind = "_" + to_string(i) + "_" + to_string(j);

			prRjetb_xB = pr2DRjetb_xB->ProfileX(("xB"+pr_ind).c_str(),j,j);
			prRjetb_xB_s_p = pr2DRjetb_xB_s_p->ProfileX(("xB_s"+pr_ind).c_str(),j,j);

			evtW_xB.push_back(prRjetb_xB->GetBinEntries(i));
			evtWs_xB.push_back(prRjetb_xB_s_p->GetBinEntries(i));
			sem_xB.push_back(prRjetb_xB->GetBinError(i));
			vals_xB.push_back(prRjetb_xB->GetBinContent(i));

			delete prRjetb_xB;
			delete prRjetb_xB_s_p;
		}

		vector<vector<double>> vec_evtW{evtW_xB};

		vector<vector<double>> vec_evtWs{evtWs_xB};

		vector<vector<double>> vec_sem{sem_xB};

		vector<vector<double>> vec_vals{vals_xB};

		//set small number so that we dont divide by zero 
		evtWtot_xB = 1e-10;
		evtWtots_xB = 1e-10;

		for (double x : evtW_xB) evtWtot_xB += x;	
		for (double x : evtWs_xB) evtWtots_xB += x;

		evtWtot = evtWtot_xB;
		evtWtots = evtWtots_xB;

		//loop over different scale factors
		errorb = 0;
		for (int j = 0; j < hRjetb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hRjetb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}

		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb->GetBinContent(i) == 0) diffb->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb_p->SetBinError(i,errorb);
  }

	//############### minus variation ####################
  //Loop over the TProfile bins
  for (int i = 1; i < hRjetb->GetNbinsX()+1; i++){
		evtW_xB.clear();
		evtWs_xB.clear();
		sem_xB.clear();
		vals_xB.clear();

		for (int j = 1; j < pr2DRjetb_xB->GetNbinsY()+1; j++) {
			string pr_ind = "_" + to_string(i) + "_" + to_string(j);

			prRjetb_xB = pr2DRjetb_xB->ProfileX(("xB"+pr_ind).c_str(),j,j);
			prRjetb_xB_s_m = pr2DRjetb_xB_s_m->ProfileX(("xB_s"+pr_ind).c_str(),j,j);

			evtW_xB.push_back(prRjetb_xB->GetBinEntries(i));
			evtWs_xB.push_back(prRjetb_xB_s_m->GetBinEntries(i));
			sem_xB.push_back(prRjetb_xB->GetBinError(i));
			vals_xB.push_back(prRjetb_xB->GetBinContent(i));

			delete prRjetb_xB;
			delete prRjetb_xB_s_m;
		}

		vector<vector<double>> vec_evtW{evtW_xB};

		vector<vector<double>> vec_evtWs{evtWs_xB};

		vector<vector<double>> vec_sem{sem_xB};

		vector<vector<double>> vec_vals{vals_xB};

		//set small number so that we dont divide by zero 
		evtWtot_xB = 1e-10;
		evtWtots_xB = 1e-10;

		for (double x : evtW_xB) evtWtot_xB += x;	
		for (double x : evtWs_xB) evtWtots_xB += x;

		evtWtot = evtWtot_xB;
		evtWtots = evtWtots_xB;

		//loop over different scale factors
		errorb = 0;
		for (int j = 0; j < hRjetb->GetNbinsX(); j++) {

			//go through the different scaling categories 
			for (int k = 0; k < vec_evtW.size(); k++){
				//stat error of the event weights
				errorb += pow((vec_evtWs[k][j]/evtWtots - vec_evtW[k][j]/evtWtot)*vec_sem[k][j],2);

				//propagated stat error of the scaling factors 
				//calculating the numerator first 
				double numer = 0; 
				for (int l = 0; l < vec_evtW.size(); l++) {
					for (int m = 0; m < hRjetb->GetNbinsX(); m++) {
						numer += vec_evtW[k][j]*vec_evtWs[l][m]*(vec_vals[k][j]-vec_vals[l][m]);
					}
				}
				errorb += pow(numer/(evtWtots*evtWtots)*vec_sf_sem[k][j],2);
			}
		}

		errorb = sqrt(errorb);

		//Root won't plot empty bins so replace empty bins with very small number
		if (diffb->GetBinContent(i) == 0) diffb->SetBinContent(i,1e-6);

		//Set the new bin errorbs
		diffb_m->SetBinError(i,errorb);
  }

	//Scale to the percentages
  diffb->Scale(100.0);
  diffb_p->Scale(100.0);
  diffb_m->Scale(100.0);

	Rjetb_map["diffb"] = diffb;
	Rjetb_map["diffb_p"] = diffb_p;
	Rjetb_map["diffb_m"] = diffb_m;

	return Rjetb_map;
}

