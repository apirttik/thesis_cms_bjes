#include "responseDiff.h"

map<string, TH1D*> calc_total (vector<TH1D*> vec,vector<TH1D*> vec_p,vector<TH1D*> vec_m){
	map<string, TH1D*> total_histos;

	//calculating the total sum 
	TH1D* sum = (TH1D*) vec[0]->Clone(); 
	for (int i = 1; i < vec.size(); i++){
		sum->Add(vec[i]);
	}

	total_histos["normal"] = sum;

	//plus variation
	TH1D* sum_p = (TH1D*) vec_p[0]->Clone();
	TH1D* h_temp;
	sum_p->Reset();

	vector<TH1D*> vec_p_copy;
	for (int i = 0; i < vec_p.size(); i++) {
		h_temp = (TH1D*) vec_p[i]->Clone();
		vec_p_copy.push_back(h_temp);
	}	

	//go through the different categories 
	for (int i = 0; i < vec.size(); i++) {

		//calc distance squared between the original and varied
		vec_p_copy[i]->Add(vec[i],-1);
		for (int j = 0; j <= vec_p_copy[0]->GetNcells(); j++) {
			vec_p_copy[i]->SetBinContent(j,abs(vec_p_copy[i]->GetBinContent(j)));
		}
		vec_p_copy[i]->Multiply(vec_p_copy[i]);

		sum_p->Add(vec_p_copy[i]);
	} 

	//calculate the squareroot and its error 
	for (int i = 0; i < sum_p->GetNcells(); i++) {

		if (sum_p->GetBinError(i) != 0) {
			sum_p->SetBinError(i,sum_p->GetBinError(i)/(2*sqrt(sum_p->GetBinContent(i))));
		} else {
			sum_p->SetBinError(i,0);
		}

		if (sum_p->GetBinContent(i) != 0) {
			sum_p->SetBinContent(i,sqrt(sum_p->GetBinContent(i)));
		} else {
			sum_p->SetBinContent(i,0);
		}	
	}

	TH1D* sum_p_final = (TH1D*) sum->Clone();
	sum_p_final->Add(sum_p,-1); 

	total_histos["plus"] = sum_p_final;

	//minus variation
	TH1D* sum_m = (TH1D*) vec_m[0]->Clone();
	sum_m->Reset();

	vector<TH1D*> vec_m_copy;
	for (int i = 0; i < vec_m.size(); i++) {
		h_temp = (TH1D*) vec_m[i]->Clone();;
		vec_m_copy.push_back(h_temp);
	}	

	//go through the different categories 
	for (int i = 0; i < vec.size(); i++) {

		//calc distance sqaured between the original and varied
		vec_m_copy[i]->Add(vec[i],-1);
		for (int j = 0; j <= vec_m_copy[0]->GetNcells(); j++) {
			vec_m_copy[i]->SetBinContent(j,abs(vec_m_copy[i]->GetBinContent(j)));
		}
		vec_m_copy[i]->Multiply(vec_m_copy[i]);

		sum_m->Add(vec_m_copy[i]);
	} 

	//calculate the squareroot and its error 
	for (int i = 0; i < sum_m->GetNcells(); i++) {

		if (sum_m->GetBinError(i) != 0) {
			sum_m->SetBinError(i,sum_m->GetBinError(i)/(2*sqrt(sum_m->GetBinContent(i))));
		} else {
			sum_m->SetBinError(i,0);
		}

		if (sum_m->GetBinContent(i) != 0) {
			sum_m->SetBinContent(i,sqrt(sum_m->GetBinContent(i)));
		} else {
			sum_m->SetBinContent(i,0);
		}	
	}

	TH1D* sum_m_final = (TH1D*) sum->Clone();
	sum_m_final->Add(sum_m); 

	total_histos["minus"] = sum_m_final;

	return total_histos;
}

void all_shifts_same() {

	bool mpf = 1;
	bool pTbal = 1;
	bool Rjet = 1;

	bool admix_BtoSl = 1;
	bool indiv_BtoSl = 0;

	if (mpf) {
		map<string, TH1D*> BtoSlBr_mpf_map = mpf_BtoSLBr();
		map<string, TH1D*> indvBtoSlBr_mpf_map = mpf_indvBtoSLBr();
		map<string, TH1D*> BtoCBr_mpf_map = mpf_BtoCBr();
		map<string, TH1D*> BprodFrac_mpf_map = mpf_BprodFrac();
		map<string, TH1D*> lundBowler_mpf_map = mpf_lundBowler();

		TH1D* BtoSlBr_mpf = BtoSlBr_mpf_map["diffb"];
		TH1D* indvBtoSlBr_mpf = indvBtoSlBr_mpf_map["diffb"];
		TH1D* BtoCBr_mpf = BtoCBr_mpf_map["diffb"];
		TH1D* BprodFrac_mpf = BprodFrac_mpf_map["diffb"];
		TH1D* lundBowler_mpf = lundBowler_mpf_map["diffb"];

		TH1D* BtoSlBr_mpf_p = BtoSlBr_mpf_map["diffb_p"];
		TH1D* indvBtoSlBr_mpf_p = indvBtoSlBr_mpf_map["diffb_p"];
		TH1D* BtoCBr_mpf_p = BtoCBr_mpf_map["diffb_p"];
		TH1D* BprodFrac_mpf_p = BprodFrac_mpf_map["diffb_p"];
		TH1D* lundBowler_mpf_p = lundBowler_mpf_map["diffb_p"];

		TH1D* BtoSlBr_mpf_m = BtoSlBr_mpf_map["diffb_m"];
		TH1D* indvBtoSlBr_mpf_m = indvBtoSlBr_mpf_map["diffb_m"];
		TH1D* BtoCBr_mpf_m = BtoCBr_mpf_map["diffb_m"];
		TH1D* BprodFrac_mpf_m = BprodFrac_mpf_map["diffb_m"];
		TH1D* lundBowler_mpf_m = lundBowler_mpf_map["diffb_m"];

		//going through the histograms to find the values for envelope curve
		//exc. BtoSL 
		vector<TH1D*> vec_exc_BtoSlBr = {BtoCBr_mpf,BprodFrac_mpf,lundBowler_mpf};
		vector<TH1D*> vec_p_exc_BtoSlBr = {BtoCBr_mpf_p,BprodFrac_mpf_p,lundBowler_mpf_p};
		vector<TH1D*> vec_m_exc_BtoSlBr = {BtoCBr_mpf_m,BprodFrac_mpf_m,lundBowler_mpf_m};
		map<string, TH1D*> histos_exc_BtoSlBr = calc_total(vec_exc_BtoSlBr,vec_p_exc_BtoSlBr,vec_m_exc_BtoSlBr);

		TH1D* env_BtoSlBr_mpf;
		TH1D* env_BtoSlBr_mpf_p;
		TH1D* env_BtoSlBr_mpf_m;

		if (admix_BtoSl) {
			env_BtoSlBr_mpf = BtoSlBr_mpf_map["diffb"];
			env_BtoSlBr_mpf_p = BtoSlBr_mpf_map["diffb_p"];
			env_BtoSlBr_mpf_m = BtoSlBr_mpf_map["diffb_m"];
		} else if (indiv_BtoSl) {
			env_BtoSlBr_mpf = indvBtoSlBr_mpf_map["diffb"];
			env_BtoSlBr_mpf_p = indvBtoSlBr_mpf_map["diffb_p"];
			env_BtoSlBr_mpf_m = indvBtoSlBr_mpf_map["diffb_m"];
		}

		//exc. BtoCBr
		vector<TH1D*> vec_exc_BtoCBr = {env_BtoSlBr_mpf,BprodFrac_mpf,lundBowler_mpf};
		vector<TH1D*> vec_p_exc_BtoCBr = {env_BtoSlBr_mpf_p,BprodFrac_mpf_p,lundBowler_mpf_p};
		vector<TH1D*> vec_m_exc_BtoCBr = {env_BtoSlBr_mpf_m,BprodFrac_mpf_m,lundBowler_mpf_m};
		map<string, TH1D*> histos_exc_BtoCBr = calc_total(vec_exc_BtoCBr,vec_p_exc_BtoCBr,vec_m_exc_BtoCBr);	

		//exc. BprodFrac
		vector<TH1D*> vec_exc_BprodFrac = {env_BtoSlBr_mpf,BtoCBr_mpf,lundBowler_mpf};
		vector<TH1D*> vec_p_exc_BprodFrac = {env_BtoSlBr_mpf_p,BtoCBr_mpf_p,lundBowler_mpf_p};
		vector<TH1D*> vec_m_exc_BprodFrac = {env_BtoSlBr_mpf_m,BtoCBr_mpf_m,lundBowler_mpf_m};
		map<string, TH1D*> histos_exc_BprodFrac = calc_total(vec_exc_BprodFrac,vec_p_exc_BprodFrac,vec_m_exc_BprodFrac);

		//exc. lundBowler
		vector<TH1D*> vec_exc_lundBowler = {env_BtoSlBr_mpf,BtoCBr_mpf,BprodFrac_mpf};
		vector<TH1D*> vec_p_exc_lundBowler = {env_BtoSlBr_mpf_p,BtoCBr_mpf_p,BprodFrac_mpf_p};
		vector<TH1D*> vec_m_exc_lundBowler = {env_BtoSlBr_mpf_m,BtoCBr_mpf_m,BprodFrac_mpf_m};
		map<string, TH1D*> histos_exc_lundBowler = calc_total(vec_exc_lundBowler,vec_p_exc_lundBowler,vec_m_exc_lundBowler);

		//going through the histograms to find the values for envelope curve
		vector<TH1D*> histos = {histos_exc_BtoSlBr["plus"],histos_exc_BtoCBr["plus"],
								histos_exc_BprodFrac["plus"],histos_exc_lundBowler["plus"],
	 							histos_exc_BtoSlBr["minus"],histos_exc_BtoCBr["minus"],
								histos_exc_BprodFrac["minus"],histos_exc_lundBowler["minus"]};
		
		int N_points = histos_exc_BtoSlBr["plus"]->GetNcells(); 
		double env_up[N_points], env_dw[N_points], x_val[N_points],
					 y_val[N_points],bin_widths[N_points];
		double env_up_err[N_points], env_dw_err[N_points];
		double env_max, env_min, env_max_err, env_min_err;

		for (int i = 0; i < histos_exc_BtoSlBr["plus"]->GetNcells(); i++) {
			env_max = 0; env_min = 0;
			x_val[i] = histos_exc_BtoSlBr["plus"]->GetBinCenter(i);
			y_val[i] = 0;
			bin_widths[i] = histos_exc_BtoSlBr["plus"]->GetBinWidth(i)/2.0;

			for (auto histo : histos) {
				if (histo->GetBinContent(i) > env_max) {
					env_max = histo->GetBinContent(i);
					env_max_err = histo->GetBinError(i);
				} 
				if (histo->GetBinContent(i) < env_min) {
					env_min = histo->GetBinContent(i);
					env_min_err = histo->GetBinError(i);
				}
			}
			env_up[i] = env_max + env_max_err;
			env_dw[i] = abs(env_min - env_min_err);
		}	

		TGraphAsymmErrors* envelope = new TGraphAsymmErrors(N_points,x_val,y_val,
																			bin_widths,bin_widths,env_dw,env_up);	

		envelope->SetFillColorAlpha(kRed-7,0.2);
		envelope->SetLineWidth(0);

		//Canvas
		TCanvas* canv_MPF = new TCanvas("MPF","",500,400);
		canv_MPF->SetLeftMargin(0.13);
		canv_MPF->SetBottomMargin(0.13);
		canv_MPF->SetLogx();

		BtoSlBr_mpf  ->SetMarkerStyle(kFullCircle);   BtoSlBr_mpf  ->SetMarkerColor(kRed+1);
		BtoSlBr_mpf  ->SetLineColor(kRed+1);  
	  BtoSlBr_mpf    ->SetMarkerSize(0.5);

		indvBtoSlBr_mpf  ->SetMarkerStyle(kFullCircle);   indvBtoSlBr_mpf  ->SetMarkerColor(kBlue+1);
		indvBtoSlBr_mpf  ->SetLineColor(kBlue+1);  
	  indvBtoSlBr_mpf    ->SetMarkerSize(0.5);

		BtoCBr_mpf  ->SetMarkerStyle(kFullCircle);   BtoCBr_mpf  ->SetMarkerColor(kGreen+1);
		BtoCBr_mpf  ->SetLineColor(kGreen+1);  
	  BtoCBr_mpf    ->SetMarkerSize(0.5);

		BprodFrac_mpf  ->SetMarkerStyle(kFullCircle);   BprodFrac_mpf  ->SetMarkerColor(kOrange+1);
		BprodFrac_mpf  ->SetLineColor(kOrange+1);  
	  BprodFrac_mpf    ->SetMarkerSize(0.5);

		lundBowler_mpf  ->SetMarkerStyle(kFullCircle);   lundBowler_mpf  ->SetMarkerColor(kCyan+1);
		lundBowler_mpf  ->SetLineColor(kCyan+1);  
	  lundBowler_mpf    ->SetMarkerSize(0.5);

		//plus
		BtoSlBr_mpf_p  ->SetMarkerStyle(kOpenSquare);   BtoSlBr_mpf_p  ->SetMarkerColor(kRed+1);
		BtoSlBr_mpf_p  ->SetLineColor(kRed+1);  
	  BtoSlBr_mpf_p    ->SetMarkerSize(0.5);

		indvBtoSlBr_mpf_p  ->SetMarkerStyle(kOpenSquare);   indvBtoSlBr_mpf_p  ->SetMarkerColor(kBlue+1);
		indvBtoSlBr_mpf_p  ->SetLineColor(kBlue+1);  
	  indvBtoSlBr_mpf_p    ->SetMarkerSize(0.5);

		BtoCBr_mpf_p  ->SetMarkerStyle(kOpenSquare);   BtoCBr_mpf_p  ->SetMarkerColor(kGreen+1);
		BtoCBr_mpf_p  ->SetLineColor(kGreen+1);  
	  BtoCBr_mpf_p    ->SetMarkerSize(0.5);

		BprodFrac_mpf_p  ->SetMarkerStyle(kOpenSquare);   BprodFrac_mpf_p  ->SetMarkerColor(kOrange+1);
		BprodFrac_mpf_p  ->SetLineColor(kOrange+1);  
	  BprodFrac_mpf_p    ->SetMarkerSize(0.5);

		lundBowler_mpf_p  ->SetMarkerStyle(kOpenSquare);   lundBowler_mpf_p  ->SetMarkerColor(kCyan+1);
		lundBowler_mpf_p  ->SetLineColor(kCyan+1);  
	  lundBowler_mpf_p    ->SetMarkerSize(0.5);

		//minus
		BtoSlBr_mpf_m  ->SetMarkerStyle(kOpenDiamond);   BtoSlBr_mpf_m  ->SetMarkerColor(kRed+1);
		BtoSlBr_mpf_m  ->SetLineColor(kRed+1);  
	  BtoSlBr_mpf_m    ->SetMarkerSize(0.8);

		indvBtoSlBr_mpf_m  ->SetMarkerStyle(kOpenDiamond);   indvBtoSlBr_mpf_m  ->SetMarkerColor(kBlue+1);
		indvBtoSlBr_mpf_m  ->SetLineColor(kBlue+1);  
	  indvBtoSlBr_mpf_m    ->SetMarkerSize(0.8);

		BtoCBr_mpf_m  ->SetMarkerStyle(kOpenDiamond);   BtoCBr_mpf_m  ->SetMarkerColor(kGreen+1);
		BtoCBr_mpf_m  ->SetLineColor(kGreen+1);  
	  BtoCBr_mpf_m    ->SetMarkerSize(0.8);

		BprodFrac_mpf_m  ->SetMarkerStyle(kOpenDiamond);   BprodFrac_mpf_m  ->SetMarkerColor(kOrange+1);
		BprodFrac_mpf_m  ->SetLineColor(kOrange+1);  
	  BprodFrac_mpf_m    ->SetMarkerSize(0.8);

		lundBowler_mpf_m  ->SetMarkerStyle(kOpenDiamond);   lundBowler_mpf_m  ->SetMarkerColor(kCyan+1);
		lundBowler_mpf_m  ->SetLineColor(kCyan+1);  
	  lundBowler_mpf_m    ->SetMarkerSize(0.8);

	  //Legend
	  TLegend* lz_MPF = new TLegend(0.01,0.92,0.99,0.99);
		lz_MPF->SetNColumns(5);

	  lz_MPF->SetBorderSize(0);

		if (admix_BtoSl) {
		  lz_MPF->AddEntry(BtoSlBr_mpf,   "B to SL BR (admix)",   "p");
		} else if (indiv_BtoSl) {
	  	lz_MPF->AddEntry(indvBtoSlBr_mpf,   "B to SL BR (indv.)",   "p");
		}

	  lz_MPF->AddEntry(BtoCBr_mpf,   "B to C BR",   "p");
	  lz_MPF->AddEntry(BprodFrac_mpf,   "B production fraction",   "p");
	  lz_MPF->AddEntry(lundBowler_mpf,   "B fragmentation",   "p");
//	  lz_MPF->AddEntry(envelope,   "Total unc.",   "f");

	  TLegend* lz_MPF2 = new TLegend(0.2,0.8,0.8,0.89);
		lz_MPF2->SetNColumns(4);
	  lz_MPF2->SetBorderSize(0);
	  lz_MPF2->AddEntry(BtoSlBr_mpf,   "Central var.",   "p");
	  lz_MPF2->AddEntry(BtoSlBr_mpf_p,   "Up var.",   "p");
	  lz_MPF2->AddEntry(BtoSlBr_mpf_m,   "Down var.",   "p");
		lz_MPF2->AddEntry(envelope,   "Total unc.",   "f");

	  //Title and axis setup
//	  envelope->SetStats(0);
	  envelope->SetTitle("");
	  envelope->GetXaxis()->SetMoreLogLabels();
	  envelope->GetXaxis()->SetNoExponent();
	  envelope->GetYaxis()->SetTitleOffset(1.1);
	  envelope->GetXaxis()->SetTitleOffset(1.1);
	  envelope->GetXaxis()->SetTitle("p_{T,tag}^{reco} (GeV)");
	  envelope->GetYaxis()->SetTitle("R_{MPF,scaled} - R_{MPF,nominal} (%)");
	  envelope->GetYaxis()->SetTitleSize(0.05);
	  envelope->GetXaxis()->SetTitleSize(0.05);

	  gPad->SetTickx();   gPad->SetTicky();

	  //Plot
		envelope->Draw("5Asame");

		BtoCBr_mpf->Draw("psame");
		BprodFrac_mpf->Draw("psame");
		lundBowler_mpf->Draw("psame");

		BtoCBr_mpf_p->Draw("psame");
		BprodFrac_mpf_p->Draw("psame");
		lundBowler_mpf_p->Draw("psame");

		BtoCBr_mpf_m->Draw("psame");
		BprodFrac_mpf_m->Draw("psame");
		lundBowler_mpf_m->Draw("psame");

		if (admix_BtoSl) {
			BtoSlBr_mpf->Draw("psame");
			BtoSlBr_mpf_p->Draw("psame");
			BtoSlBr_mpf_m->Draw("psame");
		} else if (indiv_BtoSl) {
			indvBtoSlBr_mpf->Draw("psame");
			indvBtoSlBr_mpf_p->Draw("psame");
			indvBtoSlBr_mpf_m->Draw("psame");
		}

	  lz_MPF->Draw();
	  lz_MPF2->Draw();

		//drawing reference line
		TF1* line0 = new TF1("line0","0",0,1200);
		line0->SetLineColor(kBlack);
		line0->SetLineStyle(kDashed);
		line0->SetLineWidth(1);
		line0->Draw("SAME");

		envelope->SetMaximum(0.4);
  	envelope->SetMinimum(-0.53); 
		envelope->GetXaxis()->SetLimits(15., 900.);

	  //Save plot
		if (admix_BtoSl) {
	  	canv_MPF->Print("plots/mpf_plot_all_sep_admix_same_thesis.pdf"); delete canv_MPF;
		} else if (indiv_BtoSl) {
	  	canv_MPF->Print("plots/mpf_plot_all_sep_indv_same_thesis.pdf"); delete canv_MPF;
		}
	}	

	if (pTbal) { 
		map<string, TH1D*> BtoSlBr_pTbal_map = pTbal_BtoSLBr();
		map<string, TH1D*> indvBtoSlBr_pTbal_map = pTbal_indvBtoSLBr();
		map<string, TH1D*> BtoCBr_pTbal_map = pTbal_BtoCBr();
		map<string, TH1D*> BprodFrac_pTbal_map = pTbal_BprodFrac();
		map<string, TH1D*> lundBowler_pTbal_map = pTbal_lundBowler();

		TH1D* BtoSlBr_pTbal = BtoSlBr_pTbal_map["diffb"];
		TH1D* indvBtoSlBr_pTbal = indvBtoSlBr_pTbal_map["diffb"];
		TH1D* BtoCBr_pTbal = BtoCBr_pTbal_map["diffb"];
		TH1D* BprodFrac_pTbal = BprodFrac_pTbal_map["diffb"];
		TH1D* lundBowler_pTbal = lundBowler_pTbal_map["diffb"];

		TH1D* BtoSlBr_pTbal_p = BtoSlBr_pTbal_map["diffb_p"];
		TH1D* indvBtoSlBr_pTbal_p = indvBtoSlBr_pTbal_map["diffb_p"];
		TH1D* BtoCBr_pTbal_p = BtoCBr_pTbal_map["diffb_p"];
		TH1D* BprodFrac_pTbal_p = BprodFrac_pTbal_map["diffb_p"];
		TH1D* lundBowler_pTbal_p = lundBowler_pTbal_map["diffb_p"];

		TH1D* BtoSlBr_pTbal_m = BtoSlBr_pTbal_map["diffb_m"];
		TH1D* indvBtoSlBr_pTbal_m = indvBtoSlBr_pTbal_map["diffb_m"];
		TH1D* BtoCBr_pTbal_m = BtoCBr_pTbal_map["diffb_m"];
		TH1D* BprodFrac_pTbal_m = BprodFrac_pTbal_map["diffb_m"];
		TH1D* lundBowler_pTbal_m = lundBowler_pTbal_map["diffb_m"];

		//going through the histograms to find the values for envelope curve
		//exc. BtoSL 
		vector<TH1D*> vec_exc_BtoSlBr = {BtoCBr_pTbal,BprodFrac_pTbal,lundBowler_pTbal};
		vector<TH1D*> vec_p_exc_BtoSlBr = {BtoCBr_pTbal_p,BprodFrac_pTbal_p,lundBowler_pTbal_p};
		vector<TH1D*> vec_m_exc_BtoSlBr = {BtoCBr_pTbal_m,BprodFrac_pTbal_m,lundBowler_pTbal_m};
		map<string, TH1D*> histos_exc_BtoSlBr = calc_total(vec_exc_BtoSlBr,vec_p_exc_BtoSlBr,vec_m_exc_BtoSlBr);

		TH1D* env_BtoSlBr_pTbal;
		TH1D* env_BtoSlBr_pTbal_p;
		TH1D* env_BtoSlBr_pTbal_m;

		if (admix_BtoSl) {
			env_BtoSlBr_pTbal = BtoSlBr_pTbal_map["diffb"];
			env_BtoSlBr_pTbal_p = BtoSlBr_pTbal_map["diffb_p"];
			env_BtoSlBr_pTbal_m = BtoSlBr_pTbal_map["diffb_m"];
		} else if (indiv_BtoSl) {
			env_BtoSlBr_pTbal = indvBtoSlBr_pTbal_map["diffb"];
			env_BtoSlBr_pTbal_p = indvBtoSlBr_pTbal_map["diffb_p"];
			env_BtoSlBr_pTbal_m = indvBtoSlBr_pTbal_map["diffb_m"];
		}

		//exc. BtoCBr
		vector<TH1D*> vec_exc_BtoCBr = {env_BtoSlBr_pTbal,BprodFrac_pTbal,lundBowler_pTbal};
		vector<TH1D*> vec_p_exc_BtoCBr = {env_BtoSlBr_pTbal_p,BprodFrac_pTbal_p,lundBowler_pTbal_p};
		vector<TH1D*> vec_m_exc_BtoCBr = {env_BtoSlBr_pTbal_m,BprodFrac_pTbal_m,lundBowler_pTbal_m};
		map<string, TH1D*> histos_exc_BtoCBr = calc_total(vec_exc_BtoCBr,vec_p_exc_BtoCBr,vec_m_exc_BtoCBr);	

		//exc. BprodFrac
		vector<TH1D*> vec_exc_BprodFrac = {env_BtoSlBr_pTbal,BtoCBr_pTbal,lundBowler_pTbal};
		vector<TH1D*> vec_p_exc_BprodFrac = {env_BtoSlBr_pTbal_p,BtoCBr_pTbal_p,lundBowler_pTbal_p};
		vector<TH1D*> vec_m_exc_BprodFrac = {env_BtoSlBr_pTbal_m,BtoCBr_pTbal_m,lundBowler_pTbal_m};
		map<string, TH1D*> histos_exc_BprodFrac = calc_total(vec_exc_BprodFrac,vec_p_exc_BprodFrac,vec_m_exc_BprodFrac);

		//exc. lundBowler
		vector<TH1D*> vec_exc_lundBowler = {env_BtoSlBr_pTbal,BtoCBr_pTbal,BprodFrac_pTbal};
		vector<TH1D*> vec_p_exc_lundBowler = {env_BtoSlBr_pTbal_p,BtoCBr_pTbal_p,BprodFrac_pTbal_p};
		vector<TH1D*> vec_m_exc_lundBowler = {env_BtoSlBr_pTbal_m,BtoCBr_pTbal_m,BprodFrac_pTbal_m};
		map<string, TH1D*> histos_exc_lundBowler = calc_total(vec_exc_lundBowler,vec_p_exc_lundBowler,vec_m_exc_lundBowler);

		//going through the histograms to find the values for envelope curve
		vector<TH1D*> histos = {histos_exc_BtoSlBr["plus"],histos_exc_BtoCBr["plus"],
								histos_exc_BprodFrac["plus"],histos_exc_lundBowler["plus"],
	 							histos_exc_BtoSlBr["minus"],histos_exc_BtoCBr["minus"],
								histos_exc_BprodFrac["minus"],histos_exc_lundBowler["minus"]};
		
		int N_points = histos_exc_BtoSlBr["plus"]->GetNcells(); 
		double env_up[N_points], env_dw[N_points], x_val[N_points],
					 y_val[N_points],bin_widths[N_points];
		double env_up_err[N_points], env_dw_err[N_points];
		double env_max, env_min, env_max_err, env_min_err;

		for (int i = 0; i < histos_exc_BtoSlBr["plus"]->GetNcells(); i++) {
			env_max = 0; env_min = 0;
			x_val[i] = histos_exc_BtoSlBr["plus"]->GetBinCenter(i);
			y_val[i] = 0;
			bin_widths[i] = histos_exc_BtoSlBr["plus"]->GetBinWidth(i)/2.0;

			for (auto histo : histos) {
				if (histo->GetBinContent(i) > env_max) {
					env_max = histo->GetBinContent(i);
					env_max_err = histo->GetBinError(i);
				} 
				if (histo->GetBinContent(i) < env_min) {
					env_min = histo->GetBinContent(i);
					env_min_err = histo->GetBinError(i);
				}
			}
			env_up[i] = env_max + env_max_err;
			env_dw[i] = abs(env_min - env_min_err);
		}	

		TGraphAsymmErrors* envelope = new TGraphAsymmErrors(N_points,x_val,y_val,
																			bin_widths,bin_widths,env_dw,env_up);	

		envelope->SetFillColorAlpha(kRed-7,0.2);
		envelope->SetLineWidth(0);

		//Canvas
		TCanvas* canv_pTbal = new TCanvas("pTbal","",500,400);
		canv_pTbal->SetLeftMargin(0.13);
		canv_pTbal->SetBottomMargin(0.13);
		canv_pTbal->SetLogx();

		BtoSlBr_pTbal  ->SetMarkerStyle(kFullCircle);   BtoSlBr_pTbal  ->SetMarkerColor(kRed+1);
		BtoSlBr_pTbal  ->SetLineColor(kRed+1);  
	  BtoSlBr_pTbal    ->SetMarkerSize(0.5);

		indvBtoSlBr_pTbal  ->SetMarkerStyle(kFullCircle);   indvBtoSlBr_pTbal  ->SetMarkerColor(kBlue+1);
		indvBtoSlBr_pTbal  ->SetLineColor(kBlue+1);  
	  indvBtoSlBr_pTbal    ->SetMarkerSize(0.5);

		BtoCBr_pTbal  ->SetMarkerStyle(kFullCircle);   BtoCBr_pTbal  ->SetMarkerColor(kGreen+1);
		BtoCBr_pTbal  ->SetLineColor(kGreen+1);  
	  BtoCBr_pTbal    ->SetMarkerSize(0.5);

		BprodFrac_pTbal  ->SetMarkerStyle(kFullCircle);   BprodFrac_pTbal  ->SetMarkerColor(kOrange+1);
		BprodFrac_pTbal  ->SetLineColor(kOrange+1);  
	  BprodFrac_pTbal    ->SetMarkerSize(0.5);

		lundBowler_pTbal  ->SetMarkerStyle(kFullCircle);   lundBowler_pTbal  ->SetMarkerColor(kCyan+1);
		lundBowler_pTbal  ->SetLineColor(kCyan+1);  
	  lundBowler_pTbal    ->SetMarkerSize(0.5);

		//plus
		BtoSlBr_pTbal_p  ->SetMarkerStyle(kOpenSquare);   BtoSlBr_pTbal_p  ->SetMarkerColor(kRed+1);
		BtoSlBr_pTbal_p  ->SetLineColor(kRed+1);  
	  BtoSlBr_pTbal_p    ->SetMarkerSize(0.5);

		indvBtoSlBr_pTbal_p  ->SetMarkerStyle(kOpenSquare);   indvBtoSlBr_pTbal_p  ->SetMarkerColor(kBlue+1);
		indvBtoSlBr_pTbal_p  ->SetLineColor(kBlue+1);  
	  indvBtoSlBr_pTbal_p    ->SetMarkerSize(0.5);

		BtoCBr_pTbal_p  ->SetMarkerStyle(kOpenSquare);   BtoCBr_pTbal_p  ->SetMarkerColor(kGreen+1);
		BtoCBr_pTbal_p  ->SetLineColor(kGreen+1);  
	  BtoCBr_pTbal_p    ->SetMarkerSize(0.5);

		BprodFrac_pTbal_p  ->SetMarkerStyle(kOpenSquare);   BprodFrac_pTbal_p  ->SetMarkerColor(kOrange+1);
		BprodFrac_pTbal_p  ->SetLineColor(kOrange+1);  
	  BprodFrac_pTbal_p    ->SetMarkerSize(0.5);

		lundBowler_pTbal_p  ->SetMarkerStyle(kOpenSquare);   lundBowler_pTbal_p  ->SetMarkerColor(kCyan+1);
		lundBowler_pTbal_p  ->SetLineColor(kCyan+1);  
	  lundBowler_pTbal_p    ->SetMarkerSize(0.5);

		//minus
		BtoSlBr_pTbal_m  ->SetMarkerStyle(kOpenDiamond);   BtoSlBr_pTbal_m  ->SetMarkerColor(kRed+1);
		BtoSlBr_pTbal_m  ->SetLineColor(kRed+1);  
	  BtoSlBr_pTbal_m    ->SetMarkerSize(0.8);

		indvBtoSlBr_pTbal_m  ->SetMarkerStyle(kOpenDiamond);   indvBtoSlBr_pTbal_m  ->SetMarkerColor(kBlue+1);
		indvBtoSlBr_pTbal_m  ->SetLineColor(kBlue+1);  
	  indvBtoSlBr_pTbal_m    ->SetMarkerSize(0.8);

		BtoCBr_pTbal_m  ->SetMarkerStyle(kOpenDiamond);   BtoCBr_pTbal_m  ->SetMarkerColor(kGreen+1);
		BtoCBr_pTbal_m  ->SetLineColor(kGreen+1);  
	  BtoCBr_pTbal_m    ->SetMarkerSize(0.8);

		BprodFrac_pTbal_m  ->SetMarkerStyle(kOpenDiamond);   BprodFrac_pTbal_m  ->SetMarkerColor(kOrange+1);
		BprodFrac_pTbal_m  ->SetLineColor(kOrange+1);  
	  BprodFrac_pTbal_m    ->SetMarkerSize(0.8);

		lundBowler_pTbal_m  ->SetMarkerStyle(kOpenDiamond);   lundBowler_pTbal_m  ->SetMarkerColor(kCyan+1);
		lundBowler_pTbal_m  ->SetLineColor(kCyan+1);  
	  lundBowler_pTbal_m    ->SetMarkerSize(0.8);

	  //Legend
	  TLegend* lz_pTbal = new TLegend(0.01,0.92,0.99,0.99);
		lz_pTbal->SetNColumns(5);

	  lz_pTbal->SetBorderSize(0);
		if (admix_BtoSl) {
		  lz_pTbal->AddEntry(BtoSlBr_pTbal,   "B to SL BR (admix)",   "p");
		} else if (indiv_BtoSl) {
	  	lz_pTbal->AddEntry(indvBtoSlBr_pTbal,   "B to SL BR (indv.)",   "p");
		}
	  lz_pTbal->AddEntry(BtoCBr_pTbal,   "B to C BR",   "p");
	  lz_pTbal->AddEntry(BprodFrac_pTbal,   "B production fraction",   "p");
	  lz_pTbal->AddEntry(lundBowler_pTbal,   "B fragmentation",   "p");
//	  lz_pTbal->AddEntry(envelope,   "Total unc.",   "f");

	  TLegend* lz_pTbal2 = new TLegend(0.2,0.8,0.8,0.89);
		lz_pTbal2->SetNColumns(4);
	  lz_pTbal2->SetBorderSize(0);
	  lz_pTbal2->AddEntry(BtoSlBr_pTbal,   "Central var.",   "p");
	  lz_pTbal2->AddEntry(BtoSlBr_pTbal_p,   "Up var.",   "p");
	  lz_pTbal2->AddEntry(BtoSlBr_pTbal_m,   "Down var.",   "p");
		lz_pTbal2->AddEntry(envelope,   "Total unc.",   "f");

	  //Title and axis setup
//	  envelope->SetStats(0);
	  envelope->SetTitle("");
	  envelope->GetXaxis()->SetMoreLogLabels();
	  envelope->GetXaxis()->SetNoExponent();
	  envelope->GetYaxis()->SetTitleOffset(1.3);
	  envelope->GetXaxis()->SetTitleOffset(1.1);
	  envelope->GetXaxis()->SetTitle("p_{T,tag}^{reco} (GeV)");
	  envelope->GetYaxis()->SetTitle("p_{T,scaled}^{probe}/p_{T,scaled}^{tag} - p_{T,nominal}^{probe}/p_{T,nominal}^{tag} (%)");
//	  envelope->GetYaxis()->SetTitleSize(0.05);
	  envelope->GetYaxis()->SetTitleSize(0.04);
	  envelope->GetXaxis()->SetTitleSize(0.05);

	  gPad->SetTickx();   gPad->SetTicky();

	  //Plot
		envelope->Draw("5Asame");

		BtoCBr_pTbal->Draw("psame");
		BprodFrac_pTbal->Draw("psame");
		lundBowler_pTbal->Draw("psame");

		BtoCBr_pTbal_p->Draw("psame");
		BprodFrac_pTbal_p->Draw("psame");
		lundBowler_pTbal_p->Draw("psame");

		BtoCBr_pTbal_m->Draw("psame");
		BprodFrac_pTbal_m->Draw("psame");
		lundBowler_pTbal_m->Draw("psame");

		if (admix_BtoSl) {
			BtoSlBr_pTbal->Draw("psame");
			BtoSlBr_pTbal_p->Draw("psame");
			BtoSlBr_pTbal_m->Draw("psame");
		} else if (indiv_BtoSl) {
			indvBtoSlBr_pTbal->Draw("psame");
			indvBtoSlBr_pTbal_p->Draw("psame");
			indvBtoSlBr_pTbal_m->Draw("psame");
		}

	  lz_pTbal->Draw();
	  lz_pTbal2->Draw();

		//drawing reference line
		TF1* line0 = new TF1("line0","0",0,1200);
		line0->SetLineColor(kBlack);
		line0->SetLineStyle(kDashed);
		line0->SetLineWidth(1);
		line0->Draw("SAME");

		envelope->SetMaximum(0.4);
  	envelope->SetMinimum(-0.53); 
		envelope->GetXaxis()->SetLimits(15., 900.);

	  //Save plot
		if (admix_BtoSl) {
	  	canv_pTbal->Print("plots/pTbal_plot_all_sep_admix_same_thesis.pdf"); delete canv_pTbal;
		} else if (indiv_BtoSl) {
	  	canv_pTbal->Print("plots/pTbal_plot_all_sep_indv_same_thesis.pdf"); delete canv_pTbal;
		}
	}

	if (Rjet) {
		map<string, TH1D*> BtoSlBr_Rjet_map = Rjet_BtoSLBr();
		map<string, TH1D*> indvBtoSlBr_Rjet_map = Rjet_indvBtoSLBr();
		map<string, TH1D*> BtoCBr_Rjet_map = Rjet_BtoCBr();
		map<string, TH1D*> BprodFrac_Rjet_map = Rjet_BprodFrac();
		map<string, TH1D*> lundBowler_Rjet_map = Rjet_lundBowler();

		TH1D* BtoSlBr_Rjet = BtoSlBr_Rjet_map["diffb"];
		TH1D* indvBtoSlBr_Rjet = indvBtoSlBr_Rjet_map["diffb"];
		TH1D* BtoCBr_Rjet = BtoCBr_Rjet_map["diffb"];
		TH1D* BprodFrac_Rjet = BprodFrac_Rjet_map["diffb"];
		TH1D* lundBowler_Rjet = lundBowler_Rjet_map["diffb"];

		TH1D* BtoSlBr_Rjet_p = BtoSlBr_Rjet_map["diffb_p"];
		TH1D* indvBtoSlBr_Rjet_p = indvBtoSlBr_Rjet_map["diffb_p"];
		TH1D* BtoCBr_Rjet_p = BtoCBr_Rjet_map["diffb_p"];
		TH1D* BprodFrac_Rjet_p = BprodFrac_Rjet_map["diffb_p"];
		TH1D* lundBowler_Rjet_p = lundBowler_Rjet_map["diffb_p"];

		TH1D* BtoSlBr_Rjet_m = BtoSlBr_Rjet_map["diffb_m"];
		TH1D* indvBtoSlBr_Rjet_m = indvBtoSlBr_Rjet_map["diffb_m"];
		TH1D* BtoCBr_Rjet_m = BtoCBr_Rjet_map["diffb_m"];
		TH1D* BprodFrac_Rjet_m = BprodFrac_Rjet_map["diffb_m"];
		TH1D* lundBowler_Rjet_m = lundBowler_Rjet_map["diffb_m"];

		//going through the histograms to find the values for envelope curve
		//exc. BtoSL 
		vector<TH1D*> vec_exc_BtoSlBr = {BtoCBr_Rjet,BprodFrac_Rjet,lundBowler_Rjet};
		vector<TH1D*> vec_p_exc_BtoSlBr = {BtoCBr_Rjet_p,BprodFrac_Rjet_p,lundBowler_Rjet_p};
		vector<TH1D*> vec_m_exc_BtoSlBr = {BtoCBr_Rjet_m,BprodFrac_Rjet_m,lundBowler_Rjet_m};
		map<string, TH1D*> histos_exc_BtoSlBr = calc_total(vec_exc_BtoSlBr,vec_p_exc_BtoSlBr,vec_m_exc_BtoSlBr);

		TH1D* env_BtoSlBr_Rjet;
		TH1D* env_BtoSlBr_Rjet_p;
		TH1D* env_BtoSlBr_Rjet_m;

		if (admix_BtoSl) {
			env_BtoSlBr_Rjet = BtoSlBr_Rjet_map["diffb"];
			env_BtoSlBr_Rjet_p = BtoSlBr_Rjet_map["diffb_p"];
			env_BtoSlBr_Rjet_m = BtoSlBr_Rjet_map["diffb_m"];
		} else if (indiv_BtoSl) {
			env_BtoSlBr_Rjet = indvBtoSlBr_Rjet_map["diffb"];
			env_BtoSlBr_Rjet_p = indvBtoSlBr_Rjet_map["diffb_p"];
			env_BtoSlBr_Rjet_m = indvBtoSlBr_Rjet_map["diffb_m"];
		}

		//exc. BtoCBr
		vector<TH1D*> vec_exc_BtoCBr = {env_BtoSlBr_Rjet,BprodFrac_Rjet,lundBowler_Rjet};
		vector<TH1D*> vec_p_exc_BtoCBr = {env_BtoSlBr_Rjet_p,BprodFrac_Rjet_p,lundBowler_Rjet_p};
		vector<TH1D*> vec_m_exc_BtoCBr = {env_BtoSlBr_Rjet_m,BprodFrac_Rjet_m,lundBowler_Rjet_m};
		map<string, TH1D*> histos_exc_BtoCBr = calc_total(vec_exc_BtoCBr,vec_p_exc_BtoCBr,vec_m_exc_BtoCBr);	

		//exc. BprodFrac
		vector<TH1D*> vec_exc_BprodFrac = {env_BtoSlBr_Rjet,BtoCBr_Rjet,lundBowler_Rjet};
		vector<TH1D*> vec_p_exc_BprodFrac = {env_BtoSlBr_Rjet_p,BtoCBr_Rjet_p,lundBowler_Rjet_p};
		vector<TH1D*> vec_m_exc_BprodFrac = {env_BtoSlBr_Rjet_m,BtoCBr_Rjet_m,lundBowler_Rjet_m};
		map<string, TH1D*> histos_exc_BprodFrac = calc_total(vec_exc_BprodFrac,vec_p_exc_BprodFrac,vec_m_exc_BprodFrac);

		//exc. lundBowler
		vector<TH1D*> vec_exc_lundBowler = {env_BtoSlBr_Rjet,BtoCBr_Rjet,BprodFrac_Rjet};
		vector<TH1D*> vec_p_exc_lundBowler = {env_BtoSlBr_Rjet_p,BtoCBr_Rjet_p,BprodFrac_Rjet_p};
		vector<TH1D*> vec_m_exc_lundBowler = {env_BtoSlBr_Rjet_m,BtoCBr_Rjet_m,BprodFrac_Rjet_m};
		map<string, TH1D*> histos_exc_lundBowler = calc_total(vec_exc_lundBowler,vec_p_exc_lundBowler,vec_m_exc_lundBowler);

		//going through the histograms to find the values for envelope curve
		vector<TH1D*> histos = {histos_exc_BtoSlBr["plus"],histos_exc_BtoCBr["plus"],
														histos_exc_BprodFrac["plus"],histos_exc_lundBowler["plus"],
	 													histos_exc_BtoSlBr["minus"],histos_exc_BtoCBr["minus"],
														histos_exc_BprodFrac["minus"],histos_exc_lundBowler["minus"]};
		
		int N_points = histos_exc_BtoSlBr["plus"]->GetNcells(); 
		double env_up[N_points], env_dw[N_points], x_val[N_points],
					 y_val[N_points],bin_widths[N_points];
		double env_up_err[N_points], env_dw_err[N_points];
		double env_max, env_min, env_max_err, env_min_err;

		for (int i = 0; i < histos_exc_BtoSlBr["plus"]->GetNcells(); i++) {
			env_max = 0; env_min = 0;
			x_val[i] = histos_exc_BtoSlBr["plus"]->GetBinCenter(i);
			y_val[i] = 0;
			bin_widths[i] = histos_exc_BtoSlBr["plus"]->GetBinWidth(i)/2.0;

			for (auto histo : histos) {
				if (histo->GetBinContent(i) > env_max) {
					env_max = histo->GetBinContent(i);
					env_max_err = histo->GetBinError(i);
				} 
				if (histo->GetBinContent(i) < env_min) {
					env_min = histo->GetBinContent(i);
					env_min_err = histo->GetBinError(i);
				}
			}
			env_up[i] = env_max + env_max_err;
			env_dw[i] = abs(env_min - env_min_err);
		}	

		TGraphAsymmErrors* envelope = new TGraphAsymmErrors(N_points,x_val,y_val,
																			bin_widths,bin_widths,env_dw,env_up);	

		envelope->SetFillColorAlpha(kRed-7,0.2);
		envelope->SetLineWidth(0);

		//Canvas
		TCanvas* canv_Rjet = new TCanvas("Rjet","",500,400);
		canv_Rjet->SetLeftMargin(0.13);
		canv_Rjet->SetBottomMargin(0.13);
		canv_Rjet->SetLogx();

		BtoSlBr_Rjet  ->SetMarkerStyle(kFullCircle);   BtoSlBr_Rjet  ->SetMarkerColor(kRed+1);
		BtoSlBr_Rjet  ->SetLineColor(kRed+1);  
	  BtoSlBr_Rjet    ->SetMarkerSize(0.5);

		indvBtoSlBr_Rjet  ->SetMarkerStyle(kFullCircle);   indvBtoSlBr_Rjet  ->SetMarkerColor(kBlue+1);
		indvBtoSlBr_Rjet  ->SetLineColor(kBlue+1);  
	  indvBtoSlBr_Rjet    ->SetMarkerSize(0.5);

		BtoCBr_Rjet  ->SetMarkerStyle(kFullCircle);   BtoCBr_Rjet  ->SetMarkerColor(kGreen+1);
		BtoCBr_Rjet  ->SetLineColor(kGreen+1);  
	  BtoCBr_Rjet    ->SetMarkerSize(0.5);

		BprodFrac_Rjet  ->SetMarkerStyle(kFullCircle);   BprodFrac_Rjet  ->SetMarkerColor(kOrange+1);
		BprodFrac_Rjet  ->SetLineColor(kOrange+1);  
	  BprodFrac_Rjet    ->SetMarkerSize(0.5);

		lundBowler_Rjet  ->SetMarkerStyle(kFullCircle);   lundBowler_Rjet  ->SetMarkerColor(kCyan+1);
		lundBowler_Rjet  ->SetLineColor(kCyan+1);  
	  lundBowler_Rjet    ->SetMarkerSize(0.5);

		//plus
		BtoSlBr_Rjet_p  ->SetMarkerStyle(kOpenSquare);   BtoSlBr_Rjet_p  ->SetMarkerColor(kRed+1);
		BtoSlBr_Rjet_p  ->SetLineColor(kRed+1);  
	  BtoSlBr_Rjet_p    ->SetMarkerSize(0.5);

		indvBtoSlBr_Rjet_p  ->SetMarkerStyle(kOpenSquare);   indvBtoSlBr_Rjet_p  ->SetMarkerColor(kBlue+1);
		indvBtoSlBr_Rjet_p  ->SetLineColor(kBlue+1);  
	  indvBtoSlBr_Rjet_p    ->SetMarkerSize(0.5);

		BtoCBr_Rjet_p  ->SetMarkerStyle(kOpenSquare);   BtoCBr_Rjet_p  ->SetMarkerColor(kGreen+1);
		BtoCBr_Rjet_p  ->SetLineColor(kGreen+1);  
	  BtoCBr_Rjet_p    ->SetMarkerSize(0.5);

		BprodFrac_Rjet_p  ->SetMarkerStyle(kOpenSquare);   BprodFrac_Rjet_p  ->SetMarkerColor(kOrange+1);
		BprodFrac_Rjet_p  ->SetLineColor(kOrange+1);  
	  BprodFrac_Rjet_p    ->SetMarkerSize(0.5);

		lundBowler_Rjet_p  ->SetMarkerStyle(kOpenSquare);   lundBowler_Rjet_p  ->SetMarkerColor(kCyan+1);
		lundBowler_Rjet_p  ->SetLineColor(kCyan+1);  
	  lundBowler_Rjet_p    ->SetMarkerSize(0.5);

		//minus
		BtoSlBr_Rjet_m  ->SetMarkerStyle(kOpenDiamond);   BtoSlBr_Rjet_m  ->SetMarkerColor(kRed+1);
		BtoSlBr_Rjet_m  ->SetLineColor(kRed+1);  
	  BtoSlBr_Rjet_m    ->SetMarkerSize(0.8);

		indvBtoSlBr_Rjet_m  ->SetMarkerStyle(kOpenDiamond);   indvBtoSlBr_Rjet_m  ->SetMarkerColor(kBlue+1);
		indvBtoSlBr_Rjet_m  ->SetLineColor(kBlue+1);  
	  indvBtoSlBr_Rjet_m    ->SetMarkerSize(0.8);

		BtoCBr_Rjet_m  ->SetMarkerStyle(kOpenDiamond);   BtoCBr_Rjet_m  ->SetMarkerColor(kGreen+1);
		BtoCBr_Rjet_m  ->SetLineColor(kGreen+1);  
	  BtoCBr_Rjet_m    ->SetMarkerSize(0.8);

		BprodFrac_Rjet_m  ->SetMarkerStyle(kOpenDiamond);   BprodFrac_Rjet_m  ->SetMarkerColor(kOrange+1);
		BprodFrac_Rjet_m  ->SetLineColor(kOrange+1);  
	  BprodFrac_Rjet_m    ->SetMarkerSize(0.8);

		lundBowler_Rjet_m  ->SetMarkerStyle(kOpenDiamond);   lundBowler_Rjet_m  ->SetMarkerColor(kCyan+1);
		lundBowler_Rjet_m  ->SetLineColor(kCyan+1);  
	  lundBowler_Rjet_m    ->SetMarkerSize(0.8);

	  //Legend
	  TLegend* lz_Rjet = new TLegend(0.01,0.92,0.99,0.99);
		lz_Rjet->SetNColumns(5);

	  lz_Rjet->SetBorderSize(0);
		if (admix_BtoSl) {
		  lz_Rjet->AddEntry(BtoSlBr_Rjet,   "B to SL BR (admix)",   "p");
		} else if (indiv_BtoSl) {
	  	lz_Rjet->AddEntry(indvBtoSlBr_Rjet,   "B to SL BR (indv.)",   "p");
		}
	  lz_Rjet->AddEntry(BtoCBr_Rjet,   "B to C BR",   "p");
	  lz_Rjet->AddEntry(BprodFrac_Rjet,   "B production fraction",   "p");
	  lz_Rjet->AddEntry(lundBowler_Rjet,   "B fragmentation",   "p");
//	  lz_Rjet->AddEntry(envelope,   "Total unc.",   "f");

	  TLegend* lz_Rjet2 = new TLegend(0.2,0.8,0.8,0.89);
		lz_Rjet2->SetNColumns(4);
	  lz_Rjet2->SetBorderSize(0);
	  lz_Rjet2->AddEntry(BtoSlBr_Rjet,   "Central var.",   "p");
	  lz_Rjet2->AddEntry(BtoSlBr_Rjet_p,   "Up var.",   "p");
	  lz_Rjet2->AddEntry(BtoSlBr_Rjet_m,   "Down var.",   "p");
		lz_Rjet2->AddEntry(envelope,   "Total unc.",   "f");

	  //Title and axis setup
//	  envelope->SetStats(0);
	  envelope->SetTitle("");
	  envelope->GetXaxis()->SetMoreLogLabels();
	  envelope->GetXaxis()->SetNoExponent();
	  envelope->GetYaxis()->SetTitleOffset(1.1);
	  envelope->GetXaxis()->SetTitleOffset(1.1);
	  envelope->GetXaxis()->SetTitle("p_{T,tag}^{reco} (GeV)");
	  envelope->GetYaxis()->SetTitle("R_{jet,scaled} - R_{jet,nominal} (%)");
	  envelope->GetYaxis()->SetTitleSize(0.05);
	  envelope->GetXaxis()->SetTitleSize(0.05);

	  gPad->SetTickx();   gPad->SetTicky();

	  //Plot
		envelope->Draw("5Asame");

		BtoCBr_Rjet->Draw("psame");
		BprodFrac_Rjet->Draw("psame");
		lundBowler_Rjet->Draw("psame");

		BtoCBr_Rjet_p->Draw("psame");
		BprodFrac_Rjet_p->Draw("psame");
		lundBowler_Rjet_p->Draw("psame");

		BtoCBr_Rjet_m->Draw("psame");
		BprodFrac_Rjet_m->Draw("psame");
		lundBowler_Rjet_m->Draw("psame");

		if (admix_BtoSl) {
			BtoSlBr_Rjet->Draw("psame");
			BtoSlBr_Rjet_p->Draw("psame");
			BtoSlBr_Rjet_m->Draw("psame");
		} else if (indiv_BtoSl) {
			indvBtoSlBr_Rjet->Draw("psame");
			indvBtoSlBr_Rjet_p->Draw("psame");
			indvBtoSlBr_Rjet_m->Draw("psame");
		}

	  lz_Rjet->Draw();
	  lz_Rjet2->Draw();

		//drawing reference line
		TF1* line0 = new TF1("line0","0",0,1200);
		line0->SetLineColor(kBlack);
		line0->SetLineStyle(kDashed);
		line0->SetLineWidth(1);
		line0->Draw("SAME");

		envelope->SetMaximum(0.4);
  	envelope->SetMinimum(-0.53); 
		envelope->GetXaxis()->SetLimits(15., 900.);

	  //Save plot
		if (admix_BtoSl) {
	  	canv_Rjet->Print("plots/Rjet_plot_all_sep_admix_same_thesis.pdf"); delete canv_Rjet;
		} else if (indiv_BtoSl) {
	  	canv_Rjet->Print("plots/Rjet_plot_all_sep_indv_same_thesis.pdf"); delete canv_Rjet;
		}
	}
}
