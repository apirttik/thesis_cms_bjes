//script to calculate the fractions of different types of b-hadron assuming pT indepency of the fractions.
//This means that constant fits are made to fractions.
void BtoSlBr_Sf() 
{
	gStyle->SetOptFit();
	auto c1 = new TCanvas("c1","c1",700,500);

	ifstream file_BSl_w("../../scale_factors/BtoSlBr/normal/BSl_sf.txt");
	ifstream file_BTauSl_w("../../scale_factors/BtoSlBr/normal/BTauSl_sf.txt");
	ifstream file_BCSl_w("../../scale_factors/BtoSlBr/normal/BCSl_sf.txt");
	ifstream file_BCbarSl_w("../../scale_factors/BtoSlBr/normal/BCbarSl_sf.txt");
	ifstream file_BNonSl_w("../../scale_factors/BtoSlBr/normal/BNonSl_sf.txt");

	ifstream file_BSl_w_err("../../scale_factors/BtoSlBr/stat_err/BSl_sf_stat.txt");
	ifstream file_BTauSl_w_err("../../scale_factors/BtoSlBr/stat_err/BTauSl_sf_stat.txt");
	ifstream file_BCSl_w_err("../../scale_factors/BtoSlBr/stat_err/BCSl_sf_stat.txt");
	ifstream file_BCbarSl_w_err("../../scale_factors/BtoSlBr/stat_err/BCbarSl_sf_stat.txt");
	ifstream file_BNonSl_w_err("../../scale_factors/BtoSlBr/stat_err/BNonSl_sf_stat.txt");

	ifstream file_BSl_w_sys("../../scale_factors/BtoSlBr/sys_err/BSl_sf_sys.txt");
	ifstream file_BTauSl_w_sys("../../scale_factors/BtoSlBr/sys_err/BTauSl_sf_sys.txt");
	ifstream file_BCSl_w_sys("../../scale_factors/BtoSlBr/sys_err/BCSl_sf_sys.txt");
	ifstream file_BCbarSl_w_sys("../../scale_factors/BtoSlBr/sys_err/BCbarSl_sf_sys.txt");
	ifstream file_BNonSl_w_sys("../../scale_factors/BtoSlBr/sys_err/BNonSl_sf_sys.txt");

	double BSl_w, BTauSl_w, BCSl_w, BCbarSl_w, BNonSl_w;
	double BSl_w_arr[23], BTauSl_w_arr[23], BCSl_w_arr[23], BCbarSl_w_arr[23], BNonSl_w_arr[23];

	double BSl_w_err, BTauSl_w_err, BCSl_w_err, BCbarSl_w_err, BNonSl_w_err;
	double BSl_w_arr_err[23], BTauSl_w_arr_err[23], BCSl_w_arr_err[23], BCbarSl_w_arr_err[23], BNonSl_w_arr_err[23];

	double BSl_w_sys, BTauSl_w_sys, BCSl_w_sys, BCbarSl_w_sys, BNonSl_w_sys;
	double BSl_w_arr_sys[23], BTauSl_w_arr_sys[23], BCSl_w_arr_sys[23], BCbarSl_w_arr_sys[23], BNonSl_w_arr_sys[23];

	int i = 0;
	while (file_BSl_w >> BSl_w) {
		file_BTauSl_w >> BTauSl_w;
		file_BCSl_w >> BCSl_w;
		file_BCbarSl_w >> BCbarSl_w;
		file_BNonSl_w >> BNonSl_w;	

		file_BSl_w_err >> BSl_w_err;
		file_BTauSl_w_err >> BTauSl_w_err;
		file_BCSl_w_err >> BCSl_w_err;
		file_BCbarSl_w_err >> BCbarSl_w_err;
		file_BNonSl_w_err >> BNonSl_w_err;	

		file_BSl_w_sys >> BSl_w_sys;
		file_BTauSl_w_sys >> BTauSl_w_sys;
		file_BCSl_w_sys >> BCSl_w_sys;
		file_BCbarSl_w_sys >> BCbarSl_w_sys;
		file_BNonSl_w_sys >> BNonSl_w_sys;	

		//ignore the under- and overflow bins 
		if (i != 0 && i != 24) {
			BSl_w_arr[i-1] = BSl_w;
			BTauSl_w_arr[i-1] = BTauSl_w;
			BCSl_w_arr[i-1] = BCSl_w;
			BCbarSl_w_arr[i-1] = BCbarSl_w;
			BNonSl_w_arr[i-1] = BNonSl_w;

			BSl_w_arr_err[i-1] = BSl_w_err;
			BTauSl_w_arr_err[i-1] = BTauSl_w_err;
			BCSl_w_arr_err[i-1] = BCSl_w_err;
			BCbarSl_w_arr_err[i-1] = BCbarSl_w_err;
			BNonSl_w_arr_err[i-1] = BNonSl_w_err;	

			BSl_w_arr_sys[i-1] = BSl_w_sys;
			BTauSl_w_arr_sys[i-1] = BTauSl_w_sys;
			BCSl_w_arr_sys[i-1] = BCSl_w_sys;
			BCbarSl_w_arr_sys[i-1] = BCbarSl_w_sys;
			BNonSl_w_arr_sys[i-1] = BNonSl_w_sys;	
		}
		i++;
	}

  int const nbinsMPF = 24;
  const double binsxMPF[nbinsMPF] = {0,2,5,10,15,20,25,30,35,40,45,50,60,85,105,130,175,240,300,400,500,700, 1000, 1500};

	double ptx[23];

	for(int i = 0; i < 23; i++){
		ptx[i] = binsxMPF[i] + (binsxMPF[i+1]-binsxMPF[i])/2.0;
	}

	double ptx_sys[23];

	for(int i = 0; i < 23; i++){
		ptx_sys[i] = (binsxMPF[i+1]-binsxMPF[i])/2.0;
	}

	TGraphErrors* gr_BSl = new TGraphErrors(23,ptx,BSl_w_arr,0,BSl_w_arr_err);
	TGraphErrors* gr_BTauSl = new TGraphErrors(23,ptx,BTauSl_w_arr,0,BTauSl_w_arr_err);
	TGraphErrors* gr_BCSl = new TGraphErrors(23,ptx,BCSl_w_arr,0,BCSl_w_arr_err);
	TGraphErrors* gr_BCbarSl = new TGraphErrors(23,ptx,BCbarSl_w_arr,0,BCbarSl_w_arr_err);
	TGraphErrors* gr_BNonSl = new TGraphErrors(23,ptx,BNonSl_w_arr,0,BNonSl_w_arr_err);

	TGraphErrors* gr_BSl_sys = new TGraphErrors(23,ptx,BSl_w_arr,ptx_sys,BSl_w_arr_sys);
	TGraphErrors* gr_BTauSl_sys = new TGraphErrors(23,ptx,BTauSl_w_arr,ptx_sys,BTauSl_w_arr_sys);
	TGraphErrors* gr_BCSl_sys = new TGraphErrors(23,ptx,BCSl_w_arr,ptx_sys,BCSl_w_arr_sys);
	TGraphErrors* gr_BCbarSl_sys = new TGraphErrors(23,ptx,BCbarSl_w_arr,ptx_sys,BCbarSl_w_arr_sys);
	TGraphErrors* gr_BNonSl_sys = new TGraphErrors(23,ptx,BNonSl_w_arr,ptx_sys,BNonSl_w_arr_sys);

  gr_BSl->SetMarkerStyle(kFullCircle);
  gr_BTauSl->SetMarkerStyle(kFullCircle);
  gr_BCSl->SetMarkerStyle(kFullCircle);
  gr_BCbarSl->SetMarkerStyle(kFullCircle);
  gr_BNonSl->SetMarkerStyle(kFullCircle);

  gr_BSl->SetMarkerColor(kRed+1);
  gr_BTauSl->SetMarkerColor(kBlue+1);
  gr_BCSl->SetMarkerColor(kGreen+1);
  gr_BCbarSl->SetMarkerColor(kOrange+1);
  gr_BNonSl->SetMarkerColor(kGreen+1);

  gr_BSl->SetMarkerSize(0.8);
  gr_BTauSl->SetMarkerSize(0.8);
  gr_BCSl->SetMarkerSize(0.8);
  gr_BCbarSl->SetMarkerSize(0.8);
  gr_BNonSl->SetMarkerSize(0.8);

  gr_BSl->SetLineColor(kRed+1);
  gr_BTauSl->SetLineColor(kBlue+1);
  gr_BCSl->SetLineColor(kGreen+1);
  gr_BCbarSl->SetLineColor(kOrange+1);
  gr_BNonSl->SetLineColor(kGreen+1);

  gr_BSl_sys->SetFillColorAlpha(kRed+1,0.2);
  gr_BTauSl_sys->SetFillColorAlpha(kBlue+1,0.2);
  gr_BCSl_sys->SetFillColorAlpha(kGreen+1,0.2);
  gr_BCbarSl_sys->SetFillColorAlpha(kOrange+1,0.2);
  gr_BNonSl_sys->SetFillColorAlpha(kGreen+1,0.2);

  gr_BSl_sys->SetLineColorAlpha(kRed+1,0.2);
  gr_BTauSl_sys->SetLineColorAlpha(kBlue+1,0.2);
  gr_BCSl_sys->SetLineColorAlpha(kGreen+1,0.2);
  gr_BCbarSl_sys->SetLineColorAlpha(kOrange+1,0.2);
  gr_BNonSl_sys->SetLineColorAlpha(kGreen+1,0.2);

//  gr_BSl_sys->SetLineWidth(0);
//  gr_BTauSl_sys->SetLineWidth(0);
//  gr_BCSl_sys->SetLineWidth(0);
//  gr_BCbarSl_sys->SetLineWidth(0);
//  gr_BNonSl_sys->SetLineWidth(0);

	//make the legend
  TLegend* lg = new TLegend(0.12, 0.71, 0.75, 0.85);
  lg->SetBorderSize(0);
	lg->SetNColumns(4);

	lg->AddEntry(gr_BSl,"#it{b}#rightarrow#mu/#it{e}","p");
	lg->AddEntry(gr_BTauSl,"#it{b}#rightarrow#tau#rightarrow#mu/#it{e}","p");
	lg->AddEntry(gr_BCSl,"#it{b}#rightarrow#it{c}#rightarrow#mu/#it{e}","p");
	lg->AddEntry(gr_BCbarSl,"#it{b}#rightarrow#bar{#it{c}}#rightarrow#mu/#it{e}","p");
//	lg->AddEntry(gr_BNonSl,"b->other","p");

	c1->SetLogx();
  gr_BCbarSl_sys->GetXaxis()->SetMoreLogLabels();
  gr_BCbarSl_sys->GetXaxis()->SetNoExponent();

  gr_BCbarSl_sys->GetXaxis()->SetLimits(3.0,1000);
  gr_BCbarSl_sys->GetHistogram()->SetMinimum(0.5);
  gr_BCbarSl_sys->GetHistogram()->SetMaximum(2.0);

  gr_BCbarSl_sys->GetXaxis()->SetTitleOffset(1.1);
  gr_BCbarSl_sys->GetYaxis()->SetTitleOffset(0.9);

  gr_BCbarSl_sys->GetYaxis()->SetTitle("Scaling factor");
  gr_BCbarSl_sys->GetYaxis()->SetTitleSize(0.045);
  gr_BCbarSl_sys->GetXaxis()->SetTitle("p_{T,hadron}^{gen} (GeV)");
  gr_BCbarSl_sys->GetXaxis()->SetTitleSize(0.04);
	gr_BCbarSl_sys->SetTitle("");

	gStyle->SetLineScalePS(1);

	gr_BCbarSl_sys->Draw("5ASAME");
  gr_BTauSl_sys->Draw("5SAME");
  gr_BSl_sys->Draw("5SAME");
  gr_BCSl_sys->Draw("5SAME");
	gr_BNonSl_sys->Draw("5SAME");

	gr_BCbarSl->Draw("PSAME");
  gr_BTauSl->Draw("PSAME");
  gr_BSl->Draw("PSAME");
  gr_BCSl->Draw("PSAME");
	gr_BNonSl->Draw("PSAME");

	TF1* line0 = new TF1("line0","1",0,1200);
	line0->SetLineColor(kBlack);
	line0->SetLineStyle(kDashed);
	line0->SetLineWidth(1);
	line0->Draw("SAME");

	lg->Draw();

	c1->Update();

	c1->Print("plots/BtoSlBr_Sf_thesis.pdf");
}
