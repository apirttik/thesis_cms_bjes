//script to calculate the fractions of different types of b-hadron assuming pT indepency of the fractions.
//This means that constant fits are made to fractions.
void lundBowler_Sf() 
{
	gStyle->SetOptFit();
	auto c1 = new TCanvas("c1","c1",700,500);

	ifstream file_xB_sf("../../scale_factors/lundBowler/normal/lund_bowler_sf.txt");
	ifstream file_xB_sf_stat("../../scale_factors/lundBowler/stat_err/lund_bowler_sf_stat.txt");
	ifstream file_xB_sf_sys("../../scale_factors/lundBowler/sys_err/lund_bowler_sf_sys.txt");

	double xB_sf, xB_sf_stat, xB_sf_sys;
	double xB_sf_arr[25], xB_sf_arr_stat[25], xB_sf_arr_sys[25];

	int i = 0;
	while (file_xB_sf >> xB_sf) {
		file_xB_sf_stat >> xB_sf_stat;
		file_xB_sf_sys >> xB_sf_sys;

		xB_sf_arr[i] = xB_sf;
		xB_sf_arr_stat[i] = xB_sf_stat;
		xB_sf_arr_sys[i] = xB_sf_sys;

		i++;
	}

	int const nbinsxB = 26;
	const double binsxxB[nbinsxB] = {0.0  ,  0.05,  0.1 ,  0.15,  0.2 ,  0.25,  0.3 ,  0.35,  0.4 ,
        0.45,  0.5,  0.53333333,  0.56666667,  0.6       ,  0.63333333,
        0.66666667,  0.7       ,  0.73333333,  0.76666667,  0.8       ,
        0.83333333,  0.86666667,  0.9       ,  0.93333333,  0.96666667,  1.0};	

	double ptx[nbinsxB-1];

	for(int i = 0; i < nbinsxB-1; i++){
		ptx[i] = binsxxB[i] + (binsxxB[i+1]-binsxxB[i])/2.0;
	}

	double ptx_sys[nbinsxB-1];

	for(int i = 0; i < nbinsxB-1; i++){
		ptx_sys[i] = (binsxxB[i+1]-binsxxB[i])/2.0;
	}

	TGraphErrors* gr_xB_sf = new TGraphErrors(26,ptx,xB_sf_arr,0,xB_sf_arr_stat);

	TGraphErrors* gr_xB_sf_sys = new TGraphErrors(26,ptx,xB_sf_arr,ptx_sys,xB_sf_arr_sys);

  gr_xB_sf->SetMarkerStyle(kFullCircle);
  gr_xB_sf->SetMarkerColor(kRed+1);
  gr_xB_sf->SetLineColor(kRed+1);

  gr_xB_sf_sys->SetLineColorAlpha(kRed+1,0.2);
  gr_xB_sf_sys->SetFillColorAlpha(kRed+1,0.2);

//	//make the legend
//  TLegend* lg = new TLegend(0.4, 0.7, 0.8, 0.8);
//  lg->SetBorderSize(0);
//	lg->SetNColumns(5);

//	lg->AddEntry(gr_xB_sf,"Lund-Bowler scale factors","p");

  gr_xB_sf_sys->GetXaxis()->SetLimits(0,1);
//  gr_xB_sf_sys->GetHistogram()->SetMinimum(0.6);
//  gr_xB_sf_sys->GetHistogram()->SetMaximum(1.2);
  gr_xB_sf_sys->GetHistogram()->SetMinimum(0.8);
  gr_xB_sf_sys->GetHistogram()->SetMaximum(1.4);

  gr_xB_sf_sys->GetXaxis()->SetTitleOffset(1.1);
  gr_xB_sf_sys->GetYaxis()->SetTitleOffset(0.9);

  gr_xB_sf_sys->GetYaxis()->SetTitle("Scaling factor");
  gr_xB_sf_sys->GetYaxis()->SetTitleSize(0.045);
  gr_xB_sf_sys->GetXaxis()->SetTitle("x_{B}=p_{T}(B)/p_{T}(jet)");
  gr_xB_sf_sys->GetXaxis()->SetTitleSize(0.04);
	gr_xB_sf_sys->SetTitle("");

	gStyle->SetLineScalePS(1);

	gr_xB_sf_sys->Draw("5ASAME");
	gr_xB_sf->Draw("PSAME");

	TF1* line0 = new TF1("line0","1",0,1200);
	line0->SetLineColor(kBlack);
	line0->SetLineStyle(kDashed);
	line0->SetLineWidth(1);
	line0->Draw("SAME");

//	lg->Draw();

	c1->Update();

	c1->Print("plots/lundBowler_Sf.pdf");
}
