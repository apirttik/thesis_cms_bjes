#define CMSJES_cxx
#include "CMSJES.h"
#include "assert.h"
#include "RConfigure.h"
#include "Math/MultiRootFinder.h"
#include "Math/WrappedMultiTF1.h"
#include "TError.h"

using namespace ROOT::Math;
using namespace std;

//Created using ROOT TMakeClass
void CMSJES::Loop()
{

  clock_t t_begin = clock();

  int studyMode = -1;
  if (ReadName.find("Zjet")!=string::npos) {
    studyMode = 3;
    cout << "File is for Z+jet sample, with stydy mode: "<< studyMode << endl;
  } else {
    cout << "Error: File not for Z+jet sample! Exiting." << endl;
    return;
  }

  //call Sumw2 to all histos and TProfiles
  TH1::SetDefaultSumw2(true);

  if (fChain == 0) return;
  Long64_t nentries            = fChain->GetEntriesFast();

//  int nentries = 4000000;

  string outname                = "./output_ROOT_files/CMSJES_" + ReadName; //Output file
  outname += "_lundBowlerScaling.root";
  TFile *fout = new TFile(outname.c_str(),"RECREATE");

  Long64_t nbytes = 0, nb = 0;

  double highest_pt_lund_bowler;

  int xB_id;
  double external_pt, neutrino_pt;
  TLorentzVector xB_jet_p4;
  TLorentzVector neutrino_p4;
  TLorentzVector B_decay_p4;

  double  prtn_PDG,PDG;

  int const nbinsxB = 26;
  const double binsxxB[nbinsxB] = {0.  ,  0.05,  0.1 ,  0.15,  0.2 ,  0.25,  0.3 ,  0.35,  0.4 ,
        0.45,  0.5,  0.53333333,  0.56666667,  0.6       ,  0.63333333,
        0.66666667,  0.7       ,  0.73333333,  0.76666667,  0.8       ,
        0.83333333,  0.86666667,  0.9       ,  0.93333333,  0.96666667,  1.0};  

  //Lund-bowler
  TH1D* hxB = new TH1D("hxB", "", nbinsxB-1, binsxxB);

  double w_sum_normal = 0;
  double w_sum_scaled = 0;

  
  //Loop Tree entries = events
  for (Long64_t jentry=0; jentry != nentries; ++jentry) {
    //Print progress for long runs
    if ((jentry % 100000==0)){
      cout << "Looping event " << jentry; cout << " in Z+jet" << endl;
    }

    Long64_t ientry = LoadTree(jentry);  //Load new event
    if (ientry < 0) break;    //When no more events
    nb = fChain->GetEntry(jentry); nbytes += nb;

    highest_pt_lund_bowler = -1;

    xB_id = -1;
    xB_jet_p4.SetPtEtaPhiE(0,0,0,0);
    neutrino_p4.SetPtEtaPhiE(0,0,0,0);
    B_decay_p4.SetPtEtaPhiE(0,0,0,0);


    //Loop through partons of the probe jet, as the hadrons are saved only in the parton level
    for (unsigned int i=0; i!=prtn_pdgid->size(); ++i) {
      prtn_PDG = abs((*prtn_pdgid)[i]);

      prtn_PDG = (*prtn_pdgid)[i];
      if (hasBottom(prtn_PDG) && (*prtn_pt)[i] > highest_pt_lund_bowler) {
        highest_pt_lund_bowler = (*prtn_pt)[i];
        xB_id = i;
      }
    }

    //Fill the xB histogram
    neutrino_pt = 0;
    external_pt = 0;

    xB_jet_p4.SetPtEtaPhiE((*jet_pt)[(*prtn_jet)[xB_id]],  (*jet_eta)[(*prtn_jet)[xB_id]],
                       (*jet_phi)[(*prtn_jet)[xB_id]], (*jet_e)[(*prtn_jet)[xB_id]]);

    //Loop over all event particles an look for neutrinos 
    for (int j=0; j!=prtclnij_pt->size(); ++j) {

      PDG = abs((*prtclnij_pdgid)[j]);
      if (PDG == 12 || PDG == 14 || PDG == 16) {
        neutrino_p4.SetPtEtaPhiE((*prtclnij_pt)[j],(*prtclnij_eta)[j],
                        (*prtclnij_phi)[j],(*prtclnij_e)[j]);

        //Check whether the neutrino is inside the probe jet 
        if (neutrino_p4.DeltaR(xB_jet_p4) < 0.4) {
          neutrino_pt += (*prtclnij_pt)[j];
        }
      } 

      //track down the out of the cone pT of particles decayed from the b-hadron 
      if ((*prtclnij_b_decay_tag)[j] == 1) {
        B_decay_p4.SetPtEtaPhiE((*prtclnij_pt)[j],(*prtclnij_eta)[j],(*prtclnij_phi)[j],(*prtclnij_e)[j]);
        if (B_decay_p4.DeltaR(xB_jet_p4) > 0.4) {
          external_pt += (*prtclnij_pt)[j];
        }            
      }
    }

    hxB->Fill((*prtn_pt)[xB_id]/(external_pt + neutrino_pt + (*jet_pt)[(*prtn_jet)[xB_id]]),weight);      

  } //Loop Tree entries

  fout->Write();
  fout->Close();

  clock_t t_end = clock();
  cout << "Execution time: " << (double)(t_end - t_begin) / CLOCKS_PER_SEC << endl;
}

bool CMSJES::hasBottom(int id) {
  int code1 = (abs(id)/ 100)%10;
  int code2 = (abs(id)/1000)%10;
  if (code1 == 5 or code2 == 5) return true;
  return false;
}

//-----------------------------------------------------------------------------
//Construct input filenames for functions using multiple CMSJES
//objects.
//Properties are chosen based on the object doing this analysis, since the
//CMSJES objects instantiated in this function will have the same properties.
// => The user cannot e.g. accidentally enter a file savename that would
//    imply properties that are not really there.
//Saves the filenames into the zjFile string
//belonging to this CMSJES object.
void CMSJES::InputNameConstructor() {

  //Prevent overwriting existing filenames
  if (zjFile!="") return;

  //Init strings to contain parts of the resulting filenames
  string num = "";  //#events in the sample to study

  //Generator and sample event content indicators
  if      (ReadName.find("P6")!=string::npos) {zjFile="P6_";}
  else if (ReadName.find("P8")!=string::npos) {zjFile="P8_";}
  else if (ReadName.find("H7")!=string::npos) {zjFile="H7_";}
  zjFile += "Zjet_";

  //#events
  if      (ReadName.find("10000000") !=string::npos) num = "10000000";
  else if (ReadName.find("5000000")  !=string::npos) num = "5000000";
  else if (ReadName.find("1000000")  !=string::npos) num = "1000000";
  else if (ReadName.find("600000")   !=string::npos) num = "600000";
  else if (ReadName.find("300000")   !=string::npos) num = "300000";
  else if (ReadName.find("100000")   !=string::npos) num = "100000";
  else if (ReadName.find("30000")    !=string::npos) num = "30000";
  else if (ReadName.find("10000")    !=string::npos) num = "10000";
  else if (ReadName.find("3000")     !=string::npos) num = "3000";
  else {
    cout << "ERROR: fitting not supported for this file" << endl;
    return;
  }
  //The resulting filenames. Suffix ".root" to be added in CMSJES constructor
  zjFile += num;
}
