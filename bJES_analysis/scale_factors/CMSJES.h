////////////////////////////////////////////////////////////////
// This class has been automatically generated on             //
// Thu Feb  1 12:21:30 2018 by ROOT version 6.13/01           //
// from TTree Pythia8Jets/Pythia8 particle data               //
// found on files: P6_dijet_*.root, H7_dijet_*.root, etc.     //
//                                                            //
// The code has been modified further along the way by        //
// toni.makela@cern.ch of Helsinki Institute of Physics, 2018 //
////////////////////////////////////////////////////////////////

#ifndef CMSJES_h
#define CMSJES_h

// C/C++
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <ios>
#include <pthread.h>
#include <time.h>
#include <sstream>
#include <map>

/*#include <assert.h>*/
/*#include "TASImage.h"*/
/*#include "tdrstyle_mod15.C"*/
// Header file for the classes stored in the TTree if any.
#include <vector>

// ROOT
#include "TROOT.h"
#include "TChain.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TH2D.h"
#include "TFile.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TH1.h"
#include "TH2.h"
#include "THStack.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TLorentzVector.h"
#include "TMath.h"
#include "TMatrix.h"
#include "TMatrixD.h"
#include "TProfile.h"
#include "TProfile2D.h"
#include "TRandom3.h"
#include "TStyle.h"
#include "TThread.h"
#include "TVector.h"
#include "TVector3.h"
#include "TVectorD.h"
#include "Riostream.h"
#include "TLine.h"

using std::string;
using std::vector;
using std::cout;
using std::cin;
using std::endl;
using std::fstream;
using std::ifstream;
using std::max;
using std::min;
using std::ofstream;
using std::stringstream;

class CMSJES {
public :
  string ReadName;	//ROOT filename to read
  string OpenName;	// -||- with ".root" -suffix

  /* Uncomment the subdirectory of input_ROOT_files to fetch input from */
  string inSubDir = "";

  TTree* fChain;	//Pointer to the analyzed TTree or TChain
  Int_t  fCurrent;	//Current Tree number in a TChain

  string zjFile="";	//Z+jet -||-

  // Fixed size dimensions of array or collections stored in the TTree if any.
  // Declaration of leaf types
  Float_t         weight;		// Event weight
  //Particle lvl
  vector<unsigned char> *prtcl_jet;	// Jet which particle belongs to 0 - highest pT
  vector<int>     *prtcl_pdgid;		// Particle ID - anti
  vector<float>   *prtcl_pt;		// Particle pT
  vector<float>   *prtcl_eta;		// Pseudorapidity
  vector<float>   *prtcl_phi;		// Phi
  vector<float>   *prtcl_e;		// energy
  //Particles not associated with jets
  vector<int>     *prtclnij_pdgid;
  vector<float>   *prtclnij_pt;
  vector<float>   *prtclnij_eta;
  vector<float>   *prtclnij_phi;
  vector<float>   *prtclnij_e;
  vector<int>	  *prtclnij_b_decay_tag;
  //Parton lvl - used for jet tagging and 
  vector<char>    *prtn_jet;		// Jet originated parton?
  vector<int>     *prtn_pdgid;		// Parton ID
  vector<char>    *prtn_tag;		// 
  vector<float>   *prtn_pt;		// pT of the parton
  vector<float>   *prtn_eta;		// -- 
  vector<float>   *prtn_phi;		// --
  vector<int>   *prtn_semilep_tag;		// --
  vector<int>   *prtn_b_to_c_tag;		// --
  vector<int>   *prtn_bc_decay_tag;		// --
  vector<float>   *prtn_e;		//
  vector<float>   *jet_pt;		// Gen jets
  vector<float>   *jet_eta;		//
  vector<float>   *jet_phi;		//
  vector<float>   *jet_e;		//
  Float_t         met;			// Neutrino E_T sum not used

  // List of branches
  //Particle lvl
  TBranch        *b_weight;
  TBranch        *b_prtcl_jet;
  TBranch        *b_prtcl_pdgid;
  TBranch        *b_prtcl_pt;
  TBranch        *b_prtcl_eta;
  TBranch        *b_prtcl_phi;
  TBranch        *b_prtcl_e;
  //Particles not in jets
  TBranch        *b_prtclnij_pdgid;
  TBranch        *b_prtclnij_pt;
  TBranch        *b_prtclnij_eta;
  TBranch        *b_prtclnij_phi;
  TBranch        *b_prtclnij_e;
  TBranch		 *b_prtclnij_b_decay_tag;
  //Parton lvl
  TBranch        *b_prtn_jet;
  TBranch        *b_prtn_pdgid;
  TBranch        *b_prtn_tag;
  TBranch        *b_prtn_pt;
  TBranch        *b_prtn_eta;
  TBranch        *b_prtn_phi;
  TBranch        *b_prtn_e;
  TBranch        *b_prtn_semilep_tag;
  TBranch        *b_prtn_b_to_c_tag;
  TBranch				 *b_prtn_bc_decay_tag;
  //Jet lvl
  TBranch        *b_jet_pt;
  TBranch        *b_jet_eta;
  TBranch        *b_jet_phi;
  TBranch        *b_jet_e;
  TBranch        *b_met;

  //Constructor and destructor
  CMSJES(TTree *tree=0, string="");
  virtual ~CMSJES();

  //ROOT TTree specific functions
  virtual Int_t    Cut(Long64_t entry);
  virtual Int_t    GetEntry(Long64_t entry);
  virtual Long64_t LoadTree(Long64_t entry);
  virtual void     Init(TTree *tree);

  //Other memeber functions
  virtual void     InputNameConstructor();
  virtual void     Loop();
  virtual Bool_t   Notify();
  virtual void     Show(Long64_t entry = -1);

	static double w_sum_err(double m, double N);
	bool hasBottom(int id);

	std::map<string, vector<double>> BtoSlBr_scale_factors();
	static double f_BSl (double *x, double *p);
	static double f_BTauSl (double *x, double *p);
	static double f_BCSl (double *x, double *p);
	static double f_BCbarSl (double *x, double *p);
	static double f_BSlTotal (double *x, double *p);

	std::map<string, vector<double>> indv_BtoSlBr_scale_factors();

	std::map<string, vector<double>> BtoCBr_scale_factors();
	static double f_B0BpDp (double *x, double *p);
	static double f_B0BpD0 (double *x, double *p);
	static double f_B0BpDps (double *x, double *p);
	static double f_B0BpTotal (double *x, double *p);

	std::map<string, vector<double>> BprodFrac_scale_factors();

	TH1* lundBowler_scale_factors(TH1* xb_norm);

  void   ParamReader(string file, int n1, int n2, vector<vector<double>> &params);

};

#endif

#ifdef CMSJES_cxx

//A function to read hadron response function parameters from files
//Params:	file		The filename to read as a string
//		n1,n2		Dimensions of the params tensor
//		params		Reference to the tensor to read parameters into
void CMSJES::ParamReader(string file, int n1, int n2, vector<vector<double>> &params)
{
  //Init temps to read into
  double p=0;
  vector<double> v;
  vector<vector<double>> M;		//Temp matrix
  //Read the parameters from files
  ifstream in;
  string paramFile;
  //paramFile = "./spr_mc/RunIIa" + file; 
  paramFile = "./spr_mc/CMS" + file; 

  in.open(paramFile);
  if (!in.is_open()) {
    cout << "Error opening " << paramFile << endl;
    return;
  }
  for (int lines=0; lines!=n1; ++lines) {
    in >> p;		//1st string on line is eta region low. bd., omit it
    for (int i=0; i!=n2; ++i) {
      in >> p;  v.push_back(p);
    }
    M.push_back(v);
    v.clear();
  }

  params = M;
  M.clear();
  in.close();
} //ParamReader


//Constructor
CMSJES::CMSJES(TTree *tree, string toRead) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
  if (tree == 0) {	//No pointer to a tree given
    if (toRead != "") ReadName = toRead;
    else {
      //Add here all the files one should be able to study
      vector<string> files;	//To contain studiable filenames
      files.push_back("P8_Zjet_3000"  );
      files.push_back("H7_Zjet_3000"  );
      files.push_back("P8_Zjet_10000" );
      files.push_back("P8_Zjet_300000" );
      files.push_back("H7_Zjet_30000" );
      files.push_back("P8_Zjet_100000");
      files.push_back("P8_Zjet_600000");
      files.push_back("P8_Zjet_1000000");
      files.push_back("P8_Zjet_2000000");
      files.push_back("H7_Zjet_1000000");
      files.push_back("P8_Zjet_4000000");
      files.push_back("P8_Zjet_6000000");
      files.push_back("P8_Zjet_9000000");
      files.push_back("P8_Zjet_10000000");
      //User interface
      printf("No filename given, choose file (y = default file):\n");
      for (int i=0; i!=files.size(); ++i) {
        if (i%4==0) printf("\n");
        printf("%2d:%56s\n", i, files[i].c_str());
      }
      printf("Want something else? Add it to the constructor in header\n");
      //Read input
      int input;	//For cin
      cin >> input;
      if (input < 0 || input >= files.size()) {	//Check for erroneous input
			cout<< "Invalid input! Chose 0" <<endl;
        input = 0;
      }
      ReadName = files[input];			//Pick the filename to study
    }

		//where to read the input data 
    OpenName = "../../event_generation/" + inSubDir + ReadName + ".root";

    printf("Opening file: %s\n", OpenName.c_str());
    TFile *f = (TFile*)gROOT->GetListOfFiles()
			    ->FindObject(OpenName.c_str());
    if (!f || !f->IsOpen()) f = new TFile(OpenName.c_str());
    if (ReadName.find("P6")!=string::npos) f->GetObject("Pythia6Jets",tree);
    else if (ReadName.find("P8")!=string::npos) f->GetObject("Pythia8Jets",tree);
    else f->GetObject("HerwigTree",tree);

	} else {	
		//read the file name from the main.cpp
		ReadName = toRead;
	}	
  Init(tree);	//Setup branch adresses etc.


} //Constructor

//-----------------------------------------------------------------------------
//Destructor
CMSJES::~CMSJES()
{
  if (!fChain) return;
  delete fChain->GetCurrentFile();
}
//-----------------------------------------------------------------------------
Int_t CMSJES::GetEntry(Long64_t entry)
{
// Read contents of entry.
  if (!fChain) return 0;
  return fChain->GetEntry(entry);
}
//-----------------------------------------------------------------------------
Long64_t CMSJES::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
  if (!fChain) return -5;
  Long64_t centry = fChain->LoadTree(entry);
  if (centry < 0) return centry;
  if (fChain->GetTreeNumber() != fCurrent) {
     fCurrent = fChain->GetTreeNumber();
     Notify();
  }
  return centry;
}
//-----------------------------------------------------------------------------
void CMSJES::Init(TTree *tree)
{
  // The Init() function is called when the selector needs to initialize
  // a new tree or chain. Typically here the branch addresses and branch
  // pointers of the tree will be set.
  // It is normally not necessary to make changes to the generated
  // code, but the routine can be extended by the user if needed.
  // Init() will be called many times when running on PROOF
  // (once per file to be processed).

  // Set object pointers initially to NULL
  //Particle lvl
  prtcl_jet = 0;
  prtcl_pdgid = 0;
  prtcl_pt = 0;
  prtcl_eta = 0;
  prtcl_phi = 0;
  prtcl_e = 0;
  //Particles not in jets
  prtclnij_pdgid = 0;
  prtclnij_pt = 0;
  prtclnij_eta = 0;
  prtclnij_phi = 0;
  prtclnij_e = 0;
  prtclnij_b_decay_tag = 0;
  //Parton lvl
  prtn_jet = 0;
  prtn_pdgid = 0;
  prtn_tag = 0;
  prtn_pt = 0;
  prtn_eta = 0;
  prtn_phi = 0;
  prtn_e = 0;
  prtn_semilep_tag = 0;
  prtn_b_to_c_tag = 0;
  prtn_bc_decay_tag = 0;
  //Jet lvl
  jet_pt = 0;
  jet_eta = 0;
  jet_phi = 0;
  jet_e = 0;
  // Set branch addresses and branch pointers
  if (!tree) return;
  fChain = tree;
  fCurrent = -1;
  fChain->SetMakeClass(1);

  fChain->SetBranchAddress("weight", &weight, &b_weight);	//Event weight
  //Particle lvl  
  fChain->SetBranchAddress("prtcl_jet", &prtcl_jet, &b_prtcl_jet);
  fChain->SetBranchAddress("prtcl_pdgid", &prtcl_pdgid, &b_prtcl_pdgid);
  fChain->SetBranchAddress("prtcl_pt", &prtcl_pt, &b_prtcl_pt);
  fChain->SetBranchAddress("prtcl_eta", &prtcl_eta, &b_prtcl_eta);
  fChain->SetBranchAddress("prtcl_phi", &prtcl_phi, &b_prtcl_phi);
  fChain->SetBranchAddress("prtcl_e", &prtcl_e, &b_prtcl_e);
  //Particles not in jets lvl
  fChain->SetBranchAddress("prtclnij_pdgid",&prtclnij_pdgid, &b_prtclnij_pdgid);
  fChain->SetBranchAddress("prtclnij_pt", &prtclnij_pt, &b_prtclnij_pt);
  fChain->SetBranchAddress("prtclnij_eta", &prtclnij_eta, &b_prtclnij_eta);
  fChain->SetBranchAddress("prtclnij_phi", &prtclnij_phi, &b_prtclnij_phi);
  fChain->SetBranchAddress("prtclnij_e", &prtclnij_e, &b_prtclnij_e);
  fChain->SetBranchAddress("prtclnij_b_decay_tag", &prtclnij_b_decay_tag, &b_prtclnij_b_decay_tag);
  //Parton lvl
  fChain->SetBranchAddress("prtn_jet", &prtn_jet, &b_prtn_jet);
  fChain->SetBranchAddress("prtn_pdgid", &prtn_pdgid, &b_prtn_pdgid);
  fChain->SetBranchAddress("prtn_tag", &prtn_tag, &b_prtn_tag);
  fChain->SetBranchAddress("prtn_pt", &prtn_pt, &b_prtn_pt);
  fChain->SetBranchAddress("prtn_eta", &prtn_eta, &b_prtn_eta);
  fChain->SetBranchAddress("prtn_phi", &prtn_phi, &b_prtn_phi);
  fChain->SetBranchAddress("prtn_e", &prtn_e, &b_prtn_e);
  fChain->SetBranchAddress("prtn_semilep_tag", &prtn_semilep_tag, &b_prtn_semilep_tag);
  fChain->SetBranchAddress("prtn_b_to_c_tag", &prtn_b_to_c_tag, &b_prtn_b_to_c_tag);
  fChain->SetBranchAddress("prtn_bc_decay_tag", &prtn_bc_decay_tag, &b_prtn_bc_decay_tag);
  //Jet lvl
  fChain->SetBranchAddress("jet_pt", &jet_pt, &b_jet_pt);
  fChain->SetBranchAddress("jet_eta", &jet_eta, &b_jet_eta);
  fChain->SetBranchAddress("jet_phi", &jet_phi, &b_jet_phi);
  fChain->SetBranchAddress("jet_e", &jet_e, &b_jet_e);
  fChain->SetBranchAddress("met", &met, &b_met);
  Notify();
}
//-----------------------------------------------------------------------------
Bool_t CMSJES::Notify()
{
  // The Notify() function is called when a new file is opened. This
  // can be either for a new TTree in a TChain or when when a new TTree
  // is started when using PROOF. It is normally not necessary to make changes
  // to the generated code, but the routine can be extended by the
  // user if needed. The return value is currently not used.

  return kTRUE;
}
//-----------------------------------------------------------------------------
void CMSJES::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
  if (!fChain) return;
  fChain->Show(entry);
}
//-----------------------------------------------------------------------------
Int_t CMSJES::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
  return 1;
}
#endif // #ifdef CMSJES_cxx
