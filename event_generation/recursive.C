
int factorial(int n) {
	if (n > 1) {
		return n * factorial(n-1);
	} else {
		return 1;
	}
}

void recursive() {
	
	int n, result;

	cout << factorial(5) << endl;

}
